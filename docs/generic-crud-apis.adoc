= Generic CRUD APIs

Some of the API resources comply to a generic CRUD pattern exposing:

[horizontal]
POST /:: to create an instance
GET /:id:: to retrieve instance details by id
GET /:: to retrieve all instances
PUT /:id:: to update an instance by id (accepts partial updates like a PATCH)
DELETE /:id:: to delete an instance by id

//== Model description
// For each operation

== How-to develop

. Create model class.
For example, `class Speaker`.

.. Ensure it implements `Traceable` interface.
Easiest method to comply is:

.. Add audit attributes
+
[,java]
.Audit attributes
----
@JsonSerialize(using = ObjectIdJsonSerializer.class)
private final String _id;

@JsonDeserialize(using = LocalDateTimeDeserializer.class)
@JsonSerialize(using = LocalDateTimeJsonSerializer.class)
private final LocalDateTime createdDate;
@JsonDeserialize(using = LocalDateTimeDeserializer.class)
@JsonSerialize(using = LocalDateTimeJsonSerializer.class)
private final LocalDateTime lastUpdate;
private String lastActionBy;
----
+
.. Add Lombok's `@Getter` annotation to class
+
.. Add required fields.
Mark them as `final` and annotate them with `@RequiredForCreation` for automatic validation during creation and update to work.
+
[,java]
----
@RequiredForCreation
private final String name;
----
+
TIP: Optionally, add `@AllArgsConstructor` for convinience constructor for tests.

.. Add constructor for required fields with required Jackson annotations.
+
[,java]
----
@JsonCreator
public Room(@JsonProperty("name") String name) {
    this.name = name;
    this._id = null;
    this.createdDate = this.lastUpdate = null; <1>
}
----
<1> Audit fields are set at the persistence layer, just set them to null or do not define as `final`.

. Setup HTTP controller
.. Instantiate new `GenericCrudController` in `HttpServerVerticle`
.. Bind it to Vert.x
+
[,java]
----
var controller = new GenericCrudController(vertx, Speaker.class);
...
bindGenericController(router, controller, "/api/speakers", ADMIN);
----

. Setup persistence
.. Instantiate new `CrudMongoHandler`
+
[,java]
----
new CrudMongoHandler(Speaker.class, mongoDatabase, TRACKS),
----

..Ensure it's deployed. For now, add to `standardHandlersFutures` variable.
