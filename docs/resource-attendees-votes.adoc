==== Attendees votes
ifndef::api-context[:api-context: api]
ifndef::api-public-context[:api-public-context: public]
ifndef::icons[:icons: font]
:resource: attendees/votes
:example-endpoint: https://cfpapi.jbcnconf.com/api/{resource}

Attendees votes are votes emited by attendees on a specific talk.
It contains the following fields:

* `talkId`: String
* `userEmail`: String
* `value`: Integer
* `delivery`: String
* `other`: String
* and `_id`, `createdDate`, `lastUpdate`

===== Create votes

NOTE: TODO

===== Get by id

NOTE: TODO

===== Filter votes

List all votes that match specific filter conditions.


[cols=2,width=35%]
|===
| url pattern
| /{api-context}/{resource}

| method
| POST

| authenticated
| yes
|===

[source, json]
.successful response: 200 OK
----
{
  "status": true,
  "data": {
    "total": 10, // <1>
    "items": [
      {
        "_id": "5c366147aefd3941ba97f5ef",
        "talkId": "5cb204b1aefd395a3989a681",
        "userEmail": "superanna@bcnjug.com",
        "createdDate": 1547067719098,
        "updatedDate": 1547067719098
      },
      {
        "_id": "5cb204faaefd395bb0404d48",
        "talkId": "5cb204b1aefd395a3989a681",
        "userEmail": "superanna@bcnjug.com",
        "createdDate": 1547067719098,
        "updatedDate": 1547067719098
      },
      {
        "_id": "5cb204faaefd395bb0404d53",
        "talkId": "5cb204b1aefd395a3989a681",
        "userEmail": "superanna@bcnjug.com",
        "createdDate": 1547067719098,
        "updatedDate": 1547067719098
      }
    ]
  }
}
----
<1> Number of element that match the filters.
Can be greater than the returned items if the page size is lower than it.

.errors
|===
|Error code |Description

|===

====== Examples

Note that all fields are optional.

[source,bash,subs=attributes+]
.cURL
----
curl {example-endpoint}/search \
-X POST \
-H "Authorization: Bearer ${token}" \
-H "Content-Type: application/json" \
-d '{
  "filters": [],
  "page": { <.>
    "index": 0
    "size": 20, <.>
  },
  "fields": ["_id","value"], <.>
  "sort": "createdDate", <.>
  "asc": false
}'
----
<.> `index` starts at 0
<.> default `size`: 50
<.> default `fields`: all fields.
Note that `_id` is always returned.
<.> default `sort`: `createdDate`
