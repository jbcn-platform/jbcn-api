==== Notes
ifndef::api-context[:api-context: api]
ifndef::icons[:icons: font]
:resource: notes
:unauthorized-text: Note that only the note's author can update the note. Attempting to update a note of another user will result in a 403 (Forbidden) response.

Notes are pieces of free information that committee members (`voter` role) can add to a paper.
Each note contains:

* The author who wrote the note (named `owner`).
* Text itself of the note (currently untreated).
* The date and time in which the note was written (`createdDate`).
* The date and time in which the note was modified the last time (`lastUpdate`).
* A reference to the paper to which it belongs.

In summary, the `/{resource}` endpoint exposes the features to manage notes.
Those are; creation, modification, retrieval (by id), listing and deletion.

[IMPORTANT]
====
* All operations require VOTER or ADMIN rights.
====

// TODO should consider using 204 & 202 codes instead of JSON with ({status:true})
// TODO note's owner, paperId & text cannot be empty (should return 400 Bad Request)
// TODO validate that Paper exists before inserting

===== Create new note

Creates a note and returns its id.

[cols=2,width=35%]
|===
| url pattern
| /{api-context}/{resource}

| method
| POST

| authenticated
| yes
|===

[source, json]
.payload
----
{
  "text": "This is the text"
  "paperId": "5a9135f161e9a41cb444d878"
}
----

[source, json]
.successful response: 201 CREATED
----
{
  "status": true,
  "data": {
    "id": "5a778b55ea67c30c58738b2f"
  }
}
----

===== Get note (by id)

Retrieves a note by id.

[cols=2,width=35%]
|===
| url pattern
| /{api-context}/{resource}/{note-id}

| method
| GET

| authenticated
| yes
|===

[source, json]
.successful response: 200 OK
----
{
  "status": true,
  "data": {
    "id": "5a8f2c42f48e995165045f2d",
    "owner": "committee_user",
    "paperId": "5a778d29ea67c30c58738b31",
    "text": "This is a note"
    "createdDate": 1519329524259,
    "lastUpdate": 1519329962605,
  }
}
----

===== Update note

Updates an existing note text.
{unauthorized-text}

[cols=2,width=35%]
|===
| url pattern
| /{api-context}/{resource}/{note-id}

| method
| PUT

| authenticated
| yes
|===

[source, json]
.payload
----
{
  "text": "This is the new note's text"
}
----

[source, json]
.successful response: 200 OK
----
{
  "status": true
}
----

===== Delete note

Permanently deletes a note.
{unauthorized-text}

[cols=2,width=35%]
|===
| url pattern
| /{api-context}/{resource}/{note-id}

| method
| DELETE

| authenticated
| yes
|===

[source, json]
.successful response: 200 OK
----
{
  "status": true
}
----

===== List notes

// TODO later on we can add a POST /search for advanced features following same patterns as other resources

Retrieves a collection of notes that match a set of conditions sent as url parameters.
Any of the note's properties can be used as condition: id, paperId, owner, text, createdDate and lastUpdate. +
Note that the results are sorted by descending `createdDate` (newer first) to some filters.

IMPORTANT: At least one filter is required.

[cols=2,width=35%]
|===
| url pattern
| /{api-context}/{resource}?param1=value1&param2=value2&...

examples:

`- /{api-context}/{resource}?paperId=5a778d29ea67c30c58738b31`
`- /{api-context}/{resource}?owner=some_user`

| method
| GET

| authenticated
| yes
|===

[source, json]
.successful response: 200 OK
----
{
  "status": true,
  "data" : {
    "size": 2
    "items" : [
      {
        "id": "5a8f2c25f48e995165045f24",
        "owner": "committee_user",
        "paperId": "5a778d29ea67c30c58738b31",
        "text": "This is a note",
        "createdDate": 1519329524259,
        "lastUpdate": 1519329962630
      },
      {
        "id": "5a8f2c42f48e995165045f2d",
        "owner": "another_committee_user",
        "paperId": "5a778d29ea67c30c58738b31",
        "text": "This is a note",
        "createdDate": 1519329524259,
        "lastUpdate": 1519329962605
      }
    ]
  }
}
----
