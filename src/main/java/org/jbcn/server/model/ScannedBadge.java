package org.jbcn.server.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.model.sponsor.Sponsor;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScannedBadge implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private String _id;

    private Attendee attendee;
    private Sponsor sponsor;
    // additional data filled by person scanning badge
    private String details;

    // TODO create common abstract class with this
    private String lastActionBy;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime lastUpdate;


    public static ScannedBadge of(final Attendee attendee, final Sponsor sponsor) {
        return new ScannedBadge(null, attendee, sponsor, null, null, null, null);
    }

    public static ScannedBadge of(final Attendee attendee, final Sponsor sponsor, final String details) {
        return new ScannedBadge(null, attendee, sponsor, details, null, null, null);
    }

}
