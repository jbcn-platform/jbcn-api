package org.jbcn.server.model.personal;

public enum Gender {
    M, F
}
