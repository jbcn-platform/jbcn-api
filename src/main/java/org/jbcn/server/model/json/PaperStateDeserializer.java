package org.jbcn.server.model.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.jbcn.server.model.PaperState;

import java.io.IOException;


public class PaperStateDeserializer extends JsonDeserializer<PaperState> {

    @Override
    public PaperState deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        final JsonNode node = p.getCodec().readTree(p);
        return PaperState.toPaperState(node.asText());
    }

}
