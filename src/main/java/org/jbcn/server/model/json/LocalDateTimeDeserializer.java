package org.jbcn.server.model.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.LongNode;
import org.jbcn.server.utils.TimeUtils;

import java.io.IOException;
import java.time.LocalDateTime;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {

        final JsonNode node = p.getCodec().readTree(p);

        if (node.get("monthValue") != null) {
            int month = node.get("monthValue").asInt();
            int year = node.get("year").asInt();
            int dayOfMonth = node.get("dayOfMonth").asInt();
            int hour = node.get("hour").asInt();
            int minute = node.get("minute").asInt();
            int second = node.get("second").asInt();
            int nanoSecond = node.get("nano").asInt();
            return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second, nanoSecond);
        } else if (node.get("$numberLong") != null) {
            long numberLong = node.get("$numberLong").asLong();
            return TimeUtils.toLocalDateTime(numberLong);
        } else if (node instanceof LongNode) {
            return TimeUtils.toLocalDateTime(node.longValue());
        }
        throw new RuntimeException("Invalid date");
    }
}
