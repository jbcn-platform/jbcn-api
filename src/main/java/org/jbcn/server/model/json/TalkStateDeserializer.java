package org.jbcn.server.model.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.jbcn.server.model.TalkState;

import java.io.IOException;

public class TalkStateDeserializer extends JsonDeserializer<TalkState> {

    @Override
    public TalkState deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        final JsonNode node = p.getCodec().readTree(p);
        return TalkState.toTalkState(node.asText());
    }

}
