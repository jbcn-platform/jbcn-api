package org.jbcn.server.model.votes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeetingVote {

    private String _id;
    private String deviceId;
    private int vote;
    private String comment;
    private Date date;

}
