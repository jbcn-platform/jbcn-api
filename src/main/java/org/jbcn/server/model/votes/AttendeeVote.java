package org.jbcn.server.model.votes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

import static org.jbcn.server.model.Traceable.CREATED_DATE;

/**
 * Model for votes coming from the Gluon mobile app.
 * <p>
 * Do not confuse with operations coming from AttendeeVote:
 * - this.talkController::upsertMobileVote
 * - this.talkController::getMobileVote
 * - this.talkController::getMobileVotes
 */
@Getter
@AllArgsConstructor
public class AttendeeVote implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;
    // mandatory
    private String talkId;
    // mandatory
    private String userEmail;
    // mandatory: 1 to 5
    private Integer value;
    // emoji identifier for additional feedback
    private String delivery;
    // feedback text
    private String other;

    private final VoteSource source;


    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;

    private String lastActionBy;

    @JsonCreator
    private AttendeeVote(
            @JsonProperty("talkId") final String talkId,
            @JsonProperty("userEmail") final String userEmail,
            @JsonProperty("value") final Integer value,
            @JsonProperty("source") final VoteSource source,
            @JsonProperty(CREATED_DATE) final LocalDateTime createdDate) {
        this._id = null;
        this.talkId = talkId;
        this.userEmail = userEmail;
        this.value = value;
        this.source = source;
        this.createdDate = this.lastUpdate = createdDate;
    }

    public static AttendeeVote of(final String talkId, final String userEmail, final Integer value, final VoteSource source, final LocalDateTime createdDate) {
        return new AttendeeVote(talkId, userEmail, value, source, createdDate);
    }

}
