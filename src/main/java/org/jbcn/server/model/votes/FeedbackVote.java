package org.jbcn.server.model.votes;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackVote {

    Date date;
    String schedule_id;
    int score;
    int talk_id;

}
