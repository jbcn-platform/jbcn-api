package org.jbcn.server.model.votes;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

/**
 * Model for favourited talks coming from the Gluon mobile app.
 * <p>
 * NOTE: the Gluon class is called sessionId, but it is talkdId.
 */
@Getter
@AllArgsConstructor
public class FavouritedTalk implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;

    // mandatory
    private String talkId;
    // mandatory
    private String user;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;

    private String lastActionBy;

    @JsonCreator
    private FavouritedTalk(
            @JsonProperty("talkId") final String talkId,
            @JsonProperty("user") final String user) {
        this._id = null;
        this.talkId = talkId;
        this.user = user;
        this.createdDate = this.lastUpdate = null;
    }

    public static FavouritedTalk of(final String talkId, final String user) {
        return new FavouritedTalk(talkId, user);
    }

}
