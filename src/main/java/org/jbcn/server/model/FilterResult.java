package org.jbcn.server.model;

import lombok.Data;

import java.util.List;

@Data
public class FilterResult<T> {

    private final List<T> items;
    private final int size;

    private FilterResult(List<T> items) {
        this.items = items;
        this.size = items == null ? 0 : items.size();
    }

    public static FilterResult of(List<?> items) {
        return new FilterResult(items);
    }

}
