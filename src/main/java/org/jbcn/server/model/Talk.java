package org.jbcn.server.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.TalkStateSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.model.json.TalkStateDeserializer;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Talk implements Traceable {

    public static final String STATE = "state";
    public static final String PUBLISHED = "published";

    private String _id;
    private String paperId;
    private String title;
    private String edition;
    private String type;
    private String level;
    private String paperAbstract;
    private List<String> languages;
    private Boolean sponsor;
    private double totalVote;
    private List<String> tags;
    private String responsiblePerson;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime dateAndTime;

    private String preferenceDay;
    private String comments;
    private List<Speaker> speakers;

    @JsonDeserialize(using = TalkStateDeserializer.class)
    @JsonSerialize(using = TalkStateSerializer.class)
    private TalkState state;
    private Boolean published;

    private String sessionId;

    private String lastActionBy;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime lastUpdate;

    public static Talk toTalk(Paper paper) {
        final LocalDateTime now = LocalDateTime.now();
        return Talk.builder()
                .paperId(paper.get_id())
                .title(paper.getTitle())
                .edition(paper.getEdition())
                .type(paper.getType())
                .level(paper.getLevel())
                .paperAbstract(paper.getPaperAbstract())
                .languages(paper.getLanguages())
                .totalVote(paper.getAverageVote())
                .tags(paper.getTags())
                .preferenceDay(paper.getPreferenceDay())
                .comments(paper.getComments())
                .speakers(convertSenders(paper.getSenders()))
                .createdDate(now)
                .lastUpdate(now)
                .state(TalkState.TALK_STATE_UNCONFIRMED)
                .sponsor(paper.getSponsor())
                .published(Boolean.FALSE)
                .build();
    }

    private static List<Speaker> convertSenders(List<Sender> senders) {
        return senders.stream()
                .map(sender -> Speaker.fromSender(sender))
                .collect(Collectors.toList());
    }

}
