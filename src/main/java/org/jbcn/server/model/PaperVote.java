package org.jbcn.server.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import org.jbcn.server.model.jackson.MongoDateConverter;

import java.util.Date;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PaperVote {

    private String userId;
    private String username;
    private Double vote;
    @JsonDeserialize(using = MongoDateConverter.class)
    private Date date;

    public static PaperVote of(String username, Double vote) {
        return new PaperVote(null, username, vote, null);
    }

}
