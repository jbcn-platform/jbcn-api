package org.jbcn.server.model.scheduling;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.platform.RequiredForCreation;

import java.time.LocalDateTime;

@Getter
public class Room implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;
    @RequiredForCreation
    private final String name;
    @Setter
    private Integer capacity;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime lastUpdate;

    private String lastActionBy;

    @JsonCreator
    public Room(@JsonProperty("name") String name) {
        this.name = name;
        this._id = null;
        this.createdDate = this.lastUpdate = null;
    }
}
