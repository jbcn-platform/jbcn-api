package org.jbcn.server.model.scheduling;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.platform.RequiredForCreation;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class Session implements Traceable {

    //    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private String _id;

    // from gluon api?
    @RequiredForCreation
    private String slotId;
    @RequiredForCreation
    private String day;
    private String startTime;
    private String endTime;
    private String trackId;
    private String roomId;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime lastUpdate;

    private String lastActionBy;

}
