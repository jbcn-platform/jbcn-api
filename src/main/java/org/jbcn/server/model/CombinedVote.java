package org.jbcn.server.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CombinedVote {
    String talkId;
    String title;
    String speaker;
    float sum;
    float average;
    float count;

    public CombinedVote incCount() {
        this.count++;
        return this;
    }

    public CombinedVote add(int n) {
        this.sum = this.sum + n;
        return this;
    }
}
