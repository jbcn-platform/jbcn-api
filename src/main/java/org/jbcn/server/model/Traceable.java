package org.jbcn.server.model;


import java.time.LocalDateTime;


/**
 * Interface to enforce classes that must offer tracing information.
 */
public interface Traceable {

    String CREATED_DATE = "createdDate";
    String LAST_UPDATE = "lastUpdate";
    // TODO replace by createdBy, updatedBy
    String LAST_ACTION_BY = "lastActionBy";

    LocalDateTime getCreatedDate();

    LocalDateTime getLastUpdate();

    String getLastActionBy();

}
