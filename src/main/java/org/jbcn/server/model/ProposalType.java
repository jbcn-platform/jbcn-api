package org.jbcn.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

// TODO replace String attributes in Paper a Talk by this enum
@Getter
@AllArgsConstructor
public enum ProposalType {

    TALK("talk"), // default
    WORKSHOP("workshop");

    private final String type;

    // TODO create common EnumUtils to remove this duplicated code in all enums
    public static ProposalType toTalkType(String level) {
        return Arrays.stream(values())
                .filter(it -> it.getType().equals(level))
                .findFirst()
                .orElse(TALK);
    }

    // TODO create common EnumUtils to remove this duplicated code in all enums
    public static boolean isValid(String level) {
        return Arrays.stream(values())
                .filter(it -> it.getType().equals(level))
                .findFirst()
                .isPresent();
    }

}
