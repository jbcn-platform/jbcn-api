package org.jbcn.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TalkState {

    TALK_STATE_UNCONFIRMED("unconfirmed"), // default
    TALK_STATE_CONFIRMED("confirmed"),
    TALK_STATE_SCHEDULED("scheduled"),
    TALK_STATE_CANCELED("canceled");

    private final String state;

    public static TalkState toTalkState(String state) {
        return Arrays.stream(values())
                .filter(it -> it.getState().equals(state))
                .findFirst()
                .orElse(TALK_STATE_UNCONFIRMED);
    }

    public static boolean isValid(String state) {
        return Arrays.stream(values())
                .filter(it -> it.getState().equals(state))
                .findFirst()
                .isPresent();
    }

}
