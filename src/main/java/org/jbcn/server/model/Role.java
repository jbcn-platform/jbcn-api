package org.jbcn.server.model;

import io.vertx.ext.auth.mongo.MongoAuth;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Role {

    ADMIN("admin"),
    VOTER("voter"),
    HELPER("helper"),
    SPEAKER("speaker"),
    SPONSOR("sponsor");

    private final String role;

    public static Role of(String role) {
        return Arrays.stream(values())
                .filter(it -> it.getRole().equals(role))
                .findFirst()
                .orElse(null);
    }

    public String authority() {
        return MongoAuth.ROLE_PREFIX + role;
    }
}
