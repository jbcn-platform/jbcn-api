package org.jbcn.server.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class Note implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;

    private final String owner;
    private final String paperId;
    private final String text;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;
    private String lastActionBy;

    @JsonCreator
    public Note(@JsonProperty("owner") String owner,
                @JsonProperty("paperId") String paperId,
                @JsonProperty("text") String text) {
        this.owner = owner;
        this.paperId = paperId;
        this.text = text;
        this._id = null;
        this.createdDate = this.lastUpdate = null;
    }
}
