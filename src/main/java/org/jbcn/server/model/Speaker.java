package org.jbcn.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.jbcn.server.model.compliance.DataConsent;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.HashUtils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Speaker {

    private ObjectId id;
    private String fullName;
    private String jobTitle; // description in speakers.json
    // code is used as discriminator to identify repeated speakers in several operations
    private String code;
    // generated field using generateRef
    private String ref;
    private String email;
    private String company;
    private String biography;
    private String web;
    private String twitter;
    private String linkedin;

    private boolean travelCost;
    private boolean attendeesParty;
    private boolean speakersParty;
    private String tshirtSize;
    private String picture;
    // picture used for site an mobile app
    private String publicPicture;
    private String allergies;
    private boolean starred;

    private Date createdDate;
    private Date lastUpdate;
    private DataConsent dataConsent;

    public static Speaker fromSender(Sender sender) {
        final Speaker speaker = DocumentConverter.parseToObject(DocumentConverter.toJson(sender), Speaker.class);
        speaker.setCode(calculateCode(sender.getFullName(), sender.getEmail()));
        return speaker;
    }

    public static String calculateCode(String fullName, String email) {
        final List<String> tokens = fullName == null ? new ArrayList<>() : new ArrayList<>(Arrays.asList(fullName.split(" ")));
        if (email != null)
            tokens.add(email);

        return tokens.stream()
                .map(token -> Normalizer.normalize(token, Normalizer.Form.NFC))
                .collect(Collectors.joining());
    }

    public static String generateRef(String fullName, String email) {
        return HashUtils.generateReference(calculateCode(fullName, email));
    }

}
