package org.jbcn.server.model.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.jbcn.server.model.TalkState;

import java.io.IOException;

public class TalkStateSerializer extends JsonSerializer<TalkState> {

    @Override
    public void serialize(TalkState value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(value.getState());
    }
}
