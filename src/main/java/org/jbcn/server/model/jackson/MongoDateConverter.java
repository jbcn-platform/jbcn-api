package org.jbcn.server.model.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.LongNode;

import java.io.IOException;
import java.util.Date;

public class MongoDateConverter extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.readValueAsTree();
        if (node.getClass().isAssignableFrom(LongNode.class)) {
            return new Date(node.longValue());
        } else {
            return new Date(node.get("$numberLong").asLong());
        }
    }
}
