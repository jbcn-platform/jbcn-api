package org.jbcn.server.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class Edition {

    private String code;
    private List<String> voters;

}
