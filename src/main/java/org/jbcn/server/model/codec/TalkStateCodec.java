package org.jbcn.server.model.codec;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.jbcn.server.model.TalkState;

public class TalkStateCodec implements Codec<TalkState> {
    @Override
    public TalkState decode(BsonReader reader, DecoderContext decoderContext) {
        return TalkState.toTalkState(reader.readString());
    }

    @Override
    public void encode(BsonWriter writer, TalkState value, EncoderContext encoderContext) {
        writer.writeString(value.getState());
    }

    @Override
    public Class<TalkState> getEncoderClass() {
        return TalkState.class;
    }
}
