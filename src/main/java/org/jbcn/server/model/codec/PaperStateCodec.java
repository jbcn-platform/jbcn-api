package org.jbcn.server.model.codec;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.jbcn.server.model.PaperState;

public class PaperStateCodec implements Codec<PaperState> {

    @Override
    public PaperState decode(final BsonReader reader, final DecoderContext decoderContext) {
        return PaperState.toPaperState(reader.readString());
    }

    @Override
    public void encode(BsonWriter writer, PaperState value, EncoderContext encoderContext) {
        writer.writeString(value.getState());
    }

    @Override
    public Class<PaperState> getEncoderClass() {
        return PaperState.class;
    }
}
