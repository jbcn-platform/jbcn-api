package org.jbcn.server.model.sponsor;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.platform.RequiredForCreation;

import java.time.LocalDateTime;
import java.util.Map;

@Getter
@AllArgsConstructor
public class Sponsor implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;

    @RequiredForCreation
    private final String name;
    @RequiredForCreation
    private final String description;
    @RequiredForCreation
    private final String level;
    /**
     * Example of links: logo, web, twitter, linkedin, instagram
     */
    @Setter
    private Map<String, String> links;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;
    private String lastActionBy;

    @JsonCreator
    public Sponsor(@JsonProperty("name") String name,
                   @JsonProperty("description") String description,
                   @JsonProperty("level") String level) {
        this.name = name;
        this.description = description;
        this.level = level;
        this._id = null;
        this.createdDate = this.lastUpdate = null;
    }
}
