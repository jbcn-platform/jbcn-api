package org.jbcn.server.model.sponsor;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.platform.RequiredForCreation;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class JobOffer implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;

    @RequiredForCreation
    private final String title;
    @RequiredForCreation
    private final String description;
    @RequiredForCreation
    private final String link;
    @RequiredForCreation
    private final String sponsorId;

    private final String location;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;
    private String lastActionBy;

    @JsonCreator
    public JobOffer(@JsonProperty("title") String title,
                    @JsonProperty("description") String description,
                    @JsonProperty("link") String link,
                    @JsonProperty("sponsorId") String sponsorId,
                    @JsonProperty("location") String location) {
        this.title = title;
        this.description = description;
        this.link = link;
        this.sponsorId = sponsorId;
        this.location = location;
        this._id = null;
        this.createdDate = this.lastUpdate = null;
    }
}
