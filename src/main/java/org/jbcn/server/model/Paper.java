package org.jbcn.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jbcn.server.model.jackson.MongoDateConverter;
import org.jbcn.server.model.json.PaperStateDeserializer;
import org.jbcn.server.utils.DocumentConverter;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.model.Traceable.LAST_ACTION_BY;
import static org.jbcn.server.utils.CollectionUtils.isEmpty;
import static org.jbcn.server.utils.CollectionUtils.removeBlankValues;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Paper {

    public static final String STATE = "state";
    public static final String SPONSOR = "sponsor";

    public static final List<String> BASE_FIELD_NAMES = List.of(
        "_id",
        Traceable.CREATED_DATE,
        Traceable.LAST_UPDATE,
        STATE,
        "title",
        "edition",
        "level",
        "type",
        "languages",
        "comments",
        SPONSOR,
        "senders",
        "tags",
        "abstract",
        "preferenceDay",
        "feedback"
    );

    private String _id;
    private String title;
    private String edition;
    private List<Sender> senders;
    private String type;
    private String level;
    @JsonProperty("abstract")
    private String paperAbstract;
    private List<String> languages;
    @JsonDeserialize(using = PaperStateDeserializer.class)
    private PaperState state;
    @Builder.Default
    private double averageVote = 0.0f;
    private int votesCount;
    private List<String> tags;
    private String preferenceDay;
    private String comments;
    private Boolean sponsor;

    private List<PaperVote> votes;

    private Feedback feedback;

    @JsonDeserialize(using = MongoDateConverter.class)
    private Date createdDate;
    @JsonDeserialize(using = MongoDateConverter.class)
    private Date lastUpdate;

    // TODO Replace by DocumentConverter
    public Document toBsonDocument() {
        final Document result = new Document();
        if (_id != null)
            result.append("_id", new ObjectId(this.get_id()));
        if (createdDate != null)
            result.append(CREATED_DATE, this.getCreatedDate().getTime());
        if (lastUpdate != null)
            result.append(LAST_UPDATE, this.getLastUpdate().getTime());

        result.append("title", this.getTitle())
            .append("edition", this.getEdition())
            .append("level", this.getLevel())
            .append("type", this.getType().toLowerCase())
            .append("abstract", this.getPaperAbstract())
            .append("comments", this.getComments())
            .append("sponsor", getSponsor())
            .append("averageVote", this.getAverageVote())
            .append(STATE, this.getState().getState());

        if (!isEmpty(languages))
            result.append("languages", removeBlankValues(languages));

        if (!isEmpty(senders)) {
            result.append("senders", getSenders().stream()
                .map(DocumentConverter::getDocument)
                .collect(Collectors.toList()));
        }

        if (!isEmpty(tags))
            result.append("tags", removeBlankValues(tags));

        if (!isNullOrBlank(preferenceDay))
            result.append("preferenceDay", preferenceDay);

        if (isEmpty(votes)) {
            result.append("votesCount", 0);
        } else {
            result.append("votes", votes.stream().map(DocumentConverter::getDocument).collect(Collectors.toList()));
            result.append("votesCount", votes.size());
        }

        return result;
    }
}
