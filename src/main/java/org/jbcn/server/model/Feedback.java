package org.jbcn.server.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

/**
 * Feedback added to a paper to be sent to speaker(s) in case
 * paper is not accepted.
 */
@Getter
public class Feedback {

    private final String text;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;

    @JsonCreator
    public Feedback(
        @JsonProperty("text") String text,
        @JsonProperty("createdDate") LocalDateTime createdDate,
        @JsonProperty("lastUpdate") LocalDateTime lastUpdate) {
        this.text = text;
        this.createdDate = createdDate;
        this.lastUpdate = lastUpdate;
    }
}
