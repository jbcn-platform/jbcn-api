package org.jbcn.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

// TODO replace String attributes in Paper a Talk by this enum
@Getter
@AllArgsConstructor
public enum ProposalLevel {

    BEGINNER("beginner"), // default
    MIDDLE("middle"),
    ADVANCED("advanced");

    private final String type;

    public static ProposalLevel toTalkType(String type) {
        return Arrays.stream(values())
                .filter(it -> it.getType().equals(type))
                .findFirst()
                .orElse(BEGINNER);
    }

    public static boolean isValid(String type) {
        return Arrays.stream(values())
                .filter(it -> it.getType().equals(type))
                .findFirst()
                .isPresent();
    }

}
