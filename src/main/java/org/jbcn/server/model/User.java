package org.jbcn.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Getter;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

@Getter
@Builder
public class User implements Traceable {

    private String _id;

    private String username;
    private String password;
    private String email;
    private Set<Role> roles;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private LocalDateTime lastUpdate;

    // TODO do proper implementation
    @JsonIgnore
    private String lastActionBy;

    public static User getAdminUser() {
        return User.builder()
                .username("admin")
                .password("despacitosiempredespacito")
                .email("admin@jbcnconf.org")
                .roles(Collections.singleton(Role.ADMIN))
                .build();
    }

}
