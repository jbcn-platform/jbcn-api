package org.jbcn.server.model.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jbcn.server.utils.CollectionUtils;

import java.util.List;

/**
 * Represents a set of search conditions.
 * - all: conditions that all results must match.
 * - any: conditions that any of the results can match (at least one).
 */
@Getter
@AllArgsConstructor
public class SearchConditions {

    private final List<SearchFilter> all;
    private final List<SearchFilter> any;

    public boolean isEmpty() {
        return CollectionUtils.isEmpty(all) && CollectionUtils.isEmpty(any);
    }

}
