package org.jbcn.server.model.search;

import io.vertx.core.json.JsonObject;
import lombok.Getter;
import lombok.ToString;
import org.jbcn.server.utils.StringUtils;

@Getter
@ToString
public class SearchFilter {

    public static final String EQUALS = "equals";
    public static final String CONTAINS = "contains";

    public static final String IN = "in";

    private String name;
    private String op;
    private Object value;

    public SearchFilter(String name, Object value) {
        this.name = name;
        this.value = value;
        this.op = EQUALS;
    }

    public SearchFilter(String name, Object value, String operand) {
        this.name = name;
        this.value = value;
        this.op = operand;
    }

    private SearchFilter(JsonObject json) {
        this.name = json.getString("name");
        this.value = json.getValue("value");
        this.op = json.getString("op");
        if (StringUtils.isNullOrBlank(this.op))
            this.op = EQUALS;
    }

    public static SearchFilter of(JsonObject json) {
        return new SearchFilter(json);
    }

}
