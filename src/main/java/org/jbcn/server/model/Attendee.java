package org.jbcn.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jbcn.server.model.personal.Gender;

/**
 * NOTE: This class is prepared to work as independent entity, but right now is nested inside ScannedBadge.
 * To do so, add _id + Traceable fields, and appropriate API methods.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Attendee {

    private String name;
    private String email;
    private String language;
    private Integer age;
    private Gender gender;

    private String company;
    private String jobTitle;
    private String city;
    private String country;
    // made String for simplicity
    private String programmingLanguages;

}
