package org.jbcn.server.model.utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.jbcn.server.Constants;

import java.util.List;
import java.util.stream.Collectors;

public class ModelValidator {

    /**
     * Returns the list of error codes for errors found in the object passed as JSON.
     */
    public static List<String> checkRequiredFields(List<String> requiredFields, JsonObject json) {
        return requiredFields
            .stream()
            .filter(requiredField -> {
                // composed field. e.g. `sponsor: {id: "aaa"}` would be `sponsor.id`
                if (requiredField.contains(".")) {
                    final String[] path = requiredField.split("\\.");
                    Object value = json;
                    for (String part : path) {
                        value = ((JsonObject) value).getValue(part);
                        if (value == null || (value instanceof String && ((String) value).length() == 0)) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    final Object value = json.getValue(requiredField);
                    return value == null
                        || (value instanceof String && ((String) value).length() == 0)
                        || (value instanceof JsonArray && ((JsonArray) value).size() == 0);
                }
            })
            .map(requiredField -> Constants.FIELD_REQUIRED_IN_BODY + requiredField)
            .collect(Collectors.toList());
    }
}
