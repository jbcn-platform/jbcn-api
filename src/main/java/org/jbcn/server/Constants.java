package org.jbcn.server;

public class Constants {

    public static final String USER_QUEUE = "USER:QUEUE";
    public static final String PAPER_QUEUE = "PAPER:QUEUE";
    public static final String AUTH_QUEUE = "AUTH:QUEUE";
    public static final String NOTE_QUEUE = "NOTE:QUEUE";
    public static final String TALK_QUEUE = "TALK:QUEUE";
    public static final String SPONSOR_QUEUE = "SPONSOR:QUEUE";
    public static final String MOBILE_VOTE_QUEUE = "MOBILE_VOTE:QUEUE";

    public static final int UNKNOWN_ACTION_ERROR = 0;
    public static final int UNEXPECTED_ERROR = 500;
    public static final int BAD_REQUEST = 400;
    public static final int PERMISSION_DENIED = 403;
    public static final int NOT_FOUND = 404;

    public static final String ID_INVALID = "error.id.invalid_id";
    // TODO this should replace all "unknown" errors
    public static final String ID_NOT_FOUND = "error.id.not_found";
    public static final String EMAIL_TYPE_NOT_FOUND = "error.email.type.not_found";
    public static final String EMAIL_EMPTY_FEEDBACK = "error.paper.empty_feedback";

    // paper fields validation uses code `mandatory`, not sure between required & mandatory
    public static final String BODY_REQUIRED = "error.body.required";
    // name of field is concatenated at the end
    public static final String FIELD_REQUIRED_IN_URL_PARAM = "error.url_param.required_field.";
    public static final String FIELD_SINGLE_VALUE_IN_URL_PARAM = "error.url_param.not_single_value.";
    public static final String FIELD_REQUIRED_IN_BODY = "error.body.required_field.";

    public static final String INVALID_JSON_FORMAT = "error.json.invalid_format";

    public static final String UNKNOWN_ACTION_ERROR_CODE = "error.event_bus.unknown_error";

    public static final String UNKNOWN_TALK_ERROR = "error.talk.unknown_talk";
    public static final String TALK_INVALID_STATE = "error.talk.invalid_state";
    public static final String TALK_INVALID_TYPE = "error.talk.invalid_type";
    public static final String TALK_INVALID_LEVEL = "error.talk.invalid_level";

    public static final String UNKNOWN_SPONSOR_ERROR = "error.sponsor.unknown_sponsor";

    public static final String UNKNOWN_PAPER_ERROR = "error.paper.unknown_paper";
    public static final String PAPER_INVALID_STATE = "error.paper.invalid_state";
    // TODO Paper & Talk should use same error codes for shared fields: e.g. type, level, sponsor...
    public static final String PAPER_INVALID_TYPE = "error.paper.invalid_type";
    public static final String PAPER_INVALID_LEVEL = "error.paper.invalid_level";
    public static final String PAPER_SPONSOR_INVALID_FORMAT = "error.paper.sponsor.invalid_format";

    public static final String COMPANY_INVALID_TYPE = "error.company.invalid_proposal_type";

    public static final String UNKNOWN_NOTE_ERROR = "error.note.unknown_note";
    public static final String NOTE_PERMISSION_ERROR = "error.note.permission";

    public static final String UNKNOWN_USER_ERROR = "error.user.unknown_user";
    public static final String ROLE_INVALID_ROLE = "error.role.invalid";
    public static final String USER_INVALID_CREDENTIAL = "error.user.invalid_credentials";

    public static final String TALK_VOTE_INVALID_FORMAT = "error.talk.vote.invalid_format";

    public static final String SEARCH_CONDITION_MANDATORY = "error.search.conditions_minimum";

    public static final String ATTENDEE_VOTE_INVALID_VALUE = "error.attendee.vote.invalid_value";

}
