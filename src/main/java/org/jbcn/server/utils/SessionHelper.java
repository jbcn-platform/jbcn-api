package org.jbcn.server.utils;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.Optional;

public class SessionHelper {

    public static Optional<String> getCurrentUsername(RoutingContext context) {
        final JsonObject principal = context.user().principal();
        return principal == null ?
                Optional.empty() : Optional.of(principal.getString("sub"));
    }

}
