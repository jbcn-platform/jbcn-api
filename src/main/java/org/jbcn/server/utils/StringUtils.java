package org.jbcn.server.utils;

import java.util.function.Consumer;
import java.util.function.Predicate;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class StringUtils {

    public static boolean isNullOrBlank(final String text) {
        return text == null || text.trim().length() == 0;
    }

    /**
     * Applies a validation to a string, if succeeds, returns the sanitized string and and a boolean telling if there was a validation violation.
     *
     * @param value     value to validate
     * @param validator business validation to apply
     */
    // NOTE: not happy to how this method ended up
    public static Pair<String, Boolean> validate(final String value, Predicate<String> validator, Consumer<String> consumer) {
        if (!StringUtils.isNullOrBlank(value)) {
            final String aux = value.trim();
            if (validator.test(aux)) {
                consumer.accept(aux);
                return Pair.of(aux, FALSE);
            } else {
                return Pair.of(null, TRUE);
            }
        }
        return Pair.of(null, FALSE);
    }
}
