package org.jbcn.server.utils;

import lombok.SneakyThrows;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class HashUtils {

    @SneakyThrows // All exceptions are controlled
    public static String generateReference(String input) {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();

        byte[] buffer = input.getBytes(StandardCharsets.UTF_8.name());
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
        }
        return hexStr;
    }
}
