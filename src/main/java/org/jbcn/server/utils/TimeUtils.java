package org.jbcn.server.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;


public class TimeUtils {

    private static final ZoneOffset ZONE_OFFSET = ZoneOffset.of("Z");

    public static long currentTime() {
        return LocalDateTime.now().atZone(ZONE_OFFSET).toInstant().toEpochMilli();
    }

    public static long toTimestamp(LocalDateTime dateTime) {
        return dateTime.atOffset(ZONE_OFFSET).toInstant().toEpochMilli();
    }

    public static LocalDateTime toLocalDateTime(long numberLong) {
        return Instant.ofEpochMilli(numberLong).atZone(ZONE_OFFSET).toLocalDateTime();
    }
}
