package org.jbcn.server.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.SneakyThrows;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

// TODO Split this into 2 different Mappers: 1 for Pojo -> JSON (rest layer), 2 for Pojo/JsonObject to MongoDB (data layer)
public class DocumentConverter {

    /*
     * For MongoDB we want to keep nulls in the database as null values.
     * We need to copy vert.x to include vertx' JsonArray & JsonObject serialization.
     */
    private static final ObjectMapper mapper = Json.mapper.copy()
        .setSerializationInclusion(JsonInclude.Include.ALWAYS);

    /**
     * NOTE: removes _id key if exists
     */
    @SneakyThrows
    public static Document getDocument(Object object) {
        Map entries = mapper.convertValue(object, Map.class);
        entries.remove("_id");
        return Document.parse(mapper.writeValueAsString(entries));
    }

    /**
     * NOTE: removes _id key if exists
     */
    @SneakyThrows
    public static Document getDocument(JsonObject jsonObject) {
        jsonObject.remove("_id");
        return Document.parse(mapper.writeValueAsString(jsonObject));
    }

    /**
     * @param object Regular pojo or bson {@link Document}.
     */
    public static JsonObject toJson(Object object) {
        // note: doing it manually cause overriding the default Mongo ObjectId codec caused issues
        if (object instanceof Document) {
            ObjectId id = ((Document) object).getObjectId("_id");
            if (id != null) {
                ((Document) object).put("_id", id.toString());
            }
        }
        return JsonObject.mapFrom(object);
    }

    public static JsonArray toJson(List objects) {
        return (JsonArray) objects.stream()
            .map(obj -> JsonObject.mapFrom(obj))
            .collect(JsonArray::new, (BiConsumer<JsonArray, JsonObject>) JsonArray::add, (BiConsumer<JsonArray, JsonArray>) JsonArray::addAll);
    }

    public static <T> T parseToObject(JsonObject jsonObject, Class<T> clazz) {
        return jsonObject.mapTo(clazz);
    }

    public static <T> T parseToObject(Document document, Class<T> clazz) {
        JsonObject jsonObject = new JsonObject(document.toJson());
        if (jsonObject.containsKey("_id")) {
            Object id = jsonObject.getValue("_id");
            if (id instanceof JsonObject) {
                id = ((JsonObject) id).getString("$oid");
            }
            jsonObject.put("_id", id);
        }
        return jsonObject.mapTo(clazz);
    }
}
