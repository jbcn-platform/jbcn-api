package org.jbcn.server.utils;

import io.vertx.ext.web.RoutingContext;

public class RequestUtils {

    public static boolean containsEmptyBody(RoutingContext context) {
        final String body = context.getBodyAsString();
        return body == null || body.isBlank() || body.trim().equals("{}");
    }

}
