package org.jbcn.server.utils;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class CollectionUtils {

    public static boolean isEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(final Iterable<?> iterable) {
        return iterable == null || !iterable.iterator().hasNext();
    }

    public static boolean containsNotBlankValues(Collection<String> values) {
        return values.stream()
                .anyMatch(it -> !isNullOrBlank(it));
    }

    public static List<String> removeBlankValues(List<String> languages) {
        return languages
                .stream()
                .filter(Objects::nonNull)
                .filter(it -> !isNullOrBlank(it))
                .map(String::trim)
                .collect(Collectors.toList());
    }
}
