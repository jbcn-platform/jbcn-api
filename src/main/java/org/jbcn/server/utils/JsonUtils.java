package org.jbcn.server.utils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class JsonUtils {

    public static JsonObject removeNullValues(JsonObject jsonObject) {
        return jsonObject.stream()
                .filter(entry -> entry.getValue() != null)
                .collect(JsonObject::new,
                        (json, entry) -> json.put(entry.getKey(), entry.getValue()),
                        (entries, entries2) -> entries2.stream().forEach(e -> entries.put(e.getKey(), e.getValue())));
    }

    public static boolean containsAny(JsonArray array, String... values) {
        for (String value: values) {
            if (array.contains(value))
                return true;
        }
        return false;
    }

}
