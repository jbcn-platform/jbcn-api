package org.jbcn.server.utils;

import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static java.lang.Boolean.TRUE;

public class ResponseUtils {

    private static final String STATUS = "status";

    private static final int OK = 200;
    private static final int FORBIDDEN = 403;

    public static void errorResult(RoutingContext context, String errorMessage) {
        JsonObject body = new JsonObject()
            .put(STATUS, false)
            .put("error", errorMessage);
        sendBody(context, body, OK);
    }

    public static void errorResult(RoutingContext context, int statusCode, String errorMessage) {
        JsonObject body = new JsonObject()
            .put(STATUS, false)
            .put("error", errorMessage);
        sendBody(context, body, statusCode);
    }

    public static void notFound(RoutingContext context, String errorMessage) {
        JsonObject body = new JsonObject()
            .put(STATUS, false)
            .put("error", errorMessage);
        sendBody(context, body, 404);
    }

    public static void badRequest(RoutingContext context, String errorMessage) {
        JsonObject body = new JsonObject()
            .put(STATUS, false)
            .put("error", errorMessage);
        sendBody(context, body, 400);
    }

    public static void created(RoutingContext context) {
        context.response()
            .setStatusCode(201)
            .end();
    }

    public static void noContent(RoutingContext context) {
        context.response()
            .setStatusCode(204)
            .end();
    }
    
    public static void simpleResult(RoutingContext context) {
        JsonObject body = new JsonObject()
            .put(STATUS, true);
        sendBody(context, body, OK);
    }

    public static void dataResult(RoutingContext context, JsonObject data) {
        final JsonObject body = new JsonObject()
            .put(STATUS, true)
            .put("data", data);
        sendBody(context, body, OK);
    }

    public static void dataResult(RoutingContext context, JsonArray data) {
        final JsonObject body = new JsonObject()
            .put(STATUS, true)
            .put("data", data);
        sendBody(context, body, OK);
    }

    public static void dataResult(RoutingContext context, AsyncResult<Message<Object>> result) {
        final JsonObject body = new JsonObject()
            .put(STATUS, Boolean.TRUE)
            .put("data", result.result().body());
        sendBody(context, body, OK);
    }

    public static void sendBody(RoutingContext context, Object body, int statusCode) {
        context.response()
            .setStatusCode(statusCode)
            .putHeader(CONTENT_TYPE, "application/json")
            .end(List.class.isAssignableFrom(body.getClass()) ?
                DocumentConverter.toJson((List) body).toBuffer()
                : DocumentConverter.toJson(body).toBuffer());
    }

    public static void sendBody(RoutingContext context, JsonObject body, int statusCode) {
        context.response()
            .setStatusCode(statusCode)
            .putHeader(CONTENT_TYPE, "application/json")
            .end(body.toBuffer());
    }

    // TODO: use total for paginates searches

    public static void searchResult(RoutingContext context, AsyncResult<Message<Object>> reply) {
        final JsonObject body = new JsonObject()
            .put(STATUS, reply.succeeded());
        if (reply.succeeded()) {

            final Object bodyObj = reply.result().body();
            if (bodyObj instanceof JsonArray) {
                body.put("data", new JsonObject()
                    .put("items", (JsonArray) bodyObj)
                    .put("total", ((JsonArray) bodyObj).size()));
            } else {
                body.put("data", new JsonObject()
                    .put("total", ((JsonObject) bodyObj).getLong("total"))
                    .put("items", ((JsonObject) bodyObj).getJsonArray("items")));
            }
        } else {
            body.put("error", reply.cause().getMessage());
        }
        sendBody(context, body, OK);
    }

    public static void searchResult(RoutingContext context, JsonArray array) {
        final JsonObject body = new JsonObject()
            .put(STATUS, true)
            .put("data", array.size() == 0 ?
                new JsonObject().put("size", array.size()) :
                new JsonObject().put("size", array.size()).put("items", array)
            );
        sendBody(context, body, OK);
    }

    public static void simpleResult(RoutingContext context, AsyncResult<Message<Object>> reply) {
        final JsonObject body = new JsonObject()
            .put(STATUS, true)
            .put("message", reply.result().body());
        sendBody(context, body, OK);
    }

    public static void handleSimpleReply(RoutingContext context, AsyncResult<Message<Object>> reply) {
        if (reply.succeeded()) {
            ResponseUtils.simpleResult(context);
        } else {
            ResponseUtils.errorResult(context, reply.cause().getMessage());
        }
    }

    public static void handleSearchReply(RoutingContext context, AsyncResult<Message<Object>> reply) {
        if (reply.succeeded()) {
            ResponseUtils.dataResult(context, reply);
        } else {
            ResponseUtils.errorResult(context, reply.cause().getMessage());
        }
    }

    public static void csvResult(RoutingContext context, String body) {
        context.response()
            .setStatusCode(OK)
            .putHeader(CONTENT_TYPE, "text/csv")
            .end(body);
    }

    public static void forbidden(RoutingContext context) {
        context.response()
            .setStatusCode(FORBIDDEN)
            .end();
    }

    public static void entityResponse(RoutingContext context, JsonObject entity) {
        final JsonObject body = new JsonObject()
            .put("status", TRUE)
            .put("data", entity);
        sendBody(context, body, OK);
    }
}
