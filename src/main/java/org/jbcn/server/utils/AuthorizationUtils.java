package org.jbcn.server.utils;

import io.vertx.core.Future;
import io.vertx.ext.auth.User;
import org.jbcn.server.model.Role;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Role.ADMIN;

public class AuthorizationUtils {

    public static Future<Boolean> isAuthorized(User user, Role role) {
        final Future<Boolean> future = Future.future();
        user.isAuthorized(role.authority(), event -> future.complete(event.succeeded() && event.result()));
        return future;
    }

    public static Future<Boolean> isAuthorized(Boolean preAuth, User user, Role role) {
        if (preAuth)
            return Future.succeededFuture(TRUE);

        final Future<Boolean> future = Future.future();
        user.isAuthorized(role.authority(), event -> future.complete(event.succeeded() && event.result()));
        return future;
    }

    public static Future<List<String>> getAuthorizations(User user) {
        return getAuthorizations(user, ADMIN)
            .compose(authorizations -> getAuthorizations(user, Role.VOTER)
                .map(voterAuth -> {
                    final List<String> joinedAuthorizations = new ArrayList<>();
                    if (!authorizations.isEmpty())
                        joinedAuthorizations.addAll(authorizations);
                    if (!voterAuth.isEmpty())
                        joinedAuthorizations.addAll(voterAuth);
                    return joinedAuthorizations;
                })
            );
    }

    private static Future<List<String>> getAuthorizations(User user, Role role) {
        final Future<List<String>> future = Future.future();
        user.isAuthorized(role.authority(), event -> {
            if (event.succeeded() && event.result())
                future.complete(List.of(role.authority()));
            else
                future.complete(List.of());
        });
        return future;
    }
}
