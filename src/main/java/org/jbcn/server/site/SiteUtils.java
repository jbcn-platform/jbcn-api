package org.jbcn.server.site;

import org.apache.commons.io.FilenameUtils;

public class SiteUtils {

    // base path of speakers images in the public site
    private static final String SITE_SPEAKERS_PATH = "assets/img/speakers/";
    private static final String DEFAULT_IMAGE_TYPE = "jpg";

    public static String getImageUrl(String url) {
        int lastUrlSection = url.lastIndexOf("/");
        int paramsStart = url.lastIndexOf("?");
        if (paramsStart < 0) paramsStart = url.length();

        final String candidate = SITE_SPEAKERS_PATH + url.substring(lastUrlSection + 1, paramsStart);

        final String extension = FilenameUtils.getExtension(candidate);
        if (extension == null || extension.length() == 0)
            return candidate + "." + DEFAULT_IMAGE_TYPE;
        else
            return candidate;

    }

}
