package org.jbcn.server;

import com.mongodb.async.client.MongoDatabase;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.jwt.JWTAuth;
import org.jbcn.server.auth.JwtAuthProviderFactory;
import org.jbcn.server.config.JacksonConfiguration;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.email.MailHandler;
import org.jbcn.server.handlers.*;
import org.jbcn.server.http.HttpServerVerticle;
import org.jbcn.server.mail.GoogleMailSender;
import org.jbcn.server.mail.MailSender;
import org.jbcn.server.mail.TemplateProcessor;
import org.jbcn.server.model.scheduling.Room;
import org.jbcn.server.model.scheduling.Session;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.model.sponsor.JobOffer;
import org.jbcn.server.model.sponsor.Sponsor;
import org.jbcn.server.persistence.MongodbConnector;
import org.jbcn.server.platform.CrudMongoHandler;
import org.jbcn.server.services.UrlRedirect;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.stream.Collectors;

import static org.jbcn.server.persistence.Collections.*;

public class MainVerticle extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Future<Void> startFuture) {
        logger.info("Starting Main Verticle");
        long startTime = System.currentTimeMillis();

        loadJULConfiguration();
        JacksonConfiguration.initializeJacksonMapper();

        final JWTAuth jwtAuth = new JwtAuthProviderFactory(
            JbcnProperties.get("api.auth.algorithm", true),
            JbcnProperties.get("api.auth.private_key", true),
            JbcnProperties.get("api.auth.public_key", true)
        ).jwtAuthProvider(vertx);

        List<Future> handlerFutures = new ArrayList<>();
        handlerFutures.add(deployVerticle(new AuthHandler(jwtAuth)));
        handlerFutures.add(deployVerticle(new HttpServerVerticle(jwtAuth)));

        if (JbcnProperties.getBoolean("mail.enabled", false)) {
            final TemplateProcessor templateProcessor = new TemplateProcessor();
            final MailSender mailSender = new GoogleMailSender();
            handlerFutures.add(deployVerticle(new MailHandler(templateProcessor, mailSender)));
        }

        final MongoDatabase mongoDatabase = MongodbConnector.initializeMongoDb();

        final List<Future> standardHandlersFutures = List.of(
                new UserHandler(mongoDatabase, USER),
                new PaperHandler(mongoDatabase, PAPER),
                new TalkHandler(mongoDatabase, TALK),
                new NotesHandler(mongoDatabase, NOTES),
                new MobileVoteHandler(mongoDatabase, ATTENDEES_VOTES),
                new AttendeesVotesHandler(mongoDatabase, ATTENDEES_VOTES),
                new BadgesHandler(mongoDatabase, BADGES),
                new FavouritesHandler(mongoDatabase, FAVOURITED_TALKS),
                new CrudMongoHandler(Room.class, mongoDatabase, ROOMS),
                new CrudMongoHandler(Track.class, mongoDatabase, TRACKS),
                new CrudMongoHandler(Session.class, mongoDatabase, SESSIONS),
                new CrudMongoHandler(Sponsor.class, mongoDatabase, SPONSORS),
                new CrudMongoHandler(JobOffer.class, mongoDatabase, JOB_OFFERS),
                new CrudMongoHandler(UrlRedirect.class, mongoDatabase, URL_REDIRECTS)
            )
            .stream()
            .map(instance -> {
                Future<String> future1 = Future.future();
                vertx.deployVerticle(instance, future1);
                return future1;
            })
            .collect(Collectors.toList());

        handlerFutures.addAll(standardHandlersFutures);

        CompositeFuture
            .all(standardHandlersFutures)
            .setHandler(ar -> {
                if (ar.succeeded()) {
                    startFuture.complete();
                    logger.info(String.format("Main Verticle started in %s millis", System.currentTimeMillis() - startTime));
                } else {
                    startFuture.fail(ar.cause());
                }
            });
    }

    private Future deployVerticle(AbstractVerticle verticle) {
        Future future = Future.future();
        vertx.deployVerticle(verticle, future);
        return future;
    }

    // Workaround to load JUL config from Jar
    private void loadJULConfiguration() {
        final InputStream inputStream = MainVerticle.class.getResourceAsStream("/logging.properties");
        if (null != inputStream) {
            try {
                LogManager.getLogManager().readConfiguration(inputStream);
            } catch (IOException e) {
//                Logger.getGlobal().log(Level.SEVERE, "init logging system", e);
            }
        }
    }

    /*
     * Programmatic start.
     * Used to ease local development and debug.
     */
    public static void main(String[] args) {
        Vertx.vertx().
            deployVerticle(MainVerticle.class.getCanonicalName());
    }
}
