package org.jbcn.server.mail;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

public class TemplateProcessor {

    private static final Logger logger = LoggerFactory.getLogger(TemplateProcessor.class);

    /**
     * Returns content of template after applying value substitutions.
     *
     * @param templatePath path to template
     * @param substitutions key-value with values to replace and value to apply
     */
    @SneakyThrows
    public String render(Path templatePath, Map<String, String> substitutions) {
        final InputStream is = this.getClass().getResourceAsStream(templatePath.toString());
        if (is == null)
            throw new FileNotFoundException(templatePath.toString());

        return render(IOUtils.toString(is, UTF_8), substitutions);
    }

    public String render(String content, Map<String, String> substitutions) {
        if (logger.isInfoEnabled())
            logger.info("Is there full name?:" + (content.indexOf("{{fullname}}") < 0 ? false : true));
        for (String key : substitutions.keySet()) {
            if (substitutions.get(key) != null) {
                content = content.replaceAll("\\{\\{" + key + "\\}\\}", substitutions.get(key));
            }
        }
        return content;
    }
}
