package org.jbcn.server.mail;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.utils.StringUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class GoogleMailSender implements MailSender {

    private static final Logger logger = LoggerFactory.getLogger(GoogleMailSender.class);

    private final Boolean mailEnabled;
    private final String from;
    private final String mailHost;
    private final String mailPort;

    public GoogleMailSender() {
        mailEnabled = JbcnProperties.getBoolean("mail.enabled", false);

        from = JbcnProperties.get("mail.confirmation.from");
        mailHost = JbcnProperties.get("mail.hostname");
        mailPort = JbcnProperties.get("mail.port");
    }

    @Override
    public void sendEmail(String to, String bcc, String subject, String content, String contentType) {
        javax.mail.Message message = new MimeMessage(getMailSession());
        try {
            InternetAddress toAddress = new InternetAddress(to);
            message.addRecipients(javax.mail.Message.RecipientType.TO, new Address[]{toAddress});
            InternetAddress from = new InternetAddress(this.from);
            message.setFrom(from);
            message.setReplyTo(new Address[]{from});
            if (!StringUtils.isNullOrBlank(bcc)) {
                InternetAddress bbc = new InternetAddress(this.from);
                message.addRecipient(javax.mail.Message.RecipientType.BCC, bbc);
            }
            message.setSubject(subject);
            message.setContent(content, contentType);
            Transport.send(message);
            logger.debug("Email sent");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Session getMailSession() {
        final Properties props = new Properties();
        props.put("mail.smtp.host", mailHost);
        props.put("mail.smtp.port", mailPort);
        if (mailEnabled) {
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", true);
            props.put("mail.smtp.auth.mechanisms", "XOAUTH2");
            props.put("mail.smtp.starttls.enable", true);
            props.put("mail.debug", true);
        }
        return Session.getInstance(props, GOOGLE_AUTHENTICATOR);
    }

    private static final Authenticator GOOGLE_AUTHENTICATOR = new GoogleTokenCacheAuthenticator(
        JbcnProperties.get("mail.user"),
        JbcnProperties.get("google.oauth.client_id"),
        JbcnProperties.get("google.oauth.secret"),
        JbcnProperties.get("google.oauth.refresh_token"),
        JbcnProperties.get("google.oauth.token_url")
    );

    public static class GoogleTokenCacheAuthenticator extends Authenticator {

        private long tokenTTL = 1458168133864L;

        private String accessToken;

        private final String userName;
        private final String oauthClientId;
        private final String oauthSecret;
        private final String refreshToken;
        private final String oauthRefreshUrl;

        public GoogleTokenCacheAuthenticator(String username,
                                             String oauthClientId, String oauthSecret, String refreshToken, String oauthRefreshUrl) {
            this.oauthClientId = oauthClientId;
            this.oauthSecret = oauthSecret;
            this.refreshToken = refreshToken;
            this.oauthRefreshUrl = oauthRefreshUrl;
            this.userName = username;
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            final String token = getToken();
            logger.info("Obtained mail token");

            return new PasswordAuthentication(userName, token);
        }

        private synchronized String getToken() {
            if (System.currentTimeMillis() > tokenTTL) {
                logger.info("Email token expired");
                try {
                    final String request = new StringBuilder()
                        .append("client_id=" + URLEncoder.encode(oauthClientId, UTF_8))
                        .append("&client_secret=" + URLEncoder.encode(oauthSecret, UTF_8))
                        .append("&refresh_token=" + URLEncoder.encode(refreshToken, UTF_8))
                        .append("&grant_type=refresh_token")
                        .toString();

                    logger.info("Request oauth data:" + request);

                    final HttpURLConnection conn = (HttpURLConnection) new URL(oauthRefreshUrl).openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    PrintWriter out = new PrintWriter(conn.getOutputStream());
                    out.print(request); // note: println causes error
                    out.flush();
                    out.close();

                    final String response = new BufferedReader(new InputStreamReader(conn.getInputStream()))
                        .lines()
                        .collect(Collectors.joining("\n"));
                    logger.info("Email token response successful");

                    final Map<String, Object> result = new ObjectMapper().readValue(response, new TypeReference<HashMap<String, Object>>() {
                    });
                    accessToken = (String) result.get("access_token");
                    logger.info("Token updated");
                    tokenTTL = System.currentTimeMillis() + (((Number) result.get("expires_in")).intValue() * 1000);

                } catch (Exception e) {
                    logger.error("error recovering gmail token", e);
                }
            }
            return accessToken;
        }
    }
}
