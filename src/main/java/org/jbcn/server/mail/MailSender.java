package org.jbcn.server.mail;

public interface MailSender {

    void sendEmail(String to, String bcc, String subject, String content, String contentType);

}
