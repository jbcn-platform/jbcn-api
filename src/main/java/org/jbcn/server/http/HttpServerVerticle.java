package org.jbcn.server.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.ext.web.sstore.SessionStore;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.controllers.*;
import org.jbcn.server.email.MailController;
import org.jbcn.server.mobile.MobileController;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.ScannedBadge;
import org.jbcn.server.model.scheduling.Room;
import org.jbcn.server.model.scheduling.Session;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.model.sponsor.JobOffer;
import org.jbcn.server.model.sponsor.Sponsor;
import org.jbcn.server.model.votes.AttendeeVote;
import org.jbcn.server.model.votes.FavouritedTalk;
import org.jbcn.server.platform.GenericCrudController;
import org.jbcn.server.services.UrlRedirect;
import org.jbcn.server.services.UrlRedirectController;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.StringUtils;

import static org.jbcn.server.model.Role.*;
import static org.jbcn.server.utils.AuthorizationUtils.isAuthorized;

public class HttpServerVerticle extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(HttpServerVerticle.class);

    private UserController userController;
    private PaperController paperController;
    private AuthController authController;
    private VoteController voteController;
    private TalkController talkController;
    private NotesController noteController;
    private CounterController counterController;
    private CompaniesController companiesController;
    private MobileController mobileController;
    private MailController mailController;

    private GenericCrudController<Room> roomsController;
    private GenericCrudController<Track> tracksController;
    private GenericCrudController<Session> sessionsController;
    private GenericCrudController<AttendeeVote> attendeesVotesController;
    private GenericCrudController<FavouritedTalk> favouritesController;
    private GenericCrudController<ScannedBadge> badgesController;
    private GenericCrudController<Sponsor> sponsorsController;
    private GenericCrudController<JobOffer> jobOffersController;
    private GenericCrudController<UrlRedirect> redirectsController;

    private final JWTAuth jwt;

    public HttpServerVerticle(JWTAuth jwt) {
        this.jwt = jwt;
    }

    public void start(Future<Void> startFuture) {

        initControllers();

        final HttpServer server = vertx.createHttpServer();
        final Router router = Router.router(vertx);

        final String corsConfig = JbcnProperties.get("api.cors");
        if (!StringUtils.isNullOrBlank(corsConfig)) {
            logger.info("Enabling CORS: " + corsConfig);
            router.route()
                .handler(CorsHandler.create(corsConfig)
                    .allowedMethod(io.vertx.core.http.HttpMethod.GET)
                    .allowedMethod(io.vertx.core.http.HttpMethod.HEAD)
                    .allowedMethod(io.vertx.core.http.HttpMethod.POST)
                    .allowedMethod(io.vertx.core.http.HttpMethod.PUT)
                    .allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
                    .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
                    .allowCredentials(true)
                    .allowedHeader("Authorization")
                    .allowedHeader("Access-Control-Allow-Method")
                    .allowedHeader("Access-Control-Allow-Origin")
                    .allowedHeader("Access-Control-Allow-Credentials")
                    .allowedHeader("X-XSRF-TOKEN")
                    .allowedHeader("Content-Type"));
        }

        router.route().handler(CookieHandler.create());

        SessionStore store = LocalSessionStore.create(vertx);
        SessionHandler sessionHandler = SessionHandler.create(store);
        router.route().handler(sessionHandler);

        router.get("/").handler(event -> event.response().end());

        // API
        router.route("/api/*").handler(JWTAuthHandler.create(jwt, null));

        //User routes
        router.get("/api/checkSession").handler(this.authController::checkSession);
        router.route("/api/users/*")
            .handler(context -> isAuthorized(context.user(), ADMIN)
                .setHandler(getAuthorizedHandler(context)))
            .handler(BodyHandler.create());
        router.post("/api/users").handler(this.userController::addUser);
        router.get("/api/users").handler(this.userController::listUsers);
        router.get("/api/users/:id").handler(this.userController::getUser);
        router.put("/api/users/:id").handler(this.userController::updateUser);
        router.delete("/api/users/:id").handler(this.userController::deleteUser);
        router.post("/api/users/search").handler(this.userController::search);

        // Papers routes
        // router.get("/api/paper").handler(this.paperController::getPapers);
        router.route("/api/paper/*")
            .handler(context -> {
                final User user = context.user();
                isAuthorized(user, ADMIN)
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, VOTER))
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, HELPER))
                    .setHandler(getAuthorizedHandler(context));
            })
            .handler(BodyHandler.create());
        router.get("/api/paper/count/:edition").handler(this.counterController::countPapers);
        router.get("/api/paper/:id").handler(this.paperController::getPaper);
        router.put("/api/paper/:id/sponsor/:sponsor").handler(this.paperController::putSponsorPaper);
        router.post("/api/paper/:id/approve").handler(this.paperController::approvePaper);
        router.put("/api/paper/:id").handler(this.paperController::updatePaper);
        router.post("/api/paper").handler(this.paperController::addPaper);
        router.post("/api/paper/search").handler(this.paperController::searchPapers);
        router.delete("/api/paper/:id").handler(this.paperController::deletePaper);
        router.get("/api/paper/:id/feedback").handler(this.paperController::getFeedback);
        router.post("/api/paper/:id/feedback").handler(this.paperController::upsertFeedback);
        router.put("/api/paper/:id/feedback").handler(this.paperController::upsertFeedback);
        router.delete("/api/paper/:id/feedback").handler(this.paperController::deleteFeedback);

        router.route("/api/emails/*")
            .handler(context -> {
                final User user = context.user();
                isAuthorized(user, ADMIN)
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, VOTER))
                    .setHandler(getAuthorizedHandler(context));
            })
            .handler(BodyHandler.create());
        router.post("/api/emails/:type").handler(this.mailController::sendEmail);

        // Talks API
        router.route("/api/talk/*")
            .handler(context -> {
                final User user = context.user();
                isAuthorized(user, ADMIN)
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, VOTER))
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, HELPER))
                    .setHandler(getAuthorizedHandler(context));
            })
            .handler(BodyHandler.create());
        router.get("/api/talk/:id").handler(this.talkController::getTalk);
        router.put("/api/talk/:id").handler(this.talkController::updateTalk);
        router.delete("/api/talk/:id").handler(this.talkController::delete);
        router.put("/api/talk/:id/state").handler(this.talkController::changeState);
        router.put("/api/talk/:id/published").handler(this.talkController::publishTalk);
        router.post("/api/talk/search").handler(this.talkController::search);
        router.get("/api/talk/count/:edition").handler(this.counterController::countTalks);
        router.get("/api/talk/:id/feedback").handler(this.talkController::getFeedback);

        // Speakers (uses Talks data)
        router.route("/api/speakers/*").handler(BodyHandler.create());
        router.get("/api/speakers").handler(this.talkController::getAllSpeakers);

        // Senders (aka. proponents)
        router.route("/api/sender/*").handler(BodyHandler.create());
        router.post("/api/sender/star").handler(this.paperController::senderStarred);
        router.get("/api/senders").handler(this.paperController::getAllSenders);

        // Paper vote routes (by org members)
        router.get("/api/voters").handler(this.userController::getVoters);

        router.route("/api/vote/*")
            .handler(context -> {
                final User user = context.user();
                isAuthorized(user, ADMIN)
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, VOTER))
                    .setHandler(getAuthorizedHandler(context));
            })
            .handler(BodyHandler.create());
        router.post("/api/vote").handler(this.voteController::upsertVote);
        router.get("/api/vote/stream").handler(this.voteController::getPapersStream);

        router.route("/api/notes/*")
            .handler(context -> {
                final User user = context.user();
                isAuthorized(user, ADMIN)
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, VOTER))
                    .setHandler(getAuthorizedHandler(context));
            })
            .handler(BodyHandler.create());
        router.post("/api/notes").handler(this.noteController::addNote);
        router.get("/api/notes/:id").handler(this.noteController::getNote);
        router.put("/api/notes/:id").handler(this.noteController::updateNote);
        router.delete("/api/notes/:id").handler(this.noteController::deleteNote);
        router.get("/api/notes").handler(this.noteController::listNotes);

        // List of companies that present papers
        // TODO refactor as /papers/companies, /talks/companies
        router.route("/api/companies/*").handler(BodyHandler.create());
        router.get("/api/companies").handler(this.companiesController::listCompanies);

        // Public paper routes
        router.route("/public/api/paper/*").handler(BodyHandler.create());
        //router.get("/public/api/paper/:id").handler(this.paperController::getPaper);
        // TODO should be "/public/api/paper/:id/state"
        router.get("/public/api/paper/state/:id").handler(this.paperController::getPaperState);
        router.post("/public/api/paper").handler(this.paperController::addPaper);
        //router.put("/public/api/paper/:id").handler(this.paperController::updatePaper);

        // Public data
        router.route("/public/talks/*").handler(BodyHandler.create());
        router.get("/public/talks/:id").handler(this.talkController::getPublishedTalk);
        router.get("/public/talks").handler(this.talkController::getPublishedTalks);
        router.route("/public/speakers/*").handler(BodyHandler.create());
        router.get("/public/speakers/:id").handler(this.talkController::getPublishedSpeaker);
        router.get("/public/speakers").handler(this.talkController::getPublishedSpeakers);

        // Mobile voting API
        // @Deprecated: TO DELETE
        router.route("/public/api/vote/*").handler(BodyHandler.create());
        router.get("/public/api/vote/:scheduleId/:deviceId/:vote").handler(this.talkController::upsertMobileVote);
        router.get("/public/api/vote/:scheduleId").handler(this.talkController::getMobileVote);
        router.get("/public/api/votes").handler(this.talkController::getMobileVotes);

        router.route("/api/counter/*").handler(BodyHandler.create());
        router.get("/api/counter/tag/:edition").handler(this.counterController::countTags);

        // router.put("/api/talk/app/:scheduleId/vote/:deviceId/:stars").handler(this.talkController::putAppVote);
        // router.get("/api/talk/app/:scheduleId").handler(this.talkController::getAppVotes);
        // router.post("/api/talk/app/comment").handler(this.talkController::putAppComment);

        bindGenericController(router, roomsController, "/api/rooms", ADMIN, HELPER);
        bindGenericController(router, tracksController, "/api/tracks", ADMIN, HELPER);
        bindGenericController(router, sessionsController, "/api/sessions", ADMIN, HELPER);
        bindGenericController(router, sponsorsController, "/api/sponsors", ADMIN, HELPER);
        bindGenericController(router, jobOffersController, "/api/job-offers", ADMIN, HELPER);
        bindGenericController(router, redirectsController, "/api/url-redirects", ADMIN);

        // Attendees Votes (from mobile app)
        router.route("/api/attendees/votes/*").handler(BodyHandler.create());
        router.post("/api/attendees/votes").handler(attendeesVotesController::add);
        router.put("/api/attendees/votes").handler(attendeesVotesController::upsert);
        router.get("/api/attendees/votes/:id").handler(attendeesVotesController::get);
        router.get("/api/attendees/votes").handler(attendeesVotesController::getAll);
        router.post("/api/attendees/votes/search").handler(attendeesVotesController::filter);
        router.delete("/api/attendees/votes/:id").handler(attendeesVotesController::delete);

        router.route("/api/talks/favourites/*").handler(BodyHandler.create());
        router.post("/api/talks/favourites").handler(favouritesController::add);
        router.get("/api/talks/favourites/:id").handler(favouritesController::get);
        router.get("/api/talks/favourites").handler(favouritesController::getAll);
        router.post("/api/talks/favourites/search").handler(favouritesController::filter);
        router.delete("/api/talks/favourites/").handler(favouritesController::delete);
        router.delete("/api/talks/favourites/:id").handler(favouritesController::delete);

        router.route("/api/talks/tags/*").handler(BodyHandler.create());
        router.get("/api/talks/tags").handler(talkController::getAllTags);
        // router.delete("/api/talks/favourites/:id").handler(favouritesController::delete);

        router.route("/api/talks/votes/*").handler(BodyHandler.create());
        router.get("/api/talks/votes").handler(talkController::combineVotes);

        // Badge info (scanned from mobile app)
        router.route("/api/badges/*")
            .handler(context -> {
                final User user = context.user();
                isAuthorized(user, ADMIN)
                    .compose(preAuthorized -> isAuthorized(preAuthorized, user, SPONSOR))
                    .setHandler(getAuthorizedHandler(context));
            })
            .handler(BodyHandler.create());
        router.post("/api/badges").handler(badgesController::add);
        router.put("/api/badges").handler(badgesController::upsert);
        router.get("/api/badges/:id").handler(badgesController::get);
        router.get("/api/badges").handler(badgesController::getAll);
        router.delete("/api/badges/:id").handler(badgesController::delete);
        router.post("/api/badges/search").handler(badgesController::filter);

        // Auth routes
        router.route("/login/*").handler(BodyHandler.create());
        router.post("/login").handler(this.authController::login);
        router.route("/logout/*").handler(BodyHandler.create());
        router.get("/logout").handler(this.authController::logout);

        // OAuth
        router.get("/oauth/token").handler(this.authController::getOauth2Token);

        // Mobile routes
        router.get("/public/mobile/conferences/*").handler(mobileController::getConference);
        router.get("/public/mobile/talks").handler(mobileController::getTalks);
        router.get("/public/mobile/speakers").handler(mobileController::getSpeakers);

        // Support services
        final var httpUrlRedirectController = new UrlRedirectController(vertx);
        router.get("/r/*").handler(BodyHandler.create());
        router.get("/r/:name").handler(httpUrlRedirectController::get);

        server.requestHandler(router::handle).listen(8080, ar -> {
            if (ar.succeeded()) {
                logger.info("HTTP server running on port 8080");
                startFuture.complete();
            } else {
                logger.info("Could not start a HTTP server", ar.cause());
                startFuture.fail(ar.cause());
            }
        });
    }

    private void initControllers() {
        userController = new UserController(vertx);
        paperController = new PaperController(vertx);
        authController = new AuthController(vertx);
        voteController = new VoteController(vertx);
        talkController = new TalkController(vertx);
        noteController = new NotesController(vertx);
        counterController = new CounterController(vertx);
        companiesController = new CompaniesController(vertx);
        mobileController = new MobileController(vertx);
        mailController = new MailController(vertx);

        // design: instantiate directly for faster startup. Avoid auto-scan or similar.
        roomsController = new GenericCrudController(vertx, Room.class);
        tracksController = new GenericCrudController(vertx, Track.class);
        sessionsController = new GenericCrudController(vertx, Session.class);
        attendeesVotesController = new GenericCrudController(vertx, AttendeeVote.class);
        badgesController = new GenericCrudController(vertx, ScannedBadge.class);
        favouritesController = new FavouritedTalksController(vertx, FavouritedTalk.class);
        sponsorsController = new GenericCrudController(vertx, Sponsor.class);
        jobOffersController = new GenericCrudController(vertx, JobOffer.class);

        redirectsController = new GenericCrudController<>(vertx, UrlRedirect.class);
    }

    /**
     * Adds generic route bindings for CRUD apis.
     *
     * @param router Vert.x {@link Router}.
     * @param controller {@link GenericCrudController} implementing required {@link Handler}.
     * @param resource API url resource component (e.g. "thing" to expose api as /thing).
     * @param roles roles authorized on the resource (max allowed 2).
     */
    private <T> void bindGenericController(Router router,
                                           GenericCrudController<T> controller,
                                           String resource,
                                           Role... roles) {
        // TODO support more than 2
        if (roles.length == 0 || roles.length > 2) {
            throw new IllegalArgumentException("1 or 2 roles are required");
        }

        router.route(resource + "/*")
            .handler(context -> {
                final User user = context.user();
                // For some reason, this does not work
                if (roles.length == 1) {
                    isAuthorized(user, roles[0])
                        .setHandler(getAuthorizedHandler(context));
                } else {
                    isAuthorized(user, roles[0])
                        .compose(preAuthorized -> isAuthorized(preAuthorized, user, roles[1]))
                        .setHandler(getAuthorizedHandler(context));
                }
            })
            .handler(BodyHandler.create());
        router.post(resource).handler(controller::add);
        router.get(resource + "/:id").handler(controller::get);
        router.get(resource).handler(controller::getAll);
        router.put(resource + "/:id").handler(controller::update);
        router.delete(resource + "/:id").handler(controller::delete);
    }

    private Handler<AsyncResult<Boolean>> getAuthorizedHandler(RoutingContext context) {
        return event -> {
            if (event.succeeded() && event.result())
                context.next();
            else
                ResponseUtils.forbidden(context);
        };
    }
}
