package org.jbcn.server.http;

import io.vertx.ext.web.RoutingContext;

import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class HttpOptions {

    public static boolean isHtmlFormatEnabled(RoutingContext context) {
        final String format = context.request().getParam("format");
        return !isNullOrBlank(format) && format.equals("html");
    }
}
