package org.jbcn.server.mobile;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.mobile.model.*;
import org.jbcn.server.model.TalkState;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.platform.GenericCrudController;
import org.jbcn.server.utils.ResponseUtils;

import java.util.*;
import java.util.stream.Collectors;

import static org.jbcn.server.Constants.TALK_QUEUE;
import static org.jbcn.server.utils.ResponseUtils.sendBody;

public class MobileController {

    private static final Logger logger = LoggerFactory.getLogger(MobileController.class);

    private static final int OK_STATUS = 200;

    private static final String TRACKS_QUEUE = GenericCrudController.getQueueName(Track.class);

    private final MobileMappers mappers = new MobileMappers();
    private final Vertx vertx;


    public MobileController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void getConference(RoutingContext routingContext) {

        final Future tracksFuture = Future.future();
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", getGetAllActionName(Track.class));
        vertx.eventBus().send(TRACKS_QUEUE, new JsonObject(), options, tracksFuture);

        final Future<Message<JsonObject>> talksFuture = Future.future();
        final DeliveryOptions talkOptions = new DeliveryOptions().addHeader("action", "talk-tags-count");
        vertx.eventBus().send(TALK_QUEUE, new JsonObject(), talkOptions, talksFuture);

        CompositeFuture.all(tracksFuture, talksFuture)
            .setHandler(reply -> {
                if (reply.succeeded()) {
                    List<Track> tracks = ((JsonArray) ((Message) reply.result().resultAt(0)).body())
                        .stream()
                        .map(json -> mappers.trackMapper((JsonObject) json))
                        .collect(Collectors.toList());

                    final List<Tag> tags = ((JsonObject) ((Message) reply.result().resultAt(1)).body())
                        .stream()
                        .map(entry -> mappers.mapTag(entry))
                        .sorted(Comparator.comparing(Tag::getValue))
                        .collect(Collectors.toList());

                    sendBody(routingContext, new Conference(tracks, tags), OK_STATUS);
                } else {
                    logger.error("Error retrieving conference info", reply.cause());
                    ResponseUtils.errorResult(routingContext, reply.cause().getMessage());
                }
            });

    }

    public void getTalks(RoutingContext routingContext) {

        final JsonObject searchOptions = publishedTalksSearchOptions();

        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-search");
        vertx.eventBus().send(Constants.TALK_QUEUE, searchOptions, options, reply -> {
            if (reply.succeeded()) {
                final List<Talk> talks = ((JsonObject) reply.result().body()).getJsonArray("items")
                    .stream()
                    .filter(talk -> !((JsonObject) talk).getString("state").equals(TalkState.TALK_STATE_CANCELED.getState()))
                    .map(talk -> mappers.mapTalk((JsonObject) talk))
                    .collect(Collectors.toList());

                sendBody(routingContext, talks, OK_STATUS);
            } else {
                logger.error("Error retrieving conference talks", reply.cause());
                ResponseUtils.errorResult(routingContext, reply.cause().getMessage());
            }
        });
    }

    private JsonObject publishedTalksSearchOptions() {
        return new JsonObject()
            .put("conditions", new JsonObject()
                .put("all", new JsonArray()
                    .add(new JsonObject()
                        .put("name", "published")
                        .put("value", Boolean.TRUE))))
            .put("sort", "createdDate")
            .put("asc", false)
            .put("page", 0)
            .put("size", 150);
    }


    private String getGetAllActionName(Class clazz) {
        return clazz.getSimpleName().toLowerCase() + "-list";
    }

    public void getSpeakers(RoutingContext routingContext) {

        final JsonObject searchOptions = publishedTalksSearchOptions();

        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-search");
        vertx.eventBus().send(Constants.TALK_QUEUE, searchOptions, options, reply -> {
            if (reply.succeeded()) {

                JsonArray talks = ((JsonObject) reply.result().body())
                    .getJsonArray("items")
                    .stream()
                    .filter(t -> !((JsonObject) t).getString("state").equals(TalkState.TALK_STATE_CANCELED.getState()))
                    .collect(JsonArray::new, JsonArray::add, JsonArray::addAll);

                final List<Talk> mappedTalks = talks.stream()
                    .map(t -> mappers.mapTalk((JsonObject) t))
                    .collect(Collectors.toList());

                List<org.jbcn.server.mobile.model.Speaker> allSpeakers = mappedTalks.stream()
                    .flatMap(t -> t.getSpeakersDetails().stream())
                    .sorted(Comparator.comparing(org.jbcn.server.mobile.model.Speaker::getFullName))
                    .collect(Collectors.toList());

                final List<org.jbcn.server.mobile.model.Speaker> filteredSpeakers = removeDuplicatedByCode(allSpeakers)
                    .stream()
                    .peek(speaker -> speaker.setAcceptedTalks(filterSpeakerTalks(mappedTalks, speaker)))
                    .collect(Collectors.toList());

                for (var s : filteredSpeakers) {
                    for (var t : s.getAcceptedTalks()) {
                        t.setSpeakersDetails(null);
                    }
                }

                sendBody(routingContext, filteredSpeakers, OK_STATUS);
            } else {
                logger.error("Error retrieving conference talks", reply.cause());
                ResponseUtils.errorResult(routingContext, reply.cause().getMessage());
            }
        });
    }

    // FIXME duplicated method
    private List<Speaker> removeDuplicatedByCode(List<Speaker> speakers) {
        final List<Speaker> res = new ArrayList<>();
        final Set<String> codes = new HashSet(speakers.size());
        for (Speaker speaker : speakers) {
            final String code = speaker.getUuid();
            if (!codes.contains(code)) {
                codes.add(code);
                res.add(speaker);
            }
        }
        return res;
    }

    private List<Talk> filterSpeakerTalks(List<Talk> talks, Speaker speaker) {
        return talks.stream()
            .filter(t -> t.getSpeakersDetails().stream().anyMatch(s -> s.getUuid().equals(speaker.getUuid())))
            .collect(Collectors.toList());
    }

}
