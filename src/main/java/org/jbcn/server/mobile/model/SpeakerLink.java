package org.jbcn.server.mobile.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SpeakerLink {

    final Link link;
    final String name;

}
