package org.jbcn.server.mobile.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Tag {
    
    final String value;
    final Integer count;

}
