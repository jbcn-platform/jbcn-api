package org.jbcn.server.mobile.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Speaker {

    final String uuid;
    final String bio;
    final String bioAsHtml;
    final String fullName;
    final String firstName;
    final String lastName;
    final String avatarURL;
    final String company;
    final String blog;
    final String twitter;
    final String lang;
    final List<Link> links;
    @Setter
    List<Talk> acceptedTalks;

    final String summary;

    public String getSummary() {
        return bio.replace("\\[(.*?)]\\(.*?\\)", "$1");
    }
}
