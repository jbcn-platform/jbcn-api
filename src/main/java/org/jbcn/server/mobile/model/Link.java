package org.jbcn.server.mobile.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Link {

    final String href;
    final String rel;
    final String title;

}
