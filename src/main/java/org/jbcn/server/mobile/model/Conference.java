package org.jbcn.server.mobile.model;

import lombok.Getter;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.scheduling.Track;

import java.util.List;
import java.util.Map;

@Getter
public class Conference {

    private static final String JBCNCONF_SITE_URL = "https://www.jbcnconf.com/";
    private final String edition = JbcnProperties.getCurrentEdition();

    final String id = "1";
    final String name = "JBCNConf Barcelona " + edition;
    final String website = JBCNCONF_SITE_URL + edition + "/";
    final String imageURL = JBCNCONF_SITE_URL + edition + "/assets/img/logos/logo-jbcnconf.png";
    final String fromDate = "2020-07-06";
    final String endDate = "2020-07-08";
    final String eventType = "CONFERENCE";
    final String cfpURL = JBCNCONF_SITE_URL + edition + "/callForPapers.html";
    final String cfpVersion = "1.0";
    final Integer locationId = 1;
    final Boolean myBadgeActive = Boolean.TRUE;
    final List sessionTypes = List.of(
            Map.of("id", 1,
                    "name", "talk",
                    "pause", false),
            Map.of("id", 2,
                    "name", "workshop",
                    "pause", false));
    final List floorPlans = List.of(
            Map.of("id", 1,
                    "name", "General map",
                    "imageURL", JBCNCONF_SITE_URL + edition + "/assets/img/venue/venue_maps.png")
    );

    final List<Track> tracks;
    final List<Tag> tags;

    public Conference(List<Track> tracks, List<Tag> tags) {
        this.tracks = tracks;
        this.tags = tags;
    }

}
