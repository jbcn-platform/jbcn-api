package org.jbcn.server.mobile.model;

import io.vertx.core.json.JsonObject;
import org.jbcn.server.mobile.ConferenceUriBuilder;
import org.jbcn.server.model.scheduling.Track;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MobileMappers {

    private static final String LANG = "en";

    private Map<String, String> COMPOUND_NAMES = Map.of(
            "Juan Luis", "Ozáez",
            "Ana Maria", "Mihalceanu"
    );

    private boolean isCompoundName(String fullName) {
        return COMPOUND_NAMES.keySet().stream().anyMatch(fullName::startsWith);
    }

    public Track trackMapper(JsonObject json) {
        return new Track(
                json.getString("_id"),
                json.getString("name"),
                json.getString("description"),
                null, null, null);
    }

    public Tag mapTag(Map.Entry<String, Object> entry) {
        return new Tag(entry.getKey(), Integer.valueOf(entry.getValue().toString()));
    }

    public Talk mapTalk(JsonObject talk) {
        return new Talk(
                talk.getString("_id"),
                talk.getString("title"),
                talk.getBoolean("published"),
                talk.getString("type"),
                null,
                null,
                LANG,
                talk.getString("level"),
                talk.getString("paperAbstract"),
                null,
                null, // TODO
                talk.getJsonArray("tags").stream()
                        .map(tag -> new Tag((String) tag, null))
                        .collect(Collectors.toList()),
                talk.getJsonArray("speakers").stream()
                        .map(speaker -> mapSpeakerLink((JsonObject) speaker))
                        .collect(Collectors.toList()),
                talk.getJsonArray("speakers").stream()
                        .map(speaker1 -> mapSpeaker((JsonObject) speaker1))
                        .collect(Collectors.toList())
        );
    }

    private SpeakerLink mapSpeakerLink(JsonObject speaker) {
        return new SpeakerLink(
                new Link(
                        new ConferenceUriBuilder().append("speakers").append(speaker.getString("ref")).build(),
                        new ConferenceUriBuilder().append("speakers").build(),
                        speaker.getString("fullName")
                ),
                speaker.getString("fullName")
        );
    }

    public Speaker mapSpeaker(JsonObject speaker) {
        final String speakerUri = new ConferenceUriBuilder().append("speakers").append(speaker.getString("ref")).build();

        final String fullName = speaker.getString("fullName");
        String firstName;
        String lastName;

        if (isCompoundName(fullName)) {
            firstName = COMPOUND_NAMES.keySet().stream().filter(key -> fullName.startsWith(key)).findFirst().get();
            lastName = COMPOUND_NAMES.get(firstName);
        } else {
            firstName = fullName.substring(0, fullName.indexOf(" "));
            lastName = fullName.substring(fullName.indexOf(" ") + 1);
        }

        return new Speaker(
                speaker.getString("ref"),
                speaker.getString("biography"),
                null,
                fullName,
                firstName,
                lastName,
                Optional.ofNullable(speaker.getString("publicPicture")).orElse("https://www.jbcnconf.com/2019/assets/img/logos/logo-jbcnconf.png"),
                speaker.getString("company"),
                speaker.getString("web"),
                speaker.getString("twitter"),
                LANG,
                List.of(new Link(speakerUri, speakerUri, fullName)),
                null,
                null
        );
    }
}
