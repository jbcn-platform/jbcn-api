package org.jbcn.server.mobile.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Talk {

    final String id;
    final String title;
    final Boolean published;
    final String talkType;
    final String track;
    final String trackId;
    final String lang;
    final String audienceLevel;
    final String summary;
    final String summaryAsHtml;
    final String requirements;
    final List<Tag> tags;
    final List<SpeakerLink> speakers;
    @Setter
    List<Speaker> speakersDetails;

}
