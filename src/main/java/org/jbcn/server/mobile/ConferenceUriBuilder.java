package org.jbcn.server.mobile;

import lombok.SneakyThrows;
import org.jbcn.server.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConferenceUriBuilder {

    private static final String DOMAIN = "jbcnapi.cleverapps.io";

    private List<String> sections = new ArrayList<>();


    public ConferenceUriBuilder() {
    }

    public ConferenceUriBuilder append(String section) {
        if (!StringUtils.isNullOrBlank(section))
            sections.add(removeSurroundingSlashes(section));
        return this;
    }

    @SneakyThrows
    public String build() {
        return String.format("https://%s/%s", DOMAIN, sections.stream().collect(Collectors.joining("/")));
    }

    private String removeSurroundingSlashes(String section) {
        int start = section.startsWith("/") ? 1 : 0;
        int end = section.endsWith("/") ? section.length() - 1 : section.length();
        return section.substring(start, end);
    }

}
