package org.jbcn.server.config;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class JbcnProperties {

    private static final Logger logger = LoggerFactory.getLogger(JbcnProperties.class);

    private static final String JBCN_ENV_KEY = "JBCN_ENV";
    private static final String REQUIRED_VOTES_PREFIX = "votes.required.";

    private final Properties properties = new Properties();

    private final Map<String, Integer> requiredVotesCount;
    private final List<String> editions;

    private static final JbcnProperties instance;

    static {
        instance = new JbcnProperties();
    }

    public static String getPapersPublicUrL() {
        final String candidate = JbcnProperties.get("papers.public.url");
        return endsWithSlash(candidate) ? candidate : candidate + "/";
    }

    private static boolean endsWithSlash(String candidate) {
        return candidate.lastIndexOf('/') == (candidate.length() - 1);
    }

    /**
     * Returns the properties config file.
     * The config file pattern is:
     * - jbcn.properties, when `JBCN_ENV` environment variable is "dev" or null.
     * - jbcn.${JBCN_ENV}.properties, otherwise.
     */
    public static String getPropertiesFile() {
        final String envName = System.getenv(JBCN_ENV_KEY);
        final String suffix = (envName == null || envName.equals("dev")) ? "" : "." + envName.toLowerCase();
        final String configFile = "/jbcn" + suffix + ".properties";
        logger.info("Loading configuration from file: " + configFile);
        return configFile;
    }

    private JbcnProperties() {
        final String propertiesPath = getPropertiesFile();
        logger.info("Loading properties: " + propertiesPath);
        try (InputStream is = this.getClass().getResourceAsStream(propertiesPath)) {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        editions = getStringList("editions");
        requiredVotesCount = editions.stream()
            .collect(Collectors.toMap(identity(), edition -> getInnerInteger(REQUIRED_VOTES_PREFIX + edition.trim())));
    }

    private static JbcnProperties getInstance() {
        return instance;
    }

    public static String get(String key) {
        return get(key, false);
    }

    public static String get(String key, boolean required) {
        final String environmentConfiguration = System.getenv(buildEnvironmentVariableKey(key));
        var candidateValue = !isNullOrBlank(environmentConfiguration) ? environmentConfiguration : getInstance().properties.getProperty(key);
        if (required && isNullOrBlank(candidateValue))
            throw new IllegalStateException(String.format("%s cannot be null or blank", key));
        return candidateValue;
    }

    private static String buildEnvironmentVariableKey(String key) {
        return key.replaceAll("\\.", "_")
            .replaceAll("-", "_")
            .toUpperCase();
    }

    public static Boolean getBoolean(String key, boolean defaultValue) {
        final String value = get(key);
        if (value != null && !value.equals("true") && !value.equals("false"))
            throw new IllegalArgumentException("value is not boolean: " + value);
        return value == null ? defaultValue : Boolean.valueOf(value);
    }

    public static Integer getInteger(String key) {
        final String value = get(key);
        return value == null ? null : Integer.valueOf(value);
    }

    public Integer getInnerInteger(String key) {
        String value = System.getenv(buildEnvironmentVariableKey(key));
        value = !isNullOrBlank(value) ? value : properties.getProperty(key);
        return value == null ? null : Integer.valueOf(value);
    }

    private List<String> getStringList(String key) {
        // this method is invoked from constructor, so cannot use public `get`
        final String environmentConfiguration = System.getenv(buildEnvironmentVariableKey(key));
        final String value = !isNullOrBlank(environmentConfiguration) ? environmentConfiguration : properties.getProperty(key);
        return Optional.of(value)
            .map(v -> Arrays.asList(v.split(",")))
            .orElse(Collections.emptyList());
    }

    public static String getMongoConnectionUri() {
        return get("mongodb.uri");
    }

    public static String getMongoDatabaseName() {
        return get("mongodb.db");
    }

    public static List<String> getEditions() {
        return getInstance().editions;
    }

    public static String getCurrentEdition() {
        return getEditions().get(0);
    }

    public static int getRequiredVotesCount(String edition) {
        return getInstance().requiredVotesCount.get(edition);
    }
}
