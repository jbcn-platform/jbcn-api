package org.jbcn.server.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.vertx.core.json.Json;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.RoleSerializer;

import java.time.LocalDateTime;

public class JacksonConfiguration {

    public static ObjectMapper initializeJacksonMapper() {
        Json.mapper
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)
                .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerModule(new SimpleModule()
                        .addSerializer(LocalDateTime.class, new LocalDateTimeJsonSerializer())
                        .addSerializer(Role.class, new RoleSerializer())
                );
        return Json.mapper;
    }

}
