package org.jbcn.server.services;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.jackson.ObjectIdJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.jbcn.server.platform.RequiredForCreation;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class UrlRedirect implements Traceable {

    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private final String _id;

    @RequiredForCreation
    private final String url;
    @RequiredForCreation
    private final String name;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime createdDate;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
    private final LocalDateTime lastUpdate;
    private String lastActionBy;

    @JsonCreator
    public UrlRedirect(
        @JsonProperty("url") String url,
        @JsonProperty("name") String name) {
        this.url = url;
        this.name = name;
        this._id = null;
        this.createdDate = this.lastUpdate = null;
    }
}
