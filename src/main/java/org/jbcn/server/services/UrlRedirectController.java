package org.jbcn.server.services;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.platform.CrudMongoHandler;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.utils.ResponseUtils;

import static org.jbcn.server.Constants.ID_INVALID;

public class UrlRedirectController {

    private static final Logger logger = LoggerFactory.getLogger(UrlRedirectController.class);

    private final Vertx vertx;
    private final String queue;

    public UrlRedirectController(Vertx vertx) {
        this.vertx = vertx;
        this.queue = DatabaseHandler.getQueueName(UrlRedirect.class);
    }

    public void get(RoutingContext context) {
        final DeliveryOptions action = new DeliveryOptions()
            .addHeader("action", CrudMongoHandler.listActionName(UrlRedirect.class));

        vertx.eventBus()
            .send(queue, new JsonObject(), action, reply -> {
                if (reply.succeeded()) {
                    final JsonArray redirects = (JsonArray) reply.result().body();
                    redirects.stream()
                        .filter(json -> ((JsonObject) json).getString("name").equals(context.request().getParam("name")))
                        .findFirst()
                        .ifPresentOrElse(
                            redirect -> context.response()
                                .setStatusCode(301)
                                .putHeader("Location", ((JsonObject) redirect).getString("url"))
                                .end(),
                            () -> ResponseUtils.notFound(context, ID_INVALID)
                        );
                } else {
                    logger.error("Save note Error", reply.cause());
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
    }
}
