package org.jbcn.server.platform;

import com.mongodb.async.client.FindIterable;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import lombok.Getter;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.utils.ModelValidator;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.platform.ObjectIdValidator.extractObjectId;

public abstract class DatabaseHandler extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseHandler.class);

    @Getter
    private final MongoDatabase mongoDatabase;
    private final String collectionName;

    @Getter
    protected MongoCollection<Document> collection;

    protected DatabaseHandler(MongoDatabase mongoDatabase, String collectionName) {
        this.mongoDatabase = mongoDatabase;
        this.collectionName = collectionName;
    }

    protected Future<Void> prepareDatabase() {
        final Future<Void> future = Future.future();
        try {
            this.collection = mongoDatabase.getCollection(collectionName);
            logger.info("Retrieved MongoDB collection: " + collectionName);
        } catch (Throwable e) {
            future.fail(e);
        }
        future.complete();
        return future;
    }

    public void replaceDocument(ObjectId oid, Document document, Message<JsonObject> message) {
        collection.replaceOne(
            Filters.eq("_id", oid),
            document,
            (updateResult, updateError) -> {
                if (updateError != null) {
                    logger.debug(updateError.getMessage(), updateError);
                    message.fail(Constants.UNEXPECTED_ERROR, updateError.getMessage());
                } else {
                    message.reply(new JsonObject());
                }
            });
    }

    public Future<JsonObject> updateDocument(ObjectId oid, List<Bson> updates) {
        return updateDocument(oid, updates, null);
    }

    public Future<JsonObject> updateDocument(ObjectId oid, List<Bson> updates, Handler<AsyncResult<JsonObject>> handler) {
        final Future<JsonObject> future = Future.future();
        if (handler != null)
            future.setHandler(handler);

        collection.updateOne(
            Filters.eq("_id", oid),
            Updates.combine(updates),
            (updateResult, updateError) -> {
                if (updateError != null) {
                    logger.error(updateError.getCause());
                    future.fail(updateError);
                } else {
                    if (updateResult.getModifiedCount() == 0) {
                        future.fail(Constants.ID_NOT_FOUND);
                    } else {
                        future.complete();
                    }
                }
            });
        return future;
    }

    public <T> void add(T instance, Handler<AsyncResult<JsonObject>> handler) {
        final Future<JsonObject> future = Future.future();
        future.setHandler(handler);

        final Document document = DocumentConverter.getDocument(instance);
        if (Traceable.class.isAssignableFrom(instance.getClass())) {
            final long timestamp = TimeUtils.currentTime();
            document.put(Traceable.CREATED_DATE, timestamp);
            document.put(LAST_UPDATE, timestamp);
            // TODO add lastActionBy (eventually creator and updater)
        }

        collection.insertOne(document, (result, error) -> {
            if (error != null) {
                logger.error(error.getCause());
                future.fail(error.getCause().getMessage());
            } else {
                future.complete(new JsonObject().put("_id", document.getObjectId("_id").toString()));
            }
        });
    }

    /**********************************************************
     * Generic methods directly reusable by most Handlers
     *********************************************************/

    /**
     * @param message
     * @param requiredFields list of names of required fields that must be present. Note that:
     *                       - Empty strings are not valid, but blank are
     *                       - Nested properties can be indicated as `parent.child1.child2`
     * @param clazz
     */
    protected <T> void add(Message<JsonObject> message, List<String> requiredFields, Class<T> clazz) {
        final JsonObject json = message.body();

        final List<String> errors = ModelValidator.checkRequiredFields(requiredFields, json);
        if (errors.size() > 0) {
            message.fail(Constants.BAD_REQUEST, errors.get(0));
        } else {
            add(DocumentConverter.parseToObject(message.body(), clazz), result -> {
                if (result.succeeded()) {
                    message.reply(result.result());
                } else {
                    message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
                }
            });
        }
    }

    protected void getById(Message<JsonObject> message) {
        getById(message, Constants.ID_NOT_FOUND);
    }

    protected void getById(Message<JsonObject> message, final String notFoundMessage) {

        final String id = message.body().getString("id");
        final JsonArray fields = message.body().getJsonArray("fields");

        extractObjectId(id, message)
            .ifPresent(oid -> {
                FindIterable<Document> queryBuilder = getCollection()
                    .find(eq("_id", oid));

                if (fields != null && !fields.isEmpty())
                    queryBuilder.projection(Projections.include(fields.getList()));

                queryBuilder
                    .first((doc, error) -> {
                        if (error != null) {
                            message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                        } else {
                            if (doc == null) {
                                message.fail(Constants.NOT_FOUND, notFoundMessage);
                            } else {
                                message.reply(DocumentConverter.toJson(doc));
                            }
                        }
                    });
            });
    }

    protected void getAll(Message<JsonObject> message) {
        getCollection()
            .find()
            .sort(Sorts.ascending(CREATED_DATE))
            .map(doc -> DocumentConverter.toJson(doc))
            .into(new ArrayList<>(), (result, error) -> message.reply(new JsonArray(result)));
    }

    protected void update(Message<JsonObject> message) {

        final JsonObject body = message.body();

        extractObjectId(message.body().getString("id"), message)
            .ifPresent(oid -> {
                final List<Bson> updates = new ArrayList<>();
                updates.add(Updates.set(LAST_UPDATE, TimeUtils.currentTime()));
                updates.add(Updates.set(LAST_ACTION_BY, body.getString(LAST_ACTION_BY)));

                updates.addAll(buildUpdates(body));

                updateDocument(oid, updates, result -> {
                    if (result.succeeded()) {
                        message.reply(new JsonObject().put("_id", oid.toString()));
                    } else {
                        final String errorMessage = result.cause().getMessage();
                        if (errorMessage.equals(Constants.ID_NOT_FOUND)) {
                            message.fail(Constants.NOT_FOUND, Constants.ID_NOT_FOUND);
                        } else {
                            message.fail(Constants.UNEXPECTED_ERROR, errorMessage);
                            message.fail(Constants.UNEXPECTED_ERROR, errorMessage);
                        }
                    }
                });
            });
    }

    protected void filter(Message<JsonObject> message) {

        final JsonObject page = message.body().getJsonObject("page");
        final Integer index = page != null ? page.getInteger("index", 0) : 0;
        final Integer size = page != null ? page.getInteger("size", 50) : 50;
        final String sortColumn = message.body().getString("sort", Traceable.CREATED_DATE);
        final Boolean asc = message.body().getBoolean("asc", Boolean.TRUE);

        final JsonObject body = message.body();

        // TODO fix issue when filters contains empty array or no `field` or `value` properties exist
        final List<Bson> filters = body.containsKey("filters") ? body.getJsonArray("filters")
            .stream()
            .map(filter -> Filters.eq(((JsonObject) filter).getString("field"), ((JsonObject) filter).getString("value")))
            .collect(Collectors.toList()) : null;

        logger.info("filters: " + filters);

        final Bson projection = body.containsKey("fields") ? Projections.include(body.getJsonArray("fields").getList()) : new Document();

        final Bson query = filters != null ? Filters.and(filters) : new Document();

        this.collection.countDocuments(query, (count, countError) -> {
            if (countError != null) {
                message.fail(Constants.UNEXPECTED_ERROR, countError.getMessage());
            } else {

                final List<JsonObject> filteredItems = new ArrayList<>();
                collection.find(query)
                    .sort(asc ? Sorts.ascending(sortColumn) : Sorts.descending(sortColumn))
                    .skip(index * size)
                    .limit(size)
                    .projection(projection)
                    .map(doc -> DocumentConverter.toJson(doc))
                    .into(filteredItems, (result, error) -> {
                        if (error != null) {
                            message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                        } else {
                            logger.info("Recovered items:" + filteredItems.size());
                            message.reply(new JsonObject()
                                .put("total", count)
                                .put("items", filteredItems));
                        }
                    });
            }
        });
    }

    /**
     * Return update operations for a given vertx JsonObject.
     */
    public abstract List<Bson> buildUpdates(JsonObject data);

    // NOTE: should not check when instances does not exist cause post-condition is the same
    protected void delete(Message<JsonObject> message) {
        extractObjectId(message.body().getString("id"), message)
            .ifPresent(oid -> getCollection()
                .deleteOne(eq("_id", oid), (result, error) -> {
                    if (error != null) {
                        message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                    } else {
                        message.reply(new JsonObject());
                    }
                }));
    }


    public static <T> String getQueueName(Class<T> entityClass) {
        return entityClass.getSimpleName().toUpperCase() + ":QUEUE";
    }
}
