package org.jbcn.server.platform;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import lombok.Getter;
import org.jbcn.server.Constants;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.SessionHelper;

import static org.jbcn.server.utils.RequestUtils.containsEmptyBody;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class GenericCrudController<T> {

    private static final Logger logger = LoggerFactory.getLogger(GenericCrudController.class);

    @Getter
    private final Vertx vertx;
    @Getter
    private final Class<T> entity;
    private final String actionBase;
    @Getter
    private final String queueName;


    public GenericCrudController(Vertx vertx, Class<T> entityClass) {
        this.vertx = vertx;
        this.entity = entityClass;
        this.actionBase = entityClass.getSimpleName().toLowerCase();
        this.queueName = getQueueName(entityClass);
    }

    public static String getQueueName(Class clazz) {
        return clazz.getSimpleName().toUpperCase() + ":QUEUE";
    }

    public String getAddActionName() {
        return actionBase + "-add";
    }

    public String getGetActionName() {
        return actionBase + "-get";
    }

    public String getGetAllActionName() {
        return actionBase + "-list";
    }

    public String getUpdateActionName() {
        return actionBase + "-update";
    }

    private String getUpsertActionName() {
        return actionBase + "-upsert";
    }

    public String getFilterActionName() {
        return actionBase + "-filter";
    }

    public String getDeleteActionName() {
        return actionBase + "-delete";
    }

    public void add(RoutingContext context) {
        if (containsEmptyBody(context)) {
            ResponseUtils.errorResult(context, Constants.BAD_REQUEST, Constants.BODY_REQUIRED);
            return;
        }

        // TODO find cohesive way to handle Traceable info. Since user info is in HTTP context, we need to do this here,
        //  but then, the date fields are set in the Handler -> it should be all in one place
        save(context,
            context.getBodyAsJson().put(Traceable.LAST_ACTION_BY, SessionHelper.getCurrentUsername(context).orElse(null)),
            getAddActionName());
    }

    public void update(RoutingContext context) {
        // TODO use _id
        final JsonObject data = new JsonObject(context.getBody())
            .put("id", extractId(context))
            .put(Traceable.LAST_ACTION_BY, SessionHelper.getCurrentUsername(context).orElse(null));

        save(context, data, getUpdateActionName());
    }

    public void upsert(RoutingContext context) {
        final JsonObject data = new JsonObject(context.getBody())
            .put(Traceable.LAST_ACTION_BY, SessionHelper.getCurrentUsername(context).orElse(null));

        save(context, data, getUpsertActionName());
    }

    public void get(RoutingContext context) {
        vertx.eventBus().send(queueName,
            new JsonObject().put("id", extractId(context)),
            new DeliveryOptions().addHeader("action", getGetActionName()),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.entityResponse(context, (JsonObject) reply.result().body());
                } else {
                    final ReplyException cause = (ReplyException) reply.cause();
                    ResponseUtils.errorResult(context, cause.failureCode(), cause.getMessage());
                }
            });
    }

    public void getAll(final RoutingContext context) {
        final DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", getGetAllActionName());
        vertx.eventBus().send(queueName, new JsonObject(), options, reply -> {
            ResponseUtils.searchResult(context, reply);
        });
    }

    public void delete(RoutingContext context) {
        vertx.eventBus().send(queueName,
            new JsonObject().put("id", extractId(context)),
            new DeliveryOptions().addHeader("action", getDeleteActionName()),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.simpleResult(context);
                } else {
                    ResponseUtils.errorResult(context, reply.cause().getMessage());
                }
            });
    }

    private String extractId(RoutingContext context) {
        final HttpServerRequest req = context.request();
        // TODO refactor all objects to use the same name (NOTE: new ones use _id)
        String candidate = req.getParam("_id");
        return isNullOrBlank(candidate) ? req.getParam("id") : candidate;
    }

    private void save(RoutingContext context, JsonObject inputJson, String actionName) {

        final DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", actionName);

        vertx.eventBus().send(queueName, inputJson, options, reply -> {
            if (reply.succeeded()) {
                final JsonObject body = (JsonObject) reply.result().body();
                if (actionName.equals(getUpdateActionName())) {
                    body.remove("_id");
                    body.remove("id");
                }
                if (body.containsKey("_id"))
                    ResponseUtils.dataResult(context, new JsonObject().put("_id", body.getString("_id")));
                else
                    ResponseUtils.simpleResult(context);
            } else {
                ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
            }
        });
    }

    public void filter(RoutingContext context) {
        vertx.eventBus().send(queueName,
            context.getBodyAsJson(),
            new DeliveryOptions().addHeader("action", getFilterActionName()),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.searchResult(context, reply);
                } else {
                    logger.error("Error filtering collection", reply.cause());
                    ResponseUtils.errorResult(context, reply.cause().getMessage());
                }
            });
    }
}
