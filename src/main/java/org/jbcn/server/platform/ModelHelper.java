package org.jbcn.server.platform;

import org.jbcn.server.model.Traceable;
import org.jbcn.server.utils.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Facilitates obtaining information from model from classes
 * that follow the API conventions. 
 */
public class ModelHelper {


    public List<String> getCreationRequiredFields(Class<? extends Traceable> clazz) {
        List result = new ArrayList();
        for (Field field : clazz.getDeclaredFields()) {
            if (!isSystemManagedField(field) && isRequired(field)) {
                result.add(field.getName());
            }
        }
        return result;
    }

    private boolean isRequired(Field field) {
        return field.getAnnotation(RequiredForCreation.class) != null;
    }

    /**
     * Can't use for Paper or Talk since they need to manually translate 'abstract' to 'paperAbstract'.
     */
    public List<Pair<String, Class>> getUpdatableFields(Class<? extends Traceable> clazz) {
        if (clazz == null)
            return Collections.emptyList();

        List result = new ArrayList();
        for (Field field : clazz.getDeclaredFields()) {
            if (!isSystemManagedField(field)) {
                result.add(Pair.of(field.getName(), field.getType()));
            }
        }
        return result;
    }

    private boolean isSystemManagedField(Field field) {
        final String fieldName = field.getName();
        return fieldName.equals("_id") || fieldName.equals("id") ||
            fieldName.equals(Traceable.CREATED_DATE) ||
            fieldName.equals(Traceable.LAST_UPDATE) ||
            fieldName.equals(Traceable.LAST_ACTION_BY);
    }

}
