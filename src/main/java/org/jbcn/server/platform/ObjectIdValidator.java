package org.jbcn.server.platform;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;

import java.util.Optional;

public class ObjectIdValidator {

    /**
     * Returns a valid ObjectId if the id is a valid oid, or else published a failed message + empty.
     */
    public static Optional<ObjectId> extractObjectId(String id, Message<JsonObject> message) {
        if (ObjectId.isValid(id)) {
            return Optional.of(new ObjectId(id));
        } else {
            message.fail(Constants.BAD_REQUEST, Constants.ID_INVALID);
            return Optional.empty();
        }
    }

}
