package org.jbcn.server.platform;

import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.conversions.Bson;
import org.jbcn.server.Constants;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.utils.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class CrudMongoHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(CrudMongoHandler.class);

    private final ModelHelper modelHelper = new ModelHelper();

    private final Class<? extends Traceable> entity;
    private final List<String> requiredFields;
    private final List<Pair<String, Class>> updateFields;

    // TODO maybe extract collectionName via annotation, but do on startup
    // TODO maybe extract updateFields via annotation, but do on startup
    public CrudMongoHandler(Class<? extends Traceable> clazz,
                            MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
        this.entity = clazz;
        // init these on start to improve runtime performance
        this.requiredFields = modelHelper.getCreationRequiredFields(clazz);
        this.updateFields = modelHelper.getUpdatableFields(clazz);
    }

    public void start(Future<Void> startFuture) {
        prepareDatabase()
            .setHandler(event -> startFuture.complete());
        vertx.eventBus().consumer(getQueueName(entity), this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        logger.info(this.getClass().getSimpleName() + " received action:" + action);

        if (action.equals(addActionName(entity))) {
            add(message, requiredFields(), entity);
        } else if (action.equals(getActionName(entity))) {
            getById(message);
        } else if (action.equals(listActionName(entity))) {
            getAll(message);
        } else if (action.equals(updateActionName(entity))) {
            update(message);
        } else if (action.equals(deleteActionName(entity))) {
            delete(message);
        } else {
            logger.warn("Unknown action:" + action);
            message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
        }
    }

    public static String addActionName(Class<? extends Traceable> entity) {
        return entity.getSimpleName().toLowerCase() + "-add";
    }

    public static String getActionName(Class<? extends Traceable> entity) {
        return entity.getSimpleName().toLowerCase() + "-get";
    }

    public static String updateActionName(Class<? extends Traceable> entity) {
        return entity.getSimpleName().toLowerCase() + "-update";
    }

    public static String deleteActionName(Class<? extends Traceable> entity) {
        return entity.getSimpleName().toLowerCase() + "-delete";
    }

    public static String listActionName(Class<? extends Traceable> entity) {
        return entity.getSimpleName().toLowerCase() + "-list";
    }

    public List<String> requiredFields() {
        return requiredFields;
    }

    // Assumes properties are called the same in json and persistence
    @Override
    public List<Bson> buildUpdates(JsonObject json) {
        final List<Bson> updates = new ArrayList<>();
        // TODO optimize with low level hack approach of int array with consumers
        // return pair(string, int) and each type processor is encoded in an array
        for (var property : updateFields) {
            final Class<?> type = property.getSecond();
            final String name = property.getFirst();
            if (type.equals(String.class)) {
                final String candidate = json.getString(name);
                if (!isNullOrBlank(candidate))
                    updates.add(Updates.set(name, candidate.trim()));
            }
            if (type.equals(Integer.class)) {
                final Integer aInteger = json.getInteger(name);
                if (aInteger != null)
                    updates.add(Updates.set(name, aInteger));
            }
            if (type.equals(Double.class)) {
                final Double aDouble = json.getDouble(name);
                if (aDouble != null)
                    updates.add(Updates.set(name, aDouble));
            }
            if (type.equals(Boolean.class)) {
                final Boolean aBoolean = json.getBoolean(name);
                if (aBoolean != null)
                    updates.add(Updates.set(name, aBoolean));
            }
            // complex types
            if (type.equals(Map.class)) {
                JsonObject stringsMap = json.getJsonObject(name);
                if (stringsMap != null) {
                    final Map<String, Object> validValues = stringsMap.getMap()
                        .entrySet()
                        .stream()
                        .filter(e -> e.getValue() instanceof String)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                    updates.add(Updates.set("links", validValues));
                }
            }
        }
        return updates;
    }
}
