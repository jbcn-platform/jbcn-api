package org.jbcn.server.persistence;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.jbcn.server.site.SiteUtils;
import org.jbcn.server.utils.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Filters data to return only public information.
 *
 * "public information" is that which does not contain personal
 * and sensible information (ie. emial).
 */
public class PublicDataProcessor {

    private Mappers mappers = new Mappers();

    public JsonArray getTalks(List<Talk> talks) {
        return talks
            .stream()
            .map(mappers::mapTalk)
            .collect(JsonArray::new, JsonArray::add, JsonArray::addAll);
    }

    public JsonObject findTalk(List<Talk> talks, String id) {
        for (Talk talk : talks) {
            if (talk.get_id().equals(id)) {
                return mappers.mapTalk(talk);
            }
        }
        return null;
    }

    public JsonArray getSpeakers(List<Talk> talks) {
        return flatmapSpeakers(talks)
            .stream()
            .map(mappers::mapSpeaker)
            .collect(JsonArray::new, JsonArray::add, JsonArray::addAll);
    }

    public JsonObject findSpeaker(List<Talk> talks, String speakerId) {
        for (Talk talk : talks) {
            for (Speaker speaker : talk.getSpeakers()) {
                if (speaker.getRef().equals(speakerId))
                    return mappers.mapSpeaker(speaker);
            }
        }
        return null;
    }

    private List<Speaker> flatmapSpeakers(List<Talk> talks) {
        List<Speaker> speakers = new ArrayList<>();
        Map<String, Speaker> speakerMap = new HashMap<>();
        for (Talk talk : talks) {
            for (Speaker speaker : talk.getSpeakers()) {
                String key = Speaker.calculateCode(speaker.getFullName(), speaker.getEmail());
                speakerMap.put(key, speaker);
            }
        }
        speakers.addAll(speakerMap.values());
        return speakers;
    }

    private final class Mappers {

        private JsonObject mapTalk(Talk talk) {
            return JsonUtils.removeNullValues(
                new JsonObject()
                    .put("id", talk.get_id())
                    .put("title", talk.getTitle())
                    .put("languages", talk.getLanguages())
                    .put("abstract", talk.getPaperAbstract())
                    .put("type", talk.getType() == null ? null : talk.getType().toLowerCase())
                    .put("tags", talk.getTags())
                    .put("level", talk.getLevel())
                    // .put("video", "")
                    // .put("scheduleId", "")
                    .put("speakers", talk.getSpeakers() == null ? null : talk.getSpeakers()
                        .stream()
                        .map(speaker -> Speaker.generateRef(speaker.getFullName(), speaker.getEmail()))
                        .collect(Collectors.toList())));
        }

        private JsonObject mapSpeaker(Speaker speaker) {
            final String updatedRef = Speaker.generateRef(speaker.getFullName(), speaker.getEmail());
            return JsonUtils.removeNullValues(
                /**
                 * `enabled` does not seem to be used, however the site code has references to it
                 */
                new JsonObject()
                    .put("enabled", "1")
                    .put("name", speaker.getFullName())
                    .put("company", speaker.getCompany())
                    .put("description", speaker.getJobTitle() == null ? "" : speaker.getJobTitle())
                    .put("biography", speaker.getBiography())
                    .put("image", SiteUtils.getImageUrl(speaker.getPicture()))
                    .put("ref", updatedRef)
                    .put("url", "infoSpeaker.html?ref=" + updatedRef)
                    .put("homepage", speaker.getWeb())
                    .put("twitter", speaker.getTwitter())
                    .put("linkedin", speaker.getLinkedin()));
        }
    }
}
