package org.jbcn.server.persistence;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.jbcn.server.model.codec.PaperStateCodec;
import org.jbcn.server.model.codec.TalkStateCodec;

import static org.jbcn.server.config.JbcnProperties.getMongoConnectionUri;
import static org.jbcn.server.config.JbcnProperties.getMongoDatabaseName;


public class MongodbConnector {

    private static final Logger logger = LoggerFactory.getLogger(MongodbConnector.class);

    public static MongoDatabase initializeMongoDb() {
        final CodecRegistry codecRegistry = CodecRegistries.fromRegistries(MongoClients.getDefaultCodecRegistry(),
                CodecRegistries.fromCodecs(new PaperStateCodec(), new TalkStateCodec()));

        final String mongoConnectionUri = getMongoConnectionUri();
        final MongoClient mongoClient = MongoClients.create(mongoConnectionUri);
        final MongoDatabase mongoDatabase = mongoClient.getDatabase(getMongoDatabaseName()).withCodecRegistry(codecRegistry);
        logger.info("Connected to MongoDB instance '" + mongoConnectionUri + "'");
        return mongoDatabase;
    }
}
