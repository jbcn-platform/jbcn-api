package org.jbcn.server.persistence;

import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.utils.DocumentConverter;

import java.util.ArrayList;
import java.util.List;

import static org.jbcn.server.persistence.Collections.PAPER;

public class PaperRepository {

    private final MongoDatabase mongoDatabase;

    public PaperRepository(MongoDatabase mongoDatabase) {
        this.mongoDatabase = mongoDatabase;
    }

    public void findAllPapersBySender(String fullName, Handler<AsyncResult<List<Paper>>> handler) {
        final Future<List<Paper>> future = prepareFuture(handler);
        getCollection()
            .find(Filters.eq("senders.fullName", fullName))
            .map(it -> DocumentConverter.parseToObject(it, Paper.class))
            .into(new ArrayList<>(), (result, error) -> {
                if (error != null) {
                    future.fail(error);
                } else {
                    future.complete(result);
                }
            });
    }

    public void findAllByEdition(String edition, List<String> fields, Handler<AsyncResult<List<Paper>>> handler) {
        final Future<List<Paper>> future = prepareFuture(handler);
        getCollection()
            .find(Filters.eq("edition", edition))
            .projection(Projections.include(fields))
            .map(it -> DocumentConverter.parseToObject(it, Paper.class))
            .into(new ArrayList<>(), (result, error) -> {
                if (error != null) {
                    future.fail(error);
                } else {
                    future.complete(result);
                }
            });
    }

    public void countByEdition(String edition, Handler<AsyncResult<Long>> handler) {
        final Future<Long> future = prepareFuture(handler);
        getCollection()
            .countDocuments(Filters.eq("edition", edition), (count, error) -> {
                if (error != null) {
                    future.fail(error);
                } else {
                    future.complete(count);
                }
            });
    }

    private <T> Future<T> prepareFuture(Handler<AsyncResult<T>> handler) {
        final Future<T> future = Future.future();
        future.setHandler(handler);
        return future;
    }

    private MongoCollection<Document> getCollection() {
        return mongoDatabase.getCollection(PAPER);
    }
}
