package org.jbcn.server.persistence;

public class Collections {

    public static final String USER = "user";

    // core
    public static final String PAPER = "paper";
    public static final String TALK = "talk";
    public static final String NOTES = "notes";

    public static final String PUBLIC_VOTES = "public_votes";
    public static final String FAVOURITED_TALKS = "favourites";
    // Info of badges scanned by sponsors
    public static final String BADGES = "badges";
    // Scheduling
    public static final String ROOMS = "rooms";
    public static final String SESSIONS = "sessions";
    public static final String TRACKS = "tracks";
    // Sponsors
    public static final String SPONSORS = "sponsors";
    public static final String JOB_OFFERS = "job_offers";
    // talk votes from gluon mobile api
    public static final String ATTENDEES_VOTES = "attendees_votes";
    // Services
    public static final String URL_REDIRECTS = "url_redirects";
}
