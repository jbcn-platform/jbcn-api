package org.jbcn.server.persistence;

import com.mongodb.client.model.Filters;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jbcn.server.model.search.SearchConditions;
import org.jbcn.server.model.search.SearchFilter;
import org.jbcn.server.utils.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

public class MongoQueryBuilder {

    private final MongoSearchFilterFactory filterFactory;

    public MongoQueryBuilder(MongoSearchFilterFactory filterFactory) {
        this.filterFactory = filterFactory;
    }

    public Bson build(SearchConditions conditions, String edition) {
        if (conditions == null || conditions.isEmpty())
            return new Document();

        Bson allFilters = null;
        if (!CollectionUtils.isEmpty(conditions.getAll())) {
            List<Bson> filters = conditions.getAll()
                .stream()
                .map(filter -> filterFactory.getFilter(filter, edition))
                .collect(Collectors.toList());
            allFilters = Filters.and(filters);
        }

        Bson anyFilters = null;
        if (!CollectionUtils.isEmpty(conditions.getAny())) {
            List<Bson> filters = conditions.getAny()
                .stream()
                .map(filter -> filterFactory.getFilter(filter, edition))
                .collect(Collectors.toList());
            anyFilters = Filters.or(filters);
        }

        if (allFilters != null && anyFilters != null)
            return Filters.and(allFilters, anyFilters);
        if (allFilters != null)
            return allFilters;
        if (anyFilters != null)
            return anyFilters;

        return null;
    }

    public Bson prepareMongoQuery(JsonObject conditions, String edition) {
        if (conditions != null) {
            final List<SearchFilter> all = conditions.getJsonArray("all", new JsonArray()).stream()
                .map(it -> SearchFilter.of((JsonObject) it))
                .collect(Collectors.toList());
            final List<SearchFilter> any = conditions.getJsonArray("any", new JsonArray()).stream()
                .map(it -> SearchFilter.of((JsonObject) it))
                .collect(Collectors.toList());
            return build(new SearchConditions(all, any), edition);
        }
        return new Document();
    }
}
