package org.jbcn.server.persistence;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.jbcn.server.model.sponsor.Sponsor;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @deprecated No longer used. Kept while /badges api is not migrated. 
 */
@Deprecated
public class SponsorRepository {

    private final ObjectMapper mapper = new ObjectMapper();

    private List<Sponsor> sponsors;

    public SponsorRepository() {
        final InputStream jsonFile = getClass().getResourceAsStream("/json/sponsors.json");
        try {
            sponsors = mapper.readValue(jsonFile, new TypeReference<List<Sponsor>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void list(Handler<AsyncResult<List<Sponsor>>> handler) {
        final Future<List<Sponsor>> future = Future.future();
        future.setHandler(handler)
            .complete(sponsors);
    }

    public void get(String id, Handler<AsyncResult<Sponsor>> handler) {
        final Sponsor sponsorResult = sponsors.stream()
            .filter(sponsor -> sponsor.get_id().equals(id))
            .findFirst()
            .get();

        final Future<Sponsor> future = Future.future();
        future.setHandler(handler)
            .complete(sponsorResult);
    }
}
