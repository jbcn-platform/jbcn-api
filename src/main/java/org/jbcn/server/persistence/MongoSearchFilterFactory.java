package org.jbcn.server.persistence;

import com.mongodb.client.model.Filters;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.search.SearchFilter;

import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.*;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class MongoSearchFilterFactory {

    private static final Logger logger = LoggerFactory.getLogger(MongoSearchFilterFactory.class);

    public static final String NOT_SET = "(not_set)";

    private final TalkRepository talkRepository;


    public MongoSearchFilterFactory(TalkRepository talkRepository) {
        this.talkRepository = talkRepository;
    }

    // TODO implement proper error cases that do not cause NullPointerExceptions
    public Bson getFilter(SearchFilter filter, String edition) {
        logger.info("Processing search filter: " + filter.toString());


        final String searchKey = filter.getName();
        if (searchKey.contains(".")) {
            String[] parts = searchKey.split("\\.");
            // error
            if (parts.length != 2)
                return null;
            switch (parts[0]) {
                case "sender":
                case "speaker":
                    final String fieldName = parts[0] + "s." + parts[1];
                    return filter.getValue().equals(NOT_SET) ?
                        emptyStringFilter(fieldName) :
                        operandCondition(new SearchFilter(fieldName, filter.getValue(), filter.getOp()));
                default:
                    return null;
            }
        }

        switch (searchKey) {
            // deprecated
            case "term":
                final String pattern = filter.getValue().toString();
                return or(
                    containsCondition("title", pattern),
                    containsCondition("abstract", pattern),
                    containsCondition("comments", pattern),
                    containsCondition("senders.fullName", pattern),
                    containsCondition("senders.biography", pattern),
                    containsCondition("speakers.fullName", pattern),
                    containsCondition("speakers.biography", pattern)
                );
            case "sessionId":
            case "paperId":
            case "title":
            case "edition":
            case "level":
            case "type":
            case "comments":
            case "sponsor":
            case "abstract":
            case "paperAbstract":
            case "preferenceDay":
            case "paper":
            case "responsiblePerson":
            case "state":
            case "published":
                return filter.getValue().equals(NOT_SET) ? emptyStringFilter(searchKey) : operandCondition(filter);
            case "not-published-speakers":
                return buildNoSpeakerWithPublishedTalksFilter(edition);
            case "vote":
                final String username = ((JsonObject) filter.getValue()).getString("username");
                final Double vote = ((JsonObject) filter.getValue()).getDouble("vote");
                return buildVoteFilter(username, vote);
            case "tags":
                if (filter.getOp().equals(SearchFilter.CONTAINS))
                    return containsCondition(searchKey, (String) filter.getValue());
                else
                    return Filters.in("tags", filter.getValue());
            default:
                return null;
        }
    }

    private Bson operandCondition(SearchFilter filter) {
        if (filter.getOp().equals(SearchFilter.CONTAINS)) {
            return containsCondition(filter.getName(), filter.getValue().toString());
        }
        return eq(filter.getName(), filter.getValue());
    }

    private Bson containsCondition(String name, String value) {
        return Filters.regex(name, value, "i");
    }

    private Bson buildVoteFilter(String voteUsername, Double voteValue) {
        if (!isNullOrBlank(voteUsername) && voteValue != null)
            return elemMatch("votes", and(eq("username", voteUsername), eq("vote", voteValue)));
            // TODO conditions not tested
        else if (!isNullOrBlank(voteUsername) && voteValue == null)
            return eq("votes.username", voteUsername);
        else if (isNullOrBlank(voteUsername) && voteValue != null)
            return eq("votes.vote", voteValue);
        else
            throw new RuntimeException("At least 'username' or 'voter' must be informed");
    }

    private Bson emptyStringFilter(String searchKey) {
        return or(not(exists(searchKey)), eq(searchKey, null), eq(searchKey, ""));
    }

    /**
     * This filter removes all papers where a sender has a Talk accepted
     */
    private Bson buildNoSpeakerWithPublishedTalksFilter(String edition) {
        logger.info("Recovering noSpeaker published filter");
        final Future<Bson> future = Future.future();

        talkRepository.findAllSpeakers(edition, speakers -> {
            if (speakers.succeeded()) {
                final List<String> speakerCodes = speakers.result()
                    .stream()
                    .map(Speaker::getCode)
                    .distinct()
                    .collect(Collectors.toList());
                if (speakerCodes.isEmpty()) {
                    future.complete(new Document());
                } else {
                    final Bson filter = elemMatch("senders", not(in("code", speakerCodes)));
                    future.complete(filter);
                }
            } else {
                future.fail(speakers.cause());
            }
        });
        while (!future.isComplete()) ;
        return future.result();
    }
}
