package org.jbcn.server.persistence;

import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.jbcn.server.model.TalkState;
import org.jbcn.server.model.search.SearchConditions;
import org.jbcn.server.model.search.SearchFilter;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.persistence.Collections.TALK;

public class TalkRepository {

    private static final Logger logger = LoggerFactory.getLogger(TalkRepository.class);

    private final MongoDatabase mongoDatabase;

    private final MongoSearchFilterFactory filterFactory;
    private final MongoQueryBuilder queryBuilder;

    public TalkRepository(MongoDatabase mongoDatabase) {
        this.mongoDatabase = mongoDatabase;
        this.filterFactory = new MongoSearchFilterFactory(this);
        this.queryBuilder = new MongoQueryBuilder(filterFactory);
    }


    public void get(String id, Handler<AsyncResult<Talk>> handler) {
        final Future<Talk> future = Future.future();
        future.setHandler(handler);

        if (!ObjectId.isValid(id)) {
            future.fail(Constants.ID_INVALID);
        } else {
            getCollection()
                .find(Filters.eq("_id", new ObjectId(id)))
                .first((document, error) -> {
                    if (error != null) {
                        future.fail(error);
                    } else {
                        if (document == null) {
                            future.complete(null);
                        } else {
                            Talk talk = DocumentConverter.parseToObject(document, Talk.class);
                            // TODO check if this manual conversion is required
                            talk.setState(TalkState.toTalkState(document.getString("state")));
                            future.complete(talk);
                        }
                    }
                });
        }
    }

    /**
     * On success returns a JSON message with the new id (eg: {_id: 5c35e3d2aefd396ec8e7e9b1 })
     */
    public void save(Talk talk, Handler<AsyncResult<Void>> handler) {
        final JsonObject entries = JsonObject.mapFrom(talk);
        entries.remove("_id");
        logger.info("Adding Talk: " + entries.encode());

        final Future future = Future.future();
        future.setHandler(handler);

        final Document instance = Document.parse(entries.encode());
        getCollection()
            .insertOne(instance, (result, error) -> {
                if (error != null) {
                    logger.error("Talk save error.", error);
                    future.fail(error);
                } else {
                    future.complete(new JsonObject().put("_id", instance.getObjectId("_id").toString()));
                    logger.info("Talk saved:" + instance.toJson());
                }
            });
    }

    public List<Bson> getFilters(JsonArray filters) {
        final List<Bson> result = new ArrayList<>();
        if (filters != null) {
            for (int i = 0; i < filters.size(); i++) {
                JsonObject filter = filters.getJsonObject(i);
                final Bson criteria = filterFactory.getFilter(
                    SearchFilter.of(filter),
                    JbcnProperties.getCurrentEdition()
                );
                if (criteria != null) {
                    result.add(criteria);
                }
            }
        }
        return result;
    }

    public void search(int size, int page,
                       String sortColumn, boolean asc,
                       JsonObject conditions,
                       List<String> fields,
                       Handler<AsyncResult<JsonObject>> handler) {

        final Future<JsonObject> future = Future.future();
        future.setHandler(handler);

//        final List<Bson> mongoFilters = new ArrayList<>();
//        if (!StringUtils.isNullOrBlank(term)) {
//            mongoFilters.add(Filters.or(
//                // OK
//                Filters.regex("title", term, "i"),
//                Filters.regex("paperAbstract", term, "i"),
//                Filters.regex("comments", term, "i"),
//                Filters.regex("speakers.fullName", term, "i"),
//                Filters.regex("speakers.biography", term, "i")
//            ));
//        }

        // TODO support passing edition in Api request
        final String edition = JbcnProperties.getCurrentEdition();
        final Bson query = queryBuilder.prepareMongoQuery(conditions, edition);

        final Bson projection = (fields == null || fields.size() == 0) ? new Document() : Projections.include(fields);
        // final Bson query = mongoFilters.size() == 0 ? new Document() : Filters.and(mongoFilters);

        final JsonObject data = new JsonObject();
        // `count` is necessary in case limit is less than the real number of elements
        getCollection()
            .countDocuments(query, (count, error) -> {
                if (error != null) {
                    future.fail(error);
                } else {
                    data.put("total", count);
                    this.getCollection()
                        .find(query)
                        .sort(asc ? Sorts.ascending(sortColumn) : Sorts.descending(sortColumn))
                        .skip(page * size)
                        .limit(size)
                        .projection(projection)
                        .map(doc -> DocumentConverter.toJson(doc))
                        .into(new ArrayList<>(), (result, error2) -> {
                            if (error2 != null) {
                                logger.error("Error in search", error2);
                                future.fail(error);
                            } else {
                                data.put("items", result);
                                future.complete(data);
                            }
                        });
                }
            });
    }

    public void findByPublishedState(String edition, Boolean published, Handler<AsyncResult<List<Talk>>> handler) {

        final Future<List<Talk>> future = Future.future();
        future.setHandler(handler);

        getCollection()
            .find(Filters.and(Filters.eq("edition", edition), Filters.eq("published", published)))
            .sort(Sorts.ascending(CREATED_DATE))
            .map(doc -> DocumentConverter.parseToObject(doc, Talk.class))
            .into(new ArrayList<>(), (result, error) -> {
                if (error != null) {
                    error.printStackTrace();
                    future.fail(error);
                } else {
                    future.complete(result);
                }
            });
    }

    public void findAllSpeakers(String edition, Handler<AsyncResult<Collection<Speaker>>> handler) {
        final Future<Collection<Speaker>> future = Future.future();
        future.setHandler(handler);

        this.findAllByEdition(edition, talksResult -> {
            if (talksResult.succeeded()) {

                final Collection<Speaker> results = talksResult.result()
                    .stream()
                    .flatMap(talk -> talk.getSpeakers().stream())
                    .collect(Collectors.toMap(Speaker::getCode, identity(), (s1, s2) -> s2))
                    .values();

                future.complete(results);
            } else {
                future.fail(talksResult.cause());
            }
        });
    }

    public void findAllByEdition(String edition, Handler<AsyncResult<List<Talk>>> handler) {

        final Future<List<Talk>> future = Future.future();
        future.setHandler(handler);

        getCollection()
            .find(Filters.eq("edition", edition))
            .map(doc -> DocumentConverter.parseToObject(doc, Talk.class))
            .into(new ArrayList<>(), (result, error) -> {
                if (error != null) {
                    error.printStackTrace();
                    future.fail(error);
                } else {
                    future.complete(result);
                }
            });
    }

    private MongoCollection<Document> getCollection() {
        return mongoDatabase.getCollection(TALK);
    }
}
