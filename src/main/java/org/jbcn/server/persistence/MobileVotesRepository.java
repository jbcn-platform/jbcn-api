package org.jbcn.server.persistence;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.*;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;
import org.jbcn.server.config.JbcnProperties;

import java.util.*;
import java.util.stream.Collectors;

import static org.jbcn.server.persistence.Collections.PUBLIC_VOTES;

public class MobileVotesRepository {

    private static final String SCHEDULE_ID = "scheduleId";
    public static final String DEVICE_ID = "deviceId";
    public static final String VOTE = "vote";

    private final MongoDatabase mongoDatabase;

    public MobileVotesRepository(MongoDatabase mongoDatabase) {
        this.mongoDatabase = mongoDatabase;
    }

    // TODO gives bad value in case of more than one registry per schedule + device
    public void getAverageVote(String scheduleId, Handler<AsyncResult<Double>> handler) {
        final Future<Double> future = Future.future();
        future.setHandler(handler);

        getCollection()
                .aggregate(Arrays.asList(
                        Aggregates.match(Filters.eq(SCHEDULE_ID, scheduleId)),
                        Aggregates.group("_id", new BsonField("average", new BsonDocument("$avg", new BsonString("$vote"))))
                ))
                .first((result, t) -> {
                    if (t == null) {
                        future.complete(result.getDouble("average"));
                    } else {
                        future.fail(t);
                    }
                });
    }

    public void getVote(String scheduleId, String deviceId, Handler<AsyncResult<JsonObject>> handler) {
        final Future<JsonObject> future = Future.future();
        future.setHandler(handler);

        getCollection()
                .find(Filters.and(
                        Filters.eq(SCHEDULE_ID, scheduleId),
                        Filters.eq(DEVICE_ID, deviceId)))
                .sort(Sorts.descending("date"))
                .map(doc -> doc != null ? new JsonObject(doc.toJson()) : null)
                .first((result, error) -> {
                    if (error != null) {
                        error.printStackTrace();
                        future.fail(error);
                    } else {
                        future.complete(result);
                    }
                });
    }

    public void getVotesByScheduleId(String scheduleId, Handler<AsyncResult<List<JsonObject>>> handler) {
        final Future<List<JsonObject>> future = Future.future();
        future.setHandler(handler);

        getCollection()
                .find(Filters.eq(SCHEDULE_ID, scheduleId))
                .sort(Sorts.descending("date"))
                .map(doc -> new JsonObject(doc.toJson()))
                .into(new ArrayList<>(), (result, error) -> {
                    if (error != null) {
                        future.fail(error);
                    } else {
                        future.complete(filterDuplicatedValues(result));
                    }
                });
    }

    public void getPublicVotes(Handler<AsyncResult<List<JsonObject>>> handler) {

        final Future<List<JsonObject>> future = Future.future();
        future.setHandler(handler);

        getCollection()
                .find()
                .sort(Sorts.descending(SCHEDULE_ID, "date"))
                .map(doc -> new JsonObject(doc.toJson()))
                .into(new ArrayList<>(), (result, error) -> {
                    if (error != null) {
                        future.fail(error);
                    } else {
                        future.complete(filterDuplicatedValues(result));
                    }
                });
    }


    /**
     * Given it is possible under high stress to introduce more than 1 registry for the same schedule & device,
     * this method fixes it filtering duplicated values returning only the most recent.
     */
    private List<JsonObject> filterDuplicatedValues(ArrayList<JsonObject> result) {
        final Set<String> unique = new HashSet<>();
        return result.stream()
                .filter(d -> unique.add(d.getString(SCHEDULE_ID) + d.getString(DEVICE_ID)))
                .collect(Collectors.toList());
    }

    public void saveVote(JsonObject params, Handler<AsyncResult<Void>> handler) {

        final Future<Void> future = Future.future();
        future.setHandler(handler);

        final String scheduleId = params.getString(SCHEDULE_ID);
        final String deviceId = params.getString(DEVICE_ID);
        int vote = params.getInteger(VOTE);

        getVote(scheduleId, deviceId, (talkVoteResult) -> {
            if (talkVoteResult.succeeded()) {
                if (talkVoteResult.result() != null) {

                    getCollection().updateOne(
                            Filters.and(
                                    Filters.eq(SCHEDULE_ID, scheduleId),
                                    Filters.eq(DEVICE_ID, deviceId)),
                            Updates.combine(
                                    Updates.set(VOTE, vote),
                                    Updates.set("date", new Date())),
                            (result, t) -> {
                                if (t != null) {
                                    future.fail(t);
                                } else {
                                    future.complete();
                                }
                            });
                } else {
                    final Document document = new Document();
                    document.put(SCHEDULE_ID, scheduleId);
                    document.put(DEVICE_ID, deviceId);
                    document.put(VOTE, vote);
                    document.put("date", new Date());

                    getCollection().insertOne(document, (result, t) -> {
                        if (t != null) {
                            future.fail(t);
                        } else {
                            future.complete();
                        }
                    });
                }
            } else {
                future.fail(talkVoteResult.cause());
            }
        });
    }

    private MongoCollection<Document> getCollection() {
        return mongoDatabase.getCollection(PUBLIC_VOTES);
    }

}
