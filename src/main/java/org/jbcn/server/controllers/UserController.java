package org.jbcn.server.controllers;

import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.Constants.SEARCH_CONDITION_MANDATORY;
import static org.jbcn.server.Constants.BAD_REQUEST;
import static org.jbcn.server.utils.RequestUtils.containsEmptyBody;

public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private Vertx vertx;

    public UserController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void getUser(RoutingContext context) {
        logger.info("controller - get-user");

        final String id = context.request().getParam("id");
        logger.debug("Get user with id:" + id);

        vertx.eventBus().send(Constants.USER_QUEUE,
                new JsonObject().put("id", id),
                new DeliveryOptions().addHeader("action", "user-get"),
                reply -> {
                    if (reply.succeeded()) {
                        final JsonObject body = new JsonObject()
                                .put("status", TRUE)
                                .put("data", (JsonObject) reply.result().body());
                        logger.debug("body:" + body.toString());

                        context.response()
                                .putHeader(CONTENT_TYPE, "application/json")
                                .end(body.encode());
                    } else {
                        ResponseUtils.errorResult(context, reply.cause().getMessage());
                    }
                });
    }

    public void listUsers(final RoutingContext context) {
        logger.info("controller - get-users");
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "user-getAll");
        vertx.eventBus().send(Constants.USER_QUEUE, new JsonObject(), options, reply -> {
            ResponseUtils.searchResult(context, reply);
        });
    }

    public void search(final RoutingContext context) {
        final String bodyAsString = context.getBodyAsString();
        if (bodyAsString == null || bodyAsString.isEmpty()) {
            ResponseUtils.errorResult(context, BAD_REQUEST, SEARCH_CONDITION_MANDATORY);
            return;
        }

        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "user-search");

        JsonObject bodyAsJson;
        try {
            bodyAsJson = new JsonObject(bodyAsString);
        } catch (DecodeException e) {
            logger.error("Invalid json input", e);
            ResponseUtils.errorResult(context, 400, Constants.INVALID_JSON_FORMAT);
            return;
        }

        vertx.eventBus().send(Constants.USER_QUEUE, bodyAsJson, options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.dataResult(context, reply);
            } else {
                logger.error("Error in search users: ", reply.cause());
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    public void updateUser(final RoutingContext context) {
        final JsonObject userJson = new JsonObject(context.getBodyAsString())
                .put("_id", context.request().getParam("id"));
        this.saveUser(context, userJson, new DeliveryOptions().addHeader("action", "user-update"));
    }

    public void addUser(RoutingContext context) {

        if (containsEmptyBody(context)) {
            ResponseUtils.errorResult(context, Constants.BODY_REQUIRED);
            return;
        }

        final JsonObject userJson = new JsonObject(context.getBodyAsString());
        this.saveUser(context, userJson, new DeliveryOptions().addHeader("action", "user-add"));
    }

    public void saveUser(RoutingContext context, JsonObject userJson, DeliveryOptions options) {
        vertx.eventBus().send(Constants.USER_QUEUE, userJson, options, reply -> {
            if (reply.succeeded()) {
                final JsonObject json = (JsonObject) reply.result().body();
                final JsonObject body = new JsonObject()
                        .put("status", true);
                if (json.getString("message") != null)
                    body.put("message", json.getString("message"));
                if (json.getString("id") != null)
                    body.put("data", new JsonObject().put("id", json.getString("id")));
                ResponseUtils.sendBody(context, body, 200);
            } else {
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    public void deleteUser(RoutingContext context) {

        String id = context.request().getParam("id");
        if (id == null) {
            ResponseUtils.errorResult(context, Constants.UNKNOWN_USER_ERROR);
        } else {
            JsonObject params = new JsonObject().put("id", id);
            DeliveryOptions options = new DeliveryOptions().addHeader("action", "user-delete");
            vertx.eventBus().send(Constants.USER_QUEUE, params, options, reply -> {
                handleSimpleResponse(context, reply);
            });
        }
    }

    private void handleSimpleResponse(RoutingContext context, AsyncResult<Message<Object>> reply) {
        if (reply.succeeded()) {
            ResponseUtils.simpleResult(context, reply);
        } else {
            ResponseUtils.errorResult(context, reply.cause().getMessage());
        }
    }

    public void initUser(RoutingContext routingContext) {
        logger.debug("Init users");
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "user-init");
        vertx.eventBus().send(Constants.USER_QUEUE, new JsonObject(), options, reply -> {
            handleSimpleResponse(routingContext, reply);
        });
    }

    public void getVoters(RoutingContext context) {
        final JsonObject params = new JsonObject();
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "user-get-voters");
        vertx.eventBus().send(Constants.USER_QUEUE, params, options, reply -> {
            ResponseUtils.dataResult(context, reply);
        });
    }

}
