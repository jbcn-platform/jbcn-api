package org.jbcn.server.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.formatters.HtmlFormatter;
import org.jbcn.server.model.Role;
import org.jbcn.server.utils.AuthorizationUtils;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.jbcn.server.http.HttpOptions.isHtmlFormatEnabled;
import static org.jbcn.server.model.Paper.BASE_FIELD_NAMES;
import static org.jbcn.server.model.Traceable.LAST_ACTION_BY;
import static org.jbcn.server.utils.RequestUtils.containsEmptyBody;

public class PaperController {

    private static final Logger logger = LoggerFactory.getLogger(PaperController.class);

    private final HtmlFormatter htmlFormatter = new HtmlFormatter();

    private Vertx vertx;

    public PaperController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void putSponsorPaper(RoutingContext context) {

        // id & sponsor cannot be null because they come from URL param
        final String paperId = context.request().getParam("id");

        final String sponsorValue = context.request().getParam("sponsor");
        if (!sponsorValue.equals("true") && !sponsorValue.equals("false")) {
            ResponseUtils.badRequest(context, Constants.PAPER_SPONSOR_INVALID_FORMAT);
            return;
        }
        boolean sponsor = sponsorValue.equals("true");
        logger.debug("Setting sponsor as " + sponsor + " to paper " + paperId);

        final JsonObject params = new JsonObject()
            .put("id", paperId)
            .put("sponsor", sponsor);

        vertx.eventBus().send(Constants.PAPER_QUEUE,
            params,
            new DeliveryOptions().addHeader("action", "paper-sponsor"),
            reply -> {
                if (reply.succeeded()) {
                    context.response()
                        .putHeader(CONTENT_TYPE, "application/json")
                        .end(new JsonObject().put("status", true).encode());
                } else {
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
    }

    public void getFeedback(RoutingContext context) {
        final JsonObject params = new JsonObject()
            .put("id", context.request().getParam("id"));

        vertx.eventBus().send(Constants.PAPER_QUEUE,
            params,
            new DeliveryOptions().addHeader("action", "paper-feedback-get"),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.entityResponse(context, (JsonObject) reply.result().body());
                } else {
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
    }

    public void upsertFeedback(RoutingContext context) {
        final String paperId = context.request().getParam("id");

        if (containsEmptyBody(context)) {
            ResponseUtils.badRequest(context, Constants.BODY_REQUIRED);
            return;
        }

        final String text = context.getBodyAsJson().getString("text");
        if (StringUtils.isNullOrBlank(text)) {
            ResponseUtils.badRequest(context, Constants.FIELD_REQUIRED_IN_BODY + "feedback.text");
            return;
        }

        final JsonObject params = new JsonObject()
            .put("id", paperId)
            .put("text", text)
            .put(LAST_ACTION_BY, context.user().principal().getString("sub"));

        final String actionName = "paper-feedback-" + (context.request().method().equals(HttpMethod.POST) ? "add" : "update");
        vertx.eventBus().send(Constants.PAPER_QUEUE,
            params,
            new DeliveryOptions().addHeader("action", actionName),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.simpleResult(context);
                } else {
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
    }

    public void deleteFeedback(RoutingContext context) {
        final JsonObject params = new JsonObject()
            .put("id", context.request().getParam("id"));

        final String actionName = "paper-feedback-delete";
        vertx.eventBus().send(Constants.PAPER_QUEUE,
            params,
            new DeliveryOptions().addHeader("action", actionName),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.noContent(context);
                } else {
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
    }

    public void getPaper(RoutingContext context) {

        final User user = context.user();
        AuthorizationUtils.isAuthorized(user, Role.ADMIN)
            .compose(preAuth -> AuthorizationUtils.isAuthorized(preAuth, user, Role.VOTER))
            .setHandler(event -> {
                final String id = context.request().getParam("id");
                logger.debug("Get paper with id:" + id);

                vertx.eventBus().send(Constants.PAPER_QUEUE,
                    new JsonObject()
                        .put("id", id)
                        .put("fields", new JsonArray(getAuthorizedReadFields(event.result()))),
                    new DeliveryOptions()
                        .addHeader("action", "paper-get"),
                    reply -> {
                        if (reply.succeeded()) {
                            final JsonObject paperData = (JsonObject) reply.result().body();
                            if (isHtmlFormatEnabled(context)) {
                                format(paperData);
                            }

                            final JsonObject data = new JsonObject()
                                .put("instance", paperData);
                            final JsonObject body = new JsonObject()
                                .put("status", true)
                                .put("data", data);
                            logger.debug("body:" + body.encode());

                            context.response()
                                .putHeader(CONTENT_TYPE, "application/json")
                                .end(body.toBuffer());
                        } else {
                            final ReplyException cause = (ReplyException) reply.cause();
                            ResponseUtils.errorResult(context, cause.failureCode(), cause.getMessage());
                        }
                    });
            });
    }

    private void format(JsonObject item) {
        item.put("abstract", htmlFormatter.format(item.getString("abstract")));
        for (Object it : item.getJsonArray("senders")) {
            final var sender = (JsonObject) it;
            sender.put("biography", htmlFormatter.format(sender.getString("biography")));
        }
    }

    private List<String> getAuthorizedReadFields(Boolean isAdminOrVoter) {
        final List<String> fields = new ArrayList<>(BASE_FIELD_NAMES);
        if (isAdminOrVoter) {
            fields.add("votes");
            fields.add("votesCount");
            fields.add("averageVote");
        }
        return fields;
    }

    public void approvePaper(RoutingContext context) {

        if (containsEmptyBody(context)) {
            ResponseUtils.badRequest(context, Constants.BODY_REQUIRED);
            return;
        }

        final String id = context.request().getParam("id");

        final JsonObject user = context.getBodyAsJson();
        logger.debug("Get paper with id:" + id + " and user " + user.getString("username"));


        final JsonObject params = new JsonObject()
            .put("id", id)
            .put("user", user);

        vertx.eventBus().send(Constants.PAPER_QUEUE, params,
            new DeliveryOptions().addHeader("action", "paper-approve"),
            reply -> {
                if (reply.succeeded()) {
                    final JsonObject instance = new JsonObject()
                        .put("instance", (JsonObject) reply.result().body());
                    ResponseUtils.dataResult(context, instance);
                } else {
                    final ReplyException cause = (ReplyException) reply.cause();
                    ResponseUtils.errorResult(context, cause.failureCode(), cause.getMessage());
                }
            });
    }

    /**
     * Returns only information for speakers.
     * Used for jbcn-public-cfp for speakers.
     */
    public void getPaperState(RoutingContext context) {

        final String id = context.request().getParam("id");
        logger.debug("Get public paper with id:" + id);

        // NOTE: id cannot be null because it is in the URL.
        // if not present, the method is not mapped and automatic 404 is returned
        if (id == null) {
            ResponseUtils.notFound(context, Constants.UNKNOWN_PAPER_ERROR);
        } else {
            final DeliveryOptions deliveryOptions = new DeliveryOptions().addHeader("action", "paper-get");
            vertx.eventBus().send(Constants.PAPER_QUEUE, new JsonObject().put("id", id), deliveryOptions, reply -> {
                if (reply.succeeded()) {
                    final JsonObject paper = (JsonObject) reply.result().body();

                    vertx.eventBus().send(Constants.TALK_QUEUE,
                        new JsonObject()
                            .put("conditions", new JsonObject()
                                .put("all", new JsonArray()
                                    .add(new JsonObject()
                                        .put("name", "paperId")
                                        .put("value", paper.getString("_id")))))
                            .put("fields", Collections.singletonList("_id"))
                            .put("size", 1),
                        new DeliveryOptions().addHeader("action", "talk-search"),
                        talkSearch -> {
                            final JsonObject publicPaper = new JsonObject()
                                .put("title", paper.getString("title"))
                                .put("edition", paper.getString("edition"))
                                .put("senders", paper.getJsonArray("senders"))
                                .put("type", paper.getString("type"))
                                .put("level", paper.getString("level"))
                                .put("abstract", htmlFormatter.format(paper.getString("abstract")))
                                .put("state", paper.getString("state"))
                                .put("tags", paper.getJsonArray("tags"))
                                .put("preferenceDay", paper.getString("preferenceDay"))
                                .put("comments", paper.getString("comments"));

                            final JsonObject talkSearchResponse = (JsonObject) talkSearch.result().body();

                            if (talkSearchResponse.containsKey("total")) {
                                final Integer total = talkSearchResponse.getInteger("total");
                                logger.info("Matching talks for paper found: " + total);
                                if (total > 0) {
                                    publicPaper.put("talkId", talkSearchResponse
                                        .getJsonArray("items")
                                        .getJsonObject(0)
                                        .getString("_id"));
                                }
                            }
                            final JsonObject body = new JsonObject()
                                .put("status", true)
                                .put("data", new JsonObject().put("instance", publicPaper));


                            logger.debug("body:" + publicPaper.toString());
                            context.response()
                                .putHeader(CONTENT_TYPE, "application/json")
                                .setStatusCode(200)
                                .setStatusMessage("OK")
                                .end(body.toBuffer());

                        });
                } else {
                    String error = reply.cause().getMessage();
                    ResponseUtils.errorResult(context, error);
                }
            });
        }
    }

    public void getPapers(final RoutingContext context) {
        logger.debug("Get Papers");
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "paper-getAll");
        vertx.eventBus().send(Constants.PAPER_QUEUE, new JsonObject(), options, reply -> {
            ResponseUtils.searchResult(context, reply);
        });
    }

    public void searchPapers(final RoutingContext context) {
        logger.debug("Search Papers");
        final User user = context.user();
        AuthorizationUtils.isAuthorized(user, Role.ADMIN)
            .compose(preAuth -> AuthorizationUtils.isAuthorized(preAuth, user, Role.VOTER))
            .setHandler(event -> {
                final JsonObject params = context.getBodyAsJson()
                    .put("fields", new JsonArray(getAuthorizedReadFields(event.result())));
                final DeliveryOptions options = new DeliveryOptions().addHeader("action", "paper-search");
                vertx.eventBus().send(Constants.PAPER_QUEUE, params, options, reply -> {
                    if (reply.succeeded()) {
                        ResponseUtils.dataResult(context, reply);
                    } else {
                        logger.error("Error in search paper", reply.cause());
                        ResponseUtils.errorResult(context, reply.cause().getMessage());
                    }
                });
            });
    }

    public void senderStarred(RoutingContext context) {
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "sender-star");

        final JsonObject params = context.getBodyAsJson();
        vertx.eventBus().send(Constants.PAPER_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.simpleResult(context);
            } else {
                logger.error("Error starring sender", reply.cause());
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    public void updatePaper(final RoutingContext context) {

        if (containsEmptyBody(context)) {
            ResponseUtils.errorResult(context, Constants.BODY_REQUIRED);
            return;
        }

        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "paper-update");
        final JsonObject paperJson = context.getBodyAsJson()
            .put("_id", context.request().getParam("id"));
        this.savePaper(context, paperJson, options);
    }

    public void addPaper(final RoutingContext context) {
        if (logger.isDebugEnabled())
            logger.debug("Adding paper:" + context.getBodyAsString());
        this.savePaper(context, context.getBodyAsJson(), new DeliveryOptions().addHeader("action", "paper-add"));
    }

    private void savePaper(RoutingContext context, JsonObject paperJson, DeliveryOptions options) {
        logger.info("Saving paper");
        vertx.eventBus().send(Constants.PAPER_QUEUE, paperJson, options, reply -> {

            boolean succeeded = reply.succeeded();
            logger.info("Save paper. Success: " + (succeeded ? "yes" : ("no. error: " + reply.cause())));

            if (succeeded)
                ResponseUtils.simpleResult(context);
            else {
                final ReplyException cause = (ReplyException) reply.cause();
                ResponseUtils.errorResult(context, cause.failureCode(), cause.getMessage());
            }
        });
    }

    public void deletePaper(RoutingContext context) {
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "paper-delete");
        String id = context.request().getParam("id");
        if (id == null) {
            ResponseUtils.errorResult(context, Constants.UNKNOWN_PAPER_ERROR);
        } else {
            JsonObject params = new JsonObject();
            params.put("id", id);
            vertx.eventBus().send(Constants.PAPER_QUEUE, params, options, reply -> {
                ResponseUtils.simpleResult(context);
            });
        }
    }

    public void getAllSenders(RoutingContext context) {
        final JsonObject params = new JsonObject()
            .put("edition", JbcnProperties.getCurrentEdition());
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "sender-get-all-senders");
        vertx.eventBus().send(Constants.PAPER_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.searchResult(context, reply);
            }
        });
    }

}
