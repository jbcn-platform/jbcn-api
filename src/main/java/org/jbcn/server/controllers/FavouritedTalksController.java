package org.jbcn.server.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.platform.GenericCrudController;
import org.jbcn.server.model.votes.FavouritedTalk;
import org.jbcn.server.utils.ResponseUtils;

import java.util.Map;
import java.util.stream.Collectors;

public class FavouritedTalksController extends GenericCrudController<FavouritedTalk> {

    public FavouritedTalksController(Vertx vertx, Class<FavouritedTalk> entityClass) {
        super(vertx, entityClass);
    }


    @Override
    public void delete(RoutingContext context) {

        final JsonObject params = new JsonObject(context.request().params().entries()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

        getVertx().eventBus().send(getQueueName(),
                params,
                new DeliveryOptions().addHeader("action", getDeleteActionName()),
                reply -> {
                    if (reply.succeeded()) {
                        ResponseUtils.simpleResult(context);
                    } else {
                        ResponseUtils.errorResult(context, reply.cause().getMessage());
                    }
                });
    }

}
