package org.jbcn.server.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.SessionHelper;
import org.jbcn.server.utils.StringUtils;

import static org.jbcn.server.utils.RequestUtils.containsEmptyBody;


public class VoteController {

    private final Vertx vertx;

    public VoteController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void getPapersStream(RoutingContext context) {
        final HttpServerRequest request = context.request();
        final String sortField = request.getParam("sort");
        final Boolean asc = getBoolean(request.getParam("asc"));

        vertx.eventBus().send(Constants.PAPER_QUEUE,
            new JsonObject()
                .put("username", SessionHelper.getCurrentUsername(context).get())
                .put("sort", sortField)
                .put("asc", asc),
            new DeliveryOptions()
                .addHeader("action", "papers-not-voted"),
            reply -> {
                if (reply.succeeded()) {
                    final JsonObject response = (JsonObject) reply.result().body();
                    ResponseUtils.dataResult(context, response.getJsonArray("items"));
                } else {
                    ResponseUtils.errorResult(context, reply.cause().getMessage());
                }
            });
    }

    private Boolean getBoolean(String asc) {
        if (StringUtils.isNullOrBlank(asc))
            return null;
        else
            return Boolean.getBoolean(asc);
    }

    public void upsertVote(RoutingContext context) {
        if (containsEmptyBody(context)) {
            ResponseUtils.badRequest(context, Constants.BODY_REQUIRED);
        } else {
            doVote(context, SessionHelper.getCurrentUsername(context).get());
        }
    }

    private void doVote(RoutingContext context, String username) {

        vertx.eventBus().send(Constants.PAPER_QUEUE,
            context.getBodyAsJson().put("username", username),
            new DeliveryOptions().addHeader("action", "paper-vote"),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.simpleResult(context);
                } else {
                    final ReplyException cause = (ReplyException) reply.cause();
                    ResponseUtils.errorResult(context, cause.failureCode(), cause.getMessage());
                }
            });
    }
}
