package org.jbcn.server.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.SessionHelper;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.jbcn.server.handlers.NotesHandler.*;

public class NotesController {

    private static final Logger logger = LoggerFactory.getLogger(NotesController.class);

    private final Vertx vertx;

    public NotesController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void addNote(final RoutingContext context) {

        final JsonObject message = context.getBodyAsJson()
            .put("owner", SessionHelper.getCurrentUsername(context).get());
        if (logger.isDebugEnabled())
            logger.debug("Creating:" + message.encode());

        vertx.eventBus()
            .send(Constants.NOTE_QUEUE, message, new DeliveryOptions().addHeader("action", ADD_ACTION), reply -> {
                if (reply.succeeded()) {
                    logger.info("Returning result");
                    final JsonObject body = new JsonObject()
                        .put("status", true)
                        .put("data", new JsonObject().put("id", ((JsonObject) reply.result().body()).getString("id")));
                    context.response()
                        .setStatusCode(201)
                        .putHeader(CONTENT_TYPE, "application/json")
                        .end(body.toBuffer());
                } else {
                    logger.error("Save note Error", reply.cause());
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
    }

    public void getNote(RoutingContext context) {
        final HttpServerResponse response = context.response();
        response.putHeader(CONTENT_TYPE, "application/json");

        final String id = context.request().getParam("id");
        if (id == null) {
            ResponseUtils.errorResult(context, Constants.UNKNOWN_NOTE_ERROR);
        } else {
            DeliveryOptions options = new DeliveryOptions()
                .addHeader("action", GET_ACTION);
            vertx.eventBus().send(Constants.NOTE_QUEUE, new JsonObject().put("id", id), options, reply -> {
                if (reply.succeeded()) {
                    final JsonObject result = (JsonObject) reply.result().body();
                    result.put("id", result.getString("_id"));
                    result.remove("_id");

                    ResponseUtils.dataResult(context, result);
                } else {
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
                }
            });
        }
    }

    public void updateNote(final RoutingContext context) {
        final JsonObject note = new JsonObject(context.getBody())
            .put("id", context.request().getParam("id"))
            .put("principal", SessionHelper.getCurrentUsername(context).get());

        saveNote(context, note, new DeliveryOptions().addHeader("action", UPDATE_ACTION));
    }

    public void listNotes(final RoutingContext context) {
        final JsonObject filterParams = new JsonObject(context.request().params().entries()
            .stream()
            .map(e -> e.getKey().equals("_id") ? new AbstractMap.SimpleEntry<>("id", e.getValue()) : e)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

        if (filterParams.fieldNames().size() == 0) {
            ResponseUtils.badRequest(context, Constants.SEARCH_CONDITION_MANDATORY);
        } else {
            if (logger.isInfoEnabled())
                logger.info("Filter notes by: " + filterParams.encode());
            vertx.eventBus().send(Constants.NOTE_QUEUE, filterParams, new DeliveryOptions().addHeader("action", SEARCH_ACTION), reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.searchResult(context, (JsonArray) reply.result().body());
                } else {
                    logger.error("Error listing notes", reply.cause());
                    ResponseUtils.errorResult(context, reply.cause().getMessage());
                }
            });
        }
    }

    private void saveNote(RoutingContext context, JsonObject note, DeliveryOptions options) {
        vertx.eventBus().send(Constants.NOTE_QUEUE, note, options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.simpleResult(context);
            } else {
                ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
            }
        });
    }

    public void deleteNote(RoutingContext context) {
        logger.info("Deleting");
        DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", DELETE_ACTION);

        final String id = context.request().getParam("id");

        if (id == null || id.trim().isEmpty()) {
            ResponseUtils.errorResult(context, Constants.UNKNOWN_NOTE_ERROR);
        } else {
            final JsonObject params = new JsonObject()
                .put("id", id)
                .put("principal", SessionHelper.getCurrentUsername(context).get());

            vertx.eventBus().send(Constants.NOTE_QUEUE, params, options, reply -> {
                if (reply.succeeded())
                    ResponseUtils.simpleResult(context);
                else
                    ResponseUtils.errorResult(context, ((ReplyException) reply.cause()).failureCode(), reply.cause().getMessage());
            });
        }
    }
}
