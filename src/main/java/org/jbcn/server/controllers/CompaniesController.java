package org.jbcn.server.controllers;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.StringUtils;

/**
 * Companies related to papers & talks.
 *
 * Not sure how to expose this feature due to underlying model.
 * Making company and entity (with a resource) offers more options in the future, but makes hard to define paper/talk filter,
 * cause "type" it creates a confusion with talk/workshop.
 */
public class CompaniesController {

    private static final String ACTION_ROOT = "company";
    public static final String LIST_COMPANIES = ACTION_ROOT + "-search";

    private final Vertx vertx;

    public CompaniesController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void listCompanies(final RoutingContext context) {

        final String proposalType = context.request().params().get("proposalPhase");

        if (StringUtils.isNullOrBlank(proposalType)) {
            ResponseUtils.errorResult(context, Constants.BAD_REQUEST, Constants.SEARCH_CONDITION_MANDATORY);
        } else {
            final JsonObject filterParams = new JsonObject()
                .put("proposalPhase", proposalType);

            final Handler<AsyncResult<Message<Object>>> listHandler = getAsyncResultHandler(context);
            final DeliveryOptions options = new DeliveryOptions()
                .addHeader("action", LIST_COMPANIES);
            switch (proposalType) {
                case "paper":
                    vertx.eventBus().send(Constants.PAPER_QUEUE, filterParams, options, listHandler);
                    break;
                case "talk":
                    vertx.eventBus().send(Constants.TALK_QUEUE, filterParams, options, listHandler);
                    break;
                default:
                    ResponseUtils.errorResult(context, Constants.BAD_REQUEST, Constants.COMPANY_INVALID_TYPE);
            }
        }
    }

    private Handler<AsyncResult<Message<Object>>> getAsyncResultHandler(RoutingContext context) {
        return reply -> {
            if (reply.succeeded()) {
                ResponseUtils.searchResult(context, reply);
            } else {
                ResponseUtils.errorResult(context, Constants.UNEXPECTED_ERROR, reply.cause().getMessage());
            }
        };
    }
}
