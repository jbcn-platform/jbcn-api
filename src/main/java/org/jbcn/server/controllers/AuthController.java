package org.jbcn.server.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.StringUtils;

import java.util.List;

import static org.jbcn.server.Constants.FIELD_REQUIRED_IN_URL_PARAM;
import static org.jbcn.server.Constants.FIELD_SINGLE_VALUE_IN_URL_PARAM;

public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private static final int BAD_REQUEST = 400;

    private Vertx vertx;

    public AuthController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void login(RoutingContext context) {
        final JsonObject payload = context.getBodyAsJson();
        final String username = payload.getString("username");
        final String password = payload.getString("password");

        if (StringUtils.isNullOrBlank(username)) {
            ResponseUtils.errorResult(context, BAD_REQUEST, Constants.FIELD_REQUIRED_IN_BODY + "username");
            return;
        }
        if (StringUtils.isNullOrBlank(password)) {
            ResponseUtils.errorResult(context, BAD_REQUEST, Constants.FIELD_REQUIRED_IN_BODY + "password");
            return;
        }
        final JsonObject params = new JsonObject()
            .put("username", username)
            .put("password", password);
        vertx.eventBus().send(Constants.AUTH_QUEUE, params, new DeliveryOptions().addHeader("action", "login"), reply -> {
            if (reply.succeeded()) {
                final String token = ((JsonObject) reply.result().body()).getString("token");
                ResponseUtils.dataResult(context, new JsonObject().put("token", token));
            } else {
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    public void getOauth2Token(RoutingContext context) {
        final List<String> grantType = context.queryParam("grant_type");
        final List<String> username = context.queryParam("username");
        final List<String> password = context.queryParam("password");
        final List<String> clientId = context.queryParam("client_id");
//        final List<String> clientSecret = context.queryParam("client_secret");

        validateUrlParam(context, grantType, "grant_type");
        validateUrlParam(context, username, "username");
        validateUrlParam(context, password, "password");
        validateUrlParam(context, clientId, "client_id");

        final JsonObject params = new JsonObject()
            .put("username", username.get(0))
            .put("password", password.get(0));
        vertx.eventBus().send(Constants.AUTH_QUEUE, params, new DeliveryOptions().addHeader("action", "login"), reply -> {
            if (reply.succeeded()) {
                final JsonObject responseBody = (JsonObject) reply.result().body();
                ResponseUtils.sendBody(context,
                    new JsonObject()
                        .put("access_token", responseBody.getString("token"))
                        .put("token_type", responseBody.getString("tokenType"))
                        .put("expires_in", responseBody.getInteger("expiresIn")),
                    200
                );
            } else {
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    private void validateUrlParam(RoutingContext context, List<String> param, String paramName) {
        if (param.size() == 0)
            ResponseUtils.errorResult(context, BAD_REQUEST, FIELD_REQUIRED_IN_URL_PARAM + paramName);
        if (param.size() > 1)
            ResponseUtils.errorResult(context, BAD_REQUEST, FIELD_SINGLE_VALUE_IN_URL_PARAM + paramName);
        if (StringUtils.isNullOrBlank(param.get(0))) {
            ResponseUtils.errorResult(context, BAD_REQUEST, FIELD_REQUIRED_IN_URL_PARAM + paramName);
        }
    }


    public void checkSession(RoutingContext context) {
        ResponseUtils.simpleResult(context);
    }

    public void logout(RoutingContext context) {
        logger.info("logout!");
        context.clearUser();
        ResponseUtils.simpleResult(context);
    }
}
