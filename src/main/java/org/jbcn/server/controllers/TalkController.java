package org.jbcn.server.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.formatters.HtmlFormatter;
import org.jbcn.server.handlers.AttendeesVotesHandler;
import org.jbcn.server.utils.ResponseUtils;
import org.jbcn.server.utils.SessionHelper;

import java.util.Comparator;
import java.util.Optional;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.jbcn.server.Constants.BAD_REQUEST;
import static org.jbcn.server.Constants.SEARCH_CONDITION_MANDATORY;
import static org.jbcn.server.http.HttpOptions.isHtmlFormatEnabled;
import static org.jbcn.server.utils.RequestUtils.containsEmptyBody;

public class TalkController {

    private static final Logger logger = LoggerFactory.getLogger(TalkController.class);
    private static final String APPLICATION_JSON = "application/json";

    private final HtmlFormatter htmlFormatter = new HtmlFormatter();

    private final Vertx vertx;

    public TalkController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void updateTalk(final RoutingContext context) {
        if (containsEmptyBody(context)) {
            ResponseUtils.errorResult(context, Constants.BODY_REQUIRED);
            return;
        }

        final DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-update");
        final JsonObject talk = context.getBodyAsJson()
            .put("_id", context.request().getParam("id"));
        logger.info("Saving talk");
        vertx.eventBus().send(Constants.TALK_QUEUE, talk, options, reply -> {
            ResponseUtils.handleSimpleReply(context, reply);
        });
    }

    public void changeState(RoutingContext context) {

        if (containsEmptyBody(context)) {
            ResponseUtils.errorResult(context, Constants.BODY_REQUIRED);
            return;
        }

        JsonObject params = new JsonObject(context.getBodyAsString());
        final JsonObject stateParams = new JsonObject()
            .put("id", context.request().getParam("id"))
            .put("state", params.getValue("state") != null ? params.getValue("state").toString() : null)
            .put("username", SessionHelper.getCurrentUsername(context).get());

        logger.info("Updating talk state");
        final DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-change-state");
        vertx.eventBus().send(Constants.TALK_QUEUE, stateParams, options, reply -> {
            ResponseUtils.handleSimpleReply(context, reply);
        });
    }

    public void getPublishedTalks(RoutingContext context) {
        JsonObject params = paramsWithEdition();
        DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-get-talks-json");

        vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                JsonArray json = (JsonArray) reply.result().body();
                if (isHtmlFormatEnabled(context)) {
                    formatTalks(json);
                }
                context.response()
                    .putHeader(CONTENT_TYPE, APPLICATION_JSON)
                    .end(wrapItemsArray(json).toBuffer());
            }
        });
    }

    private void formatTalks(JsonArray items) {
        for (Object current : items) {
            final var item = (JsonObject) current;
            item.put("abstract", htmlFormatter.format(item.getString("abstract")));
        }
    }

    private void formatSpeakers(JsonArray items) {
        for (Object current : items) {
            final var item = (JsonObject) current;
            item.put("biography", htmlFormatter.format(item.getString("biography")));
        }
    }

    public void getPublishedTalk(RoutingContext context) {
        JsonObject params = paramsWithEdition()
            .put("id", context.request().getParam("id"));
        DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-get-talks-json");

        vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                JsonObject json = (JsonObject) reply.result().body();
                context.response()
                    .setStatusCode(200)
                    .putHeader(CONTENT_TYPE, APPLICATION_JSON)
                    .end(json.toBuffer());
            } else {
                ResponseUtils.errorResult(context, 404, Constants.ID_NOT_FOUND);
            }
        });
    }

    public void getPublishedSpeakers(RoutingContext context) {
        JsonObject params = paramsWithEdition();
        DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-get-speakers-json");

        vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                JsonArray json = (JsonArray) reply.result().body();
                if (isHtmlFormatEnabled(context)) {
                    formatSpeakers(json);
                }

                context.response()
                    .putHeader(CONTENT_TYPE, APPLICATION_JSON)
                    .end(wrapItemsArray(json).toBuffer());
            }
        });
    }

    public void getPublishedSpeaker(RoutingContext context) {
        JsonObject params = paramsWithEdition()
            .put("id", context.request().getParam("id"));
        DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-get-speakers-json");

        vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                JsonObject json = (JsonObject) reply.result().body();

                if (isHtmlFormatEnabled(context)) {
                    formatPublicSpeaker(json);
                }

                context.response()
                    .setStatusCode(200)
                    .putHeader(CONTENT_TYPE, APPLICATION_JSON)
                    .end(json.toBuffer());
            } else {
                ResponseUtils.errorResult(context, 404, Constants.ID_NOT_FOUND);
            }
        });
    }

    private void formatPublicSpeaker(JsonObject item) {
        item.put("biography", htmlFormatter.format(item.getString("biography")));
    }

    private JsonObject paramsWithEdition() {
        return new JsonObject()
            .put("edition", JbcnProperties.getCurrentEdition());
    }

    private JsonObject wrapItemsArray(JsonArray items) {
        return new JsonObject()
            .put("items", items);
    }

    public void getTalk(RoutingContext context) {

        final String id = context.request().getParam("id");
        logger.debug("Get talk with id:" + id);

        final HttpServerResponse response = context.response()
            .putHeader(CONTENT_TYPE, APPLICATION_JSON);

        if (id == null) {
            JsonObject body = new JsonObject()
                .put("status", false)
                .put("error", "error.talk.unknown_talk");
            response.end(body.encode());
        } else {
            JsonObject params = new JsonObject().put("id", id);
            DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-get");
            vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {

                if (reply.succeeded()) {
                    JsonObject talk = (JsonObject) reply.result().body();
                    final JsonObject body = new JsonObject()
                        .put("data", new JsonObject().put("instance", talk))
                        .put("status", true);
                    logger.debug("body:" + body.toString());
                    response.end(body.toBuffer());

                } else {
                    String error = reply.cause().getMessage();
                    ResponseUtils.errorResult(context, error);
                }

            });
        }
    }

    public void search(RoutingContext context) {
        final String bodyAsString = context.getBodyAsString();
        if (bodyAsString == null || bodyAsString.isEmpty()) {
            ResponseUtils.errorResult(context, BAD_REQUEST, SEARCH_CONDITION_MANDATORY);
            return;
        }

        JsonObject body;
        try {
            body = new JsonObject(bodyAsString);
        } catch (DecodeException e) {
            logger.error("Invalid json input", e);
            ResponseUtils.errorResult(context, 400, Constants.INVALID_JSON_FORMAT);
            return;
        }

        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-search");
        vertx.eventBus().send(Constants.TALK_QUEUE, body, options, reply -> {
            ResponseUtils.handleSearchReply(context, reply);
        });
    }

    public void publishTalk(RoutingContext context) {

        if (containsEmptyBody(context)) {
            ResponseUtils.errorResult(context, Constants.BODY_REQUIRED);
            return;
        }

        final JsonObject params = new JsonObject(context.getBodyAsString());
        final JsonObject stateParams = new JsonObject()
            .put("id", context.request().getParam("id"))
            .put("published", params.getBoolean("published"))
            .put("username", SessionHelper.getCurrentUsername(context).get());

        DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-publish");
        vertx.eventBus().send(Constants.TALK_QUEUE, stateParams, options, reply -> {
            ResponseUtils.handleSimpleReply(context, reply);
        });
    }

    public void upsertMobileVote(RoutingContext context) {

        Optional<Integer> vote = getIntegerParam(context, "vote");
        if (!vote.isPresent()) {
            ResponseUtils.errorResult(context, Constants.TALK_VOTE_INVALID_FORMAT);
        } else {

            final JsonObject params = new JsonObject()
                .put("scheduleId", context.request().getParam("scheduleId"))
                .put("deviceId", context.request().getParam("deviceId"))
                .put("vote", vote.get());
            DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-attendee-vote");
            vertx.eventBus().send(Constants.MOBILE_VOTE_QUEUE, params, options, reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.dataResult(context, reply);
                } else {
                    ResponseUtils.errorResult(context, reply.cause().getMessage());
                }
            });
        }
    }

    private Optional<Integer> getIntegerParam(RoutingContext context, String name) {
        try {
            return Optional.of(Integer.parseInt(context.request().getParam(name)));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    public void getMobileVote(RoutingContext context) {
        final JsonObject params = new JsonObject()
            .put("scheduleId", context.request().getParam("scheduleId"));
        final DeliveryOptions options = new DeliveryOptions()
            .addHeader("action", "talk-get-attendee-vote");
        vertx.eventBus().send(Constants.MOBILE_VOTE_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.dataResult(context, reply);
            } else {
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    public void getMobileVotes(RoutingContext context) {
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-get-attendee-votes");
        vertx.eventBus().send(Constants.MOBILE_VOTE_QUEUE, new JsonObject(), options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.csvResult(context, (String) reply.result().body());
            } else {
                ResponseUtils.errorResult(context, reply.cause().getMessage());
            }
        });
    }

    public void getAllSpeakers(RoutingContext context) {
        final JsonObject params = paramsWithEdition();
        final DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-get-all-speakers");
        vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {
            if (reply.succeeded()) {
                ResponseUtils.searchResult(context, reply);
            }
        });
    }

    public void delete(RoutingContext context) {

        vertx.eventBus().send(Constants.TALK_QUEUE,
            new JsonObject().put("id", context.request().getParam("id")),
            new DeliveryOptions().addHeader("action", "talk-delete"),
            reply -> {
                if (reply.succeeded()) {
                    ResponseUtils.simpleResult(context);
                } else {
                    ResponseUtils.errorResult(context, reply.cause().getMessage());
                }
            });
    }

    public void getAllTags(RoutingContext context) {
        final JsonObject params = paramsWithEdition();

        final DeliveryOptions talkOptions = new DeliveryOptions().addHeader("action", "talk-tags-count");

        vertx.eventBus().send(Constants.TALK_QUEUE, params, talkOptions, talkReply -> {
            if (talkReply.succeeded()) {
                final JsonObject talkTags = (JsonObject) talkReply.result().body();
                final JsonArray collect = talkTags
                    .stream()
                    .map(entry -> new JsonObject().put("value", entry.getKey()).put("count", entry.getValue()))
                    .sorted(Comparator.comparing(o -> o.getString("value")))
                    .collect(JsonArray::new, JsonArray::add, JsonArray::addAll);
                ResponseUtils.searchResult(context, collect);
            } else {
                talkReply.cause().printStackTrace();
                ResponseUtils.errorResult(context, talkReply.cause().getMessage());
            }
        });
    }

    public void getFeedback(RoutingContext context) {

        final String id = context.request().getParam("id");

        final HttpServerResponse response = context.response()
            .putHeader(CONTENT_TYPE, APPLICATION_JSON);

        if (id == null) {
            JsonObject body = new JsonObject()
                .put("status", false)
                .put("error", "error.talk.unknown_talk");
            response.end(body.encode());
        } else {

            DeliveryOptions options = new DeliveryOptions().addHeader("action", AttendeesVotesHandler.GET_FEEDBACK_ACTION);
            vertx.eventBus().send("ATTENDEEVOTE:QUEUE",
                new JsonObject().put("id", id),
                options,
                reply -> {
                    if (reply.succeeded()) {
                        final JsonObject feedback = (JsonObject) reply.result().body();
                        response.end(feedback.encode());

                    } else {
                        String error = reply.cause().getMessage();
                        ResponseUtils.errorResult(context, error);
                    }
                });
        }
    }

    public void combineVotes(RoutingContext context) {
        final DeliveryOptions talkOptions = new DeliveryOptions().addHeader("action", "talk-combine-votes");
        vertx.eventBus().send(Constants.TALK_QUEUE, new JsonObject(), talkOptions, messageReply -> {

            final String separator = ";";

            if (messageReply.succeeded()) {
                final JsonArray result = (JsonArray) messageReply.result().body();
                StringBuilder body = new StringBuilder()
                    .append("Title").append(separator)
                    .append("Speaker").append(separator)
                    .append("Average").append(separator)
                    .append("Count").append("\n");

                for (int i = 0; i < result.size(); i++) {
                    JsonObject vote = result.getJsonObject(i);
                    body.append(vote.getString("title")).append(separator);
                    body.append(vote.getString("speaker")).append(separator);
                    body.append(vote.getFloat("average")).append(separator);
                    body.append(vote.getInteger("count")).append("\n");
                }

                ResponseUtils.csvResult(context, body.toString());
            } else {
                messageReply.cause().printStackTrace();
                ResponseUtils.errorResult(context, messageReply.cause().getMessage());
            }
        });
    }
}
