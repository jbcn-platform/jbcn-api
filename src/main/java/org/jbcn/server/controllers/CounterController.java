package org.jbcn.server.controllers;


import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;


public class CounterController {

    private static final Logger logger = LoggerFactory.getLogger(CounterController.class);

    private final Vertx vertx;

    public CounterController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void countTalks(RoutingContext context) {
        logger.debug("Count");
        String edition = context.request().getParam("edition");
        JsonObject params = new JsonObject();
        params.put("edition", edition);
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "talk-count");
        vertx.eventBus().send(Constants.TALK_QUEUE, params, options, reply -> {
            ResponseUtils.dataResult(context, reply);
        });
    }

    public void countPapers(final RoutingContext context) {
        logger.debug("Count paper");
        String edition = context.request().getParam("edition");
        JsonObject params = new JsonObject();
        params.put("edition", edition);
        DeliveryOptions options = new DeliveryOptions().addHeader("action", "paper-count");
        vertx.eventBus().send(Constants.PAPER_QUEUE, params, options, reply -> {
            long count = (Long) reply.result().body();
            ResponseUtils.dataResult(context, new JsonObject().put("count", count));
        });
    }

    public void countTags(final RoutingContext context) {
        logger.debug("Count tags");
        final String edition = context.request().getParam("edition");
        final JsonObject params = new JsonObject()
                .put("edition", edition);
        DeliveryOptions paperOptions = new DeliveryOptions().addHeader("action", "paper-tags-count");
        DeliveryOptions talkOptions = new DeliveryOptions().addHeader("action", "talk-tags-count");
        vertx.eventBus().send(Constants.PAPER_QUEUE, params, paperOptions, paperReply -> {
            if (paperReply.succeeded()) {
                JsonObject paperTags = (JsonObject) paperReply.result().body();
                vertx.eventBus().send(Constants.TALK_QUEUE, params, talkOptions, talkReply -> {
                    if (talkReply.succeeded()) {
                        final JsonObject talkTags = (JsonObject) talkReply.result().body();
                        ResponseUtils.dataResult(context, calculateTags(paperTags, talkTags));
                    } else {
                        talkReply.cause().printStackTrace();
                        ResponseUtils.errorResult(context, talkReply.cause().getMessage());
                    }
                });
            } else {
                paperReply.cause().printStackTrace();
                ResponseUtils.errorResult(context, paperReply.cause().getMessage());
            }
        });
    }


    private JsonObject calculateTags(JsonObject paperTags, JsonObject talkTags) {

        final JsonObject data = new JsonObject();
        for (String tag : paperTags.fieldNames()) {
            if (!data.containsKey(tag)) {
                data.put(tag, new JsonObject());
            }
            data.getJsonObject(tag).put("paper", paperTags.getInteger(tag));
        }

        for (String tag : talkTags.fieldNames()) {
            if (!data.containsKey(tag)) {
                data.put(tag, new JsonObject());
            }
            data.getJsonObject(tag).put("talk", talkTags.getInteger(tag));
        }

        return data;

    }
}
