package org.jbcn.server.email;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.mail.MailSender;
import org.jbcn.server.mail.TemplateProcessor;

import java.nio.file.Path;
import java.util.Map;

public class MailHandler extends AbstractVerticle {

    public static final String SEND_PAPER_FEEDBACK = "send-paper-feedback";
    public static final String QUEUE_NAME = "MAIL:QUEUE";

    // info@
    private String orgMail;
    private String headerImageUrl;
    private String footerImageUrl;

    private final TemplateProcessor templateProcessor;
    private final MailSender mailSender;

    public MailHandler(TemplateProcessor templateProcessor, MailSender mailSender) {
        this.templateProcessor = templateProcessor;
        this.mailSender = mailSender;
    }

    public void start(Future<Void> startFuture) {
        init();
        vertx.eventBus().consumer(QUEUE_NAME, this::onMessage);
    }

    public void init() {
        this.orgMail = JbcnProperties.get("mail.confirmation.from");
        this.headerImageUrl = JbcnProperties.get("mail.image.header.url");
        this.footerImageUrl = JbcnProperties.get("mail.image.footer.url");
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        switch (action) {
            case "paper-sent-message":
                sendEmailPaperReceived(message);
                break;
            case "paper-saved-backup":
                sendEmailPaperSavedBackup(message);
                break;
            case "talk-approved-message":
                sendTalkApproved(message);
                break;
            case SEND_PAPER_FEEDBACK:
                sendPaperFeedback(message);
                break;
        }
    }

    private void sendEmailPaperReceived(Message<JsonObject> message) {
        final JsonObject json = message.body();

        final String content = templateProcessor.render(
            Path.of("/paper_received_email.html"),
            Map.of(
                "edition", JbcnProperties.getCurrentEdition(),
                "username", json.getString("senderName"),
                "paper-title", json.getString("title"),
                "public-url", JbcnProperties.getPapersPublicUrL() + json.getString("paperId"),
                "header-image-url", headerImageUrl,
                "footer-image-url", footerImageUrl
            ));
        mailSender.sendEmail(
            json.getString("to"),
            orgMail,
            JbcnProperties.get("mail.confirmation.subject"),
            content,
            "text/html"
        );
        successfulResponse(message);
    }

    private void sendPaperFeedback(Message<JsonObject> message) {
        final JsonObject json = message.body();

        final String title = json.getString("title");
        final String content = templateProcessor.render(
            Path.of("/paper_feedback_email.html"),
            Map.of(
                "edition", JbcnProperties.getCurrentEdition(),
                "username", json.getString("senderName"),
                "paper-title", title,
                "paper-feedback", formatMailText(json.getString("feedback")),
                "public-url", JbcnProperties.getPapersPublicUrL() + json.getString("paperId"),
                "header-image-url", headerImageUrl,
                "footer-image-url", footerImageUrl
            ));
        mailSender.sendEmail(
            json.getString("to"),
            orgMail,
            "[JBCNConf] Your paper \"" + title + "\" could not be included in this JBCNConf " + JbcnProperties.getCurrentEdition(),
            content,
            "text/html"
        );
        successfulResponse(message);
    }

    private String formatMailText(String text) {
        return text.replaceAll("\n", "<br>");
    }

    private void sendEmailPaperSavedBackup(Message<JsonObject> message) {
        mailSender.sendEmail(
            orgMail,
            null,
            "[cfp-api] A paper has been received",
            message.body().encodePrettily(),
            "text/plain"
        );
        successfulResponse(message);
    }

    private void sendTalkApproved(Message<JsonObject> message) {
        final JsonObject json = message.body();
        final String title = json.getString("title");

        final String content = templateProcessor.render(
            Path.of("/talk_approved_email.html"),
            Map.of(
                "edition", JbcnProperties.getCurrentEdition(),
                "username", json.getString("senderName"),
                "talk-title", title,
                "header-image-url", headerImageUrl,
                "footer-image-url", footerImageUrl
            ));
        mailSender.sendEmail(
            json.getString("to"),
            orgMail,
            "[JBCNConf] Your paper \"" + title + "\" has been approved",
            content,
            "text/html"
        );
        successfulResponse(message);
    }

    private void successfulResponse(Message<JsonObject> message) {
        message.reply("ok");
    }
}
