package org.jbcn.server.email;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.jbcn.server.Constants;
import org.jbcn.server.utils.ResponseUtils;

import static org.jbcn.server.Constants.*;
import static org.jbcn.server.utils.ResponseUtils.badRequest;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class MailController {

    private static final Logger logger = LoggerFactory.getLogger(MailController.class);

    private final Vertx vertx;

    public MailController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void sendEmail(RoutingContext context) {
        final String emailType = context.request().getParam("type");

        if ("paper-feedback".equals(emailType)) {
            sendFeedbackMail(context.getBodyAsJson(), context);
        } else {
            ResponseUtils.notFound(context, EMAIL_TYPE_NOT_FOUND);
        }
    }

    private void sendFeedbackMail(JsonObject body, RoutingContext context) {

        final String paperId = body.getString("paper-id");

        vertx.eventBus()
            .send(Constants.PAPER_QUEUE,
                new JsonObject().put("id", paperId),
                options("paper-get"), reply -> {
                    if (reply.succeeded()) {
                        logger.info("Success");
                        final var paper = (JsonObject) reply.result().body();
                        final JsonArray senders = paper.getJsonArray("senders");
                        for (int i = 0; i < senders.size(); i++) {
                            sendFeedbackMail(paper, senders.getJsonObject(i), context);
                        }
                    } else {
                        badRequest(context, UNKNOWN_PAPER_ERROR);
                    }
                });
    }

    private void sendFeedbackMail(JsonObject paper, JsonObject sender, RoutingContext context) {
        final JsonObject feedback = paper.getJsonObject("feedback");
        if (feedback == null || isNullOrBlank(feedback.getString("text"))) {
            badRequest(context, EMAIL_EMPTY_FEEDBACK);
        }
        final String paperTitle = paper.getString("title");
        final var mailSendRequest = new JsonObject()
            .put("to", sender.getString("email"))
            .put("senderName", sender.getString("fullName"))
            .put("title", paperTitle)
            .put("feedback", feedback.getString("text"))
            .put("paperId", paper.getString("_id"));

        vertx.eventBus()
            .send(MailHandler.QUEUE_NAME, mailSendRequest, options(MailHandler.SEND_PAPER_FEEDBACK), reply -> {
                // TODO save mail confirmation in persistence
                final boolean succeeded = reply.succeeded();
                logger.info("Feedback mail for '" + paperTitle + "' successfully send? " + succeeded);
                ResponseUtils.created(context);
            });
    }

    private DeliveryOptions options(String value) {
        return new DeliveryOptions()
            .addHeader("action", value);
    }
}
