package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoCollection;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.bson.Document;
import org.jbcn.server.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.not;

/**
 * Mostly an utils class to avoid code duplication.
 */
public class CompaniesHandler {

    static void listCompanies(String fieldName,
                              MongoCollection<Document> collection,
                              Message<JsonObject> message) {
        final String returnPropertyName = "name";

        collection
            .distinct(fieldName, String.class)
            .filter(not(in(fieldName, "", null)))
            .map(value -> new JsonObject().put(returnPropertyName, value))
            .into(new ArrayList<>(), (result, error) -> {
                if (error == null) {
                    Collections.sort(result, Comparator.comparing(o -> o.getString(returnPropertyName)));
                    message.reply(new JsonArray(result));
                } else {
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                }
            });
    }

}
