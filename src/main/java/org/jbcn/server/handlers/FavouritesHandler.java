package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoDatabase;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.conversions.Bson;
import org.jbcn.server.Constants;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.model.votes.FavouritedTalk;
import org.jbcn.server.utils.DocumentConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static java.util.function.Function.identity;

public class FavouritesHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(FavouritesHandler.class);

    private static final String ACTION_ROOT = FavouritedTalk.class.getSimpleName().toLowerCase();
    public static final String ADD_ACTION = ACTION_ROOT + "-add";
    public static final String GET_ACTION = ACTION_ROOT + "-get";
    public static final String GET_ALL_ACTION = ACTION_ROOT + "-list";
    public static final String FILTER_ACTION = ACTION_ROOT + "-filter";
    public static final String DELETE_ACTION = ACTION_ROOT + "-delete";

    public FavouritesHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        prepareDatabase()
            .setHandler(event -> startFuture.complete());
        vertx.eventBus().consumer(getQueueName(FavouritedTalk.class), this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        logger.info(this.getClass().getSimpleName() + " received action:" + action);

        if (action.equals(ADD_ACTION)) {
            add(message, Arrays.asList("talkId", "user"), FavouritedTalk.class);
        } else if (action.equals(GET_ACTION)) {
            getById(message);
        } else if (action.equals(GET_ALL_ACTION)) {
            getAll(message);
        } else if (action.equals(FILTER_ACTION)) {
            filter(message);
        } else if (action.equals(DELETE_ACTION)) {
            delete(message);
        } else {
            logger.warn("Unknown action:" + action);
            message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
        }
    }

    // update is not supported for favoured talks
    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        return null;
    }

    @Override
    protected void getAll(Message<JsonObject> message) {
        getCollection()
            .find()
            .map(doc -> DocumentConverter.toJson(doc))
            .into(new ArrayList<>(), (result, error) -> {
                final Map<String, Integer> counters = result.stream()
                    .map(it -> it.getString("talkId"))
                    .distinct()
                    .collect(Collectors.toMap(identity(), it -> countOccurrences(it, result)));
                for (JsonObject v : result) {
                    v.put("talkFavs", counters.get(v.getString("talkId")));
                }
                message.reply(new JsonArray(result));
            });
    }

    /**
     * Count the number of elements that matck the talkId
     */
    private int countOccurrences(final String talkId, List<JsonObject> values) {
        int i = 0;
        for (JsonObject value : values)
            if (value.getString("talkId").equals(talkId)) {
                i++;
            }
        return i;
    }

    @Override
    protected void delete(Message<JsonObject> message) {
        if (message.body().containsKey("id")) {
            super.delete(message);
        } else {
            String talk = message.body().getString("talkId");
            String user = message.body().getString("user");

            getCollection()
                .deleteOne(and(eq("talkId", talk), eq("user", user)), (result, error) -> {
                    if (error != null) {
                        message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                    } else {
                        message.reply(new JsonObject());
                    }
                });
        }
    }
}
