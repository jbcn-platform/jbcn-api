package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoDatabase;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.conversions.Bson;
import org.jbcn.server.Constants;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.persistence.MobileVotesRepository;

import java.util.List;

public class MobileVoteHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(TalkHandler.class);

    private MobileVotesRepository votesRepository;

    public MobileVoteHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        this.prepareDatabase().setHandler((result) -> {
            votesRepository = new MobileVotesRepository(getMongoDatabase());
            startFuture.complete();
        });
        vertx.eventBus().consumer(Constants.MOBILE_VOTE_QUEUE, this::onMessage);
    }

    private void onMessage(Message<JsonObject> message) {
        String action = message.headers().get("action");
        logger.info("TalkHandler Received message:" + action);
        switch (action) {
            case "talk-attendee-vote":
                upsertVote(message);
                break;
            case "talk-get-attendee-vote":
                getTalkAttendeeVote(message);
                break;
            case "talk-get-attendee-votes":
                getTalkAttendeeVotes(message);
                break;
            default:
                logger.warn("Unknown action:" + action);
                message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
                break;
        }
    }

    private void upsertVote(Message<JsonObject> message) {
        final JsonObject params = message.body();

        votesRepository.saveVote(params, result -> {
            if (result.succeeded()) {
                votesRepository.getAverageVote(params.getString("scheduleId"), asyncResult -> {
                    if (asyncResult.succeeded()) {
                        message.reply(new JsonObject().put("average", asyncResult.result()));
                    } else {
                        message.fail(Constants.UNEXPECTED_ERROR, asyncResult.cause().getMessage());
                    }
                });
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void getTalkAttendeeVote(Message<JsonObject> message) {
        final JsonObject params = message.body();

        votesRepository.getAverageVote(params.getString("scheduleId"), asyncResult -> {
            if (asyncResult.succeeded()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.put("average", asyncResult.result());
                message.reply(jsonObject);
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, asyncResult.cause().getMessage());
            }
        });
    }

    private void getTalkAttendeeVotes(Message<JsonObject> message) {
        final StringBuilder csv = new StringBuilder();

        votesRepository.getPublicVotes(asyncResult -> {
            if (asyncResult.succeeded()) {
                asyncResult.result()
                    .stream()
                    .forEach(vote -> {
                        final String id = vote.getString("scheduleId");
                        csv.append("#");

                        switch (id.substring(0, 2)) {
                            case "ON":
                                csv.append("M");
                                break;
                            case "UE":
                                csv.append("T");
                                break;
                            case "ED":
                                csv.append("W");
                                break;
                            default:
                        }

                        csv.append(id)
                            .append(',')
                            .append(vote.getInteger("vote"))
                            .append("\n");
                    });
                message.reply(csv.toString());
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, asyncResult.cause().getMessage());
            }
        });
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        return null;
    }

}
