package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.conversions.Bson;
import org.jbcn.server.Constants;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.model.ScannedBadge;
import org.jbcn.server.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class BadgesHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(BadgesHandler.class);

    private static final Class<ScannedBadge> entityType = ScannedBadge.class;

    // TODO create shared helper class between handler an controller to manage queue names
    private static final String ACTION_ROOT = entityType.getSimpleName().toLowerCase();
    public static final String ADD_ACTION = ACTION_ROOT + "-add";
    public static final String GET_ACTION = ACTION_ROOT + "-get";
    public static final String GET_ALL_ACTION = ACTION_ROOT + "-list";
    public static final String UPDATE_ACTION = ACTION_ROOT + "-update";
    public static final String UPSERT_ACTION = ACTION_ROOT + "-upsert";
    public static final String FILTER_ACTION = ACTION_ROOT + "-filter";
    public static final String DELETE_ACTION = ACTION_ROOT + "-delete";

    public BadgesHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        prepareDatabase()
            .setHandler(event -> startFuture.complete());
        vertx.eventBus().consumer(getQueueName(entityType), this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        logger.info(this.getClass().getSimpleName() + " received action:" + action);

        if (action.equals(ADD_ACTION)) {
            if (!invalidFields(message)) {
                add(message);
            }
        } else if (action.equals(GET_ACTION)) {
            getById(message);
        } else if (action.equals(GET_ALL_ACTION)) {
            getAll(message);
        } else if (action.equals(UPDATE_ACTION)) {
            update(message);
        } else if (action.equals(UPSERT_ACTION)) {
            upsert(message);
        } else if (action.equals(FILTER_ACTION)) {
            filter(message);
        } else if (action.equals(DELETE_ACTION)) {
            delete(message);
        } else {
            logger.warn("Unknown action:" + action);
            message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
        }
    }

    private void add(Message<JsonObject> message) {
        add(message, Arrays.asList(
                "sponsor", "sponsor.id",
                "attendee", "attendee.email", "attendee.name"),
            ScannedBadge.class);
    }

    private boolean invalidFields(Message<JsonObject> message) {
        // TODO implement
//        if (message.body().containsKey("value")) {
//            Integer value = message.body().getInteger("value");
//            if (value != null && (value < 1 || value > 5)) {
//                message.fail(Constants.VALIDATE_ERROR, Constants.ATTENDEE_VOTE_INVALID_VALUE);
//                return true;
//            }
//        }
        return false;
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        final List<Bson> updates = new ArrayList<>();

        final String value = data.getString("details");
        if (value != null)
            updates.add(Updates.set("details", value));

        return updates;
    }

    private void upsert(Message<JsonObject> message) {
        final String id = message.body().getString("_id");
        if (StringUtils.isNullOrBlank(id)) {
            if (!invalidFields(message))
                add(message);
        } else {
            // TODO regularize to ensure no nasty conversions nee to be done
            //  -> DatabaseHandle expects "id" bc the first beans name it that way
            message.body()
                .put("id", id)
                .remove("_id");
            update(message);
        }
    }

}
