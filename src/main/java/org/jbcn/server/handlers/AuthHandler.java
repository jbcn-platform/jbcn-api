package org.jbcn.server.handlers;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.mongo.HashSaltStyle;
import io.vertx.ext.auth.mongo.HashStrategy;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.auth.mongo.impl.MongoUser;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.mongo.MongoClient;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;

import static org.jbcn.server.config.JbcnProperties.getMongoConnectionUri;
import static org.jbcn.server.config.JbcnProperties.getMongoDatabaseName;
import static org.jbcn.server.persistence.Collections.USER;

public class AuthHandler extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(AuthHandler.class);

    public static final String CREATE_USER_AUTH_DATA = "create-user-auth-data";

    private static final int TOKEN_EXPIRATION_TIME = 60 * 60 * 3;
    private static final int UNAUTHORIZED = 401;

    private MongoAuth mongoAuth;

    private final JWTAuth authProvider;

    public AuthHandler(JWTAuth authProvider) {
        this.authProvider = authProvider;
    }

    public void start(Future<Void> startFuture) {
        final String mongoConnectionUri = getMongoConnectionUri();

        mongoAuth = MongoAuth.create(MongoClient.createNonShared(vertx, new JsonObject()
                .put("connection_string", mongoConnectionUri)
                .put("db_name", getMongoDatabaseName())),
            new JsonObject()
                .put("collectionName", USER));
        mongoAuth.getHashStrategy().setSaltStyle(HashSaltStyle.COLUMN);

        logger.info("MongoDB Auth initialized");

        vertx.eventBus().consumer(Constants.AUTH_QUEUE, this::onMessage);
        startFuture.complete();
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        switch (action) {
            case "login":
                authenticate(message);
                break;
            case CREATE_USER_AUTH_DATA:
                createUserAuthData(message);
                break;
            default:
                message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
                break;
        }
    }

    private void createUserAuthData(Message<JsonObject> message) {

        final String username = message.body().getString("username");
        final String password = message.body().getString("password");

        final JsonObject userData = new JsonObject()
            .put(mongoAuth.getUsernameField(), username);

        final HashStrategy hashStrategy = mongoAuth.getHashStrategy();

        if (hashStrategy.getSaltStyle() == HashSaltStyle.COLUMN) {
            userData.put(mongoAuth.getSaltField(), DefaultHashStrategy.generateSalt());
        }

        // NOTE: do not use method with only 'username' it does not take into account salt in userData
        final MongoUser mongoUser = new MongoUser(userData, mongoAuth);
        userData
            .put(mongoAuth.getUsernameField(), username)
            .put(mongoAuth.getPasswordField(), hashStrategy.computeHash(password, mongoUser));

        message.reply(userData);
    }

    private void authenticate(Message<JsonObject> message) {
        final String usernameField = mongoAuth.getUsernameField();

        final String username = message.body().getString(usernameField);
        final JsonObject authInfo = new JsonObject()
            .put(usernameField, username)
            .put(mongoAuth.getPasswordField(), message.body().getString("password"));

        mongoAuth.authenticate(authInfo, res -> {
            if (res.succeeded()) {
                logger.info("User authenticated: " + username);
                final JsonArray roles = extractRoles(res.result());
                /* Possible custom block for Jbcn exclusive usage
                final JsonObject privateClaims = new JsonObject()
                        .put("jbcn", new JsonObject().put("roles", roles));
                 */
                final JWTOptions jwtOptions = new JWTOptions()
                    .setAlgorithm("RS256")
                    .setSubject(username)
                    .setExpiresInSeconds(TOKEN_EXPIRATION_TIME)
                    .setIssuer(JbcnProperties.get("jwt.issuer"));
                // add roles following required format for later vert.x validation
                // Vert.x uses different AbstractUser implementations during authentication from user/pass and JWT token.
                // The first requires the "role:" prefixes and validates with roles field in database.
                // The second, JWTAuthHandler processes the token and validates against "permissions" set in the JWT token.
                // So, in order to have a common model when using user.authorize(), roles should be prefixed
                roles.forEach(role -> jwtOptions.addPermission(MongoAuth.ROLE_PREFIX + role.toString()));

                final String token = authProvider.generateToken(new JsonObject(), jwtOptions);
                message.reply(new JsonObject()
                    .put("token", token)
                    .put("expiresIn", TOKEN_EXPIRATION_TIME)
                    .put("tokenType", "bearer"));
            } else {
                logger.info("Not Authenticated: " + res.cause().getMessage());
                message.fail(UNAUTHORIZED, res.cause().getMessage());
            }
        });
    }

    private JsonArray extractRoles(User user) {
        JsonArray roles = user.principal().getJsonArray("roles");
        return roles == null ? new JsonArray() : roles;
    }
}
