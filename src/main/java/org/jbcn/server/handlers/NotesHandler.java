package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;
import org.jbcn.server.model.Note;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.platform.DatabaseHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.jbcn.server.platform.ObjectIdValidator.extractObjectId;

public class NotesHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(NotesHandler.class);

    private static final String ACTION_ROOT = Note.class.getSimpleName().toLowerCase();
    public static final String ADD_ACTION = ACTION_ROOT + "-add";
    public static final String GET_ACTION = ACTION_ROOT + "-get";
    public static final String UPDATE_ACTION = ACTION_ROOT + "-update";
    public static final String SEARCH_ACTION = ACTION_ROOT + "-search";
    public static final String DELETE_ACTION = ACTION_ROOT + "-delete";

    public NotesHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        prepareDatabase()
            .setHandler(event -> startFuture.complete());
        vertx.eventBus().consumer(Constants.NOTE_QUEUE, this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        logger.info("NotesHandler received action:" + action);

        if (action.equals(ADD_ACTION)) {
            add(message);
        } else if (action.equals(GET_ACTION)) {
            getById(message, Constants.UNKNOWN_NOTE_ERROR);
        } else if (action.equals(UPDATE_ACTION)) {
            updateNote(message);
        } else if (action.equals(DELETE_ACTION)) {
            deleteNote(message);
        } else if (action.equals(SEARCH_ACTION)) {
            search(message);
        } else {
            logger.warn("Unknown action:" + action);
            message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
        }
    }

    private void add(Message<JsonObject> message) {
        final JsonObject jsonPaper = message.body();
        if (logger.isTraceEnabled())
            logger.info("Adding note:" + jsonPaper.toString());

        final String errorMessage = validateNote(jsonPaper);
        if (errorMessage != null) {
            message.fail(Constants.BAD_REQUEST, errorMessage);
        } else {
            final var note = new Note(
                jsonPaper.getString("owner"),
                jsonPaper.getString("paperId"),
                jsonPaper.getString("text"));
            super.add(note, result -> {
                if (result.succeeded()) {
//                    message.reply(result.result());
                    message.reply(new JsonObject().put("id", result.result().getString("_id")));
                } else {
                    message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
                }
            });
        }
    }

    private JsonObject bsonToJson(Document item) {
        return new JsonObject()
            .put("id", item.getObjectId("_id").toString())
            .put("owner", item.getString("owner"))
            .put("paperId", item.getString("paperId"))
            .put("text", item.getString("text"))
            .put(Traceable.CREATED_DATE, item.getLong(Traceable.CREATED_DATE))
            .put(Traceable.LAST_UPDATE, item.getLong(Traceable.LAST_UPDATE));
    }


    private void deleteNote(Message<JsonObject> message) {
        final ObjectId oid = new ObjectId(message.body().getString("id"));
        logger.info("Deleting note: " + oid);

        collection.find(Filters.eq("_id", oid)).first((result, t) -> {
            if (t != null) {
                message.fail(Constants.UNEXPECTED_ERROR, t.getCause().getMessage());
            } else if (result == null) {
                message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_NOTE_ERROR);
            } else if (!result.getString("owner").equals(message.body().getString("principal"))) {
                message.fail(Constants.PERMISSION_DENIED, Constants.NOTE_PERMISSION_ERROR);
            } else {
                collection.findOneAndDelete(Filters.eq("_id", oid), (item, deleteError) -> {
                    if (deleteError != null)
                        message.fail(Constants.UNEXPECTED_ERROR, deleteError.getMessage());
                    else
                        message.reply(new JsonObject());
                });
            }
        });
    }

    private void updateNote(Message<JsonObject> message) {
        final String id = message.body().getString("id");

        extractObjectId(id, message).ifPresent(oid -> {
            collection.find(Filters.eq("_id", oid)).first((result, t) -> {
                if (t != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, t.getCause().getMessage());
                } else if (result == null) {
                    message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_NOTE_ERROR);
                } else if (!result.getString("owner").equals(message.body().getString("principal"))) {
                    message.fail(Constants.PERMISSION_DENIED, Constants.NOTE_PERMISSION_ERROR);
                } else {
                    collection.updateOne(Filters.eq("_id", oid),
                        Updates.combine(
                            Updates.set("text", message.body().getString("text")),
                            Updates.set(Traceable.LAST_UPDATE, new Date().getTime())
                        ), (updateResult, updateError) -> {
                            if (updateError != null) {
                                message.fail(Constants.UNEXPECTED_ERROR, updateError.getMessage());
                            } else {
                                if (updateResult.getModifiedCount() == 0)
                                    message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_NOTE_ERROR);
                                message.reply(new JsonObject());
                            }
                        });
                }
            });
        });
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        return null;
    }

    private void search(Message<JsonObject> message) {

        final JsonObject body = message.body();
        final List<Bson> filters = body.fieldNames().stream()
            .map(field -> Filters.eq(field, body.getString(field)))
            .collect(Collectors.toList());

        final List<JsonObject> filteredItems = new ArrayList<>();
        collection.find(Filters.and(filters))
            .sort(Sorts.descending(Traceable.CREATED_DATE))
            .map(this::bsonToJson)
            .into(filteredItems, (result, error) -> {
                if (error != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                } else {
                    logger.info("Recovered items:" + filteredItems.size());
                    message.reply(new JsonArray(filteredItems));
                }
            });
    }

    private String validateNote(final JsonObject jsonNote) {

        if (jsonNote == null || jsonNote.encode().trim().isEmpty())
            return "error.note.missing";
        else if (isBlank(jsonNote, "owner"))
            return "error.note.owner_mandatory";
        if (isBlank(jsonNote, "paperId"))
            return "error.note.paperid_mandatory";
        if (isBlank(jsonNote, "text"))
            return "error.note.text_mandatory";

        return null;
    }

    private boolean isBlank(JsonObject paper, String key) {
        return !paper.containsKey(key) || paper.getString(key).trim().isEmpty();
    }
}
