package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.controllers.CompaniesController;
import org.jbcn.server.email.MailHandler;
import org.jbcn.server.model.*;
import org.jbcn.server.persistence.MongoQueryBuilder;
import org.jbcn.server.persistence.MongoSearchFilterFactory;
import org.jbcn.server.persistence.PaperRepository;
import org.jbcn.server.persistence.TalkRepository;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.jbcn.server.utils.StringUtils;
import org.jbcn.server.utils.TimeUtils;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.*;
import static java.lang.Boolean.FALSE;
import static org.jbcn.server.handlers.CompaniesHandler.listCompanies;
import static org.jbcn.server.model.PaperState.PAPER_STATE_SENT;
import static org.jbcn.server.model.PaperState.PAPER_STATE_VOTING;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.platform.ObjectIdValidator.extractObjectId;
import static org.jbcn.server.utils.CollectionUtils.*;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class PaperHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(PaperHandler.class);

    public static final Pattern EMAIL_REGEX =
        Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static final Pattern URL_REGEX =
        Pattern.compile("^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*", Pattern.CASE_INSENSITIVE);

    private MongoQueryBuilder queryBuilder;
    private MongoSearchFilterFactory filterFactory;
    private PaperRepository repository;

    public PaperHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {

        this.prepareDatabase().setHandler((result) -> {
            this.recalculateAll();
            repository = new PaperRepository(getMongoDatabase());
            filterFactory = new MongoSearchFilterFactory(new TalkRepository(getMongoDatabase()));
            queryBuilder = new MongoQueryBuilder(filterFactory);
            startFuture.complete();
        });

        vertx.eventBus().consumer(Constants.PAPER_QUEUE, this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        switch (action) {
            case "paper-getAll":
                listPapers(message);
                break;
            case "paper-get":
                getById(message, Constants.UNKNOWN_PAPER_ERROR);
                break;
            case "paper-search":
                searchPaper(message);
                break;
            case "paper-add":
                addPaper(message);
                break;
            case "paper-update":
                updatePaper(message);
                break;
            case "paper-delete":
                deletePaper(message);
                break;
            case "paper-vote":
                vote(message);
                break;
            case "papers-not-voted":
                getPapersNotVoted(message);
                break;
            case "paper-approve":
                approvePaper(message);
                break;
            case "sender-star":
                senderStarred(message);
                break;
            case "sender-get-all-senders":
                getSenders(message);
                break;
            case "paper-count":
                papersCount(message);
                break;
            case "paper-tags-count":
                paperTagsCount(message);
                break;
            case "paper-sponsor":
                paperSponsor(message);
                break;
            case "paper-feedback-add":
                addFeedback(message);
                break;
            case "paper-feedback-update":
                updateFeedback(message);
                break;
            case "paper-feedback-get":
                getFeedback(message);
                break;
            case "paper-feedback-delete":
                deleteFeedback(message);
                break;
            case CompaniesController.LIST_COMPANIES:
                listCompanies("senders.company", getCollection(), message);
                break;
            default:
                logger.warn("Unknown action:" + action);
                message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
                break;
        }
    }

    private void papersCount(Message<JsonObject> message) {
        JsonObject params = message.body();
        String edition = params.getString("edition");

        if (edition == null) {
            edition = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        }
        logger.info("Paper Count " + edition + " request");

        repository.countByEdition(edition, (papersResult) -> {
            if (papersResult.succeeded()) {
                Long count = papersResult.result();
                message.reply(count);
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, papersResult.cause().getMessage());
            }
        });
    }

    private void paperTagsCount(Message<JsonObject> message) {
        JsonObject params = message.body();
        String edition = params.getString("edition");

        repository.findAllByEdition(edition, List.of("_id", "tags"), papersResult -> {
            if (papersResult.succeeded()) {
                JsonObject data = new JsonObject();
                List<Paper> papers = papersResult.result();
                for (Paper paper : papers) {
                    for (String originTag : paper.getTags()) {
                        String tag = originTag.trim().toLowerCase();
                        if (!data.containsKey(tag)) {
                            data.put(tag, 0);
                        }
                        int value = data.getInteger(tag);
                        data.put(tag, value + 1);
                    }
                }
                message.reply(data);
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, papersResult.cause().getMessage());
            }
        });
    }

    private void senderStarred(Message<JsonObject> message) {
        final JsonObject params = message.body();
        final String senderName = params.getString("fullName");
        boolean starred = params.getBoolean("starred");

        repository.findAllPapersBySender(senderName, (papersResult) -> {
            if (papersResult.succeeded()) {
                if (papersResult.result().size() == 0)
                    logger.info("Found 0 papers for sender with name: " + senderName);

                final List<Future> futures = new ArrayList();
                for (Paper paper : papersResult.result()) {
                    final List<Document> updatedSenders = paper.getSenders().stream()
                        .map(sender -> {
                            sender.setStarred(starred);
                            return DocumentConverter.getDocument(sender);
                        })
                        .collect(Collectors.toList());

                    futures.add(updateDocument(
                        new ObjectId(paper.get_id()),
                        Arrays.asList(Updates.set("senders", updatedSenders))));
                }
                CompositeFuture.all(futures).setHandler(result -> {
                    if (result.succeeded())
                        message.reply(new JsonObject());
                    else
                        message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
                });
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, papersResult.cause().getMessage());
            }
        });
    }

    private void approvePaper(Message<JsonObject> message) {

        final String id = message.body().getString("id");
        logger.debug("Getting paper:" + id);

        extractObjectId(id, message).ifPresent(oid -> {

            final JsonObject user = message.body().getJsonObject("user");
            logger.debug("User:" + user);

            getCollection()
                .find(eq("_id", oid))
                .first((paperToApprove, error) -> {
                    if (error != null) {
                        message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                    } else {
                        if (paperToApprove == null) {
                            message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                        } else {
                            // Updates papers state
                            updateDocument(oid,
                                Arrays.asList(
                                    Updates.set("state", PaperState.PAPER_STATE_ACCEPTED),
                                    Updates.set(Traceable.LAST_UPDATE, new Date().getTime())
                                ),
                                result -> {
                                    if (result.succeeded()) {
                                        final JsonObject paperMsg = DocumentConverter.toJson(paperToApprove)
                                            .put("responsiblePerson", user);
                                        vertx.eventBus().send(Constants.TALK_QUEUE, paperMsg,
                                            new DeliveryOptions().addHeader("action", "talk-add"),
                                            reply -> {
                                                if (reply.succeeded()) {
                                                    message.reply(reply.result().body());
                                                } else {
                                                    logger.error("Error creating talk", reply.cause());
                                                    message.fail(Constants.UNEXPECTED_ERROR, reply.cause().getMessage());
                                                }
                                            });
                                    } else {
                                        final String errorMessage = result.cause().getMessage();
                                        if (errorMessage.equals(Constants.ID_NOT_FOUND)) {
                                            message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                                        } else {
                                            message.fail(Constants.UNEXPECTED_ERROR, errorMessage);
                                        }
                                    }
                                }
                            );

                        }
                    }
                });
        });
    }

    private void getPapersNotVoted(Message<JsonObject> message) {
        final JsonObject body = message.body();
        final String username = body.getString("username");
        final String sortField = body.getString("sort");
        final Boolean asc = body.getBoolean("asc");

        getCollection()
            .find(or(nor(exists("votes")), ne("votes.username", username)))
            .sort(sort(sortField, asc))
            .map(doc -> DocumentConverter.toJson(doc))
            .into(new ArrayList<>(), (result, error) -> {
                if (error != null) {
                    logger.error("Error recovering papers not voted by:" + username, error);
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                } else {
                    message.reply(new JsonObject().put("items", result));
                }
            });
    }

    private Bson sort(String sortField, Boolean asc) {
        if (asc == null || asc) {
            return StringUtils.isNullOrBlank(sortField)
                ? Sorts.ascending(CREATED_DATE)
                : Sorts.ascending(sortField);
        } else {
            return StringUtils.isNullOrBlank(sortField)
                ? Sorts.descending(CREATED_DATE)
                : Sorts.descending(sortField);
        }
    }

    private void listPapers(Message<JsonObject> message) {

        getCollection()
            .find()
            .map(doc -> new JsonObject(doc.toJson()))
            .into(new ArrayList<>(), (result, error) -> {
                if (error == null) {
                    logger.info("Recovered papers:" + result.size());
                    message.reply(new JsonArray(result));
                } else {
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                }
            });
    }

    private void searchPaper(Message<JsonObject> message) {

        final Integer page = message.body().getInteger("page", 0);
        final Integer size = message.body().getInteger("size", 50);
        final String sortColumn = message.body().getString("sort", "title");
        final Boolean asc = message.body().getBoolean("asc", Boolean.TRUE);

        // TODO test & document 'edition' usage. This is provisional -> edition should be input
        Object _edition = message.body().getValue("edition");
        final String edition = _edition == null ? JbcnProperties.getCurrentEdition() : _edition.toString();

        final JsonObject conditions = message.body().getJsonObject("conditions");
        final Bson mongoQuery = queryBuilder.prepareMongoQuery(conditions, edition);
        if (logger.isInfoEnabled()) {
            logger.info("MongoQuery: " + mongoQuery.toBsonDocument(Document.class, MongoClients.getDefaultCodecRegistry()).toString());
        }

        final Bson sort = asc ? Sorts.ascending(sortColumn) : Sorts.descending(sortColumn);
        getCollection()
            .countDocuments(mongoQuery, (count, error) -> {
                if (error != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                } else {
                    final JsonObject data = new JsonObject()
                        .put("votersCount", JbcnProperties.getRequiredVotesCount(JbcnProperties.getCurrentEdition()))
                        .put("total", count);
                    final JsonArray resultItems = new JsonArray();
                    getCollection()
                        .find(mongoQuery)
                        .projection(Projections.include(message.body().getJsonArray("fields").getList()))
                        .sort(sort)
                        .skip(page * size)
                        .limit(size)
                        .forEach((doc) -> {
                            resultItems.add(DocumentConverter.toJson(doc));
                        }, (result, error2) -> {
                            if (error2 != null) {
                                logger.error("Error in search", error2);
                                message.fail(Constants.UNEXPECTED_ERROR, error2.getMessage());
                            } else {
                                logger.info("Recovered " + resultItems.size() + " items");
                                data.put("items", resultItems);
                                message.reply(data);
                            }
                        });
                }
            });
    }

    private void paperSponsor(Message<JsonObject> message) {
        final JsonObject params = message.body();
        extractObjectId(params.getString("id"), message)
            .ifPresent(oid -> {
                updateDocument(oid,
                    Arrays.asList(
                        Updates.set("sponsor", params.getBoolean("sponsor")),
                        Updates.set(Traceable.LAST_UPDATE, new Date().getTime())
                    ),
                    result -> {
                        if (result.succeeded()) {
                            message.reply(new JsonObject());
                        } else {
                            final String errorMessage = result.cause().getMessage();
                            if (errorMessage.equals(Constants.ID_NOT_FOUND)) {
                                message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                            } else {
                                message.fail(Constants.UNEXPECTED_ERROR, errorMessage);
                            }
                        }
                    });
            });
    }

    private void addFeedback(Message<JsonObject> message) {
        final JsonObject params = message.body();
        extractObjectId(params.getString("id"), message)
            .ifPresent(oid -> {
                final long now = TimeUtils.currentTime();
                final Document bson = new Document()
                    .append("text", params.getString("text"))
                    .append(CREATED_DATE, now)
                    .append(LAST_UPDATE, now)
                    .append(LAST_ACTION_BY, params.getString(LAST_ACTION_BY));
                updateDocument(oid,
                    Arrays.asList(Updates.set("feedback", bson)),
                    result -> {
                        if (result.succeeded()) {
                            message.reply(new JsonObject());
                        } else {
                            final String errorMessage = result.cause().getMessage();
                            if (errorMessage.equals(Constants.ID_NOT_FOUND)) {
                                message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                            } else {
                                message.fail(Constants.UNEXPECTED_ERROR, errorMessage);
                            }
                        }
                    });
            });
    }

    private void updateFeedback(Message<JsonObject> message) {
        final JsonObject params = message.body();
        extractObjectId(params.getString("id"), message)
            .ifPresent(oid -> {
                final long now = TimeUtils.currentTime();
                updateDocument(oid,
                    Arrays.asList(
                        Updates.set("feedback.text", params.getString("text")),
                        Updates.set("feedback." + LAST_UPDATE, now),
                        Updates.set("feedback." + LAST_ACTION_BY, params.getString(LAST_ACTION_BY))
                    ),
                    result -> {
                        if (result.succeeded()) {
                            message.reply(new JsonObject());
                        } else {
                            final String errorMessage = result.cause().getMessage();
                            if (errorMessage.equals(Constants.ID_NOT_FOUND)) {
                                message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                            } else {
                                message.fail(Constants.UNEXPECTED_ERROR, errorMessage);
                            }
                        }
                    });
            });
    }

    private void getFeedback(Message<JsonObject> message) {
        final String id = message.body().getString("id");

        extractObjectId(id, message)
            .ifPresent(oid -> {
                getCollection()
                    .find(eq("_id", oid))
                    .projection(Projections.include("feedback"))
                    .first((doc, error) -> {
                        if (error != null) {
                            message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                        } else {
                            if (doc == null) {
                                message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                            } else {
                                final Document feedback = (Document) doc.get("feedback");
                                message.reply(new JsonObject()
                                    .put("text", feedback.getString("text"))
                                    .put(CREATED_DATE, feedback.getLong(CREATED_DATE))
                                    .put(LAST_UPDATE, feedback.getLong(CREATED_DATE))
                                    .put(LAST_ACTION_BY, feedback.getString(LAST_ACTION_BY)));
                            }
                        }
                    });
            });
    }

    private void deleteFeedback(Message<JsonObject> message) {
        // TODO create generic DeleteHandler for all deletes that creates audit trace
        // Code copied from DatabaseHandler
        extractObjectId(message.body().getString("id"), message)
            .ifPresent(oid -> getCollection()
                .deleteOne(eq("_id", oid), (result, error) -> {
                    if (error != null) {
                        message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                    } else {
                        message.reply(new JsonObject());
                    }
                }));
    }

    private String validatePaper(JsonObject paper) {

        if (!paper.containsKey("edition") || paper.getString("edition").isEmpty()) {
            return "error.paper.edition_mandatory";
        }

        if (!paper.containsKey("title") || paper.getString("title").isEmpty()) {
            return "error.paper.title_mandatory";
        }

        if (!paper.containsKey("level") || paper.getString("level").isEmpty()) {
            return "error.paper.level_mandatory";
        }

        if (!paper.containsKey("type") || paper.getString("type").isEmpty()) {
            return "error.paper.type_mandatory";
        }

        if (!paper.containsKey("abstract") || paper.getString("abstract").isEmpty()) {
            return "error.paper.abstract_mandatory";
        }

        if (!paper.containsKey("tags") || paper.getJsonArray("tags").isEmpty()) {
            return "error.paper.tags_mandatory";
        }

        if (!paper.containsKey("senders") || paper.getJsonArray("senders").size() < 1) {
            return "error.paper.senders_mandatory";
        }

        JsonArray senders = paper.getJsonArray("senders");
        for (int i = 0; i < senders.size(); i++) {
            String errorMessage = validateSender(senders.getJsonObject(i));
            // return as soon as an error is found
            if (errorMessage != null)
                return errorMessage;
        }
        return null;
    }

    private String validateSender(JsonObject sender) {
        if (!sender.containsKey("fullName") || sender.getString("fullName").isEmpty()) {
            return "error.paper.sender.fullname_mandatory";
        }
        if (!sender.containsKey("biography") || sender.getString("biography").isEmpty()) {
            return "error.paper.sender.biography_mandatory";
        }
        if (!sender.containsKey("email") || sender.getString("email").isEmpty()) {
            return "error.paper.sender.email_mandatory";
        }

        if (!EMAIL_REGEX.matcher(sender.getString("email")).find()) {
            return "error.paper.sender.email_bad_format";
        }

        if (!sender.containsKey("picture") || sender.getString("picture").isEmpty()) {
            return "error.paper.sender.picture_mandatory";
        }

        if (!URL_REGEX.matcher(sender.getString("picture")).find()) {
            return "error.paper.sender.picture_bad_format";
        }

        if (sender.containsKey("web") && !sender.getString("web").isEmpty()
            && !URL_REGEX.matcher(sender.getString("web")).find()) {
            return "error.paper.sender.web_bad_format";
        }
        return null;
    }

    private String validateSender(Sender sender) {
        if (isNullOrBlank(sender.getFullName())) {
            return "error.paper.sender.fullname_mandatory";
        }
        if (isNullOrBlank(sender.getBiography())) {
            return "error.paper.sender.biography_mandatory";
        }

        if (isNullOrBlank(sender.getEmail())) {
            return "error.paper.sender.email_mandatory";
        }
        if (!EMAIL_REGEX.matcher(sender.getEmail()).find()) {
            return "error.paper.sender.email_bad_format";
        }

        if (isNullOrBlank(sender.getPicture())) {
            return "error.paper.sender.picture_mandatory";
        }
        if (!URL_REGEX.matcher(sender.getPicture()).find()) {
            return "error.paper.sender.picture_bad_format";
        }

        if (sender.getWeb() != null && !sender.getWeb().isEmpty()
            && !URL_REGEX.matcher(sender.getWeb()).find()) {
            return "error.paper.sender.web_bad_format";
        }
        return null;
    }

    private void addPaper(Message<JsonObject> message) {
        logger.info("Adding Paper");
        final JsonObject jsonPaper = message.body();
        if (logger.isTraceEnabled())
            logger.info("Paper:" + jsonPaper.toString());

        final String validationMessage = validatePaper(jsonPaper);
        if (validationMessage != null) {
            message.fail(Constants.BAD_REQUEST, validationMessage);
        } else {
            final Date timestamp = new Date();
            final Paper paper = DocumentConverter.parseToObject(jsonPaper, Paper.class);
            paper.setCreatedDate(timestamp);
            paper.setLastUpdate(timestamp);
            paper.setState(PAPER_STATE_SENT);
            if (paper.getSponsor() == null)
                paper.setSponsor(FALSE);
            for (Sender s : paper.getSenders())
                s.setCode(Speaker.calculateCode(s.getFullName(), s.getEmail()));

            final Document instance = paper.toBsonDocument();

            this.collection.insertOne(instance, (result, error) -> {
                if (error != null) {
                    logger.error("Paper save error.", error);
                    message.fail(Constants.UNEXPECTED_ERROR, error.getCause().getMessage());
                } else {
                    final Codec<Document> codec = collection.getCodecRegistry().get(Document.class);

                    message.reply(new JsonObject());
                    logger.info("Paper saved:" + instance.toJson(codec));
                    final JsonObject paperJson = new JsonObject(instance.toJson(codec));
                    JsonArray senders = paperJson.getJsonArray("senders");
                    logger.info("paper result:" + paperJson.toString());
                    for (int i = 0; i < senders.size(); i++) {
                        final JsonObject sender = senders.getJsonObject(i);
                        logger.info("Sending email to: " + sender.getString("email"));
                        final JsonObject email = new JsonObject();
                        email.put("to", sender.getString("email"));
                        email.put("title", paperJson.getString("title"));
                        email.put("senderName", sender.getString("fullName"));
                        email.put("paperId", instance.getObjectId("_id").toString());

                        final DeliveryOptions mailOptions = new DeliveryOptions().addHeader("action", "paper-sent-message");
                        vertx.eventBus()
                            .send(MailHandler.QUEUE_NAME, email, mailOptions, reply -> {
                                logger.info("paper-sent-message mail successfully send? " + reply.succeeded());
                            });

                        final DeliveryOptions copyMailOptions = new DeliveryOptions().addHeader("action", "paper-saved-backup");
                        JsonObject clonedPaper = new JsonObject(paper.toBsonDocument().toJson(codec));
                        vertx.eventBus()
                            .send(MailHandler.QUEUE_NAME, clonedPaper, copyMailOptions, reply -> {
                                logger.info("paper-saved-backup mail successfully send? " + reply.succeeded());
                            });
                    }
                }
            });
        }
    }

    private void updatePaper(Message<JsonObject> message) {

        final JsonObject inputJson = message.body();
        logger.info("Updating inputJson: " + inputJson.toString());

        extractObjectId(inputJson.getString("_id"), message)
            .ifPresent(oid -> {

                final List<Bson> updates = new ArrayList<>();

                final Pair<String, Boolean> stateValidation = StringUtils.validate(inputJson.getString("state"),
                    PaperState::isValid,
                    v -> updates.add(Updates.set("state", v)));
                if (stateValidation.getSecond())
                    message.fail(400, Constants.PAPER_INVALID_STATE);

                final Pair<String, Boolean> typeValidation = StringUtils.validate(inputJson.getString("type"),
                    ProposalType::isValid,
                    v -> updates.add(Updates.set("type", v)));
                if (typeValidation.getSecond())
                    message.fail(400, Constants.PAPER_INVALID_TYPE);

                final Pair<String, Boolean> levelValidation = StringUtils.validate(inputJson.getString("level"),
                    ProposalLevel::isValid,
                    v -> updates.add(Updates.set("level", v)));
                if (levelValidation.getSecond())
                    message.fail(400, Constants.PAPER_INVALID_LEVEL);

                final Paper paper = DocumentConverter.parseToObject(inputJson, Paper.class);

                if (paper.getTitle() != null)
                    updates.add(Updates.set("title", paper.getTitle()));
                if (paper.getPaperAbstract() != null)
                    updates.add(Updates.set("abstract", paper.getPaperAbstract()));
                if (paper.getEdition() != null)
                    updates.add(Updates.set("edition", paper.getEdition()));
                if (paper.getSponsor() != null)
                    updates.add(Updates.set("sponsor", paper.getSponsor()));
                if (paper.getComments() != null)
                    updates.add(Updates.set("comments", paper.getComments()));
                if (!isNullOrBlank(paper.getPreferenceDay()))
                    updates.add(Updates.set("preferenceDay", paper.getPreferenceDay()));

                if (!isEmpty(paper.getTags()) && containsNotBlankValues(paper.getTags())) {
                    updates.add(Updates.set("tags", removeBlankValues(paper.getTags())));
                }

                if (!isEmpty(paper.getLanguages()) && containsNotBlankValues(paper.getLanguages())) {
                    updates.add(Updates.set("languages", removeBlankValues(paper.getLanguages())));
                }

                if (paper.getSenders() != null && !containsEmptySenders(paper.getSenders())) {
                    List<Document> senders = new ArrayList<>();
                    for (Sender sender : paper.getSenders()) {
                        String errorMessage = validateSender(sender);
                        if (errorMessage != null) {
                            message.fail(Constants.BAD_REQUEST, errorMessage);
                            return;
                        }
                        sender.setCode(Speaker.calculateCode(sender.getFullName(), sender.getEmail()));
                        senders.add(DocumentConverter.getDocument(sender));
                    }
                    updates.add(Updates.set("senders", senders));
                }

                if (updates.size() > 0) {
                    updates.add(Updates.set(Traceable.LAST_UPDATE, new Date().getTime()));
                    updateDocument(oid,
                        updates,
                        result -> {
                            if (result.succeeded()) {
                                message.reply(new JsonObject());
                            } else {
                                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
                            }
                        }
                    );
                } else {
                    message.reply(new JsonObject());
                }
            });
    }

    private boolean containsEmptySenders(List<Sender> senders) {
        if (senders.size() == 0)
            return true;

        for (Sender sender : senders) {
            if (isNullOrBlank(sender.getFullName()))
                return true;
        }

        return false;
    }

    private void deletePaper(Message<JsonObject> message) {
        String id = message.body().getString("id");
        if (id != null) {
            extractObjectId(id, message).ifPresent(oid -> {
                this.collection.deleteOne(eq("_id", oid), (result, error) -> {
                    if (error != null) {
                        message.fail(Constants.UNEXPECTED_ERROR, error.getCause().getMessage());
                    } else {
                        message.reply(new JsonObject());
                    }
                });
            });
        }
    }

    private void vote(Message<JsonObject> message) {
        final String id = message.body().getString("id");
        double voteValue = message.body().getDouble("vote");
        final String username = message.body().getString("username");

        Optional<ObjectId> objectId = extractObjectId(id, message);
        // FIXME should return message to avoid stack request
        if (!objectId.isPresent())
            return;

        final ObjectId oid = objectId.get();

        getCollection()
            .find(eq("_id", oid))
            .first((paperDoc, error) -> {
                if (error != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                } else {
                    if (paperDoc != null) {
                        logger.info("Paper doc found");

                        final List<Document> votes = paperDoc.get("votes", new ArrayList<>());

                        Document paperVote = votes.stream()
                            .filter(v -> v.getString("username").equals(username))
                            .findFirst()
                            .orElse(null);
                        if (paperVote == null) {
                            paperVote = new Document();
                            paperVote.put("username", username);
                            votes.add(paperVote);
                        }

                        long updateTimestamp = System.currentTimeMillis();
                        paperVote.put("vote", voteValue);
                        paperVote.put("date", updateTimestamp);

                        String state = paperDoc.getString("state");
                        if (PAPER_STATE_SENT.getState().equals(state)) {
                            state = PAPER_STATE_VOTING.getState();
                        }

                        updateDocument(oid, Arrays.asList(
                                Updates.set("votes", votes),
                                Updates.set("votesCount", votes.size()),
                                Updates.set("averageVote", calculateAverageVote(votes)),
                                Updates.set("state", state),
                                Updates.set(Traceable.LAST_UPDATE, updateTimestamp)),
                            result -> {
                                if (result.succeeded()) {
                                    message.reply(new JsonObject());
                                } else {
                                    message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
                                }
                            });
                    } else {
                        message.fail(Constants.NOT_FOUND, Constants.UNKNOWN_PAPER_ERROR);
                    }
                }
            });
    }

    private double calculateAverageVote(List<Document> votes) {

        double sum = 0.0;
        for (Document doc : votes) {
            logger.debug("Vote: " + doc.toJson());
            try {
                double vote = doc.getDouble("vote");
                sum += vote;
            } catch (ClassCastException e) {
                int vote = doc.getInteger("vote");
                sum += vote;
            }
        }

        if (sum > 0) {
            return Math.round((sum / JbcnProperties.getRequiredVotesCount(JbcnProperties.getCurrentEdition())) * 100.0) / 100.0;
        } else
            return 0.0d;
    }

    private void getSenders(Message<JsonObject> message) {
        getCollection()
            .find()
            .into(new ArrayList<>(), (result, error) -> {
                if (error != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                } else {
                    final JsonArray senders = result.stream()
                        .flatMap(p -> ((List<Document>) p.get("senders")).stream())
                        .map(p -> DocumentConverter.toJson(p))
                        .collect(JsonArray::new, JsonArray::add, JsonArray::addAll);
                    message.reply(removeDuplicatedByCode(senders));
                }
            });
    }

    private JsonArray removeDuplicatedByCode(JsonArray speakers) {
        final JsonArray res = new JsonArray();
        final Set<String> codes = new HashSet(speakers.size());
        for (Object speaker : speakers) {
            final String code = ((JsonObject) speaker).getString("code");
            if (!codes.contains(code)) {
                codes.add(code);
                res.add(speaker);
            }
        }
        return res;
    }

    public void recalculateAll() {
        // Disable this method to avoid flakes in tests given test data does not meet all conventions.
        final Boolean testModeEnabled = JbcnProperties.getBoolean("mode.test", false);
        if (testModeEnabled) return;

        final List<Document> papers = new ArrayList<>();
        collection.find()
            .forEach((document) -> papers.add(document),
                (result, error) -> {
                    if (error != null) {
                        error.printStackTrace();
                    } else {
                        logger.info("Recalculating votes for papers: " + papers.size());
                        for (Document paper : papers) {
                            double averageVote = 0.0;
                            int votesCount = 0;
                            if (paper.get("votes") != null) {
                                averageVote = calculateAverageVote((List<Document>) paper.get("votes"));
                                votesCount = ((List<Document>) paper.get("votes")).size();
                                paper.put("averageVote", averageVote);
                                paper.put("votesCount", votesCount);

                            }

                            List<Document> senders = (List<Document>) paper.get("senders");
                            for (Document senderDoc : senders) {
                                Sender sender = DocumentConverter.parseToObject(senderDoc, Sender.class);
                                senderDoc.put("code", Speaker.calculateCode(sender.getFullName(), sender.getEmail()));
                            }

                            this.getCollection().updateOne(
                                eq("_id", paper.get("_id")),
                                Updates.combine(
                                    Updates.set("votesCount", votesCount),
                                    Updates.set("averageVote", averageVote),
                                    Updates.set("senders", senders)
                                ), (updateResult, updateError) -> {
                                    if (updateError != null) {
                                        updateError.printStackTrace();
                                    }
                                });
                        }
                    }
                });
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        return null;
    }
}
