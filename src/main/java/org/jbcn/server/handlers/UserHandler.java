package org.jbcn.server.handlers;

import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.impl.MessageImpl;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.User;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.utils.CollectionUtils;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.TimeUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static org.jbcn.server.handlers.AuthHandler.CREATE_USER_AUTH_DATA;
import static org.jbcn.server.model.Role.VOTER;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.model.User.getAdminUser;
import static org.jbcn.server.model.utils.ModelValidator.checkRequiredFields;
import static org.jbcn.server.platform.ObjectIdValidator.extractObjectId;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;

public class UserHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(UserHandler.class);

    private static final String USERNAME_FIELD = "username";
    private static final String PASSWORD_FIELD = "password";
    private static final String SALT_FIELD = "salt";
    private static final String EMAIL_FIELD = "email";
    private static final String ROLES_FIELD = "roles";

    public UserHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        this.prepareDatabase().setHandler((handler) -> {
            if (JbcnProperties.getBoolean("jbcn.admin.auto-create", true)) {
                createAdminUserIfNecessary();
            }
            startFuture.complete();
        });
        vertx.eventBus().consumer(Constants.USER_QUEUE, this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        switch (action) {
            case "user-getAll":
                listUsers(message);
                break;
            case "user-search":
                searchUsers(message);
                break;
            case "user-get":
                getUser(message);
                break;
            case "user-add":
                addUser(message);
                break;
            case "user-update":
                updateUser(message);
                break;
            case "user-delete":
                deleteUser(message);
                break;
            case "user-get-voters":
                getVoters(message);
                break;
            default:
                logger.info("Default message");
                message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
                break;
        }
    }

    private void listUsers(Message<JsonObject> message) {

        // TODO study alternative of custom mapper like `bsonToJson` in NotesHandler
        getCollection()
            .find()
            .map(doc -> DocumentConverter.toJson(removeSensibleData(doc)))
            .into(new ArrayList<>(), (result, error) -> message.reply(new JsonArray(result)));
    }

    private Document removeSensibleData(Document document) {
        document.remove(PASSWORD_FIELD);
        document.remove(SALT_FIELD);
        return document;
    }

    private void searchUsers(Message<JsonObject> message) {

        final String term = message.body().getString("term", "");
        final Integer page = message.body().getInteger("page", 0);
        final Integer size = message.body().getInteger("size", 50);
        final String sort_column = message.body().getString("sort", "username");
        final Boolean asc = message.body().getBoolean("asc", Boolean.TRUE);

        final Bson query = Filters.regex("username", term);

        getCollection()
            .countDocuments(query, (count, countError) -> {
                if (countError != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, countError.getMessage());
                } else {
                    getCollection()
                        .find(query)
                        .sort(asc ? Sorts.ascending(sort_column) : Sorts.descending(sort_column))
                        .skip(page * size)
                        .limit(size)
                        .map(doc -> DocumentConverter.toJson(removeSensibleData(doc)))
                        .into(new ArrayList(), (result, searchError) -> {
                            if (searchError != null) {
                                logger.error("Error in user search: ", searchError);
                                message.fail(Constants.UNEXPECTED_ERROR, searchError.getMessage());
                            } else {
                                message.reply(new JsonObject()
                                    .put("total", count)
                                    .put("items", result));
                            }
                        });
                }
            });
    }

    private Future createAdminUserIfNecessary() {
        Future future = Future.future();
        getCollection()
            .countDocuments(eq(USERNAME_FIELD, "admin"), (count, error) -> {
                logger.info("Found " + count + " admin users");
                if (error == null) {
                    if (count == 0) {
                        logger.info("Creating admin user");
                        createDefaultUsers();
                    } else {
                        logger.info("Skipping admin user creation: already found");
                    }
                    future.complete();
                } else {
                    logger.error("Could not check for admin user", error);
                    future.fail("Could not check for admin user");
                }
            });
        return future;
    }

    private void getUser(Message<JsonObject> message) {

        final String id = message.body().getString("id");
        logger.info("GET USER:" + id);

        extractObjectId(id, message).ifPresent(oid -> {
            getCollection()
                .find(eq("_id", oid))
                .first((doc, error) -> {
                    logger.info("GET USER FIND RESULT");
                    if (error != null) {
                        message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                    } else {
                        if (doc == null) {
                            message.fail(Constants.UNEXPECTED_ERROR, Constants.UNKNOWN_USER_ERROR);
                        } else {
                            message.reply(DocumentConverter.toJson(removeSensibleData(doc)));
                        }
                    }
                });
        });
    }

    private void getVoters(Message<JsonObject> message) {

        getCollection()
            .find(eq("roles", VOTER.getRole()))
            .sort(Sorts.ascending("username"))
            .map(doc -> DocumentConverter.toJson(removeSensibleData(doc)))
            .into(new ArrayList(), (result, searchError) -> {
                if (searchError != null) {
                    logger.error("Error in user search: ", searchError);
                    message.fail(Constants.UNEXPECTED_ERROR, searchError.getMessage());
                } else {
                    message.reply(new JsonObject()
                        .put("items", result));
                }
            });
    }

    private void addUser(Message<JsonObject> message) {
        final JsonObject json = message.body();

        final List<String> errors = checkRequiredFields(Arrays.asList(USERNAME_FIELD, PASSWORD_FIELD, EMAIL_FIELD, ROLES_FIELD), json);
        final Set<Role> roles = processRoles(json, errors);

        if (errors.size() > 0) {
            message.fail(Constants.BAD_REQUEST, errors.get(0));
        } else {
            final User user = User.builder()
                .username(json.getString(USERNAME_FIELD))
                .password(json.getString(PASSWORD_FIELD))
                .email(json.getString(EMAIL_FIELD))
                .roles(roles)
                .build();

            newUser(user, message);
        }
    }

    private Set<Role> processRoles(JsonObject json, List<String> errors) {
        final Set<Role> roles = new HashSet<>();
        if (json.getJsonArray(ROLES_FIELD) != null) {
            for (Object role : json.getJsonArray(ROLES_FIELD)) {
                final Role enumValue = Role.of((String) role);
                if (enumValue == null) {
                    errors.add(Constants.ROLE_INVALID_ROLE);
                } else {
                    roles.add(enumValue);
                }
            }
        }
        return roles;
    }

    private void newUser(User user, Message<JsonObject> message) {

        // Adding custom fields is not provided, so we need to insert manually following Vert.x conventions
        // https://github.com/vert-x3/vertx-auth/issues/102
        vertx.eventBus().send(Constants.AUTH_QUEUE, new JsonObject()
                .put(USERNAME_FIELD, user.getUsername())
                .put(PASSWORD_FIELD, user.getPassword()),
            new DeliveryOptions().addHeader("action", CREATE_USER_AUTH_DATA),
            event -> {
                if (event.succeeded()) {
                    long timestamp = new Date().getTime();
                    final JsonObject jsonUser = ((JsonObject) event.result().body())
                        .put(EMAIL_FIELD, user.getEmail())
                        .put(ROLES_FIELD, getRolesAsStringList(user.getRoles()))
                        .put(CREATED_DATE, timestamp)
                        .put(LAST_UPDATE, timestamp);

                    final Document doc = Document.parse(jsonUser.encode());
                    getCollection()
                        .insertOne(doc, (result, error) -> {
                            if (error == null) {
                                if (message != null)
                                    message.reply(new JsonObject()
                                        .put("message", "User added")
                                        .put("id", doc.getObjectId("_id").toString())
                                    );
                                logger.info("Added user: " + doc.getObjectId("_id"));
                            } else {
                                error.getCause().printStackTrace();
                                if (message != null)
                                    message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                            }
                        });
                }
            }
        );
    }

    private List<String> getRolesAsStringList(Set<Role> roles) {
        return roles.stream().map(it -> it.getRole()).collect(Collectors.toList());
    }

    private void createDefaultUsers() {
        logger.info("******* Creating default users!");
        newUser(getAdminUser(), new MessageImpl<>());
    }

    private void updateUser(Message<JsonObject> message) {

        final JsonObject user = message.body();

        extractObjectId(user.getString("_id"), message)
            .ifPresent(oid -> {
                getCollection()
                    .find(eq("_id", oid))
                    .first((result, error) -> {
                        if (error != null) {
                            message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
                        } else {
                            if (result == null) {
                                message.fail(Constants.UNEXPECTED_ERROR, Constants.UNKNOWN_USER_ERROR);
                            } else {
                                logger.info("Updating found user: " + oid);
                                final String newPassword = user.getString(PASSWORD_FIELD);
                                final String newUsername = user.getString(USERNAME_FIELD);
                                if (!isNullOrBlank(newPassword) || !isNullOrBlank(newUsername)) {
                                    // Check if we are also updating username
                                    final String updateUsername = isNullOrBlank(newUsername)
                                        ? result.getString(USERNAME_FIELD)
                                        : newUsername;

                                    vertx.eventBus().send(Constants.AUTH_QUEUE, new JsonObject()
                                            .put(USERNAME_FIELD, updateUsername)
                                            .put(PASSWORD_FIELD, newPassword),
                                        new DeliveryOptions().addHeader("action", CREATE_USER_AUTH_DATA),
                                        reply -> {
                                            if (reply.succeeded()) {
                                                final JsonObject body = (JsonObject) reply.result().body();
                                                final List<Bson> updates = buildUpdates(user, message);
                                                updates.add(Updates.set(PASSWORD_FIELD, body.getString(PASSWORD_FIELD)));
                                                updates.add(Updates.set(SALT_FIELD, body.getString(SALT_FIELD)));
                                                updateUsername(oid, updates, message);
                                            } else {
                                                message.fail(Constants.UNEXPECTED_ERROR, Constants.USER_INVALID_CREDENTIAL);
                                            }
                                        }
                                    );
                                } else {
                                    // Beware during password update we run the user update in another nested callback. 
                                    // IDE may incorrectly suggest extracting 'buildUpdates' and running 'updateUsername' once
                                    updateUsername(oid, buildUpdates(user, message), message);
                                }
                            }
                        }
                    });
            });
    }

    private void updateUsername(ObjectId id, List<Bson> updates, Message<JsonObject> responseMessage) {
        if (updates.size() > 0) {
            updates.add(Updates.set(LAST_UPDATE, TimeUtils.currentTime()));

            getCollection().updateOne(
                eq("_id", id),
                Updates.combine(updates),
                processUpdateResponse(responseMessage));
        } else {
            responseMessage.reply(new JsonObject());
        }
    }

    private SingleResultCallback<UpdateResult> processUpdateResponse(Message<JsonObject> message) {
        return (UpdateResult updateResult, Throwable updateError) -> {
            if (updateError != null) {
                logger.error("Error in update:", updateError.getCause());
                message.fail(Constants.UNEXPECTED_ERROR, updateError.getMessage());
            } else {
                logger.info("Updated count:" + updateResult.getModifiedCount());
                if (updateResult.getModifiedCount() == 0)
                    message.fail(Constants.UNEXPECTED_ERROR, Constants.UNKNOWN_USER_ERROR);
                else
                    message.reply(new JsonObject());
            }
        };
    }

    private List<Bson> buildUpdates(JsonObject data, Message<JsonObject> message) {
        final List<Bson> updates = new ArrayList<>();

        final String username = data.getString(USERNAME_FIELD);
        if (!isNullOrBlank(username))
            updates.add(Updates.set(USERNAME_FIELD, username));

        final String email = data.getString(EMAIL_FIELD);
        if (!isNullOrBlank(email))
            updates.add(Updates.set(EMAIL_FIELD, email));

        if (!CollectionUtils.isEmpty(data.getJsonArray(ROLES_FIELD))) {
            final List<String> errors = new ArrayList<>();
            final Set<Role> roles = processRoles(data, errors);
            if (errors.size() > 0)
                message.fail(Constants.BAD_REQUEST, errors.get(0));
            updates.add(Updates.set(ROLES_FIELD, getRolesAsStringList(roles)));
        }
        return updates;
    }

    private void deleteUser(Message<JsonObject> message) {
        String id = message.body().getString("id");
        ObjectId oid = new ObjectId(id);
        getCollection().deleteOne(eq("_id", oid), (result, error) -> {
            if (error != null) {
                logger.error("Error deleting user", error);
                message.fail(Constants.UNEXPECTED_ERROR, error.getMessage());
            } else {
                message.reply(new JsonObject());
            }
        });
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        return null;
    }

}
