package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jbcn.server.Constants;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.model.votes.AttendeeVote;
import org.jbcn.server.model.votes.VoteSource;
import org.jbcn.server.utils.StringUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;

public class AttendeesVotesHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(AttendeesVotesHandler.class);

    private static final String ACTION_ROOT = AttendeeVote.class.getSimpleName().toLowerCase();
    public static final String ADD_ACTION = ACTION_ROOT + "-add";
    public static final String GET_ACTION = ACTION_ROOT + "-get";
    public static final String GET_ALL_ACTION = ACTION_ROOT + "-list";
    public static final String UPDATE_ACTION = ACTION_ROOT + "-update";
    public static final String UPSERT_ACTION = ACTION_ROOT + "-upsert";
    public static final String FILTER_ACTION = ACTION_ROOT + "-filter";
    public static final String DELETE_ACTION = ACTION_ROOT + "-delete";

    public static final String GET_FEEDBACK_ACTION = "get-talk-feedback";

    public AttendeesVotesHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        prepareDatabase()
            .setHandler(event -> startFuture.complete());
        vertx.eventBus().consumer(getQueueName(AttendeeVote.class), this::onMessage);
    }

    public void onMessage(Message<JsonObject> message) {
        final String action = message.headers().get("action");
        logger.info(this.getClass().getSimpleName() + " received action:" + action);

        if (action.equals(ADD_ACTION)) {
            if (!invalidFields(message))
                add(message);
        } else if (action.equals(UPSERT_ACTION)) {
            upsert(message);
        } else if (action.equals(GET_ACTION)) {
            getById(message);
        } else if (action.equals(GET_ALL_ACTION)) {
            getAll(message);
        } else if (action.equals(UPDATE_ACTION)) {
            update(message);
        } else if (action.equals(FILTER_ACTION)) {
            filter(message);
        } else if (action.equals(DELETE_ACTION)) {
            delete(message);
        } else if (action.equals(GET_FEEDBACK_ACTION)) {
            getAggregatedFeedback(message);
        } else {
            logger.warn("Unknown action:" + action);
            message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
        }
    }

    private void add(Message<JsonObject> message) {
        add(message, Arrays.asList("talkId", "userEmail", "value"), AttendeeVote.class);
    }

    private boolean invalidFields(Message<JsonObject> message) {
        if (message.body().containsKey("value")) {
            Integer value = message.body().getInteger("value");
            if (value != null && (value < 1 || value > 5)) {
                message.fail(Constants.BAD_REQUEST, Constants.ATTENDEE_VOTE_INVALID_VALUE);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        final List<Bson> updates = new ArrayList<>();

        Integer value = data.getInteger("value");
        if (value != null)
            updates.add(Updates.set("value", value));

        String delivery = data.getString("delivery");
        if (!StringUtils.isNullOrBlank(delivery))
            updates.add(Updates.set("delivery", delivery));

        String other = data.getString("other");
        if (!StringUtils.isNullOrBlank(other))
            updates.add(Updates.set("other", other));

        String source = VoteSource.valueOf(data.getString("source")).name();
        if (!StringUtils.isNullOrBlank(source))
            updates.add(Updates.set("source", source));

        return updates;
    }

    private void upsert(Message<JsonObject> message) {

        final String talkId = message.body().getString("talkId");
        final String email = message.body().getString("userEmail");

        collection.find(and(eq("talkId", talkId), eq("userEmail", email)))
            .projection(include("_id"))
            .first((result, t) -> {
                if (t != null) {
                    message.fail(Constants.UNEXPECTED_ERROR, t.getCause().getMessage());
                } else {
                    if (result != null) {
                        message.body().put("id", result.getObjectId("_id").toString());
                        update(message);
                    } else {
                        add(message);
                    }
                }
            });
    }

    private void getAggregatedFeedback(Message<JsonObject> message) {

        final String talkId = message.body().getString("id");

        collection.find(and(eq("talkId", talkId)))
            .projection(include("value", "delivery", "other"))
            .into(new ArrayList<>(), (result, error) -> {

                // aka. comments
                final Set<String> others = new HashSet<>();
                // aka. compliments
                final Map<String, Integer> deliveries = new HashMap<>();

                Double count = 0.0;
                Integer sum = 0;
                if (result != null) {
                    for (Document doc : result) {
                        others.add(doc.getString("other"));
                        sum += doc.getInteger("value");

                        final String delivery = doc.getString("delivery");
                        if (!StringUtils.isNullOrBlank(delivery) && !delivery.equals("none")) {
                            deliveries.put(delivery, deliveries.containsKey(delivery) ? deliveries.get(delivery) + 1 : 1);
                        }

                        count++;
                    }
                }

                DecimalFormat df = new DecimalFormat("#.###");
                df.setRoundingMode(RoundingMode.CEILING);

                if (count == 0) {
                    message.reply(new JsonObject()
                        .put("votes", 0));
                } else {
                    message.reply(new JsonObject()
                        .put("votes", count)
                        .put("average", Double.parseDouble(df.format(sum / count)))
                        .put("compliments", new JsonObject((Map<String, Object>) ((Map<String, ?>) deliveries)))
                        .put("comments", new JsonArray(others.stream()
                            .filter(v -> !StringUtils.isNullOrBlank(v))
                            .collect(Collectors.toList()))));
                }
            });
    }

}
