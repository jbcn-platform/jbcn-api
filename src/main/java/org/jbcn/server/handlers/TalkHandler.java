package org.jbcn.server.handlers;

import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Updates;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jbcn.server.Constants;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.controllers.CompaniesController;
import org.jbcn.server.email.MailHandler;
import org.jbcn.server.model.*;
import org.jbcn.server.persistence.PublicDataProcessor;
import org.jbcn.server.persistence.TalkRepository;
import org.jbcn.server.platform.DatabaseHandler;
import org.jbcn.server.utils.*;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static java.util.Optional.ofNullable;
import static org.jbcn.server.handlers.CompaniesHandler.listCompanies;
import static org.jbcn.server.platform.ObjectIdValidator.extractObjectId;
import static org.jbcn.server.utils.CollectionUtils.removeBlankValues;


public class TalkHandler extends DatabaseHandler {

    private static final Logger logger = LoggerFactory.getLogger(TalkHandler.class);

    private static final List<String> VALID_FIELD_NAMES = Arrays.stream(Talk.class.getDeclaredFields())
        .map(Field::getName)
        .collect(Collectors.toList());

    private TalkRepository talkRepository;

    public TalkHandler(MongoDatabase mongoDatabase, String collectionName) {
        super(mongoDatabase, collectionName);
    }

    public void start(Future<Void> startFuture) {
        prepareDatabase().setHandler((result) -> {
            talkRepository = new TalkRepository(getMongoDatabase());
            this.recalculateData();
            vertx.eventBus().consumer(Constants.TALK_QUEUE, this::onMessage);
            startFuture.complete();
        });
    }

    private void onMessage(Message<JsonObject> message) {
        String action = message.headers().get("action");
        switch (action) {
            case "talk-add":
                addTalk(message);
                break;
            case "talk-update":
                updateTalk(message);
                break;
            case "talk-get":
                getById(message, Constants.UNKNOWN_TALK_ERROR);
                break;
            case "talk-delete":
                delete(message);
                break;
            case "talk-search":
                searchTalks(message);
                break;
            case "talk-change-state":
                changeState(message);
                break;
            case "talk-publish":
                updatePublishState(message);
                break;
            case "talk-get-published":
                getPublishedTalks(message);
                break;
            case "talk-get-talks-json":
                getPublishedTalksJson(message);
                break;
            case "talk-get-speakers-json":
                getPublishedSpeakersJson(message);
                break;
            case "talk-get-all-speakers":
                getPublishedSpeakers(message);
                break;
            case "talk-count":
                countByEdition(message);
                break;
            case "talk-tags-count":
                talkTagsCount(message);
                break;
            case CompaniesController.LIST_COMPANIES:
                listCompanies("speakers.company", getCollection(), message);
                break;
            case "talk-combine-votes":
                combineVotes(message);
                break;
            default:
                logger.warn("Unknown action:" + action);
                message.fail(Constants.UNKNOWN_ACTION_ERROR, Constants.UNKNOWN_ACTION_ERROR_CODE);
                break;
        }
    }

    private void updatePublishState(Message<JsonObject> message) {
        logger.info("Changing talk published");
        JsonObject params = message.body();

        final String id = params.getString("id");
        final Boolean published = params.getBoolean(Talk.PUBLISHED);
        if (published == null) {
            message.fail(Constants.BAD_REQUEST, Constants.FIELD_REQUIRED_IN_BODY + Talk.PUBLISHED);
            return;
        }

        this.talkRepository.get(id, (result) -> {
            if (result.succeeded()) {
                if (result.result() == null) {
                    message.fail(Constants.UNEXPECTED_ERROR, Constants.UNKNOWN_TALK_ERROR);
                } else {
                    updateDocument(new ObjectId(id), Arrays.asList(
                            Updates.set(Talk.PUBLISHED, published),
                            Updates.set(Traceable.LAST_ACTION_BY, params.getString("username")),
                            Updates.set(Traceable.LAST_UPDATE, TimeUtils.currentTime())),
                        update -> {
                            if (update.succeeded()) {
                                message.reply(new JsonObject());
                            } else {
                                message.fail(Constants.UNEXPECTED_ERROR, update.cause().getMessage());
                            }
                        }
                    );
                }
            } else {
                logger.error("Error changing talk state", result.cause());
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void changeState(Message<JsonObject> message) {
        logger.info("Changing talk state");
        final JsonObject params = message.body();

        final String state = params.getString(Talk.STATE);
        if (state == null) {
            message.fail(Constants.BAD_REQUEST, Constants.FIELD_REQUIRED_IN_BODY + Talk.STATE);
            return;
        }
        if (!TalkState.isValid(state)) {
            message.fail(400, Constants.TALK_INVALID_STATE);
            return;
        }

        final String id = params.getString("id");

        this.talkRepository.get(id, (result) -> {
            if (result.succeeded()) {
                if (result.result() == null) {
                    message.fail(Constants.UNEXPECTED_ERROR, Constants.UNKNOWN_TALK_ERROR);
                } else {
                    final List<Bson> updates = new ArrayList<>();
                    updates.add(Updates.set("state", state));
                    updates.add(Updates.set(Traceable.LAST_ACTION_BY, params.getString("username")));
                    updates.add(Updates.set(Traceable.LAST_UPDATE, TimeUtils.currentTime()));
                    // If talk is not confirmed or is canceled -> then can't be published;
                    if (TalkState.TALK_STATE_UNCONFIRMED.getState().equals(state) || TalkState.TALK_STATE_CANCELED.getState().equals(state)) {
                        updates.add(Updates.set("published", false));
                    }
                    updateDocument(new ObjectId(id), updates, update -> {
                        if (update.succeeded()) {
                            message.reply(new JsonObject());
                        } else {
                            message.fail(Constants.UNEXPECTED_ERROR, update.cause().getMessage());
                        }
                    });
                }
            } else {
                logger.error("Error changing talk state", result.cause());
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void addTalk(Message<JsonObject> message) {
        logger.info("Adding Talk");
        final JsonObject jsonPaper = message.body();
        final String responsiblePerson = jsonPaper.getJsonObject("responsiblePerson").getString("username");

        final Paper paper = DocumentConverter.parseToObject(jsonPaper, Paper.class);
        final Talk talk = Talk.toTalk(paper);
        talk.setResponsiblePerson(responsiblePerson);

        this.talkRepository.save(talk, (result) -> {
            if (result.succeeded()) {
                message.reply(result.result());
                talk.getSpeakers().forEach(speaker -> sendEmail(speaker, talk));
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void sendEmail(Speaker speaker, Talk talk) {
        logger.info("Sending email to:" + speaker.getEmail());
        final JsonObject emailJson = new JsonObject()
            .put("to", speaker.getEmail())
            .put("title", talk.getTitle())
            .put("senderName", speaker.getFullName());

        final DeliveryOptions mailOptions = new DeliveryOptions().addHeader("action", "talk-approved-message");
        vertx.eventBus().send(MailHandler.QUEUE_NAME, emailJson, mailOptions, emailReply -> {
        });
    }

    private void updateTalk(Message<JsonObject> message) {
        final JsonObject talkJson = message.body();
        logger.info("Updating talkJson: " + talkJson.toString());

        extractObjectId(talkJson.getString("_id"), message)
            .ifPresent(oid -> {

                final List<Bson> updates = new ArrayList<>();

                // enum validations
                final Pair<String, Boolean> stateValidation = StringUtils.validate(talkJson.getString("state"),
                    TalkState::isValid,
                    v -> updates.add(Updates.set("state", v)));
                if (stateValidation.getSecond())
                    message.fail(400, Constants.TALK_INVALID_STATE);

                final Pair<String, Boolean> typeValidation = StringUtils.validate(talkJson.getString("type"),
                    ProposalType::isValid,
                    v -> updates.add(Updates.set("type", v)));
                if (typeValidation.getSecond())
                    message.fail(400, Constants.TALK_INVALID_TYPE);

                final Pair<String, Boolean> levelValidation = StringUtils.validate(talkJson.getString("level"),
                    ProposalLevel::isValid,
                    v -> updates.add(Updates.set("level", v)));
                if (levelValidation.getSecond())
                    message.fail(400, Constants.TALK_INVALID_LEVEL);

                // field cleanup
                JsonArray tags = talkJson.getJsonArray("tags");
                if (!emptyTags(tags))
                    talkJson.put("tags", removeBlankValues(tags.getList()));

                JsonArray languages = talkJson.getJsonArray("languages");
                if (!emptyTags(languages)) {
                    talkJson.put("languages", removeBlankValues(languages.getList()));
                }

                // fields that must not be updated
                talkJson.remove("_id");
                talkJson.remove(Traceable.CREATED_DATE);
                talkJson.remove("totalVote");

                // Recalculate speakers code
                final JsonArray speakers = talkJson.getJsonArray("speakers");
                if (speakers != null) {
                    speakers.stream().forEach(speakerObj -> {
                        final JsonObject speaker = (JsonObject) speakerObj;
                        final String code = Speaker.calculateCode(speaker.getString("fullName"), speaker.getString("email"));
                        speaker.put("code", code);
                    });
                }

                // TODO placeholder implementation: cannot convert to object with DocumentConverter
                //  because enums get the default value when received null or invalid

                updates.addAll(DocumentConverter.getDocument(talkJson)
                    .entrySet()
                    .stream()
                    .filter(entry -> VALID_FIELD_NAMES.contains(entry.getKey()))
                    .filter(entry -> {
                        final Object value = entry.getValue();
                        return !(value == null
                            || (value.getClass().equals(String.class) && ((String) value).isEmpty())
                            || (List.class.isAssignableFrom(value.getClass()) && CollectionUtils.isEmpty((Collection) value))
                        );
                    })
                    .map(entry -> Updates.set(entry.getKey(), entry.getValue()))
                    .collect(Collectors.toList()));

                if (updates.size() > 0) {
                    updates.add(Updates.set(Traceable.LAST_UPDATE, TimeUtils.currentTime()));

                    updateDocument(oid,
                        updates,
                        update -> {
                            if (update.succeeded()) {
                                message.reply(new JsonObject());
                            } else {
                                message.fail(Constants.UNEXPECTED_ERROR, update.cause().getMessage());
                            }
                        });

                } else {
                    message.reply(new JsonObject());
                }
            });
    }

    private boolean emptyTags(Object value) {
        return value == null || (List.class.isAssignableFrom(value.getClass()) && !CollectionUtils.containsNotBlankValues((List<String>) value));
    }

    private void countByEdition(Message<JsonObject> message) {
        String edition = getEdition(message);
        if (edition == null) {
            edition = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        }
        logger.info("Count Talk by edition:" + edition);
        talkRepository.findAllByEdition(edition, talksResult -> {
            if (talksResult.succeeded()) {
                List<Talk> talks = talksResult.result();
                JsonObject counters = countStatistics(talks);
                message.reply(counters);
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, talksResult.cause().getMessage());
            }
        });

    }

    private void talkTagsCount(Message<JsonObject> message) {
        String edition = getEdition(message);
        if (edition == null) {
            edition = JbcnProperties.getCurrentEdition();
        }

        talkRepository.findAllByEdition(edition, talksResult -> {
            if (talksResult.succeeded()) {
                final JsonObject data = new JsonObject();
                final List<Talk> talks = talksResult.result();
                for (Talk talk : talks) {
                    for (String tagName : talk.getTags()) {
                        final String tag = tagName.trim().toLowerCase();
                        if (!data.containsKey(tag)) {
                            data.put(tag, 0);
                        }
                        data.put(tag, data.getInteger(tag) + 1);
                    }
                }
                message.reply(data);
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, talksResult.cause().getMessage());
            }
        });
    }

    private JsonObject countStatistics(List<Talk> talks) {

        final Map<String, Map<String, Integer>> counter = new HashMap<>();
        counter.put("workshop", new HashMap<>());
        counter.get("workshop").put("total", 0);
        counter.get("workshop").put("confirmed", 0);
        counter.get("workshop").put("canceled", 0);
        counter.get("workshop").put("rejected", 0);
        counter.get("workshop").put("unconfirmed", 0);
        counter.get("workshop").put("scheduled", 0);
        counter.put("talk", new HashMap<>());
        counter.get("talk").put("total", 0);
        counter.get("talk").put("confirmed", 0);
        counter.get("talk").put("canceled", 0);
        counter.get("talk").put("rejected", 0);
        counter.get("talk").put("unconfirmed", 0);
        counter.get("talk").put("scheduled", 0);

        Map<String /* code */, Speaker> speakerMap = new HashMap<>();

        for (Talk talk : talks) {
            List<Speaker> speakers = talk.getSpeakers();
            for (Speaker speaker : speakers) {
                if (!talk.getState().getState().equals("canceled") && !talk.getState().getState().equals("rejected")) {
                    speakerMap.put(speaker.getCode(), speaker);
                }
            }

            counter.get(talk.getType())
                .put("total", counter.get(talk.getType()).get("total") + 1);
            counter.get(talk.getType())
                .put(talk.getState().getState(), counter.get(talk.getType()).get(talk.getState().getState()) + 1);
        }

        int speakersCashCount = 0;
        for (Speaker speaker : speakerMap.values()) {
            if (speaker.isTravelCost()) {
                speakersCashCount++;
            }
        }

        return new JsonObject()
            .put("counter", counter)
            .put("speakers", speakerMap.size())
            .put("speakersCash", speakersCashCount);
    }

    private void searchTalks(Message<JsonObject> message) {

        final JsonObject body = message.body();

        final Integer page = ofNullable(body.getInteger("page")).orElse(0);
        final Integer size = ofNullable(body.getInteger("size")).orElse(50);

        final String sortColumn = ofNullable(body.getString("sort")).orElse("title");
        final Boolean asc = ofNullable(body.getBoolean("asc")).orElse(Boolean.TRUE);

        final JsonObject conditions = body.getJsonObject("conditions");

        final List<String> fields = body.containsKey("fields") ? body.getJsonArray("fields").getList() : java.util.Collections.emptyList();

        talkRepository.search(size, page, sortColumn, asc, conditions, fields, (result) -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void getPublishedTalks(Message<JsonObject> message) {
        String edition = getEdition(message);
        this.talkRepository.findByPublishedState(edition, Boolean.TRUE, result -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void getPublishedTalksJson(Message<JsonObject> message) {
        final String edition = getEdition(message);
        this.talkRepository.findByPublishedState(edition, Boolean.TRUE, result -> {
            if (result.succeeded()) {
                List<Talk> talks = result.result();
                if (message.body().containsKey("id")) {
                    JsonObject talk = new PublicDataProcessor()
                        .findTalk(talks, message.body().getString("id"));
                    message.reply(talk);
                } else {
                    JsonArray items = new PublicDataProcessor().getTalks(talks);
                    message.reply(items);
                }
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void getPublishedSpeakersJson(Message<JsonObject> message) {
        String edition = getEdition(message);
        this.talkRepository.findByPublishedState(edition, Boolean.TRUE, result -> {
            if (result.succeeded()) {
                final List<Talk> talks = result.result();
                if (message.body().containsKey("id")) {
                    JsonObject speaker = new PublicDataProcessor()
                        .findSpeaker(talks, message.body().getString("id"));
                    message.reply(speaker);
                } else {
                    JsonArray items = new PublicDataProcessor().getSpeakers(talks);
                    message.reply(items);
                }
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private String getEdition(Message<JsonObject> message) {
        return message.body().getString("edition");
    }

    private void getPublishedSpeakers(Message<JsonObject> message) {
        final String edition = getEdition(message);
        this.talkRepository.findByPublishedState(edition, Boolean.TRUE, result -> {
            if (result.succeeded()) {
                final JsonArray speakers = result.result().stream()
                    .flatMap(t -> t.getSpeakers().stream())
                    .sorted(Comparator.comparing(Speaker::getFullName))
                    .map(it -> DocumentConverter.toJson(it)
                        .put("ref", Speaker.generateRef(it.getFullName(), it.getEmail())))
                    .collect(JsonArray::new, JsonArray::add, JsonArray::addAll);

                message.reply(removeDuplicatedByCode(speakers));
            } else {
                message.fail(Constants.UNEXPECTED_ERROR, result.cause().getMessage());
            }
        });
    }

    private void combineVotes(Message<JsonObject> message) {

        Map<String, CombinedVote> counters = new HashMap<>();
        this.talkRepository.findByPublishedState(JbcnProperties.getCurrentEdition(), Boolean.TRUE, result_talks -> {
            getMongoDatabase()
                .getCollection("sessions")
                .find()
                .map(doc -> DocumentConverter.toJson(doc))
                .into(new ArrayList<>(), (result_sessions, error) -> {
                    getMongoDatabase()
                        .getCollection("attendees_votes")
                        .find()
                        .map(doc -> DocumentConverter.toJson(doc))
                        .into(new ArrayList<>(), (attendees_votes, error_attendees_votes) -> {
                            try {
                                // Mobile votes
                                for (JsonObject attendeeVote : attendees_votes) {
                                    Talk talk = result_talks.result().stream().filter(t -> attendeeVote.getString("talkId").equals(t.get_id())).findFirst().get();
                                    if (counters.get(talk.get_id()) == null) {

                                        counters.put(talk.get_id(), CombinedVote.builder()
                                            .talkId(talk.get_id())
                                            .speaker(getFullSpeakersName(talk))
                                            .title(talk.getTitle())
                                            .build());
                                        counters.get(talk.get_id()).incCount().add(attendeeVote.getInteger("value"));
                                    }
                                }

                                List<JsonObject> list = new ArrayList<>();
                                for (CombinedVote vote : counters.values()) {
                                    if (vote.getCount() > 0) {
                                        vote.setAverage(vote.getSum() / vote.getCount());
                                    }

                                    JsonObject jsonCount = JsonObject.mapFrom(vote);
                                    list.add(jsonCount);

                                }
                                List ordered = list.stream().sorted((o1, o2) -> {
                                    if (o1.getFloat("average") < o2.getFloat("average")) {
                                        return 1;
                                    }
                                    if (o1.getFloat("average") > o2.getFloat("average")) {
                                        return -1;
                                    }
                                    if (o1.getFloat("average").equals(o2.getFloat("average"))) {
                                        return o1.getFloat("count") < o2.getFloat("count") ? 1 : -1;
                                    }
                                    return 0;
                                }).collect(Collectors.toList());
                                JsonArray result = new JsonArray(ordered);
                                message.reply(result);
                            } catch (Exception e) {
                                message.fail(Constants.UNEXPECTED_ERROR, e.getMessage());
                            }

                        });

                });
        });
    }

    private String getFullSpeakersName(Talk talk) {
        StringBuilder speakerNames = new StringBuilder();
        talk.getSpeakers().stream().forEach(speaker -> {
            if (!speakerNames.toString().isEmpty()) {
                speakerNames.append(", ");
            }
            speakerNames.append(speaker.getFullName());
        });
        return speakerNames.toString();
    }

    private JsonArray removeDuplicatedByCode(JsonArray speakers) {
        final JsonArray res = new JsonArray();
        final Set<String> codes = new HashSet(speakers.size());
        for (Object speaker : speakers) {
            final String code = ((JsonObject) speaker).getString("code");
            if (!codes.contains(code)) {
                codes.add(code);
                res.add(speaker);
            }
        }
        return res;
    }

    /**
     * Applies changes to data on startup to update model changes on a life environment.
     */
    private void recalculateData() {
        final List<Talk> talks = new ArrayList<>();
        getCollection()
            .find()
            .forEach(document -> talks.add(DocumentConverter.parseToObject(document, Talk.class)),
                (result, t) -> {
                    talks.stream()
                        .forEach(talk -> {
                            final List<Speaker> speakers = talk.getSpeakers()
                                .stream()
                                .map(this::updateSpeakerReferences)
                                .collect(Collectors.toList());
                            talk.setSpeakers(speakers);

                            final String talkId = talk.get_id();
                            getCollection().replaceOne(eq("_id", talkId),
                                DocumentConverter.getDocument(talk),
                                (updateResult, updateError) -> {
                                    if (updateError != null) {
                                        logger.error(updateError.getMessage(), updateError);
                                    }
                                });

                            // Set talk's paperId if not present
                            if (StringUtils.isNullOrBlank(talk.getPaperId())) {
                                getMongoDatabase()
                                    .getCollection(org.jbcn.server.persistence.Collections.PAPER)
                                    .find(Filters.and(
                                        Filters.eq("edition", JbcnProperties.getCurrentEdition()),
                                        Filters.eq("title", talk.getTitle())
                                    ))
                                    .projection(Projections.include("_id"))
                                    .limit(1)
                                    .first((result1, error) -> {
                                        if (error != null) {
                                            logger.error("Could not find paper for talk: " + talkId, error);
                                        } else {
                                            if (result1 != null) {
                                                final String paperId = result1.getObjectId("_id").toString();
                                                getCollection()
                                                    .updateOne(eq("_id", new ObjectId(talkId)),
                                                        Updates.set("paperId", paperId),
                                                        (updateResult, updateError) -> {
                                                            if (updateError != null) {
                                                                logger.error(updateError.getMessage(), updateError);
                                                            }
                                                            if (updateResult.getModifiedCount() != 1) {
                                                                logger.error(String.format("Could not update talk %s with paperId %s", talkId, paperId));
                                                            } else {
                                                                logger.info(String.format("Updated talk %s with paperId", talkId, paperId));
                                                            }
                                                        });
                                            }
                                        }
                                    });
                            }
                        });
                }
            );
    }

    private Speaker updateSpeakerReferences(Speaker speaker) {
        speaker.setCode(Speaker.calculateCode(speaker.getFullName(), speaker.getEmail()));
        speaker.setRef(Speaker.generateRef(speaker.getFullName(), speaker.getEmail()));
        return speaker;
    }

    @Override
    public List<Bson> buildUpdates(JsonObject data) {
        return null;
    }
}


