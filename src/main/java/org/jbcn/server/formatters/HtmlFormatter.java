package org.jbcn.server.formatters;

import org.jbcn.server.utils.StringUtils;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * NOTE: <p> around <ul> is not valid: https://html.spec.whatwg.org/multipage/grouping-content.html#the-p-element
 */
public class HtmlFormatter {

    private static final String UNORDERED_LIST_REGEX = "^\\s*[\\*|\\-]\\s*(.*)";

    private static final String P_START = "<p>";
    private static final String P_END = "</p>";
    private static final String LI_START = "<li>";
    private static final String LI_END = "</li>";
    private static final String UL_START = "<ul>";
    private static final String UL_END = "</ul>";


    public String format(String text) {
        final String[] lines = text.split("\n");

        final var preprocessedLines = new ArrayList<String>();
        boolean insideOL = false;
        for (String line : lines) {
            if (!StringUtils.isNullOrBlank(line)) {
                String candidate = formatLine(line.trim());
                if (candidate.startsWith(LI_START)) {
                    if (!insideOL) {
                        preprocessedLines.add(UL_START);
                        insideOL = true;
                    }
                } else {
                    if (insideOL) {
                        preprocessedLines.add(UL_END);
                        insideOL = false;
                    }
                }
                preprocessedLines.add(candidate);
            }
        }

        if (insideOL) {
            preprocessedLines.add(UL_END);
        }

        return preprocessedLines.stream()
            .collect(Collectors.joining());
    }

    private String formatLine(String line) {
        if (line.matches(UNORDERED_LIST_REGEX)) {
            return line.replaceAll(UNORDERED_LIST_REGEX, LI_START + "$1" + LI_END);
        }
        return P_START + line + P_END;
    }
}
