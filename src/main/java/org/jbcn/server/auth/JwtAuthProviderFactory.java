package org.jbcn.server.auth;

import io.vertx.core.Vertx;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;

public record JwtAuthProviderFactory(String algorithm, String secretKey, String publicKey) {

    public JWTAuth jwtAuthProvider(Vertx vertx) {
        return JWTAuth.create(vertx, new JWTAuthOptions()
            .addPubSecKey(new PubSecKeyOptions()
                .setAlgorithm(algorithm)
                .setPublicKey(publicKey)
                .setSecretKey(secretKey)
            ));
    }
}
