package labs;

import org.jbcn.server.mail.GoogleMailSender;
import org.jbcn.server.mail.MailSender;
import org.jbcn.server.mail.TemplateProcessor;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class EmailSenderLab {

    /**
     * To test:
     *
     * 1. Set env vars
     *   GOOGLE_OAUTH_TOKEN_URL
     *   GOOGLE_OAUTH_CLIENT_ID
     *   GOOGLE_OAUTH_REFRESH_TOKEN
     *   GOOGLE_OAUTH_SECRET
     *   TO_EMAIL
     *   BCC_EMAIL
     * 2. Enable mail with env var
     *   MAIL_ENABLED=true
     */
    public static void main(String[] args) {
        final TemplateProcessor templateProcessor = new TemplateProcessor();

        final String to = System.getenv("TO_EMAIL");
        final String bcc = System.getenv("BCC_EMAIL");

        final String headerImageUrl = "https://mcusercontent.com/2da0ac7d88ca2fe6a539f3d4a/images/5bcd9f53-eb15-08f4-85b6-9ac9e0a639fa.jpg";
        final String footerImageUrl = "https://gallery.mailchimp.com/2da0ac7d88ca2fe6a539f3d4a/images/d605550e-c4f4-42bf-8738-86bea57f941e.jpg";
        final Map<String, String> substitutions = Map.of(
            "header-image-url", headerImageUrl,
            "footer-image-url", footerImageUrl
        );

        List.of("/talk_approved_email.html", "/paper_received_email.html")
            .stream()
            .map(Path::of)
            .forEach(templatePath -> {
                final String content = templateProcessor.render(templatePath, substitutions);
                ((MailSender) new GoogleMailSender())
                    .sendEmail(to, bcc, "Test email: " + templatePath, content, "text/html");
            });
    }
}
