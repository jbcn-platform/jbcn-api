package org.jbcn.server.platform;

import org.jbcn.server.model.ScannedBadge;
import org.jbcn.server.model.scheduling.Room;
import org.jbcn.server.model.sponsor.JobOffer;
import org.jbcn.server.model.sponsor.Sponsor;
import org.jbcn.server.services.UrlRedirect;
import org.jbcn.server.utils.Pair;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

// TODO use parametrized when Junit5 is available
public class ModelHelperTest {

    final ModelHelper modelHelper = new ModelHelper();

    @Test
    public void should_get_updatable_fields_for_Sponsors() {
        final var clazz = Sponsor.class;

        var fields = modelHelper.getUpdatableFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder(
            Pair.of("name", String.class),
            Pair.of("description", String.class),
            Pair.of("level", String.class),
            Pair.of("links", Map.class)
        );
    }

    @Test
    public void should_get_required_fields_for_Rooms() {
        final var clazz = Room.class;

        var fields = modelHelper.getCreationRequiredFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder("name");
    }

    @Test
    public void should_get_updatable_fields_for_Rooms() {
        final var clazz = Room.class;

        var fields = modelHelper.getUpdatableFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder(
            Pair.of("name", String.class),
            Pair.of("capacity", Integer.class)
        );
    }

    @Test
    public void should_get_required_fields_for_UrlRedirect() {
        final var clazz = UrlRedirect.class;

        var fields = modelHelper.getCreationRequiredFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder("url", "name");
    }

    @Test
    public void should_get_updatable_fields_for_UrlRedirect() {
        final var clazz = UrlRedirect.class;

        var fields = modelHelper.getUpdatableFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder(
            Pair.of("url", String.class),
            Pair.of("name", String.class)
        );
    }

    @Test
    public void should_get_required_fields_for_JobOffers() {
        final var clazz = JobOffer.class;

        var fields = modelHelper.getCreationRequiredFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder("title", "description", "link", "sponsorId");
    }

    @Test
    public void should_get_updatable_fields_for_JobOffers() {
        final var clazz = JobOffer.class;

        var fields = modelHelper.getUpdatableFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder(
            Pair.of("title", String.class),
            Pair.of("description", String.class),
            Pair.of("link", String.class),
            Pair.of("sponsorId", String.class),
            Pair.of("location", String.class)
        );
    }

    @Ignore("Pending Badges mobile side reimplementation for 2022")
    @Test
    public void should_get_updatable_fields_for_ScannedBadge() {
        final var clazz = ScannedBadge.class;

        var fields = modelHelper.getUpdatableFields(clazz);

        assertThat(fields).containsExactlyInAnyOrder(
            Pair.of("details", String.class)
        );
    }
}
