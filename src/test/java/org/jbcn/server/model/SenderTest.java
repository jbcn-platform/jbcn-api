package org.jbcn.server.model;

import io.vertx.core.json.JsonObject;
import org.bson.Document;
import org.jbcn.server.model.compliance.DataConsent;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;

import java.util.HashMap;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static util.TestDataGenerator.getTestSender;

public class SenderTest {

    @Test
    public void should_convert_empty_Sender_to_mongodb_bson() {
        // given
        final Sender testSender = new Sender();
        // when
        final Document document = DocumentConverter.getDocument(testSender);
        // then
        assertThat(document.getString("fullName")).isNull();
        assertThat(document.getString("code")).isNull();
        assertThat(document.getString("email")).isNull();
        assertThat(document.getString("biography")).isNull();
        assertThat(document.getString("picture")).isNull();
        assertThat(document.getString("web")).isNull();
        assertThat(document.getString("twitter")).isNull();
        assertThat(document.getString("linkedin")).isNull();
        assertThat(document.getBoolean("travelCost")).isFalse();
        assertThat(document.getBoolean("attendeesParty")).isFalse();
        assertThat(document.getBoolean("speakersParty")).isFalse();
        assertThat(document.getString("tshirtSize")).isNull();
        assertThat(document.getString("allergies")).isNull();
        assertThat(document.getBoolean("starred")).isFalse();
        assertThat(document.get("dataConsent")).isNull();
    }

    @Test
    public void should_convert_Sender_with_data_consent_to_mongodb_bson() {
        // given
        final Sender testSender = Sender.builder()
            .dataConsent(DataConsent.consentAll())
            .build();
        // when
        final Document document = DocumentConverter.getDocument(testSender);
        // then
        assertThat(document.getString("fullName")).isNull();
        assertThat(document.getString("code")).isNull();
        assertThat(document.getString("email")).isNull();
        assertThat(document.getString("biography")).isNull();
        assertThat(document.getString("picture")).isNull();
        assertThat(document.getString("web")).isNull();
        assertThat(document.getString("twitter")).isNull();
        assertThat(document.getString("linkedin")).isNull();
        assertThat(document.getBoolean("travelCost")).isFalse();
        assertThat(document.getBoolean("attendeesParty")).isFalse();
        assertThat(document.getBoolean("speakersParty")).isFalse();
        assertThat(document.getString("tshirtSize")).isNull();
        assertThat(document.getString("allergies")).isNull();
        assertThat(document.getBoolean("starred")).isFalse();
        final Document dataConsent = (Document) document.get("dataConsent");
        assertThat(dataConsent.getBoolean("identification")).isTrue();
        assertThat(dataConsent.getBoolean("contact")).isTrue();
        assertThat(dataConsent.getBoolean("financial")).isTrue();
    }

    @Test
    public void should_convert_Sender_to_mongodb_bson() {
        // given
        final Sender testSender = getTestSender();
        // when
        final Document document = DocumentConverter.getDocument(testSender);
        // then
        assertThat(document.getString("fullName")).startsWith("Java Rockstar ");
        assertThat(document.getString("fullName").length()).isEqualTo(46);
        assertThat(document.getString("code")).isNotBlank();
        assertThat(document.getString("email")).matches("email(.){32}@barcelonajug.org");
        assertThat(document.getString("biography")).isEqualTo("Stop talking, brain thinking.\nHush. I am the Doctor, and you are the Daleks!\n\nThe way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.");
        assertThat(document.getString("picture")).isEqualTo("https://trololo.com");
        assertThat(document.getString("web")).isEqualTo("https://whatever.com");
        assertThat(document.getString("twitter")).isEqualTo("twitter");
        assertThat(document.getString("linkedin")).isEqualTo("https://www.linkedin.com/in/something-here");
        assertThat(document.getBoolean("travelCost")).isFalse();
        assertThat(document.getBoolean("attendeesParty")).isFalse();
        assertThat(document.getBoolean("speakersParty")).isFalse();
        assertThat(document.getString("tshirtSize")).isEqualTo("XXL");
        assertThat(document.getString("allergies")).isEqualTo("cheap wine, bad beer");
        assertThat(document.getBoolean("starred")).isFalse();
        final Document dataConsent = (Document) document.get("dataConsent");
        assertThat(dataConsent.getBoolean("identification")).isTrue();
        assertThat(dataConsent.getBoolean("contact")).isTrue();
        assertThat(dataConsent.getBoolean("financial")).isTrue();
    }

    @Test
    public void should_convert_Sender_from_mongodb_bson() {
        // given
        final String fullName = "This is the full name";
        final String email = "email@whatever.com";
        final String bio = "I am cool!!";
        final String pictureUrl = "https://cool-pics.org/me";
        final String web = "https://my/web.com";
        final String twitterId = "myTwitterId";
        final String linkedinUrl = "https://www.linkedin.com/es/something-here";
        final String allergies = "bad wine, cheap beer";
        final String tshirtSize = "XL";
        final Document testSender = new Document()
            .append("fullName", fullName)
            .append("email", email)
            .append("biography", bio)
            .append("picture", pictureUrl)
            .append("web", web)
            .append("twitter", twitterId)
            .append("linkedin", linkedinUrl)
            .append("starred", true)
            .append("allergies", allergies)
            .append("travelCost", true)
            .append("attendeesParty", false)
            .append("speakersParty", true)
            .append("tshirtSize", tshirtSize)
            .append("dataConsent", new HashMap() {{
                put("identification", true);
                put("contact", true);
                put("financial", true);
            }});
        // when
        final Sender sender = DocumentConverter.parseToObject(testSender, Sender.class);
        // then
        assertThat(sender.getFullName()).isEqualTo(fullName);
        assertThat(sender.getEmail()).isEqualTo(email);
        assertThat(sender.getBiography()).isEqualTo(bio);
        assertThat(sender.getPicture()).isEqualTo(pictureUrl);
        assertThat(sender.getWeb()).isEqualTo(web);
        assertThat(sender.getTwitter()).isEqualTo(twitterId);
        assertThat(sender.getLinkedin()).isEqualTo(linkedinUrl);
        assertThat(sender.isStarred()).isTrue();
        assertThat(sender.isTravelCost()).isTrue();
        assertThat(sender.isAttendeesParty()).isFalse();
        assertThat(sender.isSpeakersParty()).isTrue();
        assertThat(sender.getTshirtSize()).isEqualTo(tshirtSize);
        assertThat(sender.getAllergies()).isEqualTo(allergies);
        assertThat(sender.getDataConsent().isIdentification()).isTrue();
        assertThat(sender.getDataConsent().isContact()).isTrue();
        assertThat(sender.getDataConsent().isFinancial()).isTrue();
    }

    @Test
    public void should_convert_Sender_from_vertx_json() {
        // given
        final JsonObject testSender = JsonObject.mapFrom(getTestSender(""));
        // when
        final Sender sender = DocumentConverter.parseToObject(testSender, Sender.class);
        // then
        assertThat(sender.getFullName()).isEqualTo("Java Rockstar ");
        assertThat(sender.getEmail()).isEqualTo("email@barcelonajug.org");
        assertThat(sender.getBiography()).isEqualTo("Stop talking, brain thinking.\nHush. I am the Doctor, and you are the Daleks!\n\nThe way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.");
        assertThat(sender.getPicture()).isEqualTo("https://trololo.com");
        assertThat(sender.getWeb()).isEqualTo("https://whatever.com");
        assertThat(sender.getTwitter()).isEqualTo("twitter");
        assertThat(sender.getLinkedin()).isEqualTo("https://www.linkedin.com/in/something-here");
        assertThat(sender.isTravelCost()).isFalse();
        assertThat(sender.isAttendeesParty()).isFalse();
        assertThat(sender.isSpeakersParty()).isFalse();
        assertThat(sender.getTshirtSize()).isEqualTo("XXL");
        assertThat(sender.getAllergies()).isEqualTo("cheap wine, bad beer");
        assertThat(sender.isStarred()).isFalse();
        assertThat(sender.getDataConsent().isIdentification()).isTrue();
        assertThat(sender.getDataConsent().isContact()).isTrue();
        assertThat(sender.getDataConsent().isFinancial()).isTrue();
        assertNotNull(sender.getCode());
    }

    @Test
    public void should_convert_Sender_from_empty_vertx_json() {
        // given
        final JsonObject testSender = new JsonObject();
        // when
        final Sender sender = DocumentConverter.parseToObject(testSender, Sender.class);
        final String code = "123456789";
        sender.setCode(code);
        // then
        assertThat(sender.getFullName()).isNull();
        assertThat(sender.getEmail()).isNull();
        assertThat(sender.getBiography()).isNull();
        assertThat(sender.getPicture()).isNull();
        assertThat(sender.getWeb()).isNull();
        assertThat(sender.getTwitter()).isNull();
        assertThat(sender.getLinkedin()).isNull();
        assertThat(sender.isTravelCost()).isFalse();
        assertThat(sender.isAttendeesParty()).isFalse();
        assertThat(sender.isSpeakersParty()).isFalse();
        assertThat(sender.getTshirtSize()).isNull();
        assertThat(sender.getAllergies()).isNull();
        assertThat(sender.isStarred()).isFalse();
        assertThat(sender.getDataConsent()).isNull();
        // TODO think what should be the default value
        assertThat(sender.getCode()).isEqualTo(code);
    }
}
