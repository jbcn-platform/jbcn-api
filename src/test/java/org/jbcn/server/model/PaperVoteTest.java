package org.jbcn.server.model;

import org.bson.Document;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class PaperVoteTest {

    @Test
    public void should_convert_empty_paperVote_to_mongodb_document() {
        // given
        final PaperVote vote = PaperVote.builder()
                .build();
        // when
        final Document doc = DocumentConverter.getDocument(vote);
        // then
        assertThat(doc.keySet()).containsExactlyInAnyOrder("userId", "vote", "date", "username");
        assertThat(doc.getString("userId")).isNull();
        assertThat(doc.getDouble("vote")).isNull();
        assertThat(doc.getString("username")).isNull();
        assertThat(doc.getLong("date")).isNull();
    }

    @Test
    public void should_convert_full_paperVote_to_mongodb_document() {
        // given
        final Date now = new Date();
        final PaperVote vote = PaperVote.builder()
                .userId("some_user_id")
                .vote(1.2d)
                .date(now)
                .username("some_username")
                .build();

        // when
        final Document doc = DocumentConverter.getDocument(vote);
        // then
        assertThat(doc.keySet()).containsExactlyInAnyOrder("userId", "vote", "date", "username");
        assertThat(doc.getString("userId")).isEqualTo("some_user_id");
        assertThat(doc.getDouble("vote")).isEqualTo(1.2d);
        assertThat(doc.getString("username")).isEqualTo("some_username");
        assertThat(doc.getLong("date")).isEqualTo(now.getTime());
    }

}
