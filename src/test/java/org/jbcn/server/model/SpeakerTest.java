package org.jbcn.server.model;

import org.junit.Test;
import util.TestDataGenerator;

import static org.assertj.core.api.Assertions.assertThat;


public class SpeakerTest {

    @Test
    public void should_convert_empty_sender_to_speaker() {
        // given
        final Sender sender = new Sender();
        // when
        final Speaker speaker = Speaker.fromSender(sender);
        // then
        assertThat(speaker.getFullName()).isNull();
        // TODO think what should be default code, maybe exception?
        assertThat(speaker.getCode()).isEmpty();
        assertThat(speaker.getEmail()).isNull();
        assertThat(speaker.getBiography()).isNull();
        assertThat(speaker.getPicture()).isNull();
        assertThat(speaker.getWeb()).isNull();
        assertThat(speaker.getTwitter()).isNull();
        assertThat(speaker.getLinkedin()).isNull();
        assertThat(speaker.isTravelCost()).isFalse();
        assertThat(speaker.isAttendeesParty()).isFalse();
        assertThat(speaker.isSpeakersParty()).isFalse();
        assertThat(speaker.getTshirtSize()).isNull();
        assertThat(speaker.getAllergies()).isNull();
        assertThat(speaker.isStarred()).isFalse();
        assertThat(speaker.getDataConsent()).isNull();
    }

    @Test
    public void should_convert_sender_to_speaker() {
        // given
        final Sender sender = TestDataGenerator.getTestSender("-123");
        // when
        final Speaker speaker = Speaker.fromSender(sender);
        //
        assertThat(speaker.getFullName()).isEqualTo("Java Rockstar -123");
        assertThat(speaker.getEmail()).matches("email-123@barcelonajug.org");
        assertThat(speaker.getBiography()).isEqualTo("Stop talking, brain thinking.\nHush. I am the Doctor, and you are the Daleks!\n\nThe way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.");
        assertThat(speaker.getPicture()).isEqualTo("https://trololo.com");
        assertThat(speaker.getWeb()).isEqualTo("https://whatever.com");
        assertThat(speaker.getTwitter()).isEqualTo("twitter");
        assertThat(speaker.getLinkedin()).isEqualTo("https://www.linkedin.com/in/something-here");
        assertThat(speaker.isTravelCost()).isFalse();
        assertThat(speaker.isAttendeesParty()).isFalse();
        assertThat(speaker.isSpeakersParty()).isFalse();
        assertThat(speaker.getTshirtSize()).isEqualTo("XXL");
        assertThat(speaker.getAllergies()).isEqualTo("cheap wine, bad beer");
        assertThat(speaker.isStarred()).isFalse();
        assertThat(speaker.getDataConsent().isIdentification()).isTrue();
        assertThat(speaker.getDataConsent().isContact()).isTrue();
        assertThat(speaker.getDataConsent().isFinancial()).isTrue();
        assertThat(speaker.getCode()).isEqualTo("JavaRockstar-123email-123@barcelonajug.org");
        // this is a reminder to review this test if new fields are added or removed
        assertThat(Sender.class.getDeclaredFields().length).isEqualTo(17);
    }

    @Test
    public void should_generate_code_for_null_values() {
        String code = Speaker.calculateCode(null, null);
        assertThat(code).isEmpty();
    }

    @Test
    public void should_generate_code_for_empty_name() {
        String code = Speaker.calculateCode("", null);
        assertThat(code).isEmpty();
    }

    @Test
    public void should_generate_code_for_empty_email() {
        String code = Speaker.calculateCode(null, "");
        assertThat(code).isEmpty();
    }

    @Test
    public void should_generate_code_for_empty_inputs() {
        String code = Speaker.calculateCode("", "");
        assertThat(code).isEmpty();
    }

    @Test
    public void should_generate_code() {
        String code = Speaker.calculateCode("full_name with spaces", "myMayl123@domain.com");
        assertThat(code).isEqualTo("full_namewithspacesmyMayl123@domain.com");
    }

}
