package org.jbcn.server.mobile;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.Talk;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import static org.jbcn.server.persistence.Collections.TALK;
import static org.jbcn.server.persistence.Collections.TRACKS;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.insertToDatabase;


@RunWith(VertxUnitRunner.class)
public class ConferencesHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/public/mobile/conferences";

    @Test
    public void should_get_conference_information(TestContext context) {
        // given
        final String track1Id = insertToDatabase(TRACKS, DocumentConverter.getDocument(new Track("Tag_1", "First track")));
        final String track2Id = insertToDatabase(TRACKS, DocumentConverter.getDocument(new Track("Tag_2", "Second track")));
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        // when-then
        final Async async = context.async();
        webClient.get(RESOURCE)
                .send(assertJsonResponse(async, context, 200, data -> {
                    context.assertEquals(data.getString("id"), "1");
                    context.assertEquals(data.getString("edition"), JbcnProperties.getCurrentEdition());

                    JsonArray tracks = data.getJsonArray("tracks");
                    context.assertEquals(tracks.size(), 2);
                    JsonObject track1 = tracks.getJsonObject(0);
                    context.assertEquals(track1.fieldNames().size(), 3);
                    context.assertEquals(track1.getString("name"), "Tag_1");
                    context.assertEquals(track1.getString("description"), "First track");
                    context.assertEquals(track1.getString("_id"), track1Id);
                    JsonObject track2 = tracks.getJsonObject(1);
                    context.assertEquals(track2.fieldNames().size(), 3);
                    context.assertEquals(track2.getString("name"), "Tag_2");
                    context.assertEquals(track2.getString("description"), "Second track");
                    context.assertEquals(track2.getString("_id"), track2Id);

                    JsonArray tags = data.getJsonArray("tags");
                    context.assertEquals(tags.size(), 3);
                    JsonObject tag1 = tags.getJsonObject(0);
                    context.assertEquals(tag1.fieldNames().size(), 2);
                    context.assertEquals(tag1.getString("value"), "tag1");
                    context.assertEquals(tag1.getInteger("count"), 1);
                    JsonObject tag2 = tags.getJsonObject(1);
                    context.assertEquals(tag2.fieldNames().size(), 2);
                    context.assertEquals(tag2.getString("value"), "tag2");
                    context.assertEquals(tag2.getInteger("count"), 1);
                    JsonObject tag3 = tags.getJsonObject(2);
                    context.assertEquals(tag3.fieldNames().size(), 2);
                    context.assertEquals(tag3.getString("value"), "tag3");
                    context.assertEquals(tag3.getInteger("count"), 1);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


}

