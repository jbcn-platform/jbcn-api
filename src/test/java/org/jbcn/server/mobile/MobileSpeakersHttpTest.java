package org.jbcn.server.mobile;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;
import java.util.stream.Collectors;

import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getSpeaker;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.insertToDatabase;


@RunWith(VertxUnitRunner.class)
public class MobileSpeakersHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/public/mobile/speakers";

    @Test
    public void should_get_speakers(TestContext context) {
        // given
        Speaker speaker1 = getSpeaker();
        Speaker speaker2 = getSpeaker();
        Speaker speaker3 = getSpeaker();

        Talk testTalk1 = getTestTalk();
        testTalk1.setPublished(Boolean.TRUE);
        testTalk1.setSpeakers(List.of(speaker1));
        String testTalk1Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk1));

        Talk testTalk2 = getTestTalk();
        testTalk2.setPublished(Boolean.TRUE);
        testTalk2.setSpeakers(List.of(speaker1));
        String testTalk2Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk2));

        Talk testTalk3 = getTestTalk();
        testTalk3.setPublished(Boolean.TRUE);
        testTalk3.setSpeakers(List.of(speaker3));
        String testTalk3Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk3));

        // should not appear
        Talk testTalk4 = getTestTalk();
        testTalk4.setSpeakers(List.of(speaker3));
        String testTalk4Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk4));

        // when-then
        final Async async = context.async();
        webClient.get(RESOURCE)
                .send(assertArrayResponse(async, context, 200, body -> {
                    context.assertEquals(2, body.size());

                    final List<String> speakersIds = body.stream().map(json -> ((JsonObject) json).getString("uuid")).collect(Collectors.toList());
                    context.assertTrue(speakersIds.contains(speaker1.getRef()));
                    context.assertTrue(speakersIds.contains(speaker3.getRef()));

                    JsonObject speakerJson1 = (JsonObject) body.stream().filter(json -> ((JsonObject) json).getString("uuid").equals(speaker1.getRef())).findFirst().get();
                    context.assertEquals(2, speakerJson1.getJsonArray("acceptedTalks").size());
                    List<String> talksIds1 = speakerJson1.getJsonArray("acceptedTalks").stream().map(t -> ((JsonObject) t).getString("id")).collect(Collectors.toList());
                    context.assertTrue(talksIds1.contains(testTalk1Id));
                    context.assertTrue(talksIds1.contains(testTalk2Id));

                    JsonObject speakerJson3 = (JsonObject) body.stream().filter(json -> ((JsonObject) json).getString("uuid").equals(speaker3.getRef())).findFirst().get();
                    context.assertEquals(1, speakerJson3.getJsonArray("acceptedTalks").size());
                    List<String> talksIds3 = speakerJson3.getJsonArray("acceptedTalks").stream().map(t -> ((JsonObject) t).getString("id")).collect(Collectors.toList());
                    context.assertTrue(talksIds3.contains(testTalk3Id));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertSpeaker(TestContext context, JsonObject speaker) {
        context.assertTrue(speaker.containsKey("name"));
        context.assertTrue(speaker.containsKey("link"));
        final JsonObject speakerLinks = speaker.getJsonObject("link");
        context.assertTrue(speakerLinks.containsKey("href"));
        context.assertTrue(speakerLinks.containsKey("rel"));
        context.assertTrue(speakerLinks.containsKey("title"));
    }


}

