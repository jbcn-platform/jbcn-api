package org.jbcn.server.mobile;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConferenceUriBuilderTest {

    @Test
    public void should_build_simple_conference_speakers_uri() {
        final String speakers = new ConferenceUriBuilder()
                .append("speakers")
                .build();

        assertThat(speakers).isEqualTo("https://jbcnapi.cleverapps.io/speakers");
    }

    @Test
    public void should_build_composed_conference_speakers_uri() {
        final String speakers = new ConferenceUriBuilder()
                .append("speakers")
                .append("123456789")
                .build();

        assertThat(speakers).isEqualTo("https://jbcnapi.cleverapps.io/speakers/123456789");
    }

    @Test
    public void should_remove_slash_prefix_from_section() {
        final String speakers = new ConferenceUriBuilder()
                .append("speakers")
                .append("/123456789")
                .build();

        assertThat(speakers).isEqualTo("https://jbcnapi.cleverapps.io/speakers/123456789");
    }

    @Test
    public void should_remove_slash_sufix_from_section() {
        final String speakers = new ConferenceUriBuilder()
                .append("speakers/")
                .append("/123456789")
                .build();

        assertThat(speakers).isEqualTo("https://jbcnapi.cleverapps.io/speakers/123456789");
    }

}
