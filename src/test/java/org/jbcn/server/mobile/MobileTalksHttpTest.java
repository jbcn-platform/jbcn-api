package org.jbcn.server.mobile;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Talk;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;
import java.util.stream.Collectors;

import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.insertToDatabase;


@RunWith(VertxUnitRunner.class)
public class MobileTalksHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/public/mobile/talks";

    @Test
    public void should_get_published_talks(TestContext context) {
        // given
        Talk testTalk = getTestTalk();
        final String talk4Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        testTalk.setPublished(Boolean.TRUE);
        final String talk1Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String talk2Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String talk3Id = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        // when-then
        final Async async = context.async();
        webClient.get(RESOURCE)
                .send(assertArrayResponse(async, context, 200, body -> {
                    context.assertEquals(3, body.size());

                    List<String> ids = body.stream().map(json -> ((JsonObject) json).getString("id")).collect(Collectors.toList());
                    context.assertFalse(ids.contains(talk4Id));

                    final JsonObject firstTalk = body.getJsonObject(0);
                    context.assertEquals(10, firstTalk.fieldNames().size());
                    context.assertTrue(firstTalk.containsKey("id"));
                    context.assertTrue(firstTalk.containsKey("title"));
                    context.assertTrue(firstTalk.containsKey("published"));
                    context.assertTrue(firstTalk.containsKey("talkType"));
                    context.assertTrue(firstTalk.containsKey("lang"));
                    context.assertTrue(firstTalk.containsKey("audienceLevel"));
                    context.assertTrue(firstTalk.containsKey("summary"));
                    context.assertTrue(firstTalk.containsKey("tags"));
                    context.assertTrue(firstTalk.containsKey("speakers"));
                    context.assertTrue(firstTalk.containsKey("speakersDetails"));

                    context.assertEquals(3, firstTalk.getJsonArray("tags").size());

                    final JsonArray speakers = firstTalk.getJsonArray("speakers");
                    context.assertEquals(1, speakers.size());
                    assertSpeaker(context, speakers.getJsonObject(0));

                    final JsonArray speakersDetails = firstTalk.getJsonArray("speakersDetails");
                    context.assertEquals(1, speakersDetails.size());
                    final JsonObject speakerDetails = speakersDetails.getJsonObject(0);
                    context.assertTrue(speakerDetails.containsKey("uuid"));
                    context.assertTrue(speakerDetails.containsKey("bio"));
                    context.assertTrue(speakerDetails.containsKey("fullName"));
                    context.assertTrue(speakerDetails.containsKey("firstName"));
                    context.assertTrue(speakerDetails.containsKey("lastName"));
                    context.assertTrue(speakerDetails.containsKey("avatarURL"));
                    context.assertTrue(speakerDetails.containsKey("company"));
                    context.assertTrue(speakerDetails.containsKey("blog"));
                    context.assertTrue(speakerDetails.containsKey("twitter"));
                    context.assertTrue(speakerDetails.containsKey("lang"));
                    context.assertTrue(speakerDetails.containsKey("links"));
                    context.assertTrue(speakerDetails.containsKey("summary"));

                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertSpeaker(TestContext context, JsonObject speaker) {
        context.assertTrue(speaker.containsKey("name"));
        context.assertTrue(speaker.containsKey("link"));
        final JsonObject speakerLinks = speaker.getJsonObject("link");
        context.assertTrue(speakerLinks.containsKey("href"));
        context.assertTrue(speakerLinks.containsKey("rel"));
        context.assertTrue(speakerLinks.containsKey("title"));
    }


}

