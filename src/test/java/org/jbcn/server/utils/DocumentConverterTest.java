package org.jbcn.server.utils;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jbcn.server.config.JacksonConfiguration;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.*;
import org.jbcn.server.model.compliance.DataConsent;
import org.jbcn.server.model.jackson.LocalDateTimeJsonSerializer;
import org.jbcn.server.model.json.LocalDateTimeDeserializer;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jbcn.server.model.Traceable.*;

public class DocumentConverterTest {

    @BeforeClass
    public static void before() {
        JacksonConfiguration.initializeJacksonMapper();
    }

    @Test
    public void should_convert_an_empty_paper_to_bson() {
        // given
        final Paper paper = Paper.builder()
            .build();
        // when
        final Document bson = DocumentConverter.getDocument(paper);
        // then
        // -1 because converts all but `_id`
        assertThat(bson.keySet()).hasSize(getPrivateNonStaticFields(Paper.class).size() - 1);
        final long nonNullValues = bson.values().stream()
            .filter(v -> v != null)
            .count();
        // averageVote, votesCount
        assertThat(nonNullValues).isEqualTo(2L);
        assertThat(bson.containsValue("paperAbstract")).isFalse();
    }

    @Test
    public void should_convert_paper_with_feedback_bson_to_object() {
        final long now = TimeUtils.currentTime();
        final Document feedback = new Document()
            .append("text", "da feedback!")
            .append(CREATED_DATE, now)
            .append(LAST_UPDATE, now)
            .append(LAST_ACTION_BY, "da_user");
        final Document paper = new Document()
            .append("title", "the title")
            .append("abstract", "the abstract")
            .append("feedback", feedback);

        final Paper instance = DocumentConverter.parseToObject(paper, Paper.class);

        assertThat(instance.getTitle()).isEqualTo("the title");
        assertThat(instance.getPaperAbstract()).isEqualTo("the abstract");
        assertThat(instance.getFeedback()).isNotNull();
        assertThat(instance.getFeedback().getText()).isEqualTo("da feedback!");
        assertThat(instance.getFeedback().getCreatedDate()).isNotNull();
        assertThat(instance.getFeedback().getLastUpdate()).isNotNull();
    }

    @Test
    public void should_convert_an_empty_note_to_bson() {
        // given
        final Note note = new Note(null, null, null, null, null, null, null);
        // when
        final Document bson = DocumentConverter.getDocument(note);
        // then
        // -1 because converts all but `_id`
        assertThat(bson.values()).hasSize(Note.class.getDeclaredFields().length - 1);
        final long nonNullValues = bson.values().stream()
            .filter(v -> v != null)
            .count();
        assertThat(nonNullValues).isEqualTo(0L);
    }

    @Test
    public void should_convert_a_note_from_jsonObject() {
        // given
        final String owner = "that-s-me";
        final String text = "some fancy text";
        final long now = System.currentTimeMillis();
        final JsonObject note = new JsonObject()
            .put("owner", owner)
            .put("text", text)
            .put(CREATED_DATE, now)
            .put("in-valid", "never mind");
        // when
        final Note instance = DocumentConverter.parseToObject(note, Note.class);
        // then
        assertThat(instance.getOwner()).isSameAs(owner);
        assertThat(instance.getText()).isSameAs(text);
        assertThat(instance.getCreatedDate().atZone(ZoneId.of("Z")).toInstant().toEpochMilli()).isEqualTo(now);
        assertThat(instance.getLastUpdate()).isNull();
    }

    @Test
    public void should_ignore_unknown_fields_when_converting_from_jsonObject() {
        // given
        final String title = "Paper title!";
        final long currentTime = System.currentTimeMillis();
        final JsonObject note = new JsonObject()
            .put("title", title)
            .put(CREATED_DATE, currentTime)
            .put("Non-existing property", "something");
        // when
        final Paper instance = DocumentConverter.parseToObject(note, Paper.class);
        // then
        assertThat(instance.getCreatedDate().getTime()).isEqualTo(currentTime);
        assertThat(instance.getTitle()).isEqualTo(title);
    }

    @Test
    public void should_convert_an_empty_sender_to_bson() {
        // given
        final Sender note = Sender.builder()
            .build();
        // when
        final Document bson = DocumentConverter.getDocument(note);
        // then
        assertThat(bson.values()).hasSize(getPrivateNonStaticFields(Sender.class).size());
        final long nonNullValues = bson.values().stream()
            .filter(v -> v != null)
            .count();
        // boolean values
        assertThat(nonNullValues).isEqualTo(4L);
    }

    @Test
    public void should_convert_an_empty_paperVote_to_bson() {
        // given
        final PaperVote note = PaperVote.builder()
            .build();
        // when
        final Document bson = DocumentConverter.getDocument(note);
        // then
        assertThat(bson.keySet()).hasSize(getPrivateNonStaticFields(PaperVote.class).size());
        final long nonNullValues = bson.values().stream()
            .filter(v -> v != null)
            .count();
        assertThat(nonNullValues).isEqualTo(0L);
    }

    @Test
    public void should_convert_a_non_empty_sender_to_bson() {
        // given
        final Sender paper = Sender.builder()
            .fullName("Sender fullname")
            .jobTitle("Great speaker")
            .code("something?")
            .company("SuperCompany")
            .picture("https://amazing.picture/just/here.png")
            .biography("Somewhere\nfar beyond")
            .email("sender@emails.com")
            .web("https://my.web.com")
            .twitter("@mememe")
            .linkedin("https://www.linkedin.com/feed/")
            .tshirtSize("L")
            .travelCost(false)
            .speakersParty(true)
            .starred(false)
            .allergies("None, that I know")
            .dataConsent(DataConsent.consentAll())
            .build();
        // when
        Document bson = DocumentConverter.getDocument(paper);
        // then
        assertThat(bson.values()).hasSize(Sender.class.getDeclaredFields().length);
        final long nonNullValues = bson.values().stream()
            .filter(v -> v != null)
            .count();
        //
        assertThat(nonNullValues).isEqualTo(Long.valueOf(Sender.class.getDeclaredFields().length));
        assertValues(paper, bson, 0);
    }

    @Test
    public void should_convert_a_Talk_with_LocalDateTime_to_bson() {
        // given
        final Talk talk = Talk.builder()
            .createdDate(LocalDateTime.now())
            .lastUpdate(LocalDateTime.now())
            .build();
        // when
        final Document bson = DocumentConverter.getDocument(talk);
        // then
        int privateFields = getPrivateNonStaticFields(Talk.class).size();
        // -1 because converts all but `_id`
        assertThat(bson.keySet()).hasSize(privateFields - 1);
        final List<Object> nonNullKeys = bson.entrySet().stream()
            .filter(entry -> entry.getValue() != null)
            .map(entry -> entry.getKey())
            .collect(Collectors.toList());
        // totalVote, createdDate, lastUpdate
        assertThat(nonNullKeys).containsExactlyInAnyOrder("totalVote", CREATED_DATE, LAST_UPDATE);
        assertThat(nonNullKeys).hasSize(3);
        // LocalDateTime are converted to Epoch long
        assertThat(bson.getLong(CREATED_DATE)).isGreaterThan(0L);
        assertThat(bson.getLong(LAST_UPDATE)).isGreaterThan(0L);
        assertThat(bson.getBoolean("sponsor")).isNull();
        assertThat(bson.getDouble("totalVote")).isEqualTo(Double.parseDouble("0.0"));
        assertThat(bson.getBoolean("published")).isNull();
        assertValues(talk, bson, 0);
    }

    private List<Field> getPrivateNonStaticFields(final Class clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
            .filter(f -> Modifier.isPrivate(f.getModifiers()) && !Modifier.isStatic(f.getModifiers()))
            .collect(Collectors.toList());
    }

    @Test
    public void should_convert_a_non_empty_paper_to_bson() {
        // given
        final Paper paper = Paper.builder()
            .edition(JbcnProperties.getCurrentEdition())
            .title("Amazing talk incoming")
            .paperAbstract("this is an abstract")
            .type("Talk")
            .level("medium")
            .comments("I\nam\n\tcool!")
            .preferenceDay("")
            .state(PaperState.PAPER_STATE_SENT)
            .tags(Arrays.asList("Java", "Groovy", "Kotlin"))
            .votes(Arrays.asList(
                PaperVote.builder()
                    .userId("user_1")
                    .date(new Date())
                    .username("User One")
                    .vote(12d)
                    .build(),
                PaperVote.builder()
                    .userId("user_2")
                    .date(new Date())
                    .username("User Two")
                    .vote(16d)
                    .build()
            ))
            .senders(Arrays.asList(Sender.builder()
                .fullName("Amazing rock star developer")
                .biography("This is my life")
                .picture("https://mydomain.com/my/picture.jpg")
                .speakersParty(true)
                .attendeesParty(false)
                .travelCost(false)
                .tshirtSize("XL")
                .allergies("Only to cheap food")
                .build()))
            .createdDate(new Date())
            .build();

        // when
        Document bson = DocumentConverter.getDocument(paper);
        // then
        // enum PaperState is converted to String
        assertThat(bson.getString("state")).isEqualTo("sent");
        assertThat(bson.keySet()).hasSize(getPrivateNonStaticFields(Paper.class).size() - 1);
        assertValues(paper, bson, 0);
    }

    @SneakyThrows
    private void assertValues(Object source, Document target, int level) {
        for (Field field : getPrivateNonStaticFields(source.getClass())) {
            field.setAccessible(true);
            // Special cases first
            if (field.getName().equals("paperAbstract")) {
                assertThat(field.get(source)).isEqualTo(target.getString("abstract"));
            } else {
                if (field.getType().equals(String.class)) {
                    assertThat(target.get(field.getName())).isEqualTo(field.get(source));
                } else if (field.getType().equals(Date.class)) {
                    if (field.get(source) != null)
                        assertThat(target.getLong(field.getName())).isEqualTo(((Date) field.get(source)).getTime());
                    else
                        assertThat(target.get(field.getName())).isNull();
                } else if (field.getType().equals(boolean.class)) {
                    assertThat(target.get(field.getName())).isEqualTo(field.getBoolean(source));
                } else if (field.getType().equals(float.class)) {
                    assertThat(target.get(field.getName())).isEqualTo(field.getDouble(source));
                } else if (field.getType().isAssignableFrom(List.class)) {
                    if (isEmptyList(source, field)) {
                        // skip
                    } else if (!isEmptyList(source, field) && isStringList(source, field)) {
                        final List<String> actual = (List<String>) field.get(source);
                        assertThat(actual).containsExactlyElementsOf(actual::iterator);
                    } else if (((List) field.get(source)).size() == 1) {
                        assertThat(((List) target.get(field.getName()))).hasSize(1);
                        assertValues(((List) field.get(source)).get(0), (Document) ((List) target.get(field.getName())).get(0), level + 1);
                    } else {
                        // NOTE: converter seems to preserves order
                        final List values2 = (List) field.get(source);
                        for (int i = 0; i < values2.size(); i++) {
                            assertValues(((List) field.get(source)).get(i),
                                (Document) ((List) target.get(field.getName())).get(i), level + 1);
                        }
                    }
                }
            }
        }
    }

    private boolean isStringList(Object source, Field field) throws IllegalAccessException {
        return ((List) field.get(source)).get(0).getClass().equals(String.class);
    }

    private boolean isEmptyList(Object source, Field field) throws IllegalAccessException {
        final List list = (List) field.get(source);
        return list == null || list.size() == 0;
    }

    @Test
    public void should_convert_array_to_json() {
        // given
        final List<PaperVote> notes = new ArrayList<PaperVote>();
        notes.add(new PaperVote("111", "222", 34d, new Date()));
        notes.add(new PaperVote("333", "4444", 43d, new Date()));

        // when
        JsonArray values = DocumentConverter.toJson(notes);

        // then
        assertThat(values).hasSize(2);
        assertThat(values).allSatisfy(paperVote -> {
            assertThat(((JsonObject) paperVote).fieldNames()).containsExactlyInAnyOrder("userId", "username", "vote", "date");
        });
    }

    @Test
    public void should_convert_timestamps_back_and_forth() {
        final long now = TimeUtils.currentTime();
        final JsonObject testJsonInstance = new JsonObject()
            .put(CREATED_DATE, now);

        final TestInstance testInstance = DocumentConverter.parseToObject(testJsonInstance, TestInstance.class);

        // TimeUtils is used to set audit fields
        assertThat(testInstance.createdDate).isEqualTo(TimeUtils.toLocalDateTime(now));
    }

    static class TestInstance {
        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeJsonSerializer.class)
        private LocalDateTime createdDate;
    }
}
