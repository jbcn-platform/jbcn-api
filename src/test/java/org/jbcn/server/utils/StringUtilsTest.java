package org.jbcn.server.utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StringUtilsTest {

    @Test
    public void should_detect_null_string() {
        // given
        final String text = null;
        // when
        Boolean nullOrBlank = StringUtils.isNullOrBlank(text);
        // then
        assertThat(nullOrBlank).isTrue();
    }

    @Test
    public void should_detect_empty_string() {
        // given
        final String text = "";
        // when
        Boolean nullOrBlank = StringUtils.isNullOrBlank(text);
        // then
        assertThat(nullOrBlank).isTrue();
    }

    @Test
    public void should_detect_blank_string_with_spaces() {
        // given
        final String text = "  ";
        // when
        Boolean nullOrBlank = StringUtils.isNullOrBlank(text);
        // then
        assertThat(nullOrBlank).isTrue();
    }

    @Test
    public void should_detect_blank_string_with_tabs() {
        // given
        final String text = " \t \t";
        // when
        Boolean nullOrBlank = StringUtils.isNullOrBlank(text);
        // then
        assertThat(nullOrBlank).isTrue();
    }

    @Test
    public void should_detect_blank_string_with_a_linebreak() {
        // given
        final String text = "\n";
        // when
        Boolean nullOrBlank = StringUtils.isNullOrBlank(text);
        // then
        assertThat(nullOrBlank).isTrue();
    }

    @Test
    public void should_detect_valid_string() {
        // given
        final String text = "some real text";
        // when
        Boolean nullOrBlank = StringUtils.isNullOrBlank(text);
        // then
        assertThat(nullOrBlank).isFalse();
    }
}
