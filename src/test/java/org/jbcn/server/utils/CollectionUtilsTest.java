package org.jbcn.server.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CollectionUtilsTest {

    @Test
    public void should_detect_list_with_null_values() {
        // given
        final List<String> values = new ArrayList<>();
        values.add(null);
        // when
        Boolean nullOrBlank = CollectionUtils.containsNotBlankValues(values);
        // then
        assertThat(nullOrBlank).isFalse();
    }

    @Test
    public void should_detect_list_with_empty_values() {
        // given
        final List<String> values = Arrays.asList("");
        // when
        Boolean nullOrBlank = CollectionUtils.containsNotBlankValues(values);
        // then
        assertThat(nullOrBlank).isFalse();
    }

    @Test
    public void should_detect_list_with_blank_values() {
        // given
        final List<String> values = Arrays.asList("\t", "\n", "  ");
        // when
        Boolean nullOrBlank = CollectionUtils.containsNotBlankValues(values);
        // then
        assertThat(nullOrBlank).isFalse();
    }

    @Test
    public void should_detect_list_with_valid_values() {
        // given
        final List<String> values = Arrays.asList("\t", "\n", "  ", "valid!!");
        // when
        Boolean nullOrBlank = CollectionUtils.containsNotBlankValues(values);
        // then
        assertThat(nullOrBlank).isTrue();
    }
}
