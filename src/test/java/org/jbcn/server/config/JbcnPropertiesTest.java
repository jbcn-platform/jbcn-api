package org.jbcn.server.config;

import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assumptions.assumeThat;

public class JbcnPropertiesTest {

    @Test
    public void should_retrieve_editions() {
        // when
        final List<String> editions = JbcnProperties.getEditions();
        // then
        assertThat(editions).hasSize(3);
    }

    @Test
    public void should_retrieve_currentEdition() {
        // when
        final String edition = JbcnProperties.getCurrentEdition();
        // then
        assertThat(edition).isEqualTo("2019");
    }

    @Test
    public void should_retrieve_voters_for_all_editions() {
        // when
        final Map<String, List<String>> voters = Map.of(
            "2017", List.of("voter4", "voter5", "voter6"),
            "2018", List.of("voter1", "voter2", "voter3"));

        // then
        assertThat(voters.keySet()).hasSize(2);
        assertThat(voters.get("2017")).containsExactlyInAnyOrder("voter4", "voter5", "voter6");
        assertThat(voters.get("2018")).containsExactlyInAnyOrder("voter1", "voter2", "voter3");
    }

    @Test
    public void should_retrieve_voters_count() {
        String edition = "2017";
        // when
        int votersCount = JbcnProperties.getRequiredVotesCount(edition);

        // then
        assertThat(votersCount).isEqualTo(5);
    }

    @Test
    public void should_retrieve_mongodb_connection_url() {
        // when
        final String url = JbcnProperties.getMongoConnectionUri();
        // then
        assertThat(url).isEqualTo("mongodb://127.0.0.1:27017");
    }

    @Test
    public void should_retrieve_mongodb_database_name() {
        // when
        final String database = JbcnProperties.getMongoDatabaseName();
        // then
        assertThat(database).isEqualTo("jbcn");
    }

    @Test
    public void should_return_null_when_string_property_does_not_exist() {
        // when
        final String value = JbcnProperties.get(UUID.randomUUID().toString());
        // then
        assertThat(value).isNull();
    }

    @Test
    public void should_fail_when_string_property_does_not_exist_and_is_required() {
        final String key = UUID.randomUUID().toString();
        assertThatThrownBy(() -> JbcnProperties.get(key, true))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage(key + " cannot be null or blank");
    }

    @Test
    public void should_fail_when_string_property_is_blank_is_required() {
        final String key = "config.linebreaks";
        assertThatThrownBy(() -> JbcnProperties.get(key, true))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage(key + " cannot be null or blank");
    }

    @Test
    public void should_return_null_when_integer_property_does_not_exist() {
        // when
        final Integer value = JbcnProperties.getInteger(UUID.randomUUID().toString());
        // then
        assertThat(value).isNull();
    }

    @Test
    public void should_fail_with_exception_when_integer_property_format_is_not_valid() {

        assertThatThrownBy(() -> JbcnProperties.getInteger("mongodb.db"))
            .isInstanceOf(NumberFormatException.class);
    }

    @Test
    public void should_fail_with_exception_when_boolean_property_format_is_not_valid() {

        assertThatThrownBy(() -> JbcnProperties.getBoolean("mongodb.db", false))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageStartingWith("value is not boolean: ");
    }

    @Test
    public void should_retrieve_mongodb_database_depending_on_environment() {
        //given: setup `export MONGODB_DB="envvar-database"`
        assumeThat(System.getenv("MONGODB_DB")).isNotBlank();
        // when
        final String url = JbcnProperties.getMongoDatabaseName();
        // then
        assertThat(url).isEqualTo("envvar-database");
    }

    @Test
    public void should_automatically_retrieve_environment_variable_when_present() {
        //given: setup `export CONFIG_PROPERTY_ID="config.property.value"`
        assumeThat(System.getenv("CONFIG_PROPERTY_ID")).isNotBlank();
        // when
        final String url = JbcnProperties.get("config.property-id");
        // then
        assertThat(url).isEqualTo("config.property.value");
    }
}
