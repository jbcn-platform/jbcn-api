package org.jbcn.server.persistence;

import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.conversions.Bson;
import org.jbcn.server.model.ProposalLevel;
import org.jbcn.server.model.search.SearchConditions;
import org.jbcn.server.model.search.SearchFilter;
import org.junit.Test;

import java.util.List;

import static java.lang.Boolean.TRUE;
import static org.assertj.core.api.Assertions.assertThat;
import static util.MongoQueryAssertions.*;

public class MongoQueryBuilderTest {

    final MongoQueryBuilder queryBuilder = new MongoQueryBuilder(new MongoSearchFilterFactory(null));


    @Test
    public void should_build_empty_query() {
        // given
        final SearchConditions conditions = new SearchConditions(null, null);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        assertThat(bson.keySet()).hasSize(0);
    }

    @Test
    public void should_build_simple_all_filter_query() {
        // given
        final List<SearchFilter> allMatching = List.of(new SearchFilter("title", "something"));
        final SearchConditions conditions = new SearchConditions(allMatching, null);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        assertThat(bson.keySet()).containsExactly("title");
        assertStringKeyWithValue(bson, "title", "something");
    }

    @Test
    public void should_build_all_filter_query() {
        // given
        final List<SearchFilter> allMatching = List.of(
            new SearchFilter("title", "something"),
            new SearchFilter("sender.name", "McGrath"),
            new SearchFilter("abstract", "Kotlin", SearchFilter.CONTAINS)
        );
        final SearchConditions conditions = new SearchConditions(allMatching, null);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        // note: sender is translated to senders internally
        assertThat(bson.keySet()).containsExactlyInAnyOrder("title", "senders.name", "abstract");
        assertStringKeyWithValue(bson, "title", "something");
        assertStringKeyWithValue(bson, "senders.name", "McGrath");
        assertRegex(bson, "abstract", "Kotlin");
    }

    @Test
    public void should_build_simple_any_filter_query() {
        // given
        final List<SearchFilter> anyMatching = List.of(new SearchFilter("title", "something"));
        final SearchConditions conditions = new SearchConditions(null, anyMatching);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        assertThat(bson.keySet()).containsExactly(MONGODB_OR);
        final BsonValue orBson = bson.get(MONGODB_OR);
        assertThat(orBson.asArray()).hasSize(1);
        assertStringKeyWithValue((BsonDocument) orBson.asArray().get(0), "title", "something");
    }

    @Test
    public void should_build_any_filter_query() {
        // given
        final List<SearchFilter> anyMatching = List.of(
            new SearchFilter("title", "something"),
            new SearchFilter("sender.name", "McGrath", SearchFilter.CONTAINS),
            new SearchFilter("abstract", "Kotlin", SearchFilter.CONTAINS)
        );
        final SearchConditions conditions = new SearchConditions(null, anyMatching);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        // note: sender is translated to senders internally
        assertThat(bson.keySet()).containsExactly(MONGODB_OR);
        BsonArray bsonValues = bson.get(MONGODB_OR).asArray();
        assertThat(bsonValues).hasSize(3);
        assertStringKeyWithValue((BsonDocument) bsonValues.get(0), "title", "something");
        assertRegex((BsonDocument) bsonValues.get(1), "senders.name", "McGrath");
        assertRegex((BsonDocument) bsonValues.get(2), "abstract", "Kotlin");
    }

    @Test
    public void should_build_mixed_simple_any_all_filter_query() {
        // given
        final List<SearchFilter> allMatching = List.of(new SearchFilter("title", "something"));
        final List<SearchFilter> anyMatching = List.of(new SearchFilter("abstract", "somewhere"));
        final SearchConditions conditions = new SearchConditions(allMatching, anyMatching);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        assertThat(bson.keySet()).containsExactly("title", MONGODB_OR);
        assertStringKeyWithValue(bson, "title", "something");
        final BsonValue orBson = bson.get(MONGODB_OR);
        assertThat(orBson.asArray()).hasSize(1);
        assertStringKeyWithValue((BsonDocument) orBson.asArray().get(0), "abstract", "somewhere");
    }

    @Test
    public void should_build_mixed_any_all_filter_query() {
        // given
        final List<SearchFilter> allMatching = List.of(
            new SearchFilter("title", "something"),
            new SearchFilter("sponsor", TRUE, SearchFilter.EQUALS),
            new SearchFilter("speaker.name", "Miskatonic", SearchFilter.CONTAINS)
        );
        final List<SearchFilter> anyMatching = List.of(
            new SearchFilter("level", ProposalLevel.MIDDLE.getType(), SearchFilter.EQUALS),
            new SearchFilter("paperAbstract", "Arkham", SearchFilter.CONTAINS)
        );
        final SearchConditions conditions = new SearchConditions(allMatching, anyMatching);
        // when
        final Bson query = queryBuilder.build(conditions, null);
        // then
        final BsonDocument bson = toBsonDocument(query);
        assertThat(bson.keySet()).containsExactly("title", "sponsor", "speakers.name", MONGODB_OR);
        assertStringKeyWithValue(bson, "title", "something");
        assertBooleanKeyWithValue(bson, "sponsor", TRUE);
        assertRegex(bson, "speakers.name", "Miskatonic");
        final BsonValue orBson = bson.get(MONGODB_OR);
        assertThat(orBson.asArray()).hasSize(2);
        assertStringKeyWithValue((BsonDocument) orBson.asArray().get(0), "level", ProposalLevel.MIDDLE.getType());
        assertRegex((BsonDocument) orBson.asArray().get(1), "paperAbstract", "Arkham");
    }

}
