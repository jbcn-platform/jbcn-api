package org.jbcn.server.persistence;

import io.vertx.core.json.JsonObject;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static util.TestDataGenerator.getSpeaker;
import static util.TestDataGenerator.getTestTalk;

public class PublicDataProcessorTest {

    @Test
    public void should_generate_empty_list_of_talks_json_for_site() {
        // given
        final List<Talk> talksList = Collections.emptyList();
        // when
        final List<JsonObject> talks = new PublicDataProcessor()
            .getTalks(talksList)
            .getList();
        // then
        assertThat(talks).hasSize(0);
    }

    @Test
    public void should_generate_talks_json_for_site() {
        // given
        final Talk testTalk1 = getTestTalk("-1");
        testTalk1.set_id("talk-1");
        testTalk1.setPublished(true);
        testTalk1.setLanguages(List.of("eng", "es"));
        final Speaker speaker1 = getSpeaker("-1");
        testTalk1.setSpeakers(Arrays.asList(speaker1));

        final Talk testTalk2 = getTestTalk("-2");
        testTalk2.set_id("talk-2");
        testTalk2.setPublished(false);
        testTalk2.setLanguages(List.of("es"));
        final Speaker speaker2 = getSpeaker("-2");
        final Speaker speaker3 = getSpeaker("-3");
        final Speaker speaker4 = getSpeaker("-4");
        testTalk2.setSpeakers(Arrays.asList(speaker2, speaker3, speaker4));

        // when
        final List<JsonObject> talks = new PublicDataProcessor()
            .getTalks(Arrays.asList(testTalk1, testTalk2))
            .getList();

        // then
        assertThat(talks).hasSize(2);
        final JsonObject talk1 = findTalkBySuffix(talks, "-1");
        final JsonObject talk2 = findTalkBySuffix(talks, "-2");

        assertThat(talk1.fieldNames()).hasSize(8);
        assertThat(talk1.containsKey("published")).isFalse();
        assertThat(talk1.getString("id")).isEqualTo(testTalk1.get_id());
        assertThat(talk1.getString("title")).isEqualTo("Test talk -1");
        assertThat(talk1.getJsonArray("languages"))
            .containsExactlyInAnyOrder("eng", "es");
        assertThat(talk1.getString("abstract"))
            .startsWith("Stop talking, brain thinking. Hush.");
        assertThat(talk1.getString("type")).isEqualTo("talk");
        assertThat(talk1.getJsonArray("tags"))
            .containsExactlyInAnyOrder("tag1", "tag2", "tag3");
        assertThat(talk1.getString("level")).isEqualTo("middle");
        assertThat(talk1.getJsonArray("speakers")).hasSize(1);
        assertThat(talk1.getJsonArray("speakers"))
            .containsExactlyInAnyOrder(Speaker.generateRef(speaker1.getFullName(), speaker1.getEmail()));

        assertThat(talk2.fieldNames()).hasSize(8);
        assertThat(talk2.containsKey("published")).isFalse();
        assertThat(talk2.getString("id")).isEqualTo(testTalk2.get_id());
        assertThat(talk2.getString("title")).isEqualTo("Test talk -2");
        assertThat(talk2.getString("abstract")).startsWith("Stop talking, brain thinking. Hush.");
        assertThat(talk2.getJsonArray("languages"))
            .containsExactlyInAnyOrder("es");
        assertThat(talk2.getString("type")).isEqualTo("talk");
        assertThat(talk2.getJsonArray("tags"))
            .containsExactlyInAnyOrder("tag1", "tag2", "tag3");
        assertThat(talk2.getString("level")).isEqualTo("middle");
        assertThat(talk2.getJsonArray("speakers")).hasSize(3);
        assertThat(talk2.getJsonArray("speakers"))
            .containsExactlyInAnyOrder(
                Speaker.generateRef(speaker2.getFullName(), speaker2.getEmail()),
                Speaker.generateRef(speaker3.getFullName(), speaker3.getEmail()),
                Speaker.generateRef(speaker4.getFullName(), speaker4.getEmail())
            );
    }

    @Test
    public void should_generate_talks_and_ensure_type_in_lowercase() {
        // given
        final Talk testTalk1 = getTestTalk("-1");
        testTalk1.set_id("talk-1");
        final Talk testTalk2 = getTestTalk("-2");
        testTalk2.set_id("talk-2");
        testTalk2.setType("Workshop");
        // when
        final List<JsonObject> talks = new PublicDataProcessor()
            .getTalks(Arrays.asList(testTalk1, testTalk2))
            .getList();

        // then
        assertThat(talks).hasSize(2);
        final JsonObject talk1 = findTalkBySuffix(talks, "-1");
        assertThat(talk1.fieldNames()).hasSize(7);
        assertThat(talk1.getString("type")).isEqualTo("talk");

        final JsonObject talk2 = findTalkBySuffix(talks, "-2");
        assertThat(talk2.fieldNames()).hasSize(7);
        assertThat(talk2.getString("type")).isEqualTo("workshop");
    }

    // Note: all fields in Json are mandatory by domain requirements and should not be null in database.
    // This test is to validate PublicDataRepository in unitary way.
    @Test
    public void should_generate_talks_without_null_values() {
        // given
        final Talk testTalk1 = getTestTalk("-1");
        testTalk1.set_id("talk-1");
        testTalk1.setSpeakers(null);
        testTalk1.setTags(null);
        final Talk testTalk2 = getTestTalk("-2");
        testTalk2.set_id("talk-2");
        testTalk2.setType(null);

        // when
        final List<JsonObject> talks = new PublicDataProcessor()
            .getTalks(Arrays.asList(testTalk1, testTalk2))
            .getList();

        // then
        assertThat(talks).hasSize(2);
        final JsonObject talk1 = findTalkBySuffix(talks, "-1");
        assertThat(talk1.fieldNames()).hasSize(5);
        assertThat(talk1.getString("type")).isEqualTo("talk");
        assertThat(talk1.containsKey("speakers")).isFalse();
        assertThat(talk1.containsKey("tags")).isFalse();

        final JsonObject talk2 = findTalkBySuffix(talks, "-2");
        assertThat(talk2.fieldNames()).hasSize(6);
        assertThat(talk2.containsKey("type")).isFalse();
    }

    @Test
    public void should_generate_empty_list_of_speakers_json_for_site() {
        // given
        final List<Talk> talksList = Collections.emptyList();
        // when
        final List<JsonObject> speakers = new PublicDataProcessor()
            .getSpeakers(talksList)
            .getList();

        // then
        assertThat(speakers).hasSize(0);
    }

    @Test
    public void should_generate_speakers_json_for_site() {
        // given
        final Talk testTalk1 = getTestTalk("-1");
        testTalk1.setPublished(true);
        final Speaker speaker1 = getSpeaker("-1");
        testTalk1.setSpeakers(Arrays.asList(speaker1));

        final Talk testTalk2 = getTestTalk("-2");
        testTalk2.setPublished(false);
        final Speaker speaker2 = getSpeaker("-2");
        final Speaker speaker3 = getSpeaker("-3");
        final Speaker speaker4 = getSpeaker("-4");
        testTalk2.setSpeakers(Arrays.asList(speaker2, speaker3, speaker4));

        // when
        final List<JsonObject> speakers = new PublicDataProcessor()
            .getSpeakers(Arrays.asList(testTalk1, testTalk2))
            .getList();

        // then
        assertThat(speakers).hasSize(4);
        assertSpeaker(speaker1, findSpeakerBySuffix(speakers, "-1"));
        assertSpeaker(speaker2, findSpeakerBySuffix(speakers, "-2"));
        assertSpeaker(speaker3, findSpeakerBySuffix(speakers, "-3"));
        assertSpeaker(speaker4, findSpeakerBySuffix(speakers, "-4"));
    }

    // Note: empty values are still returned
    @Test
    public void should_generate_speakers_json_for_site_without_null_values() {
        // given
        final Talk testTalk1 = getTestTalk("-1");
        testTalk1.setPublished(true);
        final Speaker speaker1 = getSpeaker("-1");
        speaker1.setJobTitle("Amazing Speaker");
        speaker1.setWeb(null);
        speaker1.setTwitter(null);
        testTalk1.setSpeakers(Arrays.asList(speaker1));

        final Talk testTalk2 = getTestTalk("-2");
        testTalk2.setPublished(false);
        final Speaker speaker2 = getSpeaker("-2");
        speaker2.setJobTitle("Cool speaker");
        speaker2.setLinkedin(null);
        final Speaker speaker3 = getSpeaker("-3");
        final Speaker speaker4 = getSpeaker("-4");
        testTalk2.setSpeakers(Arrays.asList(speaker2, speaker3, speaker4));

        // when
        final List<JsonObject> speakers = new PublicDataProcessor()
            .getSpeakers(Arrays.asList(testTalk1, testTalk2))
            .getList();

        // then
        assertThat(speakers).hasSize(4);
        final JsonObject jsonSpeaker1 = findSpeakerBySuffix(speakers, "-1");
        assertThat(jsonSpeaker1.fieldNames()).hasSize(9);
        assertThat(jsonSpeaker1.getString("enabled")).isEqualTo("1");
        assertThat(jsonSpeaker1.getString("name")).isEqualTo(speaker1.getFullName());
        assertThat(jsonSpeaker1.getString("description")).isEqualTo("Amazing Speaker");
        assertThat(jsonSpeaker1.getString("company")).isEqualTo("Mega Corp");
        assertThat(jsonSpeaker1.getString("biography")).isEqualTo("A life of:\n* Passion\n- Adventure\n\n...and stuff");
        assertThat(jsonSpeaker1.getString("image")).isEqualTo("assets/img/speakers/file.jpg");
        final String ref1 = Speaker.generateRef(speaker1.getFullName(), speaker1.getEmail());
        assertThat(jsonSpeaker1.getString("ref")).isEqualTo(ref1);
        assertThat(jsonSpeaker1.getString("url")).isEqualTo("infoSpeaker.html?ref=" + ref1);
        assertThat(jsonSpeaker1.containsKey("twitter")).isFalse();
        assertThat(jsonSpeaker1.containsKey("homepage")).isFalse();
        assertThat(jsonSpeaker1.getString("linkedin")).isEqualTo("https://linkedin.com/work-account");

        final JsonObject jsonSpeaker2 = findSpeakerBySuffix(speakers, "-2");
        assertThat(jsonSpeaker2.fieldNames()).hasSize(10);
        assertThat(jsonSpeaker2.getString("enabled")).isEqualTo("1");
        assertThat(jsonSpeaker2.getString("name")).isEqualTo(speaker2.getFullName());
        assertThat(jsonSpeaker2.getString("description")).isEqualTo("Cool speaker");
        assertThat(jsonSpeaker2.getString("company")).isEqualTo("Mega Corp");
        assertThat(jsonSpeaker2.getString("biography")).isEqualTo("A life of:\n* Passion\n- Adventure\n\n...and stuff");
        assertThat(jsonSpeaker2.getString("image")).isEqualTo("assets/img/speakers/file.jpg");
        final String ref2 = Speaker.generateRef(speaker2.getFullName(), speaker2.getEmail());
        assertThat(jsonSpeaker2.getString("ref")).isEqualTo(ref2);
        assertThat(jsonSpeaker2.getString("url")).isEqualTo("infoSpeaker.html?ref=" + ref2);
        assertThat(jsonSpeaker2.getString("twitter")).isEqualTo("https://www.twitter.com/somedude");
        assertThat(jsonSpeaker2.getString("homepage")).isEqualTo("https://www.some.server.com/myweb");
        assertThat(jsonSpeaker2.containsKey("linkedin")).isFalse();

        assertSpeaker(speaker3, findSpeakerBySuffix(speakers, "-3"));
        assertSpeaker(speaker4, findSpeakerBySuffix(speakers, "-4"));
    }

    private void assertSpeaker(Speaker speaker, JsonObject generatedSpeaker) {
        assertThat(generatedSpeaker.fieldNames()).hasSize(11);
        assertThat(generatedSpeaker.getString("enabled")).isEqualTo("1");
        assertThat(generatedSpeaker.getString("name")).isEqualTo(speaker.getFullName());
        assertThat(generatedSpeaker.getString("description")).isBlank();
        assertThat(generatedSpeaker.getString("biography")).isEqualTo("A life of:\n* Passion\n- Adventure\n\n...and stuff");
        assertThat(generatedSpeaker.getString("company")).isEqualTo("Mega Corp");
        assertThat(generatedSpeaker.getString("image")).isEqualTo("assets/img/speakers/file.jpg");
        final String ref = Speaker.generateRef(speaker.getFullName(), speaker.getEmail());
        assertThat(generatedSpeaker.getString("ref")).isEqualTo(ref);
        assertThat(generatedSpeaker.getString("url")).isEqualTo("infoSpeaker.html?ref=" + ref);
        assertThat(generatedSpeaker.getString("twitter")).isEqualTo("https://www.twitter.com/somedude");
        assertThat(generatedSpeaker.getString("homepage")).isEqualTo("https://www.some.server.com/myweb");
        assertThat(generatedSpeaker.getString("linkedin")).isEqualTo("https://linkedin.com/work-account");
    }

    private JsonObject findSpeakerBySuffix(List<JsonObject> speakers, String suffix) {
        return speakers.stream()
            .filter(o -> o.getString("name").endsWith(suffix))
            .findFirst()
            .get();
    }

    private JsonObject findTalkBySuffix(List<JsonObject> talks, String suffix) {
        return talks.stream()
            .filter(o -> o.getString("title").endsWith(suffix))
            .findFirst()
            .get();
    }
}
