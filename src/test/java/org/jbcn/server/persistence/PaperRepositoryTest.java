package org.jbcn.server.persistence;


import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.Sender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.TestDataGenerator;
import util.VertxTestSetup;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.jbcn.server.persistence.Collections.PAPER;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class PaperRepositoryTest extends VertxTestSetup {

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }

    @Test
    public void should_return_a_single_paper_by_sender(TestContext context) {
        final Paper expected = TestDataGenerator.getTestPaper();
        final String paperId = insertToDatabase(PAPER, expected.toBsonDocument());
        expected.set_id(paperId);

        final Async async = context.async();
        new PaperRepository(getMongoDatabase())
            .findAllPapersBySender(expected.getSenders().get(0).getFullName(), (result) -> {
                context.assertNotNull(result);
                context.assertNotNull(result.result());
                List<Paper> papers = result.result();
                context.assertFalse(papers.isEmpty());
                context.assertEquals(1, papers.size());
                context.assertEquals(expected.get_id(), papers.get(0).get_id());

                async.complete();
            });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_ten_papers_by_sender(TestContext context) {
        final Sender sender = TestDataGenerator.getTestSender();
        List<Sender> senders = List.of(sender);

        List<Paper> expectedList = new ArrayList<>();
        List<String> expectedTitles = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Paper paper = TestDataGenerator.getTestPaper();
            paper.setSenders(senders);
            String paperId = insertToDatabase(PAPER, paper.toBsonDocument());
            paper.set_id(paperId);
            expectedList.add(paper);
            expectedTitles.add(paper.getTitle());
        }

        final Async async = context.async();
        new PaperRepository(getMongoDatabase())
            .findAllPapersBySender(sender.getFullName(), (result) -> {
                context.assertNotNull(result);
                context.assertNotNull(result.result());
                List<Paper> papers = result.result();
                context.assertFalse(papers.isEmpty());
                context.assertEquals(expectedList.size(), papers.size());
                papers.stream().forEach(paper -> {
                    context.assertTrue(expectedTitles.contains(paper.getTitle()));
                    context.assertEquals(senders.size(), paper.getSenders().size());
                });

                async.complete();
            });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_all_papers_by_edition(TestContext context) {
        final Sender sender = TestDataGenerator.getTestSender();
        List<Sender> senders = List.of(sender);

        final List<Paper> expectedPapers = IntStream.range(0, 10)
            .mapToObj(i -> {
                final Paper paper = TestDataGenerator.getTestPaper();
                paper.set_id(insertToDatabase(PAPER, paper.toBsonDocument()));
                return paper;
            }).collect(Collectors.toList());

        final Async async = context.async();
        final var fields = List.of("_id", "title", "abstract", "senders");
        new PaperRepository(getMongoDatabase())
            .findAllByEdition(JbcnProperties.getCurrentEdition(), fields, (result) -> {
                context.assertNotNull(result);
                context.assertNotNull(result.result());
                final List<Paper> papers = result.result();
                context.assertFalse(papers.isEmpty());
                context.assertEquals(expectedPapers.size(), papers.size());
                final List<String> expectedTitles = expectedPapers.stream().map(Paper::getTitle).collect(Collectors.toList());
                final List<String> expectedIds = expectedPapers.stream().map(Paper::get_id).collect(Collectors.toList());
                papers.stream().forEach(paper -> {
                    context.assertTrue(expectedTitles.contains(paper.getTitle()));
                    context.assertTrue(expectedIds.contains(paper.get_id()));
                    context.assertEquals(senders.size(), paper.getSenders().size());
                });
                async.complete();
            });

        async.await(TEST_TIMEOUT);
    }
}
