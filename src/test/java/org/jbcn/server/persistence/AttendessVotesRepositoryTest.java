package org.jbcn.server.persistence;


import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static java.lang.Boolean.TRUE;
import static org.jbcn.server.persistence.Collections.PUBLIC_VOTES;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class AttendessVotesRepositoryTest extends VertxTestSetup {

    @Before
    public void setup(TestContext context) {
        dropCollection(PUBLIC_VOTES);
    }


    @Test
    public void should_return_zero_public_votes_if_none_exist(TestContext context) {
        // given:
        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getPublicVotes((result) -> {
            final List<JsonObject> votes = result.result();
            context.assertEquals(0, votes.size());
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_all_public_votes(TestContext context) {
        // given: placeholder object
        for (int i = 0; i < 22; i++) {
            final Document publicVote = new Document(new HashMap<String, Object>() {{
                put("_id", UUID.randomUUID().toString().replaceAll("_", "").toLowerCase());
                put("scheduleId", UUID.randomUUID().toString());
                put("deviceId", UUID.randomUUID().toString());
                put("key1", 1);
                put("key2", "dos");
                put("key3", TRUE);
            }});
            insertToDatabase(PUBLIC_VOTES, publicVote);
        }

        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getPublicVotes((result) -> {
            final List<JsonObject> votes = result.result();
            context.assertEquals(22, votes.size());
            votes.forEach(v -> {
                context.assertEquals(6, v.fieldNames().size());
            });
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_zero_public_votes_by_schedule_id_when_none_exist(TestContext context) {
        // given
        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getVotesByScheduleId("", (result) -> {
            final List<JsonObject> votes = result.result();
            context.assertEquals(0, votes.size());
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_zero_public_votes_by_schedule_id_when_none_match(TestContext context) {
        // given: placeholder object
        for (int i = 0; i < 12; i++) {
            final Document publicVote = new Document(new HashMap<String, Object>() {{
                put("_id", UUID.randomUUID().toString().replaceAll("_", "").toLowerCase());
                put("key1", 1);
                put("key2", "dos");
                put("key3", TRUE);
            }});
            insertToDatabase(PUBLIC_VOTES, publicVote);
        }

        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getVotesByScheduleId("scid_42", (result) -> {
            final List<JsonObject> votes = result.result();
            context.assertEquals(0, votes.size());
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_public_votes_by_schedule_id(TestContext context) {
        // given: placeholder object
        final String scheduleId = "sc_0042";
        for (int i = 0; i < 12; i++) {
            final Document publicVote = new Document(new HashMap<String, Object>() {{
                put("_id", UUID.randomUUID().toString().replaceAll("_", "").toLowerCase());
                put("scheduleId", UUID.randomUUID().toString());
                put("deviceId", UUID.randomUUID().toString());
                put("key1", 1);
                put("key2", "dos");
                put("key3", TRUE);
                put("scheduleId", scheduleId);
            }});
            insertToDatabase(PUBLIC_VOTES, publicVote);
        }

        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getVotesByScheduleId(scheduleId, (result) -> {
            final List<JsonObject> votes = result.result();
            context.assertEquals(12, votes.size());
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_zero_votes_by_device_id_when_none_exist(TestContext context) {
        // given: placeholder object
        final String scheduleId = "sc_0042";
        final String deviceId = "device_123";
        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getVote(scheduleId, deviceId, (result) -> {
            context.assertTrue(result.succeeded());
            context.assertNull(result.result());
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_single_public_vote_by_device_id(TestContext context) {
        // given: placeholder object
        final String scheduleId = "sc_0042";
        final String deviceId = "device_123";
        for (int i = 0; i < 3; i++) {
            final Document publicVote = new Document(new HashMap<String, Object>() {{
                put("_id", UUID.randomUUID().toString().replaceAll("_", "").toLowerCase());
                put("key1", 1);
                put("key2", "dos");
                put("key3", TRUE);
                put("scheduleId", scheduleId);
                put("deviceId", deviceId);
            }});
            insertToDatabase(PUBLIC_VOTES, publicVote);
        }

        final MobileVotesRepository repository = new MobileVotesRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.getVote(scheduleId, deviceId, (result) -> {
            context.assertTrue(result.succeeded());
            final JsonObject vote = result.result();
            context.assertEquals(6, vote.fieldNames().size());
            context.assertEquals(36, vote.getString("_id").length());
            context.assertEquals(1, vote.getInteger("key1"));
            context.assertEquals("dos", vote.getString("key2"));
            context.assertEquals(TRUE, vote.getBoolean("key3"));
            context.assertEquals(scheduleId, vote.getString("scheduleId"));
            context.assertEquals(deviceId, vote.getString("deviceId"));
            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

}
