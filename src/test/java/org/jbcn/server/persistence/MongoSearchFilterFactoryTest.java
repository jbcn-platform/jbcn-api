package org.jbcn.server.persistence;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.jbcn.server.model.search.SearchFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.UUID;

import static java.lang.Boolean.TRUE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.jbcn.server.model.search.SearchFilter.CONTAINS;
import static org.jbcn.server.model.search.SearchFilter.IN;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.MongoQueryAssertions.*;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.insertToDatabase;


@RunWith(VertxUnitRunner.class)
public class MongoSearchFilterFactoryTest extends VertxTestSetup {

    @Test
    public void should_init_PaperFilterFactory() {
        // when
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        // then
        assertThat(factory).isNotNull();
    }

    @Test
    public void should_generate_filter_when_search_contains_state_key() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "state";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, "fake"), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly(filterKey);
        assertStringKeyWithValue(bson, filterKey, "fake");
    }

    @Test
    public void should_generate_empty_filter_when_search_contains_notPublishedSpeakers_key_and_edition_is_null() {
        // given
        final TalkRepository talkRepository = new TalkRepository(getMongoDatabase());
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(talkRepository);
        final String filterKey = "not-published-speakers";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, "fake"), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).isEmpty();
        ;
    }

    @Test
    public void should_generate_boolean_filter_when_search_contains_sponsor_key() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "sponsor";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, TRUE), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).hasSize(1);
        assertBooleanKeyWithValue(bson, filterKey, TRUE);
    }

    // TODO make query deterministic and not build query in any order
    @Test
    public void should_generate_empty_filter_when_search_contains_notPublishedSpeakers_key_and_edition_is_not_null() {
        // given
        final Talk talk1 = insertTestTalkToDatabase();
        final Talk talk2 = insertTestTalkToDatabase();

        final TalkRepository talkRepository = new TalkRepository(getMongoDatabase());
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(talkRepository);
        final String filterKey = "not-published-speakers";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, "fake"), "2019");
        // then
        final String expectedFilter1 = "{ \"senders\" : { \"$elemMatch\" : { \"code\" : { \"$not\" : { \"$in\" : [" +
            "\"" + talk1.getSpeakers().get(0).getCode() + "\", " +
            "\"" + talk2.getSpeakers().get(0).getCode() + "\"" +
            "] } } } } }";

        final String expectedFilter2 = "{ \"senders\" : { \"$elemMatch\" : { \"code\" : { \"$not\" : { \"$in\" : [" +
            "\"" + talk2.getSpeakers().get(0).getCode() + "\", " +
            "\"" + talk1.getSpeakers().get(0).getCode() + "\"" +
            "] } } } } }";

        final String actualFilter = toBsonDocument(filter).toString();
        if (expectedFilter1.equals(actualFilter))
            assertThat(actualFilter).isEqualTo(expectedFilter1);
        else
            assertThat(actualFilter).isEqualTo(expectedFilter2);
    }

    private Talk insertTestTalkToDatabase() {
        final Talk testTalk = getTestTalk();
        for (Speaker s : testTalk.getSpeakers()) {
            String code = UUID.randomUUID().toString();
            s.setCode(code);
        }
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));
        return testTalk;
    }

    @Test
    public void should_generate_sender_company_filter() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "sender.company";
        final String value = UUID.randomUUID().toString();
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, value), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly("senders.company");
        assertStringKeyWithValue(bson, "senders.company", value);
    }

    @Test
    public void should_generate_speaker_company_filter() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "speaker.company";
        final String value = UUID.randomUUID().toString();
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, value), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly("speakers.company");
        assertStringKeyWithValue(bson, "speakers.company", value);
    }

    @Test
    public void should_generate_contains_paper_abstract_filter() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "abstract";
        final String searchPattern = "fake";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, searchPattern, CONTAINS), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly(filterKey);
        assertRegex(bson, filterKey, searchPattern);
    }

    @Test
    public void should_generate_contains_talk_abstract_filter() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "paperAbstract";
        final String searchPattern = "fake";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, searchPattern, CONTAINS), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly(filterKey);
        assertRegex(bson, filterKey, searchPattern);
    }

    @Test
    public void should_generate_contains_exact_tag_filter() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "tags";
        final String searchPattern = "some_tag";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, searchPattern, IN), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly(filterKey);
        assertThat(bson.get(filterKey).toString()).isEqualTo("{ \"$in\" : [\"some_tag\"] }");
    }

    @Test
    public void should_generate_contains_like_tag_filter() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        final String filterKey = "tags";
        final String searchPattern = "some_tag";
        // when
        final Bson filter = factory.getFilter(new SearchFilter(filterKey, searchPattern, CONTAINS), null);
        // then
        final BsonDocument bson = toBsonDocument(filter);
        assertThat(bson.keySet()).containsExactly(filterKey);
        assertRegex(bson, filterKey, searchPattern);
    }

    @Test
    public void should_not_fail_when_search_filter_contains_invalid_key() {
        // given
        final MongoSearchFilterFactory factory = new MongoSearchFilterFactory(null);
        // when
        final Bson filter = factory.getFilter(new SearchFilter("any_key", "fake"), null);
        // then
        assertThatThrownBy(() -> filter.toBsonDocument(Document.class, getMongoDbCodecRegistry()))
            .isInstanceOf(NullPointerException.class);

    }

}
