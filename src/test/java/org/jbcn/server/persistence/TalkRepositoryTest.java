package org.jbcn.server.persistence;


import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Collection;
import java.util.List;

import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class TalkRepositoryTest extends VertxTestSetup {

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }

    @Test
    public void should_return_talks_by_edition(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final TalkRepository repository = new TalkRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.findAllByEdition("2019", (result) -> {
            final List<Talk> speakers = result.result();
            context.assertFalse(speakers.isEmpty());
            context.assertEquals(3, speakers.size());

            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_speakers_by_edition(TestContext context) {
        // given
        insertToDatabase(TALK, DocumentConverter.getDocument(getTestTalk()));
        insertToDatabase(TALK, DocumentConverter.getDocument(getTestTalk()));
        insertToDatabase(TALK, DocumentConverter.getDocument(getTestTalk()));
        // talks with duplicated speaker
        final List<Speaker> testSpeakers = getTestTalk().getSpeakers();
        insertToDatabase(TALK, DocumentConverter.getDocument(talkWithSpeakers(testSpeakers)));
        insertToDatabase(TALK, DocumentConverter.getDocument(talkWithSpeakers(testSpeakers)));
        insertToDatabase(TALK, DocumentConverter.getDocument(talkWithSpeakers(testSpeakers)));

        final TalkRepository repository = new TalkRepository(getMongoDatabase());

        // when
        final Async async = context.async();

        repository.findAllSpeakers("2019", (result) -> {
            final Collection<Speaker> speakers = result.result();
            context.assertFalse(speakers.isEmpty());
            // note: duplicated are removed
            context.assertEquals(4, speakers.size());
            speakers.forEach(s -> {
                context.assertNull(s.getId());
                context.assertNotNull(s.getCode());
                context.assertNotNull(s.getFullName());
                context.assertNotNull(s.getEmail());
                context.assertNotNull(s.getTwitter());
                context.assertNotNull(s.getPicture());
            });

            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_zero_published_talks(TestContext context) {
        // given
        final TalkRepository repository = new TalkRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.findByPublishedState("2019", Boolean.TRUE, (result) -> {
            final List<Talk> speakers = result.result();
            context.assertTrue(speakers.isEmpty());

            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_all_published_talks(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final TalkRepository repository = new TalkRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.findByPublishedState("2019", Boolean.TRUE, (result) -> {
            final List<Talk> speakers = result.result();
            context.assertFalse(speakers.isEmpty());
            context.assertEquals(3, speakers.size());

            async.complete();
        });

        async.await(TEST_TIMEOUT);
    }

    @Test
    public void should_return_100_talks_when_limit_is_set(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        for (int i = 0; i < 150; i++)
            insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final TalkRepository repository = new TalkRepository(getMongoDatabase());

        // when
        final Async async = context.async();
        repository.search(100, 0, CREATED_DATE, true, null, null,
            result -> {
                context.assertTrue(result.succeeded());
                JsonObject searchResult = result.result();

                context.assertEquals(150, searchResult.getInteger("total"));
                context.assertEquals(100, searchResult.getJsonArray("items").size());

                async.complete();
            });

        async.await(TEST_TIMEOUT);
    }

    private Talk talkWithSpeakers(List<Speaker> speakers) {
        Talk talk = getTestTalk();
        talk.setSpeakers(speakers);
        return talk;
    }

}
