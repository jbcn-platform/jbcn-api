package org.jbcn.server.email;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.utils.TimeUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.jbcn.server.Constants.*;
import static org.jbcn.server.model.Role.VOTER;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.PAPER;
import static util.TestDataGenerator.getTestPaper;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.VertxAsyncResultAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class EmailHttpTest extends VertxTestSetup {

    private static final String SEND_EMAIL_PAPER_FEEDBACK = "/api/emails/paper-feedback";

    private static final String TEST_USER = "papers_feedback_test_user";
    private static final String FAKE_PAPER_ID = "5c24e4b0554b4947c8163049";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }

    @Test
    public void should_fail_send_feedback_email_when_role_is_not_voter(TestContext context) {
        final JsonObject emptyMailRequest = new JsonObject();

        final Async async = context.async();
        webClient.post("/api/emails/fake_type")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPEAKER))
            .sendJsonObject(emptyMailRequest, handler -> {
                assertThat(handler)
                    .with(context)
                    .isSuccessful()
                    .returnsStatus(403)
                    .doesNotContainHeader(CONTENT_TYPE.toString());

                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_send_feedback_email_when_type_is_not_set(TestContext context) {
        final JsonObject emptyMailRequest = new JsonObject();

        final Async async = context.async();
        webClient.post("/api/emails/")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
            .sendJsonObject(emptyMailRequest, handler -> {
                // default vert.x response
                assertThat(handler)
                    .with(context)
                    .isSuccessful()
                    .returnsStatus(404)
                    .containsHeader(CONTENT_TYPE.toString(), "text/html; charset=utf-8")
                    .hasBody("<html><body><h1>Resource not found</h1></body></html>");

                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_send_feedback_email_when_type_does_not_match(TestContext context) {
        final JsonObject emptyMailRequest = new JsonObject();

        final Async async = context.async();
        webClient.post("/api/emails/fake_type")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
            .sendJsonObject(emptyMailRequest, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage(EMAIL_TYPE_NOT_FOUND);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_send_feedback_email_when_paper_does_not_exists(TestContext context) {
        final String paperId = FAKE_PAPER_ID;

        final JsonObject mailRequest = new JsonObject()
            .put("paper-id", paperId);

        final Async async = context.async();
        webClient.post(SEND_EMAIL_PAPER_FEEDBACK)
            .bearerTokenAuthentication(getNewSession(TEST_USER, VOTER))
            .sendJsonObject(mailRequest, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage(UNKNOWN_PAPER_ERROR);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_send_feedback_email_when_paper_does_not_have_feedback(TestContext context) {
        final String paperId = insertToDatabase(PAPER, createTestPaper());

        final JsonObject mailRequest = new JsonObject()
            .put("paper-id", paperId);

        final Async async = context.async();
        webClient.post(SEND_EMAIL_PAPER_FEEDBACK)
            .bearerTokenAuthentication(getNewSession(TEST_USER, VOTER))
            .sendJsonObject(mailRequest, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage(EMAIL_EMPTY_FEEDBACK);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    /**
     * 1. Replace `speakerEmail` value.
     * 2. Run with environment variables (fill those that are no empty):
     * MAIL_ENABLED=true
     * PAPERS_PUBLIC_URL=https://api.public.org
     * MAIL_IMAGE_HEADER_URL=https://header.image.public.org
     * MAIL_IMAGE_FOOTER_URL=https://footer.image.public.org
     * MAIL_USER=
     * GOOGLE_OAUTH_TOKEN_URL=https://www.googleapis.com/oauth2/v4/token
     * GOOGLE_OAUTH_CLIENT_ID=
     * GOOGLE_OAUTH_SECRET=
     * GOOGLE_OAUTH_REFRESH_TOKEN=
     * MAIL_CONFIRMATION_FROM=
     */
    @Test
    @Ignore("Requires real mail setup")
    public void should_send_email(TestContext context) {
        final String speakerEmail = "abelromero@gmail.com";
        final String feedback = "Awesome talk!\nReally\n\nFor sure!";
        final String paperId = insertToDatabase(PAPER, createTestPaper(feedback, speakerEmail));

        final JsonObject mailRequest = new JsonObject()
            .put("paper-id", paperId);

        final Async async = context.async();
        webClient.post(SEND_EMAIL_PAPER_FEEDBACK)
            .bearerTokenAuthentication(getNewSession(TEST_USER, VOTER))
            .sendJsonObject(mailRequest, handler -> {
                if (handler.succeeded()) {
                    final var result = handler.result();
                    context.assertEquals(201, result.statusCode());
                    context.assertNull(result.getHeader(CONTENT_TYPE.toString()));
                } else {
                    context.fail(handler.cause());
                }
                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private Document createTestPaper() {
        return createTestPaper(null);
    }

    private Document createTestPaper(String feedback) {
        final Document testPaper = getTestPaper().toBsonDocument();
        final long now = TimeUtils.currentTime();
        if (feedback != null) {
            final Document bson = new Document()
                .append("text", feedback)
                .append(CREATED_DATE, now)
                .append(LAST_UPDATE, now)
                .append(LAST_ACTION_BY, TEST_USER);
            testPaper.append("feedback", bson);
        }
        return testPaper;
    }

    private Document createTestPaper(String feedback, String speakerEmail) {
        final Document testPaper = createTestPaper(feedback);
        final List<Document> senders = (List<Document>) testPaper.get("senders");
        senders.get(0).put("email", speakerEmail);
        return testPaper;
    }
}
