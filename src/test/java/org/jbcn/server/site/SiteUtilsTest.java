package org.jbcn.server.site;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SiteUtilsTest {

    @Test
    public void should_get_site_path_from_url_with_filename() {
        // given
        final String sourceUrl = "https://somesite.com/images/amazingspeaker.png";
        // when
        final String targetUrl = SiteUtils.getImageUrl(sourceUrl);
        // then
        assertThat(targetUrl).isEqualTo("assets/img/speakers/" + "amazingspeaker.png");
    }

    @Test
    public void should_get_site_path_from_base_gravatar_url() {
        // given
        final String sourceUrl = "https://en.gravatar.com/audreyneveu";
        // when
        final String targetUrl = SiteUtils.getImageUrl(sourceUrl);
        // then
        assertThat(targetUrl).isEqualTo("assets/img/speakers/" + "audreyneveu.jpg");
    }

    @Test
    public void should_get_site_path_from_hash_gravatar_url() {
        // given
        final String sourceUrl = "https://www.gravatar.com/avatar/1ea016d441bdf55f27c6a26ca8e01522";
        // when
        final String targetUrl = SiteUtils.getImageUrl(sourceUrl);
        // then
        assertThat(targetUrl).isEqualTo("assets/img/speakers/" + "1ea016d441bdf55f27c6a26ca8e01522.jpg");
    }

    @Test
    public void should_get_site_path_from_hash_with_size_gravatar_url() {
        // given
        final String sourceUrl = "https://www.gravatar.com/avatar/367d990904b8520a15e75d2fbcee6183?s=512";
        // when
        final String targetUrl = SiteUtils.getImageUrl(sourceUrl);
        // then
        assertThat(targetUrl).isEqualTo("assets/img/speakers/" + "367d990904b8520a15e75d2fbcee6183.jpg");
    }

    @Test
    public void should_get_site_path_from_hash_with_size_and_extension_gravatar_url() {
        // given
        final String sourceUrl = "https://nl.gravatar.com/userimage/91524624/79f76b7546c15f58b0a28fa4e1a75e1d.jpg?size=512";
        // when
        final String targetUrl = SiteUtils.getImageUrl(sourceUrl);
        // then
        assertThat(targetUrl).isEqualTo("assets/img/speakers/" + "79f76b7546c15f58b0a28fa4e1a75e1d.jpg");
    }

    @Test
    public void should_get_site_path_from_generic_url_with_arguments() {
        // given
        final String sourceUrl = "https://www.somesite.org/path_here/img/1234567890.gif?size=512&param=2";
        // when
        final String targetUrl = SiteUtils.getImageUrl(sourceUrl);
        // then
        assertThat(targetUrl).isEqualTo("assets/img/speakers/" + "1234567890.gif");
    }

}
