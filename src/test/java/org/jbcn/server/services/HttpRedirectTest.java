package org.jbcn.server.services;

import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.UUID;
import java.util.function.Consumer;

import static org.jbcn.server.persistence.Collections.URL_REDIRECTS;
import static util.TestDataGenerator.getRandomSuffix;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class HttpRedirectTest extends VertxTestSetup {

    private static final String RESOURCE = "/r/";

    @Test
    public void should_respond_with_http_redirect_when_name_matches(TestContext context) {
        final UrlRedirect redirect = createTestRedirect();
        insertToDatabase(URL_REDIRECTS, DocumentConverter.getDocument(redirect));

        assertAsyncGet(RESOURCE + redirect.getName(), context, httpResponse -> {
            httpResponse
                .status(301)
                .containsHeader("Location", redirect.getUrl());
        });
    }

    @Test
    public void should_return_404_with_http_redirect_when_name_matches(TestContext context) {

        assertAsyncGet(RESOURCE + UUID.randomUUID(), context, httpResponse -> {
            httpResponse
                .status(404)
                .doesNotContainHeader("Location");
        });
    }

    private void assertAsyncGet(String url, TestContext context, Consumer<HttpAsserter> asserter) {
        final Async async = context.async();
        webClient.get(url)
            .send(handler -> {
                asserter.accept(HttpAsserter.assertThat(handler).with(context));
                async.complete();
            });
        async.awaitSuccess(TEST_TIMEOUT);
    }

    public UrlRedirect createTestRedirect() {
        final String suffix = getRandomSuffix();
        return new UrlRedirect("https://url.conf/" + suffix, "name-" + suffix);
    }
}
