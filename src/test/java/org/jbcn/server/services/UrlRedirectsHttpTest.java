package org.jbcn.server.services;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.jbcn.server.persistence.Collections.URL_REDIRECTS;
import static util.TestDataGenerator.getRandomSuffix;
import static util.VertxSetup.*;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.PersistedApiEntityAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class UrlRedirectsHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/api/url-redirects/";
    private static final String TEST_USER = "redirects_test_user";

    @Before
    public void before(TestContext context) {
        dropCollection(URL_REDIRECTS);
    }


    @Test
    public void should_fail_create_redirect_when_no_body_is_sent(TestContext context) {
        assert_fail_create_redirect(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_redirect_when_empty_body_is_sent(TestContext context) {
        assert_fail_create_redirect(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_redirect_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_create_redirect(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_create_redirect_when_url_is_null(TestContext context) {
        final String testRedirect = "{\"url\":null}";

        assert_fail_create_redirect(context, testRedirect, "error.body.required_field.url");
    }

    @Test
    public void should_fail_create_redirect_when_name_is_null(TestContext context) {
        final String testRedirect = "{\"url\":\"https://some.url/here\",\"name\":null}";

        assert_fail_create_redirect(context, testRedirect, "error.body.required_field.name");
    }

    @Test
    public void assert_fail_create_redirect_when_no_auth_token_is_present(TestContext context) {
        final JsonObject testRedirect = new JsonObject()
            .put("name", "some_name");

        assertNoAuthTokenIsPresentWithHttpPOST(context, RESOURCE, testRedirect);
    }

    public void assert_fail_create_redirect(TestContext context, String body, String errorMessage) {
        assert_fail(context, RESOURCE, TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_redirect_with_all_fields(TestContext context) {
        final JsonObject testRedirect = new JsonObject()
            .put("url", "https://some.url/here")
            .put("name", "name-" + UUID.randomUUID());

        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(testRedirect, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .getId();

                final Document inserted = getFromDatabase(URL_REDIRECTS, id);
                assertThat(inserted)
                    .with(context)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("url", testRedirect.getString("url"))
                    .hasStringEqualsTo("name", testRedirect.getString("name"))
                    .hasKeys(6);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_redirect_by_id(TestContext context) {
        // given
        final UrlRedirect redirect = createTestRedirect();
        final String id = insertToDatabase(URL_REDIRECTS, DocumentConverter.getDocument(redirect));

        // when
        final Async async = context.async();
        webClient.get(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isEntityResponse()
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("url", redirect.getUrl())
                    .hasStringEqualsTo("name", redirect.getName());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_getting_redirect_when_user_is_helper(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.HELPER, context);
    }

    @Test
    public void should_not_authorize_getting_redirect_when_user_is_voter(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_getting_redirect_when_user_is_sponsor(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPONSOR, context);
    }

    @Test
    public void should_not_authorize_getting_redirect_when_user_is_speaker(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPEAKER, context);
    }

    private void assert_forbidden_when_getting_by_id(Role role, TestContext context) {
        // given
        final var testRedirect = createTestRedirect();
        final String id = insertToDatabase(URL_REDIRECTS, DocumentConverter.getDocument(testRedirect));

        // when
        final Async async = context.async();
        webClient.get(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, role))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_redirect_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_redirect_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String id = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_redirect_url(TestContext context) {
        // given
        final var testRedirect = createTestRedirect();
        final String id = insertToDatabase(URL_REDIRECTS, DocumentConverter.getDocument(testRedirect));

        final String newUrl = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("url", newUrl);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                final Document updated = getFromDatabase(URL_REDIRECTS, id);
                assertThat(updated)
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("url", newUrl)
                    .hasStringEqualsTo("name", testRedirect.getName());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_redirect_name(TestContext context) {
        // given
        final var testRedirect = createTestRedirect();
        final String id = insertToDatabase(URL_REDIRECTS, DocumentConverter.getDocument(testRedirect));

        final String newName = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("name", newName);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                final Document updated = getFromDatabase(URL_REDIRECTS, id);
                assertThat(updated)
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("url", testRedirect.getUrl())
                    .hasStringEqualsTo("name", newName);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_redirect_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c4a3ea8aefd394cce055574";
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + fakeId)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_redirect_when_id_format_is_not_valid(TestContext context) {
        // given
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + "123")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_redirect(TestContext context) {
        final var testRedirect = createTestRedirect();
        final String id = insertToDatabase(URL_REDIRECTS, DocumentConverter.getDocument(testRedirect));

        final Async async = context.async();
        webClient.delete(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_fail_delete_when_id_does_not_exist(TestContext context) {
        final String id = "6206ba02368434645eb35bf2";

        final Async async = context.async();
        webClient.delete(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_track_when_id_format_is_not_valid(TestContext context) {
        final Async async = context.async();
        webClient.delete(RESOURCE + 123)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    public UrlRedirect createTestRedirect() {
        final String suffix = getRandomSuffix();
        final LocalDateTime now = LocalDateTime.now();
        return new UrlRedirect(null, "name-" + suffix, "https://url.conf/" + suffix, now, now, null);
    }
}
