package org.jbcn.server.services;

import io.vertx.core.AsyncResult;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.web.client.HttpResponse;

public class HttpAsserter {

    private final AsyncResult<HttpResponse<Buffer>> result;
    private final TestContext testContext;

    private HttpAsserter(AsyncResult<HttpResponse<Buffer>> result, TestContext testContext) {
        this.result = result;
        this.testContext = testContext;
    }

    public static HttpAsserterBuilder assertThat(AsyncResult<HttpResponse<Buffer>> result) {
        return new HttpAsserterBuilder(result);
    }

    public HttpAsserter isSuccessful() {
        testContext.assertTrue(result.succeeded());
        return this;
    }

    public HttpAsserter status(int expected) {
        testContext.assertEquals(expected, result.result().statusCode());
        return this;
    }

    public HttpAsserter containsHeader(String name, String value) {
        final String actualValue = result.result().getHeader(name);
        testContext.assertEquals(value, actualValue);
        return this;
    }

    public HttpAsserter doesNotContainHeader(String name) {
        final String actualValue = result.result().getHeader(name);
        testContext.assertNull(actualValue, "Header '" + name + "' expected to a value");
        return this;
    }

    public static class HttpAsserterBuilder {

        private final AsyncResult<HttpResponse<Buffer>> result;

        public HttpAsserterBuilder(AsyncResult<HttpResponse<Buffer>> result) {
            this.result = result;
        }

        public HttpAsserter with(TestContext testContext) {
            return new HttpAsserter(result, testContext);
        }
    }
}
