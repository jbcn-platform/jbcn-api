package org.jbcn.server.mail;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class TemplateProcessorTest {

    @Test(expected = FileNotFoundException.class)
    public void should_fail_when_template_is_not_found() {
        final TemplateProcessor templateProcessor = new TemplateProcessor();
        final Path templatePath = Path.of(UUID.randomUUID().toString());

        templateProcessor.render(templatePath, Map.of());
    }

    @Test
    public void should_apply_single_substitution_to_template() {
        final TemplateProcessor templateProcessor = new TemplateProcessor();
        final Path templatePath = Path.of("/templates/mail-test-template.html");

        final String usernameValue = randomString();
        final String content = templateProcessor.render(templatePath, Map.of("username", usernameValue));

        assertThat(content)
            .contains("* username=" + usernameValue)
            .contains("* team={{team-value}}");
    }

    @Test
    public void should_apply_all_substitutions_to_template() {
        final TemplateProcessor templateProcessor = new TemplateProcessor();
        final Path templatePath = Path.of("/templates/mail-test-template.html");

        final String usernameValue = randomString();
        final String teamValue = randomString();
        final String content = templateProcessor.render(templatePath,
            Map.of("username", usernameValue,
                "team-value", teamValue));

        assertThat(content)
            .contains("* username=" + usernameValue)
            .contains("* team=" + teamValue);
    }

    @Test
    public void should_apply_substitutions_to_string() {
        final TemplateProcessor templateProcessor = new TemplateProcessor();
        final String templateContent = "Hello {{username}}, from {{team}}";

        final String usernameValue = randomString();
        final String teamValue = randomString();
        final String content = templateProcessor.render(templateContent,
            Map.of("username", usernameValue,
                "team", teamValue));

        assertThat(content)
            .isEqualTo("Hello " + usernameValue + ", from " + teamValue);
    }

    private String randomString() {
        final String candidate = UUID.randomUUID().toString();
        return candidate.substring(0, candidate.indexOf('-'));
    }
}
