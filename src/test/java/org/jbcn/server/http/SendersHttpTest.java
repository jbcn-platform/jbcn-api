package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.Sender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import static java.lang.Boolean.TRUE;
import static org.jbcn.server.persistence.Collections.PAPER;
import static util.TestDataGenerator.getTestPaper;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class SendersHttpTest extends VertxTestSetup {

    private static final String SPEAKERS_TEST_USER = "senders_test_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }


    // Speakers are those that have an approved Talk
    @Test
    public void should_get_a_single_senders(TestContext context) {
        // given
        final Paper testTalk = getTestPaper();
        final Sender testSender = testTalk.getSenders().get(0);
        testSender.setCompany("Coop Corp");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(PAPER, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get("/api/senders")
            .bearerTokenAuthentication(getNewSession(SPEAKERS_TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.getInteger("total"));
                data.getJsonArray("items").forEach(it -> {
                    JsonObject sender = (JsonObject) it;
                    context.assertEquals(testSender.getFullName(), sender.getString("fullName"));
                    context.assertEquals(testSender.getPicture(), sender.getString("picture"));
                    context.assertEquals(testSender.getTshirtSize(), sender.getString("tshirtSize"));
                    context.assertEquals(testSender.getCompany(), sender.getString("company"));
                    context.assertEquals(testSender.getTwitter(), sender.getString("twitter"));
                    context.assertEquals(testSender.getWeb(), sender.getString("web"));
                    context.assertEquals(testSender.getLinkedin(), sender.getString("linkedin"));
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_two_different_senders(TestContext context) {
        // given
        final Paper testTalk = getTestPaper();
        final Sender testSender = testTalk.getSenders().get(0);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(PAPER, Document.parse(entries.encode()));
        final Paper testTalk2 = getTestPaper();
        final Sender testSender2 = testTalk2.getSenders().get(0);
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        insertToDatabase(PAPER, Document.parse(entries2.encode()));

        // when
        final Async async = context.async();
        webClient.get("/api/senders")
            .bearerTokenAuthentication(getNewSession(SPEAKERS_TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(2, data.getInteger("total"));

                final JsonObject sender1 = (JsonObject) data.getJsonArray("items").stream()
                    .filter(it -> ((JsonObject) it).getString("code").equals(testSender.getCode()))
                    .findFirst()
                    .get();
                context.assertEquals(testSender.getFullName(), sender1.getString("fullName"));
                context.assertEquals(testSender.getPicture(), sender1.getString("picture"));
                context.assertEquals(testSender.getTshirtSize(), sender1.getString("tshirtSize"));
                context.assertEquals(testSender.getCompany(), sender1.getString("company"));
                context.assertEquals(testSender.getTwitter(), sender1.getString("twitter"));
                context.assertEquals(testSender.getWeb(), sender1.getString("web"));
                context.assertEquals(testSender.getLinkedin(), sender1.getString("linkedin"));

                final JsonObject sender2 = (JsonObject) data.getJsonArray("items").stream()
                    .filter(it -> ((JsonObject) it).getString("code").equals(testSender2.getCode()))
                    .findFirst()
                    .get();
                context.assertEquals(testSender2.getFullName(), sender2.getString("fullName"));
                context.assertEquals(testSender2.getPicture(), sender2.getString("picture"));
                context.assertEquals(testSender2.getTshirtSize(), sender2.getString("tshirtSize"));
                context.assertEquals(testSender2.getCompany(), sender2.getString("company"));
                context.assertEquals(testSender2.getTwitter(), sender2.getString("twitter"));
                context.assertEquals(testSender2.getWeb(), sender2.getString("web"));
                context.assertEquals(testSender2.getLinkedin(), sender2.getString("linkedin"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_all_senders_without_duplicates(TestContext context) {
        // given
        final Paper testTalk = getTestPaper();
        final Sender testSender = testTalk.getSenders().get(0);
        testSender.setCompany("Coop Corp");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(PAPER, Document.parse(entries.encode()));
        insertToDatabase(PAPER, Document.parse(entries.encode()));
        insertToDatabase(PAPER, Document.parse(entries.encode()));
        insertToDatabase(PAPER, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get("/api/senders")
            .bearerTokenAuthentication(getNewSession(SPEAKERS_TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.getInteger("total"));
                data.getJsonArray("items").forEach(it -> {
                    JsonObject sender = (JsonObject) it;
                    context.assertEquals(testSender.getFullName(), sender.getString("fullName"));
                    context.assertEquals(testSender.getPicture(), sender.getString("picture"));
                    context.assertEquals(testSender.getTshirtSize(), sender.getString("tshirtSize"));
                    context.assertEquals(testSender.getCompany(), sender.getString("company"));
                    context.assertEquals(testSender.getTwitter(), sender.getString("twitter"));
                    context.assertEquals(testSender.getWeb(), sender.getString("web"));
                    context.assertEquals(testSender.getLinkedin(), sender.getString("linkedin"));
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
