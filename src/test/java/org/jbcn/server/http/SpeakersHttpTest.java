package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class SpeakersHttpTest extends VertxTestSetup {

    private static final String ENDPOINT = "/api/speakers";
    private static final String SPEAKERS_TEST_USER = "speakers_test_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }


    // Speakers are those that have an approved Talk
    @Test
    public void should_get_all_speakers(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        final Speaker testSpeaker = testTalk.getSpeakers().get(0);
        testSpeaker.setCompany("FancyMall");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(SPEAKERS_TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.getInteger("total"));
                    data.getJsonArray("items").forEach(it -> {
                        JsonObject speaker = (JsonObject) it;
                        context.assertEquals(testSpeaker.getFullName(), speaker.getString("fullName"));
                        context.assertEquals(testSpeaker.getPicture(), speaker.getString("picture"));
                        context.assertEquals(testSpeaker.getTshirtSize(), speaker.getString("tshirtSize"));
                        context.assertEquals(testSpeaker.getCompany(), speaker.getString("company"));
                        context.assertEquals(testSpeaker.getTwitter(), speaker.getString("twitter"));
                        context.assertEquals(testSpeaker.getWeb(), speaker.getString("web"));
                        context.assertEquals(testSpeaker.getLinkedin(), speaker.getString("linkedin"));
                        context.assertNotNull(speaker.getString("ref"));
                        context.assertNotNull(testSpeaker.generateRef(testSpeaker.getFullName(), testSpeaker.getEmail()), speaker.getString("ref"));
                    });
                }));
    }

    @Test
    public void should_get_all_speakers_without_duplicates(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        final Speaker testSpeaker = testTalk.getSpeakers().get(0);
        testSpeaker.setCompany("FancyMall");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));
        insertToDatabase(TALK, Document.parse(entries.encode()));
        insertToDatabase(TALK, Document.parse(entries.encode()));
        insertToDatabase(TALK, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(SPEAKERS_TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.getInteger("total"));
                    data.getJsonArray("items").forEach(it -> {
                        JsonObject speaker = (JsonObject) it;
                        context.assertEquals(testSpeaker.getFullName(), speaker.getString("fullName"));
                        context.assertEquals(testSpeaker.getPicture(), speaker.getString("picture"));
                        context.assertEquals(testSpeaker.getTshirtSize(), speaker.getString("tshirtSize"));
                        context.assertEquals(testSpeaker.getCompany(), speaker.getString("company"));
                        context.assertEquals(testSpeaker.getTwitter(), speaker.getString("twitter"));
                        context.assertEquals(testSpeaker.getWeb(), speaker.getString("web"));
                        context.assertEquals(testSpeaker.getLinkedin(), speaker.getString("linkedin"));
                    });
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
