package org.jbcn.server.http;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.User;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.USER;
import static org.jbcn.server.utils.StringUtils.isNullOrBlank;
import static org.jbcn.server.utils.TimeUtils.toTimestamp;
import static util.TestDataGenerator.getTestUser;
import static util.VertxSetup.*;

@RunWith(VertxUnitRunner.class)
public class UsersHttpTest extends VertxTestSetup {

    private static final String USERS_TEST_USER = "user_test_user";
    public static final String ENDPOINT = "/api/users/";
    public static final String USERS_SEARCH_ENDPOINT = "/api/users/search";

    @Before
    public void before(TestContext context) {
        dropCollection(USER);
    }

    @Test
    public void should_fail_add_user_when_no_body_is_sent(TestContext context) {
        assert_fail_add_user(context, null, "error.body.required");
    }

    @Test
    public void should_fail_add_user_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_user(context, "", "error.body.required");
    }

    @Test
    public void should_fail_add_user_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_user(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_user_when_username_is_null(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\":null," +
            "\"password\":null," +
            "\"email\":null" +
            "}";

        // when
        assert_fail_add_user(context, testUser, "error.body.required_field.username");
    }

    @Test
    public void should_fail_add_user_when_password_is_null(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\": \"null\"," +
            "\"password\":null," +
            "\"email\":null" +
            "}";

        // when
        assert_fail_add_user(context, testUser, "error.body.required_field.password");
    }

    @Test
    public void should_fail_add_user_when_email_is_null(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\": \"null\"," +
            "\"password\":\"null\"," +
            "\"email\":null" +
            "}";

        // when
        assert_fail(context, ENDPOINT, USERS_TEST_USER, testUser, "error.body.required_field.email", 200);
    }

    @Test
    public void should_fail_add_user_when_roles_is_null(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\": \"null\"," +
            "\"password\":\"null\"," +
            "\"email\":\"null\"," +
            "\"roles\":null" +
            "}";

        // when
        assert_fail(context, ENDPOINT, USERS_TEST_USER, testUser, "error.body.required_field.roles", 200);
    }

    @Test
    public void should_fail_add_user_when_roles_is_empty(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\": \"null\"," +
            "\"password\":\"null\"," +
            "\"email\":\"null\"," +
            "\"roles\":[]" +
            "}";

        // when
        assert_fail(context, ENDPOINT, USERS_TEST_USER, testUser, "error.body.required_field.roles", 200);
    }

    @Test
    public void should_fail_add_user_when_role_value_is_not_in_valid_range(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\": \"null\"," +
            "\"password\":\"null\"," +
            "\"email\":\"null\"," +
            "\"roles\":[ \"admin\"," + "\"NOT_a_R0l3\"]" +
            "}";

        // when
        assert_fail(context, ENDPOINT, USERS_TEST_USER, testUser, "error.role.invalid", 200);
    }

    @Test
    public void should_fail_add_user_when_no_auth_token_is_present(TestContext context) {
        // given
        final String testUser = "{" +
            "\"username\": \"test_user\"," +
            "\"password\":\"test_password\"," +
            "\"email\": \"test_email@jbcnapi.com\"" +
            "}";

        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT);
    }

    public void assert_fail_add_user(TestContext context, String body, String errorMessage) {
        assert_fail(context, ENDPOINT, USERS_TEST_USER, body, errorMessage, 200);
    }

    @Test
    public void should_create_a_user_with_admin_role(TestContext context) {
        // given
        final JsonObject testUser = new JsonObject("{" +
            "\"username\": \"test_user\"," +
            "\"password\":\"test_password\"," +
            "\"email\": \"test_email@jbcnapi.com\"," +
            "\"roles\": [\"admin\"]" +
            "}");

        // when
        createAndAssertUserCreation(context, testUser);
    }

    @Test
    public void should_create_a_user_removing_duplicated_roles(TestContext context) {
        // given
        final JsonObject testUser = new JsonObject("{" +
            "\"username\": \"test_user\"," +
            "\"password\":\"test_password\"," +
            "\"email\": \"test_email@jbcnapi.com\"," +
            "\"roles\": [\"admin\",\"admin\"]" +
            "}");

        // when
        createAndAssertUserCreation(context, testUser);
    }

    @Test
    public void should_create_a_user_with_admin_and_voter_roles(TestContext context) {
        // given
        final JsonObject testUser = new JsonObject("{" +
            "\"username\": \"test_user\"," +
            "\"password\":\"test_password\"," +
            "\"email\": \"test_email@jbcnapi.com\"," +
            "\"roles\": [\"admin\",\"voter\"]" +
            "}");

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(testUser, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals("User added", responseBody.getString("message"));
                context.assertEquals(3, responseBody.fieldNames().size());
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.fieldNames().size());
                context.assertNotNull(data.getString("id"));

                final Document insertedUser = getFromDatabase(USER, data.getString("id"));
                context.assertEquals("test_user", insertedUser.getString("username"));
                context.assertNotNull(insertedUser.getString("password"));
                context.assertEquals("test_email@jbcnapi.com", insertedUser.getString("email"));
                final List roles = (List) insertedUser.get("roles");
                context.assertEquals(2, roles.size());
                context.assertTrue(roles.contains(Role.ADMIN.getRole()));
                context.assertTrue(roles.contains(Role.VOTER.getRole()));
                context.assertNotNull(insertedUser.getLong(CREATED_DATE));
                context.assertNotNull(insertedUser.getLong(LAST_UPDATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_users_when_no_auth_token_is_present(TestContext context) {
        // when
        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
    }

    @Test
    public void should_list_0_users(TestContext context) {
        // given: clean 'user' collection

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonArray items = responseBody.getJsonObject("data").getJsonArray("items");
                context.assertEquals(0, items.size());

                context.assertEquals("{\"status\":true,\"data\":{\"items\":[],\"total\":0}}", responseBody.toString());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_user(TestContext context) {
        // given
        final User testUser = getTestUser();
        insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonArray items = responseBody.getJsonObject("data").getJsonArray("items");
                context.assertEquals(1, items.size());

                final JsonObject testUserJson = (JsonObject) items.stream()
                    .filter(it -> ((JsonObject) it).getString("username").equals(testUser.getUsername()))
                    .findFirst()
                    .get();

                context.assertEquals(6, testUserJson.fieldNames().size());
                context.assertEquals(testUser.getEmail(), testUserJson.getString("email"));
                final JsonArray roles = testUserJson.getJsonArray("roles");
                context.assertEquals(2, roles.size());
                context.assertTrue(roles.contains(Role.ADMIN.getRole()));
                context.assertTrue(roles.contains(Role.VOTER.getRole()));
                context.assertNotNull(testUserJson.getLong(CREATED_DATE));
                context.assertNotNull(testUserJson.getLong(LAST_UPDATE));
                context.assertEquals(testUserJson.getLong(CREATED_DATE), testUserJson.getLong(LAST_UPDATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_users(TestContext context) {
        // given
        int testSize = 12;
        final Map<String, User> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final User testUser = getTestUser(String.valueOf(i));
                return Pair.of(insertToDatabase(USER, DocumentConverter.getDocument(testUser)), testUser);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (User) pair.getSecond()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {

                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonArray items = responseBody.getJsonObject("data").getJsonArray("items");
                context.assertEquals(testSize, items.size());

                items.forEach(it -> {
                    final JsonObject json = (JsonObject) it;
                    final String testId = json.getString("_id");

                    final User testUser = testData.get(testId);
                    context.assertEquals(testUser.getUsername(), json.getString("username"));
                    context.assertEquals(testUser.getEmail(), json.getString("email"));
                    context.assertTrue(!json.containsKey("password"));
                    context.assertEquals(toTimestamp(testUser.getCreatedDate()), json.getLong(CREATED_DATE));
                    context.assertEquals(toTimestamp(testUser.getLastUpdate()), json.getLong(LAST_UPDATE));
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    @Test
    public void should_get_user_by_id(TestContext context) {
        // given
        final User testUser = getTestUser();
        Document document = DocumentConverter.getDocument(testUser);
        document.put("password", "1234");
        final String userId = insertToDatabase(USER, document);

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject json = responseBody.getJsonObject("data");

                context.assertEquals(testUser.getUsername(), json.getString("username"));
                context.assertEquals(testUser.getEmail(), json.getString("email"));
                // password is not returned
                context.assertTrue(!json.containsKey("password"));
                context.assertNotNull(json.getLong(CREATED_DATE));
                context.assertNotNull(json.getLong(LAST_UPDATE));
                context.assertEquals(json.getLong(LAST_UPDATE), json.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_user_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String userId = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_user_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String userId = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.user.unknown_user", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_only_user_email(TestContext context) {
        // given
        final User testUser = getTestUser();
        final String userId = insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        final String newEmail = UUID.randomUUID() + "_new_email@some_domain.com";
        final JsonObject updateBody = new JsonObject()
            .put("email", newEmail);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedUser = getFromDatabase(USER, userId);
                context.assertEquals(newEmail, updatedUser.getString("email"));
                context.assertEquals(testUser.getUsername(), updatedUser.getString("username"));
                context.assertNotNull(updatedUser.getLong(CREATED_DATE));
                context.assertNotNull(updatedUser.getLong(LAST_UPDATE));
//                    context.assertTrue(updatedUser.getDate(LAST_UPDATE) updatedUser.getDate(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_only_user_roles(TestContext context) {
        // given
        final User testUser = getTestUser();
        final String userId = insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        final List<String> roles = List.of(Role.SPONSOR.getRole(), Role.SPEAKER.getRole());
        final JsonObject updateBody = new JsonObject()
            .put("roles", roles);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedUser = getFromDatabase(USER, userId);
                context.assertEquals(testUser.getEmail(), updatedUser.getString("email"));
                context.assertEquals(testUser.getUsername(), updatedUser.getString("username"));
                List<String> updatedRoles = (List) updatedUser.get("roles");
                context.assertEquals(2, updatedRoles.size());
                context.assertTrue(updatedRoles.contains(Role.SPONSOR.getRole()));
                context.assertTrue(updatedRoles.contains(Role.SPEAKER.getRole()));
                context.assertNotNull(updatedUser.getLong(CREATED_DATE));
                context.assertNotNull(updatedUser.getLong(LAST_UPDATE));
                context.assertTrue(updatedUser.getLong(LAST_UPDATE) > updatedUser.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_roles_when_value_is_not_valid(TestContext context) {
        // given
        final User testUser = getTestUser();
        final String userId = insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        final JsonObject updateBody = new JsonObject()
            .put("roles", Arrays.asList("random_Value"));

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.role.invalid", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_fail_update_roles_when_value_is_empty(TestContext context) {
        // given
        final User testUser = getTestUser();
        final String userId = insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        final JsonObject updateBody = new JsonObject();

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedUser = getFromDatabase(USER, userId);
                // No changes are applied
                context.assertEquals(updatedUser.getLong(CREATED_DATE), updatedUser.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_user_username(TestContext context) {
        // given
        final User testUser = getTestUser();
        final String userId = insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        final Map<String, String> updateParams = new HashMap<>();
        final String newUsername = UUID.randomUUID() + "_new_Anna";
        updateParams.put("username", newUsername);
        updateParams.put("email", testUser.getEmail());
        final JsonObject updateBody = JsonObject.mapFrom(updateParams);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);

                final Document updatedUser = getFromDatabase(USER, userId);
                context.assertEquals(newUsername, updatedUser.getString("username"));
                context.assertEquals(testUser.getEmail(), updatedUser.getString("email"));
                context.assertNotNull(updatedUser.getLong(CREATED_DATE));
                context.assertNotNull(updatedUser.getLong(LAST_UPDATE));
                context.assertTrue(updatedUser.getLong(LAST_UPDATE) > updatedUser.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_user_username_and_email(TestContext context) {
        // given
        final User testUser = getTestUser();
        final String userId = insertToDatabase(USER, DocumentConverter.getDocument(testUser));

        final Map<String, String> updateParams = new HashMap<>();
        final String newUsername = UUID.randomUUID() + "_new_Anna";
        updateParams.put("username", newUsername);
        final String newEmail = UUID.randomUUID() + "_new_email@some_domain.com";
        updateParams.put("email", newEmail);
        final JsonObject updateBody = JsonObject.mapFrom(updateParams);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);

                final Document updatedUser = getFromDatabase(USER, userId);
                context.assertEquals(newUsername, updatedUser.getString("username"));
                context.assertEquals(newEmail, updatedUser.getString("email"));
                context.assertNotNull(updatedUser.getLong(CREATED_DATE));
                context.assertNotNull(updatedUser.getLong(LAST_UPDATE));
                context.assertTrue(updatedUser.getLong(LAST_UPDATE) > updatedUser.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_user_password(TestContext context) {
        // given
        final JsonObject testUser = new JsonObject("{" +
            "\"username\": \"test_user_" + UUID.randomUUID() + "\"," +
            "\"password\":\"test_password\"," +
            "\"email\": \"test_email@jbcnapi.com\"," +
            "\"roles\": [\"helper\"]" +
            "}");

        // when
        final Async async = context.async();
        createAndAssertUserCreation(context, testUser, async, user -> {
            final Map<String, String> updateParams = new HashMap<>();
            final String newPassword = "new_password-" + UUID.randomUUID();
            updateParams.put("password", newPassword);
            final JsonObject updateBody = JsonObject.mapFrom(updateParams);

            final String userId = user.getObjectId("_id").toString();

            final Document insertedUser = getFromDatabase(USER, user.getObjectId("_id").toString());
            final String originalPassword = insertedUser.getString("password");
            final String originalSalt = insertedUser.getString("salt");
            context.assertFalse(isNullOrBlank(originalPassword));
            context.assertFalse(isNullOrBlank(originalSalt));

            webClient.put(ENDPOINT + userId)
                .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
                .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                    final Document updateddUser = getFromDatabase(USER, user.getObjectId("_id").toString());
                    final String updatedPassword = updateddUser.getString("password");
                    final String updatedSalt = updateddUser.getString("salt");
                    context.assertNotEquals(originalPassword, updatedPassword);
                    context.assertNotEquals(originalSalt, updatedSalt);
                }));
        });
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_user_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String userId = "1234567890";

        final Map<String, String> updateParams = new HashMap<>();
        updateParams.put("username", UUID.randomUUID().toString());
        updateParams.put("email", UUID.randomUUID().toString());
        final JsonObject updateBody = JsonObject.mapFrom(updateParams);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_user_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String userId = "5c4a3ea8aefd394cce055574";

        final Map<String, String> updateParams = new HashMap<>();
        updateParams.put("username", UUID.randomUUID().toString());
        updateParams.put("email", UUID.randomUUID().toString());
        final JsonObject updateBody = JsonObject.mapFrom(updateParams);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.user.unknown_user", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_search_users_when_body_is_empty(TestContext context) {
        // given

        // when
        assert_fail(context, USERS_SEARCH_ENDPOINT, USERS_TEST_USER, null, "error.search.conditions_minimum");
    }

    @Test
    public void should_fail_search_users_when_parameters_are_not_a_parseable_json(TestContext context) {
        // given
        final String invalidJson = "{\"page\": 20,}";
        // when
        final Async async = context.async();
        webClient.post(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendBuffer(Buffer.buffer(invalidJson), assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals(responseBody.getString("error"), "error.json.invalid_format");
            }));


        async.awaitSuccess(TEST_TIMEOUT);
    }

    /**
     * Default values if not set:
     * - page = 0
     * - size = 50
     * - sort (column) = title
     * - asc = true
     * - filters = empty
     */
    @Test
    public void should_search_users_with_minimum_options(TestContext context) {

        // given
        int testSize = 6;
        final Map<String, User> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final User testUser = getTestUser(String.valueOf(i));
                return Pair.of(insertToDatabase(USER, DocumentConverter.getDocument(testUser)), testUser);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (User) pair.getSecond()));

        final JsonObject searchParams = new JsonObject(new HashMap<>());

        // when
        final Async async = context.async();
        webClient.post(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), testSize);

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());

                context.assertEquals(retriedIds.size(), testSize);
                for (Object insertedUser : items) {
                    final JsonObject _user = (JsonObject) insertedUser;
                    final String id = _user.getString("_id");
                    context.assertTrue(retriedIds.contains(id));

                    final User user = testData.get(id);
                    context.assertEquals(user.getUsername(), _user.getString("username"));
                    context.assertEquals(user.getPassword(), _user.getString("password"));
                    context.assertEquals(user.getEmail(), _user.getString("email"));
                    context.assertNotNull(_user.getLong(CREATED_DATE));
                    context.assertNotNull(_user.getLong(LAST_UPDATE));
                }
            }));

        async.awaitSuccess(10_000l);
    }

    @Test
    public void should_search_users_ordered_by_username(TestContext context) {
        // given
        int testSize = 6;
        final Map<String, User> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final User testUser = getTestUser(String.valueOf(i));
                return Pair.of(insertToDatabase(USER, DocumentConverter.getDocument(testUser)), testUser);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (User) pair.getSecond()));

        final JsonObject searchParams = new JsonObject()
            .put("page", 0)
            .put("size", 20)
            .put("sort", "username")
            .put("asc", true);

        // when
        final Async async = context.async();
        webClient.post(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), testSize);

                final JsonArray items = data.getJsonArray("items");

                final List<String> retrievedNames = items.stream()
                    .map(v -> ((JsonObject) v).getString("username"))
                    .collect(Collectors.toList());
                context.assertEquals("anna_0", retrievedNames.get(0));
                context.assertEquals("anna_1", retrievedNames.get(1));
                context.assertEquals("anna_2", retrievedNames.get(2));
                context.assertEquals("anna_3", retrievedNames.get(3));
                context.assertEquals("anna_4", retrievedNames.get(4));
                context.assertEquals("anna_5", retrievedNames.get(5));

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());

                context.assertEquals(retriedIds.size(), testSize);
                for (Object insertedUser : items) {
                    final JsonObject _user = (JsonObject) insertedUser;
                    final String id = _user.getString("_id");
                    context.assertTrue(retriedIds.contains(id));

                    final User user = testData.get(id);
                    context.assertEquals(user.getUsername(), _user.getString("username"));
                    context.assertEquals(user.getPassword(), _user.getString("password"));
                    context.assertEquals(user.getEmail(), _user.getString("email"));
                    context.assertNotNull(_user.getLong(CREATED_DATE));
                    context.assertNotNull(_user.getLong(LAST_UPDATE));
                }
            }));

        async.awaitSuccess(10_000l);
    }

    @Test
    public void should_search_users_and_return_default_max_size(TestContext context) {

        // given
        int testSize = 55;
        int defaultMaxSize = 50;
        final Map<String, User> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final User testUser = getTestUser(String.valueOf(i));
                return Pair.of(insertToDatabase(USER, DocumentConverter.getDocument(testUser)), testUser);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (User) pair.getSecond()));

        final JsonObject searchParams = new JsonObject(new HashMap<>());

        // when
        final Async async = context.async();
        webClient.post(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), testSize);

                final JsonArray items = data.getJsonArray("items");

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(retriedIds.size(), defaultMaxSize);
                for (Object insertedUser : items) {
                    final JsonObject _user = (JsonObject) insertedUser;
                    final String id = _user.getString("_id");
                    context.assertTrue(retriedIds.contains(id));

                    final User user = testData.get(id);
                    context.assertEquals(user.getUsername(), _user.getString("username"));
                    context.assertEquals(user.getPassword(), _user.getString("password"));
                    context.assertEquals(user.getEmail(), _user.getString("email"));
                    context.assertNotNull(_user.getLong(CREATED_DATE));
                    context.assertNotNull(_user.getLong(LAST_UPDATE));
                }
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO test out of range options
    @Test
    public void should_search_users_with_sort_options(TestContext context) {
        // given
        final User testUser1 = getTestUser("1");
        insertToDatabase(USER, DocumentConverter.getDocument(testUser1));
        final User testUser2 = getTestUser("2");
        insertToDatabase(USER, DocumentConverter.getDocument(testUser2));
        final User testUser3 = getTestUser("3");
        insertToDatabase(USER, DocumentConverter.getDocument(testUser3));

        final JsonObject searchParams = new JsonObject()
            .put("sort", "email")
            .put("asc", FALSE)
            .put("page", 0)
            .put("size", 2);

        // when
        final Async async = context.async();
        webClient.post(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());

                context.assertEquals(retriedIds.size(), 2);
                final JsonObject jsonObject = items.getJsonObject(0);
                context.assertEquals(testUser3.getUsername(), jsonObject.getString("username"));
                context.assertEquals(testUser3.getPassword(), jsonObject.getString("password"));
                context.assertEquals(testUser3.getEmail(), jsonObject.getString("email"));

                final JsonObject jsonObject1 = items.getJsonObject(1);
                context.assertEquals(testUser2.getUsername(), jsonObject1.getString("username"));
                context.assertEquals(testUser2.getPassword(), jsonObject.getString("password"));
                context.assertEquals(testUser2.getEmail(), jsonObject1.getString("email"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_users_with_pagination_options(TestContext context) {
        // given
        final User testUser1 = getTestUser("1");
        insertToDatabase(USER, DocumentConverter.getDocument(testUser1));
        final User testUser2 = getTestUser("2");
        insertToDatabase(USER, DocumentConverter.getDocument(testUser2));
        final User testUser3 = getTestUser("3");
        insertToDatabase(USER, DocumentConverter.getDocument(testUser3));

        final JsonObject searchParams = new JsonObject()
            .put("sort", "email")
            .put("asc", FALSE)
            .put("page", 2)
            .put("size", 1);

        // when
        final Async async = context.async();
        webClient.post(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());

                context.assertEquals(retriedIds.size(), 1);
                final JsonObject jsonObject = items.getJsonObject(0);
                context.assertEquals(testUser1.getUsername(), jsonObject.getString("username"));
                context.assertEquals(testUser1.getPassword(), jsonObject.getString("password"));
                context.assertEquals(testUser1.getEmail(), jsonObject.getString("email"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_user_when_user_is_not_admin(TestContext context) {
        // given
        final JsonObject testUser = new JsonObject("{" +
            "\"username\": \"test_user\"," +
            "\"password\":\"test_password\"," +
            "\"email\": \"test_email@jbcnapi.com\"," +
            "\"roles\": [\"admin\"]" +
            "}");

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.VOTER)))
            .sendJsonObject(testUser, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_user_when_user_is_not_admin(TestContext context) {
        // given
        final JsonObject testUser = new JsonObject("{\"email\": \"new_email@jbcnapi.com\"}");

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + "fakeId")
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.VOTER)))
            .sendJsonObject(testUser, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_users_when_user_is_not_admin(TestContext context) {
        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.VOTER)))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_search_users_when_user_is_not_admin(TestContext context) {
        // given
        final JsonObject searchParams = new JsonObject()
            .put("page", 0)
            .put("size", 20)
            .put("sort", "username")
            .put("asc", true);
        // when
        final Async async = context.async();
        webClient.get(USERS_SEARCH_ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.VOTER)))
            .sendJsonObject(searchParams, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_user_when_user_is_not_admin(TestContext context) {
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + "fakeId")
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.VOTER)))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void createAndAssertUserCreation(TestContext context, JsonObject testUser) {
        createAndAssertUserCreation(context, testUser, context.async(), null);
    }

    private void createAndAssertUserCreation(TestContext context, JsonObject testUser, Async async, Consumer<Document> userAssertions) {
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(USERS_TEST_USER, List.of(Role.ADMIN)))
            .sendJsonObject(testUser, assertJsonResponse(async, userAssertions == null, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals("User added", responseBody.getString("message"));
                context.assertEquals(3, responseBody.fieldNames().size());
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.fieldNames().size());
                context.assertNotNull(data.getString("id"));

                final Document insertedUser = getFromDatabase(USER, data.getString("id"));
                context.assertEquals(testUser.getString("username"), insertedUser.getString("username"));
                context.assertNotNull(insertedUser.getString("password"));
                context.assertEquals("test_email@jbcnapi.com", insertedUser.getString("email"));
                final List roles = (List) insertedUser.get("roles");
                context.assertEquals(1, roles.size());
                context.assertEquals(Role.of(testUser.getJsonArray("roles").getString(0)), Role.of((String) roles.get(0)));
                context.assertNotNull(insertedUser.getLong(CREATED_DATE));
                context.assertNotNull(insertedUser.getLong(LAST_UPDATE));

                if (userAssertions != null)
                    userAssertions.accept(insertedUser);
            }));
    }
}
