package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Talk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Arrays;
import java.util.List;

import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class TalkTagsHttpTest extends VertxTestSetup {

    private static final String TEST_USER = "tags_test_user";

    private static final String ENDPOINT = "/api/talks/tags";

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }


    // tags are returned sorted and in lower case
    @Test
    public void should_get_a_single_talk_tags(TestContext context) {
        // given
        createTestTalk(Arrays.asList("Java", "rustlang", "Cool Stuff"));

        // when
        final Async async = context.async();

        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject data = responseBody.getJsonObject("data");
                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(3, items.size());
                context.assertEquals(2, data.fieldNames().size());

                assertTag(context, items.getJsonObject(0), "cool stuff", 1);
                assertTag(context, items.getJsonObject(1), "java", 1);
                assertTag(context, items.getJsonObject(2), "rustlang", 1);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_aggregate_tags_of_several_talks(TestContext context) {
        // given
        createTestTalk(Arrays.asList("Java", "rustlang", "Cool Stuff", "Azure"));
        createTestTalk(Arrays.asList("java", "rustLang", "aws", "azure"));
        createTestTalk(Arrays.asList("Kotlin", "fine", "for me", "aZuRe"));

        // when
        final Async async = context.async();

        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {

                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject data = responseBody.getJsonObject("data");
                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(8, items.size());
                context.assertEquals(2, data.fieldNames().size());

                assertTag(context, items.getJsonObject(0), "aws", 1);
                assertTag(context, items.getJsonObject(1), "azure", 3);
                assertTag(context, items.getJsonObject(2), "cool stuff", 1);
                assertTag(context, items.getJsonObject(3), "fine", 1);
                assertTag(context, items.getJsonObject(4), "for me", 1);
                assertTag(context, items.getJsonObject(5), "java", 2);
                assertTag(context, items.getJsonObject(6), "kotlin", 1);
                assertTag(context, items.getJsonObject(7), "rustlang", 2);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private String createTestTalk(List<String> tags) {
        final Talk testTalk = getTestTalk();
        testTalk.setTags(tags);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        return insertToDatabase(TALK, Document.parse(entries.encode()));
    }

    private void assertTag(TestContext context, JsonObject tag, String tagName, Integer count) {
        context.assertEquals(tagName, tag.getString("value"));
        context.assertEquals(count, tag.getInteger("count"));
        context.assertEquals(2, tag.fieldNames().size());
    }

}
