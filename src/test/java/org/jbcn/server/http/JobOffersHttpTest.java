package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.sponsor.JobOffer;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;
import util.asserters.JsonApiEntity;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.jbcn.server.persistence.Collections.JOB_OFFERS;
import static util.TestDataGenerator.getTestJobOffer;
import static util.VertxSetup.*;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.PersistedApiEntityAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class JobOffersHttpTest extends VertxTestSetup {

    private static final String ENDPOINT = "/api/job-offers/";

    private static final String TEST_USER = "sponsor_test_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(JOB_OFFERS);
    }


    @Test
    public void should_fail_create_jobOffer_when_no_body_is_sent(TestContext context) {
        assert_fail_add_jobOffer(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_jobOffer_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_jobOffer(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_jobOffer_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_jobOffer(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_jobOffer_when_title_is_null(TestContext context) {
        // given
        final String testInstance = "{\"title\":null}";

        // when
        assert_fail_add_jobOffer(context, testInstance, "error.body.required_field.title");
    }

    @Test
    public void should_fail_add_jobOffer_when_description_is_null(TestContext context) {
        // given
        final String testInstance = "{\"title\":\"a_title\",\"description\":null}";

        // when
        assert_fail_add_jobOffer(context, testInstance, "error.body.required_field.description");
    }

    @Test
    public void should_fail_add_jobOffer_when_link_is_null(TestContext context) {
        // given
        final String testInstance = "{\"title\":\"a_title\",\"description\":\"some_description\",\"link\":null}";

        // when
        assert_fail_add_jobOffer(context, testInstance, "error.body.required_field.link");
    }

    @Test
    public void should_fail_add_jobOffer_when_sponsorId_is_null(TestContext context) {
        // given
        final String testInstance = "{\"title\":\"a_title\",\"description\":\"some_description\",\"link\":\"the_link\",\"sponsorId\":null}";

        // when
        assert_fail_add_jobOffer(context, testInstance, "error.body.required_field.sponsorId");
    }

    @Test
    public void assert_fail_add_jobOffer_when_no_auth_token_is_present(TestContext context) {
        // given
        final JsonObject testInstance = new JsonObject()
            .put("title", "some_title")
            .put("description", "some_description")
            .put("link", "the_link");
        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT, testInstance);
    }

    public void assert_fail_add_jobOffer(TestContext context, String body, String errorMessage) {
        assert_fail(context, ENDPOINT, TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_jobOffer_with_minimal_fields(TestContext context) {
        assertAllFieldsAreRequired(context);
    }

    @Test
    public void should_create_jobOffer_with_all_fields(TestContext context) {
        // given
        final UUID random = UUID.randomUUID();
        final String title = "title-" + random;
        final String description = "description-" + random;
        final String link = "link-" + random;
        final String sponsorId = "sponsor-id-" + random;
        final String location = "Barcelona, Supain";
        final JsonObject testInstance = new JsonObject()
            .put("title", title)
            .put("description", description)
            .put("link", link)
            .put("sponsorId", sponsorId)
            .put("location", location);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .sendJsonObject(testInstance, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .hasValidFormat()
                    .getId();

                assertThat(getFromDatabase(JOB_OFFERS, id))
                    .with(context)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("title", title)
                    .hasStringEqualsTo("description", description)
                    .hasStringEqualsTo("link", link)
                    .hasStringEqualsTo("sponsorId", sponsorId)
                    .hasStringEqualsTo("location", location);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertAllFieldsAreRequired(TestContext context) {
        // given
        final UUID random = UUID.randomUUID();
        final String title = "title-" + random;
        final String description = "description-" + random;
        final String link = "link-" + random;
        final String sponsorId = "sponsor-id-" + random;
        final JsonObject testInstance = new JsonObject()
            .put("title", title)
            .put("description", description)
            .put("link", link)
            .put("sponsorId", sponsorId);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .sendJsonObject(testInstance, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .hasValidFormat()
                    .getId();

                assertThat(getFromDatabase(JOB_OFFERS, id))
                    .with(context)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("title", title)
                    .hasStringEqualsTo("description", description)
                    .hasStringEqualsTo("link", link)
                    .hasStringEqualsTo("sponsorId", sponsorId);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_jobOffer_when_no_auth_token_is_present(TestContext context) {
        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
    }

    @Test
    public void should_list_0_jobOffers(TestContext context) {
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .isEmpty();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_jobOffer(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .hasSize(1)
                    .hasItem(0, item -> assertEquals(context, item, testInstance));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_jobOffers(TestContext context) {
        // given
        int testSize = 12;
        final Map<String, JobOffer> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final JobOffer testInstance = getTestJobOffer();
                return Pair.of(insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance)), testInstance);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (JobOffer) pair.getSecond()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .hasSize(12)
                    .allItems(it -> {
                        final JobOffer testInstance = testData.get(it.getId());
                        assertEquals(context, it, testInstance);
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertEquals(TestContext context, JsonApiEntity actual, JobOffer expected) {
        context.assertEquals(expected.getTitle(), actual.getString("title"));
        context.assertEquals(expected.getDescription(), actual.getString("description"));
        context.assertEquals(expected.getLink(), actual.getString("link"));
        context.assertEquals(expected.getSponsorId(), actual.getString("sponsorId"));
        context.assertNotNull(expected.getCreatedDate());
        context.assertEquals(expected.getCreatedDate(), expected.getLastUpdate());
        context.assertEquals(8, actual.fieldNames().size());
    }

    @Test
    public void should_get_jobOffer_by_id(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isEntityResponse()
                    .hasId()
                    .hasEntity(entity -> assertEquals(context, entity, testInstance));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_getting_jobOffer_when_user_is_voter(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_getting_jobOffer_when_user_is_sponsor(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPONSOR, context);
    }

    @Test
    public void should_not_authorize_getting_jobOffer_when_user_is_speaker(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPEAKER, context);
    }

    private void assert_forbidden_when_getting_by_id(Role role, TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, role))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_jobOffer_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_jobOffer_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String id = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_jobOffer_title(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        final String newTitle = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("title", newTitle);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(JOB_OFFERS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("title", newTitle)
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("link", testInstance.getLink())
                    .hasStringEqualsTo("sponsorId", testInstance.getSponsorId());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_jobOffer_description(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        final String newDescription = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("description", newDescription);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(JOB_OFFERS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("title", testInstance.getTitle())
                    .hasStringEqualsTo("description", newDescription)
                    .hasStringEqualsTo("link", testInstance.getLink())
                    .hasStringEqualsTo("sponsorId", testInstance.getSponsorId());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_jobOffer_link(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        final String newLink = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("link", newLink);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(JOB_OFFERS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("title", testInstance.getTitle())
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("link", newLink)
                    .hasStringEqualsTo("sponsorId", testInstance.getSponsorId());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_jobOffer_sponsorId(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        final String newSponsorId = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("sponsorId", newSponsorId);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(JOB_OFFERS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("title", testInstance.getTitle())
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("link", testInstance.getLink())
                    .hasStringEqualsTo("sponsorId", newSponsorId);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_jobOffer_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c4a3ea8aefd394cce055574";
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + fakeId)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_jobOffer_when_id_format_is_not_valid(TestContext context) {
        // given
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + "123")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_jobOffer(TestContext context) {
        // given
        final JobOffer testInstance = getTestJobOffer();
        final String id = insertToDatabase(JOB_OFFERS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_jobOffer_when_id_format_is_not_valid(TestContext context) {
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }
}
