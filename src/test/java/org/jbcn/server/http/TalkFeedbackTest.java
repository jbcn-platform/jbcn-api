package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.votes.AttendeeVote;
import org.jbcn.server.model.votes.VoteSource;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.lang.Boolean.FALSE;
import static org.jbcn.server.persistence.Collections.ATTENDEES_VOTES;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class TalkFeedbackTest extends VertxTestSetup {

    private static final String TEST_USER = "talk_feedback_test_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }


    @Test
    public void should_get_feedback_by_talk_id(TestContext context) {
        // given
        final String suffix = UUID.randomUUID().toString();
        final String talkId = "talkId-" + suffix;
        insertTestVote(suffix, talkId, 5, "Awes0me!!", "none");
        insertTestVote(suffix, talkId, 1, "Awes0me!!", "You rock!");
        insertTestVote(suffix, talkId, 2, "I've seen better ¬_¬", "Made me want to know more");
        insertTestVote(suffix, talkId, 3, null, "Made me want to know more");
        insertTestVote(suffix, talkId, 4, "", "Made me want to know more");
        insertTestVote(suffix, talkId, 4, "I've seen better ¬_¬", "Made me want to know more");
        insertTestVote(suffix, talkId + "--", 3, "More awes0me!!", null);

        // when
        final Async async = context.async();
        webClient.get("/api/talk/" + talkId + "/feedback")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final Integer votes = responseBody.getInteger("votes");
                context.assertEquals(6, votes);
                final Double average = responseBody.getDouble("average");
                context.assertEquals(3.167, average);

                        /*
                        {
                         "compliments" : {
                           "want to know more": 2,
                           "fun": 4,
                         }
                         "comments": ["amazing", "awes0me!!"]
                        }
                        */
                final JsonObject compliments = responseBody.getJsonObject("compliments");
                context.assertTrue(compliments.containsKey("Made me want to know more"));
                context.assertEquals(4, compliments.getInteger("Made me want to know more"));
                context.assertTrue(compliments.containsKey("You rock!"));
                context.assertEquals(1, compliments.getInteger("You rock!"));
                context.assertEquals(2, compliments.fieldNames().size());

                // filters null and empty comments
                final List<String> comments = responseBody.getJsonArray("comments").stream()
                    .map(Object::toString)
                    .collect(Collectors.toList());

                context.assertEquals(2, comments.size());
                context.assertEquals("Awes0me!!", comments.get(0));
                context.assertEquals("I've seen better ¬_¬", comments.get(1));

                context.assertEquals(4, responseBody.fieldNames().size());

                async.complete();
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_empty_feedback_if_no_votes_exist(TestContext context) {
        // given
        final String suffix = UUID.randomUUID().toString();
        final String talkId = "talkId-" + suffix;

        // when
        final Async async = context.async();
        webClient.get("/api/talk/" + talkId + "/feedback")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final Integer votes = responseBody.getInteger("votes");
                context.assertEquals(0, votes);
                context.assertEquals(1, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // redirects the request to getTalkById
    @Test
    public void should_fail_when_talkId_is_empty(TestContext context) {
        // given
        final String talkId = "";

        // when
        final Async async = context.async();
        webClient.get("/api/talk/" + talkId + "/feedback")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private String insertTestVote(String suffix, String talkId, Integer vote, String other, String delivery) {
        final LocalDateTime now = LocalDateTime.now();
        final AttendeeVote testVote = new AttendeeVote(null,
            talkId,
            "user-" + suffix + "@email.com",
            vote,
            delivery,
            other,
            VoteSource.mobile,
            now,
            now,
            TEST_USER);
        return insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));
    }

}
