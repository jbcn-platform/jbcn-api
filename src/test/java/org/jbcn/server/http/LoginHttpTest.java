package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.persistence.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static util.VertxSetup.insertToDatabase;


@RunWith(VertxUnitRunner.class)
public class LoginHttpTest extends VertxTestSetup {

    public static final String RESOURCE = "/login";

    @Test
    public void should_fail_creating_token_when_credentials_are_wrong(TestContext context) {
        // given
        final JsonObject body = new JsonObject()
                .put("username", "fake_username")
                .put("password", "fake_password");

        // when-then
        final Async async = context.async();
        webClient.post(LoginHttpTest.RESOURCE)
                .sendJsonObject(body, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("No account found for user [fake_username]", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_token(TestContext context) {
        // given
        final String username = "test-user";
        final String password = "test-password";
        initTestUser(username);

        // when-then
        final JsonObject body = new JsonObject()
                .put("username", username)
                .put("password", password);

        final Async async = context.async();

        webClient.post(LoginHttpTest.RESOURCE)
                .sendJsonObject(body, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertTrue(data.getString("token").trim().length() > 0);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_token_when_username_is_null(TestContext context) {
        // given
        // when-then
        final JsonObject body = new JsonObject()
                .put("username", (String) null)
                .put("password", "something");

        final Async async = context.async();
        webClient.post(RESOURCE)
                .sendJsonObject(body, assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required_field.username", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_token_when_username_is_empty(TestContext context) {
        // given
        // when-then
        final JsonObject body = new JsonObject()
                .put("username", "")
                .put("password", "something");

        final Async async = context.async();
        webClient.post(RESOURCE)
                .sendJsonObject(body, assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required_field.username", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_token_when_password_is_null(TestContext context) {
        // given

        // when-then
        final JsonObject body = new JsonObject()
                .put("username", "something")
                .put("password", (String) null);

        final Async async = context.async();
        webClient.post(RESOURCE)
                .sendJsonObject(body, assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required_field.password", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_token_when_password_is_empty(TestContext context) {
        // given

        // when-then
        final JsonObject body = new JsonObject()
                .put("username", "someth1ng")
                .put("password", "");

        final Async async = context.async();
        webClient.post(RESOURCE)
                .sendJsonObject(body, assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required_field.password", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    private void initTestUser(String username) {
        final Document user = new Document()
                .append("username", username)
                .append("password", "C36232D738F6B374D73028A7A8AFD14E80946D4DA3B82039F024A5E2E0F02793701123A4E19B448A10F4E8DA4D935FF574A09A1C0F1F496729F5DA1175D5F817")
                .append("salt", "E31A879094B0BE7A67E3210EDDCE638C59CA8EFA971EB04D393D9C315714E2E3")
                .append("roles", List.of("admin", "voter"));
        insertToDatabase(Collections.USER, user);
    }

}
