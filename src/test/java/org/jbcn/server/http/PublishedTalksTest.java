package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;

import static org.jbcn.server.persistence.Collections.TALK;
import static util.CustomAssertions.equalArrays;
import static util.TestDataGenerator.getSpeaker;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class PublishedTalksTest extends VertxTestSetup {

    private static final String PUBLIC_ENDPOINT = "/public/talks/";
    private static final String TALKS_TEST_USER = "talk_test_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }

    @Test
    public void should_generate_talks_json_for_site(TestContext context) {
        // given
        final Talk testTalk1 = getTestTalk();
        testTalk1.setPublished(true);
        testTalk1.setLanguages(List.of("eng", "es"));
        final JsonObject entries = JsonObject.mapFrom(testTalk1);
        entries.remove("_id");
        final String talkId1 = insertToDatabase(TALK, Document.parse(entries.encode()));

        final Talk testTalk2 = getTestTalk();
        testTalk2.setPublished(true);
        testTalk2.setLanguages(List.of("eng"));
        testTalk2.setSpeakers(List.of(getSpeaker("-1"), getSpeaker("-2"), getSpeaker("-3"), getSpeaker("-4")));
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        final String talkId2 = insertToDatabase(TALK, Document.parse(entries2.encode()));

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final JsonArray items = responseBody.getJsonArray("items");
                context.assertEquals(2, items.size());
                // id's are correctly inserted
                // results are sorted by date
                assertValidTalk(context, talkId1, items.getJsonObject(0), 1);
                assertValidTalk(context, talkId2, items.getJsonObject(1), 4);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_generate_talks_json_for_site_with_html_formatted_abstract(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        testTalk.setLanguages(List.of("eng", "es"));
        testTalk.setPaperAbstract("""
            Just some names:
            * Maedhros
            * Maglor
            * Celegorm
            * Caranthir
            * Curufin
            * Amrod
            * Amras""");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT + "?format=html")
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final JsonArray items = responseBody.getJsonArray("items");
                context.assertEquals(1, items.size());
                final String expectedAbstract = """
                    <p>Just some names:</p>
                    <ul>
                    <li>Maedhros</li>
                    <li>Maglor</li>
                    <li>Celegorm</li>
                    <li>Caranthir</li>
                    <li>Curufin</li>
                    <li>Amrod</li>
                    <li>Amras</li>
                    </ul>""".replaceAll("\n", "");
                context.assertEquals(expectedAbstract, items.getJsonObject(0).getString("abstract"));
                assertValidTalk(context, talkId, items.getJsonObject(0), 1);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_talk_by_id(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        testTalk.setLanguages(List.of("eng", "es"));
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId1 = insertToDatabase(TALK, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT + talkId1)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER))
            .send(assertJsonResponse(async, context, 200, talkJson -> {
                context.assertEquals(talkJson.getString("id"), talkId1);
                context.assertEquals(talkJson.getString("title"), testTalk.getTitle());
                context.assertEquals(talkJson.getJsonArray("languages").size(), testTalk.getLanguages().size());
                context.assertEquals(talkJson.getString("abstract"), testTalk.getPaperAbstract());
                context.assertTrue(equalArrays(talkJson.getJsonArray("tags"), testTalk.getTags()));
                context.assertEquals(talkJson.getString("level"), testTalk.getLevel());
                final Speaker speaker = testTalk.getSpeakers().get(0);
                final String generatedSpeakerRef = Speaker.generateRef(speaker.getFullName(), speaker.getEmail());
                context.assertTrue(equalArrays(talkJson.getJsonArray("speakers"), List.of(generatedSpeakerRef)));
                context.assertEquals(8, talkJson.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertValidTalk(TestContext context, String talkId1, JsonObject jsonObject, int speakersCount) {
        context.assertEquals(jsonObject.fieldNames().size(), 8);
        context.assertEquals(jsonObject.getString("id"), talkId1);
        context.assertNotNull(jsonObject.getString("title"));
        context.assertNotNull(jsonObject.getString("abstract"));
        context.assertEquals(jsonObject.getString("type"), "talk");
        context.assertNotNull(jsonObject.getJsonArray("tags"));
        context.assertNotNull(jsonObject.getString("level"));
        context.assertNotNull(jsonObject.getJsonArray("languages"));
        context.assertEquals(jsonObject.getJsonArray("speakers").size(), speakersCount);
    }
}
