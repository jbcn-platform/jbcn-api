package org.jbcn.server.http;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.*;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.TimeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.TestDataGenerator;
import util.VertxTestSetup;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.Constants.TALK_QUEUE;
import static org.jbcn.server.model.Speaker.calculateCode;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.SESSIONS;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.*;
import static util.VertxSetup.*;

// NOTE: there's no delete talk operation
@RunWith(VertxUnitRunner.class)
public class TalkHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/api/talk/";
    private static final String TALKS_TEST_USER = "talk_test_user";

    private static final String CONDITIONS = "conditions";

    private static class ConditionsAggregation {
        static final String ALL = "all";
        static final String ANY = "any";
    }

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }


    @Test
    public void should_get_talk_by_id(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final Speaker testSpeaker = testTalk.getSpeakers().get(0);
        testSpeaker.setCompany("FancyMall");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));

        // when
        final Async async = context.async();
        webClient.get(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final JsonObject talk = responseBody.getJsonObject("data").getJsonObject("instance");
                context.assertEquals(18, talk.fieldNames().size());
                context.assertEquals(talkId, talk.getString("_id"));
                context.assertEquals(testTalk.getTitle(), talk.getString("title"));
                context.assertEquals(testTalk.getPaperId(), talk.getString("paperId"));
                context.assertNotNull(talk.getLong(CREATED_DATE));
                context.assertEquals(talk.getLong(CREATED_DATE), talk.getLong(LAST_UPDATE));

                final JsonArray speakers = talk.getJsonArray("speakers");
                context.assertEquals(1, speakers.size());
                final JsonObject speaker = speakers.getJsonObject(0);
                context.assertEquals(testSpeaker.getFullName(), speaker.getString("fullName"));
                context.assertEquals(testSpeaker.getTshirtSize(), speaker.getString("tshirtSize"));
                context.assertEquals(testSpeaker.getCompany(), speaker.getString("company"));
                context.assertNotNull(testSpeaker.getRef());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_get_talk_when_user_is_sponsor(TestContext context) {
        // given
        final String talkId = "5c24e4b0554b4947c8163049";
        // when
        final Async async = context.async();
        webClient.get(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.SPONSOR))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_talk_when_id_does_not_exists(TestContext context) {
        // given
        final String talkId = "5c24e4b0554b4947c8163049";
        // when
        final Async async = context.async();
        webClient.get(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.talk.unknown_talk", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_talk_when_id_format_is_not_valid(TestContext context) {
        // given
        final String talkId = "123";
        // when
        final Async async = context.async();
        webClient.get(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_add_talk(TestContext context) {
        // given
        final Paper paper = TestDataGenerator.getTestPaper();
        final String paperId = UUID.randomUUID().toString();
        final User responsiblePerson = getTestUser();
        final JsonObject entries = DocumentConverter.toJson(paper)
            .put("responsiblePerson", JsonObject.mapFrom(responsiblePerson))
            .put("_id", paperId);

        // when
        final Async async = context.async();
        vertx.eventBus().send(TALK_QUEUE, entries, new DeliveryOptions().addHeader("action", "talk-add"), reply -> {
            if (reply.succeeded()) {
                getCollection(TALK).countDocuments((count, t) -> context.assertEquals(count, 1l));

                final Document insertedTalk = getFromDatabase(TALK, ((JsonObject) reply.result().body()).getString("_id"));
                final Talk talk = DocumentConverter.parseToObject(insertedTalk, Talk.class);
                context.assertEquals(talk.getState(), TalkState.TALK_STATE_UNCONFIRMED);
                context.assertEquals(talk.getResponsiblePerson(), responsiblePerson.getUsername());
                context.assertEquals(talk.getPaperId(), paperId);
                async.complete();

            } else {
                context.fail();
            }
        });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_title(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String newTitle = "new title";
        testTalk.setTitle(newTitle);
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                getCollection(TALK).find().first((result, t) -> {
                    final Talk talk = DocumentConverter.parseToObject(new JsonObject(result.toJson()), Talk.class);
                    context.assertEquals(newTitle, talk.getTitle());
                    context.assertTrue(talk.getLastUpdate().isAfter(talk.getCreatedDate()));
                    async.complete();
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_updating_talk_when_user_is_sponsor(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String newTitle = "new title";
        testTalk.setTitle(newTitle);
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.SPONSOR))
            .sendJsonObject(newTestTalk, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_session(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String sessionId = insertToDatabase(SESSIONS, DocumentConverter.getDocument(getTestSession()));
        final JsonObject newTestTalk = JsonObject.mapFrom(Talk.builder().sessionId(sessionId).build());

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                final Document updatedTalk = getFromDatabase(TALK, talkId);
                context.assertEquals(sessionId, updatedTalk.getString("sessionId"));
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_comments(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final String newComments = "new comment-" + UUID.randomUUID();
        final JsonObject newTalk = new JsonObject()
            .put("comments", newComments);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                getCollection(TALK).find().first((result, t) -> {
                    final Talk talk = DocumentConverter.parseToObject(new JsonObject(result.toJson()), Talk.class);
                    context.assertEquals(newComments, talk.getComments());
                    context.assertTrue(talk.getLastUpdate().isAfter(talk.getCreatedDate()));
                    async.complete();
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_languages(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final JsonObject newTalk = new JsonObject()
            .put("languages", new JsonArray(List.of("jp", "ko")));

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                getCollection(TALK).find().first((result, t) -> {
                    final Talk talk = DocumentConverter.parseToObject(new JsonObject(result.toJson()), Talk.class);
                    context.assertEquals(2, talk.getLanguages().size());
                    context.assertTrue(talk.getLanguages().containsAll(List.of("jp", "ko")));
                    context.assertTrue(talk.getLastUpdate().isAfter(talk.getCreatedDate()));
                    async.complete();
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_title_when_sending_only_title(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String newTitle = "new title-" + UUID.randomUUID();

        final JsonObject newTestTalk = new JsonObject()
            .put("title", newTitle);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final Document fromDatabase = getFromDatabase(TALK, talkId);
                context.assertEquals(18, fromDatabase.keySet().size());

                final Talk talk = DocumentConverter.parseToObject(fromDatabase, Talk.class);
                context.assertEquals(newTitle, talk.getTitle());
                context.assertEquals(null, talk.getLastActionBy());
                context.assertTrue(talk.getLastUpdate().isAfter(talk.getCreatedDate()));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_preferenceDay_when_sending_only_preferenceDay(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final String newDay = "2020/04/03";
        final JsonObject newTestTalk = new JsonObject()
            .put("preferenceDay", newDay);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final Document fromDatabase = getFromDatabase(TALK, talkId);
                context.assertEquals(18, fromDatabase.keySet().size());

                final Talk talk = DocumentConverter.parseToObject(fromDatabase, Talk.class);
                context.assertEquals(newDay, talk.getPreferenceDay());
                context.assertEquals(null, talk.getLastActionBy());
                context.assertTrue(talk.getLastUpdate().isAfter(talk.getCreatedDate()));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    @Test
    public void should_update_talk_multiple_fields(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setType(ProposalType.TALK.getType());
        testTalk.setSponsor(FALSE);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final String newTitle = "new title-" + UUID.randomUUID();
        final String newComments = "new comments-" + UUID.randomUUID();
        final JsonObject newTestTalk = new JsonObject()
            .put("title", newTitle)
            .put("type", ProposalType.WORKSHOP.getType())
            .put("comments", newComments)
            .put("sponsor", TRUE);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final Document fromDatabase = getFromDatabase(TALK, talkId);
                context.assertEquals(18, fromDatabase.keySet().size());

                final Talk talk = DocumentConverter.parseToObject(fromDatabase, Talk.class);
                context.assertEquals(newTitle, talk.getTitle());
                context.assertEquals(ProposalType.WORKSHOP.getType(), talk.getType());
                context.assertEquals(newComments, talk.getComments());
                context.assertEquals(TRUE, talk.getSponsor());

                context.assertEquals(null, talk.getLastActionBy());
                context.assertTrue(talk.getLastUpdate().isAfter(talk.getCreatedDate()));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talks_speaker_jobTitle(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String newJobTitle = "Cool and great speaker!!";
        testTalk.getSpeakers().get(0).setJobTitle(newJobTitle);
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                getCollection(TALK).find().first((result, t) -> {
                    final Talk talk = DocumentConverter.parseToObject(new JsonObject(result.toJson()), Talk.class);
                    final Speaker updatedSpeaker = talk.getSpeakers().get(0);
                    context.assertEquals(updatedSpeaker.getJobTitle(), newJobTitle);
                    context.assertNotEquals(talk.getCreatedDate(), talk.getLastUpdate());
                    // update code
                    context.assertEquals(calculateCode(updatedSpeaker.getFullName(), updatedSpeaker.getEmail()), updatedSpeaker.getCode());

                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talks_speaker_public_picture(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String newPublicPicture = "https://domain.org/public/picture";
        testTalk.getSpeakers().get(0).setPublicPicture(newPublicPicture);
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                getCollection(TALK).find().first((result, t) -> {
                    final Talk talk = DocumentConverter.parseToObject(new JsonObject(result.toJson()), Talk.class);
                    final Speaker updatedSpeaker = talk.getSpeakers().get(0);
                    context.assertEquals(updatedSpeaker.getPublicPicture(), newPublicPicture);
                    context.assertNotEquals(talk.getCreatedDate(), talk.getLastUpdate());
                    // update code
                    context.assertEquals(calculateCode(updatedSpeaker.getFullName(), updatedSpeaker.getEmail()), updatedSpeaker.getCode());
                    async.complete();
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talks_speaker_jobTitle_and_company(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String newJobTitle = "Cool and great speaker!!";
        final String newCompany = "Cool-a-cola";
        testTalk.getSpeakers().get(0).setJobTitle(newJobTitle);
        testTalk.getSpeakers().get(0).setCompany(newCompany);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        testTalk.setTitle(newJobTitle);
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                getCollection(TALK).find().first((result, t) -> {
                    final Talk talk = DocumentConverter.parseToObject(new JsonObject(result.toJson()), Talk.class);
                    final Speaker updatedSpeaker = talk.getSpeakers().get(0);
                    context.assertEquals(newJobTitle, updatedSpeaker.getJobTitle());
                    context.assertEquals(newCompany, updatedSpeaker.getCompany());
                    context.assertNotEquals(talk.getCreatedDate(), talk.getLastUpdate());
                    // update code
                    context.assertEquals(calculateCode(updatedSpeaker.getFullName(), updatedSpeaker.getEmail()), updatedSpeaker.getCode());
                    async.complete();
                });

            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_talk_when_state_is_not_valid(TestContext context) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        final Talk testTalk = getTestTalk(now);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk)
            .put("state", "1234567890");

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.talk.invalid_state", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_talk_when_type_is_not_valid(TestContext context) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(now);
        testTalk.setLastUpdate(now);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk)
            .put("type", "1234567890");

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.talk.invalid_type", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_when_level_is_not_valid(TestContext context) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(now);
        testTalk.setLastUpdate(now);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final Talk newTalk = testTalk;
        final JsonObject newTestTalk = JsonObject.mapFrom(newTalk)
            .put("level", "1234567890");

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.talk.invalid_level", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_speaker_data(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String newEmail = "newmail@newdomain.com";
        final String newLinkedIn = "https://new.linkedin.com/profile";
        final Speaker newSpeaker = testTalk.getSpeakers().get(0);
        newSpeaker.setEmail(newEmail);
        newSpeaker.setLinkedin(newLinkedIn);
        final JsonObject newTestTalk = JsonObject.mapFrom(testTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final Document updatedTalk = getFromDatabase(TALK, talkId);
                final Talk talk = DocumentConverter.parseToObject(updatedTalk, Talk.class);
                context.assertNotEquals(talk.getCreatedDate(), talk.getLastUpdate());
                final Speaker updatedSpeaker = talk.getSpeakers().get(0);
                context.assertEquals(updatedSpeaker.getEmail(), newEmail);
                context.assertEquals(updatedSpeaker.getLinkedin(), newLinkedIn);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_talk_with_same_data_and_only_modify_lastUpdate(TestContext context) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        long currentMillis = now.atZone(ZoneId.of("Z")).toInstant().toEpochMilli();

        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(now);
        testTalk.setLastUpdate(now);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final JsonObject newTestTalk = JsonObject.mapFrom(testTalk);

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {

                context.assertEquals(1, responseBody.fieldNames().size());
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);

                final Document updatedTalk = getFromDatabase(TALK, talkId);
                Long createdDate = updatedTalk.getLong(CREATED_DATE);
                Long lastUpdate = updatedTalk.getLong(LAST_UPDATE);
                context.assertEquals(currentMillis, createdDate);
                context.assertTrue(lastUpdate > createdDate);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_and_trim_talk_tags_when_they_contain_spaces(TestContext context) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        long currentMillis = now.atZone(ZoneId.of("Z")).toInstant().toEpochMilli();

        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(now);
        testTalk.setLastUpdate(now);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final JsonObject newTestTalk = new JsonObject()
            .put("tags", new JsonArray()
                .add("new_tag_1 ")
                .add(" new_tag_2")
                .add(" new_tag_3")
            );

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 200, responseBody -> {

                context.assertEquals(1, responseBody.fieldNames().size());
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);

                final Document updatedTalk = getFromDatabase(TALK, talkId);
                final List<String> tags = (List<String>) updatedTalk.get("tags");
                context.assertTrue(tags.contains("new_tag_1"));
                context.assertTrue(tags.contains("new_tag_2"));
                context.assertTrue(tags.contains("new_tag_3"));
                Long createdDate = updatedTalk.getLong(CREATED_DATE);
                Long lastUpdate = updatedTalk.getLong(LAST_UPDATE);
                context.assertEquals(currentMillis, createdDate);
                context.assertTrue(lastUpdate > createdDate);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_talk_sponsor_when_id_does_not_exists(TestContext context) {
        // given
        final String talkId = "123";
        final JsonObject talk = new JsonObject("{\"title\":\"amazing talk title\"}");

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(talk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // NOTE: id is the first validation
    @Test
    public void should_fail_update_talk_when_id_format_is_not_valid(TestContext context) {
        // given
        final String fakeId = "5c3b64d4aefd3931c0ec7b54";
        final JsonObject talk = new JsonObject("{\"title\":\"amazing talk title\"}");

        // when
        final Async async = context.async();
        webClient.put(RESOURCE + fakeId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(talk, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.not_found", responseBody.getString("error"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_talk_when_no_body_is_sent(TestContext context) {
        assert_fail_update_talk_when_empty_body(context, null);
    }

    @Test
    public void should_fail_update_talk_when_empty_body_is_sent(TestContext context) {
        assert_fail_update_talk_when_empty_body(context, "");
    }

    @Test
    public void should_fail_update_talk_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_update_talk_when_empty_body(context, "{}");
    }

    private void assert_fail_update_talk_when_empty_body(TestContext context, String newTalk) {
        final Talk testTalk = getTestTalk();
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        // when
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendBuffer(newTalk == null ? null : Buffer.buffer(newTalk), assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.body.required", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_update_talk_when_data_does_not_match_any_talk_property_name(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("random_stuff", UUID.randomUUID().toString());

        // then
        should_not_update_talk(context, emptyJson);
    }

    @Test
    public void should_not_update_talk_tags_when_empty_list_is_sent(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("tags", new ArrayList<String>());

        // then
        should_not_update_talk(context, emptyJson);
    }

    @Test
    public void should_not_update_talk_tags_when_empty_values_are_sent(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("tags", Arrays.asList("", null, "  ", "\n", "\t"));

        // then
        should_not_update_talk(context, emptyJson);
    }

    public void should_not_update_talk(TestContext context, JsonObject body) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        final Talk testTalk = getTestTalk(now);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        // then
        final Async async = context.async();
        webClient.put(RESOURCE + talkId)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(body, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                // traceable data is not modified
                final Document insertedTalk = getFromDatabase(TALK, talkId);
                long nowMillis = TimeUtils.toTimestamp(now);
                context.assertEquals(nowMillis, insertedTalk.getLong(CREATED_DATE));
                context.assertEquals(nowMillis, insertedTalk.getLong(Traceable.LAST_UPDATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_search_talks_when_body_is_empty(TestContext context) {
        // given

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals(responseBody.getString("error"), "error.search.conditions_minimum");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    /**
     * Default values if not set:
     * - page = 0
     * - size = 50
     * - sort (column) = title
     * - asc = true
     * - filters = empty
     */
    @Test
    public void should_search_talks_with_minimum_options(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId1 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId2 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId3 = insertToDatabase(TALK, Document.parse(entries.encode()));

        final JsonObject searchParams = new JsonObject();

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(retriedIds.size(), 3);
                context.assertTrue(retriedIds.contains(talkId1));
                context.assertTrue(retriedIds.contains(talkId2));
                context.assertTrue(retriedIds.contains(talkId3));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talks_and_return_selected_fields(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId1 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId2 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId3 = insertToDatabase(TALK, Document.parse(entries.encode()));

        final JsonObject searchParams = new JsonObject()
            .put("fields", Arrays.asList("title", "edition"));

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                items.forEach(it -> context.assertEquals(3, ((JsonObject) it).fieldNames().size()));

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(retriedIds.size(), 3);
                context.assertTrue(retriedIds.contains(talkId1));
                context.assertTrue(retriedIds.contains(talkId2));
                context.assertTrue(retriedIds.contains(talkId3));

                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO test out of rang options
    // TODO test with filters
    @Test
    public void should_search_talks_with_pagination_and_sort_options(TestContext context) {

        // given
        final Talk testTalk = getTestTalk();
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));
        insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId2 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId3 = insertToDatabase(TALK, Document.parse(entries.encode()));

        final JsonObject searchParams = new JsonObject()
            .put("sort", "_id")
            .put("asc", FALSE)
            .put("page", 0)
            .put("size", 2);

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 4);

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());

                context.assertEquals(retriedIds.size(), 2);
                context.assertEquals(items.getJsonObject(0).getString("_id"), talkId3);
                context.assertEquals(items.getJsonObject(1).getString("_id"), talkId2);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talks_by_published_true_all(TestContext context) {
        should_search_talks_by_conditions(ConditionsAggregation.ALL, context);
    }

    @Test
    public void should_search_talks_by_published_true_any(TestContext context) {
        should_search_talks_by_conditions(ConditionsAggregation.ANY, context);
    }

    public void should_search_talks_by_conditions(String conditionsAggregation, TestContext context) {
        // given
        final Talk testTalk = getTestTalk("-1");
        testTalk.setPublished(true);
        final String talkId1 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String talkId2 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        testTalk.setPublished(false);
        final String talkId3 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final JsonObject searchParams = new JsonObject()
            .put("sort", CREATED_DATE)
            .put("asc", FALSE)
            .put("page", 0)
            .put("size", 10)
            .put(CONDITIONS, new JsonObject().put(
                conditionsAggregation,
                List.of(
                    new JsonObject()
                        .put("name", "published")
                        .put("value", true)
                )));

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(2, data.getInteger("total"));

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(2, retriedIds.size());
                context.assertTrue(retriedIds.contains(talkId1));
                context.assertTrue(retriedIds.contains(talkId2));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talk_by_exact_title_with_parenthesis(TestContext context) {

        // given
        final Talk testTalk = getTestTalk("-1");
        final String title = "Some text with (parentheses) text";
        testTalk.setTitle(title);
        final String talkId = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final JsonObject searchParams = new JsonObject()
            .put(CONDITIONS, new JsonObject().put(
                ConditionsAggregation.ALL,
                List.of(
                    new JsonObject()
                        .put("name", "title")
                        .put("value", title)
                )));

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.getInteger("total"));

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(1, retriedIds.size());
                context.assertTrue(retriedIds.contains(talkId));
                context.assertEquals(title, items.getJsonObject(0).getString("title"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talks_by_published_false(TestContext context) {

        // given
        final Talk testTalk = getTestTalk("-1");
        testTalk.setPublished(false);
        final String talkId1 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String talkId2 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        testTalk.setPublished(true);
        final String talkId3 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final JsonObject searchParams = new JsonObject()
            .put("sort", CREATED_DATE)
            .put("asc", FALSE)
            .put("page", 0)
            .put("size", 500)
            .put(CONDITIONS, new JsonObject().put(
                ConditionsAggregation.ALL,
                List.of(
                    new JsonObject()
                        .put("name", "published")
                        .put("value", false)
                )));

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(2, data.getInteger("total"));

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(2, retriedIds.size());
                context.assertTrue(retriedIds.contains(talkId1));
                context.assertTrue(retriedIds.contains(talkId2));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talks_with_default_values_and_not_fail_when_conditions_are_null(TestContext context) {

        // given
        final Talk testTalk = getTestTalk("-1");
        testTalk.setPublished(false);
        final String talkId1 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        final String talkId2 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));
        testTalk.setPublished(true);
        final String talkId3 = insertToDatabase(TALK, DocumentConverter.getDocument(testTalk));

        final JsonObject searchParams = new JsonObject()
            .put("sort", (String) null)
            .put("asc", (String) null)
            .put("page", (String) null)
            .put("size", (String) null)
            .put(CONDITIONS, (String) null);

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(3, data.getInteger("total"));

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(3, retriedIds.size());
                context.assertTrue(retriedIds.contains(talkId1));
                context.assertTrue(retriedIds.contains(talkId2));
                context.assertTrue(retriedIds.contains(talkId3));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talks_by_sender_company(TestContext context) {

        // given: 2 of 3 talks with company
        final String company = "FunFunPlace";
        final Talk testTalk = getTestTalk();
        testTalk.getSpeakers().get(0).setCompany(company);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId1 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId2 = insertToDatabase(TALK, Document.parse(entries.encode()));
        testTalk.getSpeakers().get(0).setCompany(null);
        final JsonObject entries2 = JsonObject.mapFrom(testTalk);
        entries2.remove("_id");
        final String talkId3 = insertToDatabase(TALK, Document.parse(entries2.encode()));

        final JsonObject searchParams = new JsonObject()
            .put("sort", "_id")
            .put("asc", FALSE)
            .put("page", 0)
            .put("size", 50)
            .put(CONDITIONS, new JsonObject().put(
                ConditionsAggregation.ALL,
                List.of(
                    new JsonObject()
                        .put("name", "speaker.company")
                        .put("value", company)
                )));

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.ADMIN))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(2, data.getInteger("total"));

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(2, retriedIds.size());
                context.assertTrue(retriedIds.contains(talkId1));
                context.assertTrue(retriedIds.contains(talkId2));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_talks_by_sender_company_not_set(TestContext context) {
        should_search_talks_by_sender_company_not_set(context, Optional.empty());
    }

    @Test
    public void should_search_talks_by_sender_company_is_null(TestContext context) {
        should_search_talks_by_sender_company_not_set(context, Optional.ofNullable(null));
    }

    @Test
    public void should_search_talks_by_sender_company_is_empty_string(TestContext context) {
        should_search_talks_by_sender_company_not_set(context, Optional.ofNullable(""));
    }

    // check for null, empty and unset using keyword "(not_set)"
    public void should_search_talks_by_sender_company_not_set(TestContext context, Optional<String> value) {
        // given: 2 of 3 don't have company
        final Talk testTalk = getTestTalk();
        testTalk.getSpeakers().get(0).setCompany("valid company value");
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId1 = insertToDatabase(TALK, Document.parse(entries.encode()));

        if (value.isPresent()) {
            entries.getJsonArray("speakers").getJsonObject(0).put("company", value.get());
        } else {
            entries.getJsonArray("speakers").getJsonObject(0).remove("company");
        }
        final String talkId2 = insertToDatabase(TALK, Document.parse(entries.encode()));
        final String talkId3 = insertToDatabase(TALK, Document.parse(entries.encode()));

        final JsonObject searchParams = new JsonObject()
            .put("sort", "_id")
            .put("asc", FALSE)
            .put("page", 0)
            .put("size", 50)
            .put(CONDITIONS, new JsonObject().put(
                ConditionsAggregation.ALL,
                List.of(
                    new JsonObject()
                        .put("name", "speaker.company")
                        .put("value", "(not_set)")
                )));

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(2, data.getInteger("total"));

                final JsonArray items = data.getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertEquals(2, retriedIds.size());
                context.assertTrue(retriedIds.contains(talkId2));
                context.assertTrue(retriedIds.contains(talkId3));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_search_talks_when_parameters_are_not_a_parseable_json(TestContext context) {
        // given
        final String invalidJson = "{\"page\": 20,}";

        // when
        final Async async = context.async();
        webClient.post("/api/talk/search")
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.HELPER))
            .sendBuffer(Buffer.buffer(invalidJson), assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals(responseBody.getString("error"), "error.json.invalid_format");
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_talk(TestContext context) {
        // given
        final Talk testTrack = getTestTalk();
        final String id = insertToDatabase(TALK, DocumentConverter.getDocument(testTrack));

        // when
        final Async async = context.async();
        webClient.delete(RESOURCE + id)
            .bearerTokenAuthentication(getNewSession(TALKS_TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }
}
