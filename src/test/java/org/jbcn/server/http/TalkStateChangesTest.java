package org.jbcn.server.http;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.Talk;
import org.jbcn.server.model.TalkState;
import org.jbcn.server.persistence.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.time.LocalDateTime;
import java.util.function.Consumer;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.*;


@RunWith(VertxUnitRunner.class)
public class TalkStateChangesTest extends VertxTestSetup {

    private static final String TEST_USER = "test_user";
    private static final String NON_EXISTENT_TALK_ID = "5c2df49caefd393db8ed19c1";

    @Before
    public void before(TestContext context) {
        dropCollection(TALK);
    }


    @Test
    public void should_publish_talk(TestContext context) {
        // given
        final LocalDateTime now = LocalDateTime.now();
        final Talk testTalk = getTestTalk(now);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));

        final JsonObject publishRequest = new JsonObject()
                .put("published", true);

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendJsonObject(publishRequest, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(1, responseBody.fieldNames().size());

                    final Document updatedTalk = getFromDatabase(Collections.TALK, talkId);
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(TEST_USER, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertTrue(updatedTalk.getLong(LAST_UPDATE) > updatedTalk.getLong(CREATED_DATE));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_change_talk_to_not_published(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(LocalDateTime.now());
        testTalk.setPublished(true);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));

        final String publishRequest = "{\"published\": false }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(publishRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(1, responseBody.fieldNames().size());

                    final Document updatedTalk = getFromDatabase(Collections.TALK, talkId);
                    context.assertEquals(FALSE, updatedTalk.getBoolean("published"));
                    context.assertEquals(TEST_USER, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertTrue(updatedTalk.getLong(LAST_UPDATE) > updatedTalk.getLong(CREATED_DATE));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_talk_when_id_does_not_exists(TestContext context) {
        // given
        final String talkId = NON_EXISTENT_TALK_ID;
        final String publishRequest = "{\"published\": true }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(publishRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.talk.unknown_talk", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_publish_when_id_format_is_not_valid(TestContext context) {
        // given
        final String talkId = "1234567890";
        final String publishRequest = "{\"published\": true }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(publishRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_publish_when_no_body_is_sent(TestContext context) {
        assert_fail_publish_when_empty_body(context, null);
    }

    @Test
    public void should_fail_publish_when_empty_body_is_sent(TestContext context) {
        assert_fail_publish_when_empty_body(context, "");
    }

    @Test
    public void should_fail_publish_when_empty_json_is_sent(TestContext context) {
        assert_fail_publish_when_empty_body(context, "{}");
    }

    private void assert_fail_publish_when_empty_body(TestContext context, String body) {
        // given
        final String talkId = NON_EXISTENT_TALK_ID;

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(body == null ? null : Buffer.buffer(body), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_publish_when_published_field_is_null(TestContext context) {
        // given
        final String talkId = NON_EXISTENT_TALK_ID;
        final String publishRequest = "{\"published\": null }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(publishRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required_field.published", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_publish_when_published_field_is_not_present(TestContext context) {
        // given
        final String talkId = NON_EXISTENT_TALK_ID;
        final String publishRequest = "{\"dummy_value\": true }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(publishRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.body.required_field.published", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO implement proper control at some point. At least, should return a JSON same as other errors
    @Test
    public void should_fail_with_unhandled_error_when_published_field_is_not_boolean(TestContext context) {
        // given
        final String talkId = NON_EXISTENT_TALK_ID;
        final String publishRequest = "{\"published\": \"true\" }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/published")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(publishRequest), handler -> {
                    if (handler.succeeded()) {
                        final HttpResponse<Buffer> result = handler.result();
                        context.assertEquals(500, result.statusCode());
                        context.assertEquals("Internal Server Error", result.bodyAsString());
                    } else {
                        context.fail(handler.cause());
                    }
                    async.complete();
                });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_change_talk_state_when_id_does_not_exists(TestContext context) {
        // given
        final String talkId = NON_EXISTENT_TALK_ID;
        final String stateRequest = "{\"state\": \"confirmed\" }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/state")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(stateRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.talk.unknown_talk", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_change_talk_state_when_id_format_is_not_valid(TestContext context) {
        // given
        final String talkId = "0987654321";
        final String stateRequest = "{\"state\": \"confirmed\" }";

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/state")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(Buffer.buffer(stateRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_change_talk_state_to_unconfirmed(TestContext context) {
        should_change_talk_state("{\"state\": \"unconfirmed\" }", context,
                (response) -> {
                    context.assertEquals(TRUE, response.getBoolean("status"));
                }, (updatedTalk) -> {
                    context.assertEquals("unconfirmed", updatedTalk.getString("state"));
                    context.assertEquals(FALSE, updatedTalk.getBoolean("published"));
                    context.assertEquals(TEST_USER, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertTrue(updatedTalk.getLong(LAST_UPDATE) > updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_change_talk_state_to_confirmed(TestContext context) {
        should_change_talk_state("{\"state\": \"confirmed\" }", context,
                (response) -> {
                    context.assertEquals(TRUE, response.getBoolean("status"));
                }, (updatedTalk) -> {
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(TEST_USER, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertTrue(updatedTalk.getLong(LAST_UPDATE) > updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_change_talk_state_to_scheduled(TestContext context) {
        should_change_talk_state("{\"state\": \"scheduled\" }", context,
                (response) -> {
                    context.assertEquals(TRUE, response.getBoolean("status"));
                }, (updatedTalk) -> {
                    context.assertEquals("scheduled", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(TEST_USER, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertTrue(updatedTalk.getLong(LAST_UPDATE) > updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_change_talk_state_to_canceled(TestContext context) {
        should_change_talk_state("{\"state\": \"canceled\" }", context,
                (response) -> {
                    context.assertEquals(TRUE, response.getBoolean("status"));
                },
                (updatedTalk) -> {
                    context.assertEquals("canceled", updatedTalk.getString("state"));
                    context.assertEquals(FALSE, updatedTalk.getBoolean("published"));
                    context.assertEquals(TEST_USER, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertTrue(updatedTalk.getLong(LAST_UPDATE) > updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_not_authorize_changing_talk_state_when_user_is_sponsor(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(LocalDateTime.now());
        testTalk.setState(TalkState.TALK_STATE_CONFIRMED);
        testTalk.setPublished(true);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/state")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(entries, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_change_talk_state_when_state_is_not_valid(TestContext context) {
        should_change_talk_state("{\"state\": \"random_value\" }", context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_fail_change_talk_state_when_state_is_null(TestContext context) {
        should_change_talk_state("{\"state\": null }", context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                    context.assertEquals("error.body.required_field.state", response.getString("error"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_fail_change_talk_state_when_state_is_a_string(TestContext context) {
        should_change_talk_state("{\"state\": 123 }", context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                    context.assertEquals("error.talk.invalid_state", response.getString("error"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_fail_change_talk_state_when_no_body_is_sent(TestContext context) {
        should_change_talk_state(null, context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                    context.assertEquals("error.body.required", response.getString("error"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_fail_change_talk_state_when_empty_body_is_sent(TestContext context) {

        should_change_talk_state("", context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                    context.assertEquals("error.body.required", response.getString("error"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_fail_change_talk_state_when_empty_json_is_sent(TestContext context) {
        should_change_talk_state("{}", context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                    context.assertEquals("error.body.required", response.getString("error"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    @Test
    public void should_fail_change_talk_state_when_state_is_not_present(TestContext context) {
        should_change_talk_state("{\"another_value\": 123 }", context,
                (response) -> {
                    context.assertEquals(FALSE, response.getBoolean("status"));
                    context.assertEquals("error.body.required_field.state", response.getString("error"));
                },
                (updatedTalk) -> {
                    // original values
                    context.assertEquals("confirmed", updatedTalk.getString("state"));
                    context.assertEquals(TRUE, updatedTalk.getBoolean("published"));
                    context.assertEquals(null, updatedTalk.getString(LAST_ACTION_BY));
                    context.assertNotNull(updatedTalk.getLong(LAST_UPDATE));
                    context.assertNotNull(updatedTalk.getLong(CREATED_DATE));
                });
    }

    /**
     * @param context          Vert.x TestContent in with the test runs
     * @param responseAsserter function to apply assentions on the response of the HTTP body of the response
     * @param updateAsserter   function to apply assertions on the talk after the execution of the test
     */
    public void should_change_talk_state(String stateRequest, TestContext context, Consumer<JsonObject> responseAsserter, Consumer<Document> updateAsserter) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(LocalDateTime.now());
        testTalk.setState(TalkState.TALK_STATE_CONFIRMED);
        testTalk.setPublished(true);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));

        // when-then
        final Async async = context.async();
        webClient.put("/api/talk/" + talkId + "/state")
                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
                .sendBuffer(stateRequest == null ? null : Buffer.buffer(stateRequest), assertJsonResponse(async, context, 200, responseBody -> {
                    if (responseAsserter != null)
                        responseAsserter.accept(responseBody);

                    context.assertEquals(responseBody.getBoolean("status") ? 1 : 2, responseBody.fieldNames().size());

                    final Document updatedTalk = getFromDatabase(Collections.TALK, talkId);
                    if (updateAsserter != null)
                        updateAsserter.accept(updatedTalk);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
