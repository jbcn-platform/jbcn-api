package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.Sender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.TestDataGenerator;
import util.VertxTestSetup;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.persistence.Collections.PAPER;
import static util.VertxSetup.*;

@RunWith(VertxUnitRunner.class)
public class SenderStarredHttpTest extends VertxTestSetup {

    private static final String ENDPOINT = "/api/sender/star";
    private static final String PAPERS_TEST_USER = "paper_test_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }

    @Test
    public void should_fail_with_401_when_no_auth_is_present(TestContext context) {
        // given
        final JsonObject payload = new JsonObject()
            .put("fullName", "this is a name")
            .put("starred", true);

        // then
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT, payload);
    }

    @Test
    public void should_set_a_single_sender_starred_to_true(TestContext context) {
        should_set_a_single_sender_starred(context, FALSE, TRUE);
    }

    @Test
    public void should_set_a_single_sender_starred_to_false(TestContext context) {
        should_set_a_single_sender_starred(context, TRUE, FALSE);
    }

    public void should_set_a_single_sender_starred(TestContext context, Boolean initial, Boolean finalValue) {
        // given
        final Paper paper = TestDataGenerator.getTestPaper();
        final Sender sender = paper.getSenders().get(0);
        sender.setStarred(initial);
        final String senderName = sender.getFullName();
        final String paperId = insertToDatabase(PAPER, paper.toBsonDocument());

        final JsonObject payload = new JsonObject()
            .put("fullName", senderName)
            .put("starred", finalValue);

        final Async async = context.async();
        //To the http api call
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER))
            .sendJsonObject(payload, assertJsonResponse(async, context, 200, responseBody -> {

                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());
                context.assertNull(responseBody.getJsonArray("errors"));

                final Boolean starred = ((Document) ((List) getFromDatabase(PAPER, paperId).get("senders")).get(0))
                    .getBoolean("starred");
                context.assertNotNull(starred);
                context.assertEquals(finalValue, starred);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_set_multiple_sender_starred_to_true(TestContext context) {
        should_set_multiple_sender_starred(context, TRUE, FALSE);
    }

    @Test
    public void should_set_multiple_sender_starred_to_false(TestContext context) {
        should_set_multiple_sender_starred(context, FALSE, TRUE);
    }

    public void should_set_multiple_sender_starred(TestContext context, Boolean initial, Boolean finalValue) {
        // given
        final int papersCount = 3;

        final String senderName = "sender-" + UUID.randomUUID();
        final List<String> paperIds = IntStream.range(0, papersCount)
            .mapToObj(i -> {
                final Paper paper = TestDataGenerator.getTestPaper();
                final Sender sender = paper.getSenders().get(0);
                sender.setStarred(initial);
                sender.setFullName(senderName);
                return insertToDatabase(PAPER, paper.toBsonDocument());
            }).collect(Collectors.toList());

        final JsonObject payload = new JsonObject()
            .put("fullName", senderName)
            .put("starred", finalValue);

        final Async async = context.async();
        //To the http api call
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER))
            .sendJsonObject(payload, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());
                context.assertNull(responseBody.getJsonArray("errors"));

                getFromDatabase(PAPER, paperIds.toArray(new String[papersCount]))
                    .forEach(paper -> {
                        context.assertTrue(paperIds.contains(paper.getObjectId("_id").toString()));
                        final List<Document> senders = (List) paper.get("senders");
                        context.assertEquals(1, senders.size());
                        context.assertEquals(senderName, senders.get(0).getString("fullName"));
                        context.assertEquals(finalValue, senders.get(0).getBoolean("starred"));
                    });

                    /*
                    new PaperRepository(this.getMongoClient())
                            .findAllPapersBySender(senderName, (searchResult) -> {
                                final List<Paper> insertedPapers = searchResult.result();
                                final List<String> insertedPapersIds = insertedPapers.stream()
                                        .map(Paper::get_id)
                                        .collect(Collectors.toList());
                                context.assertEquals(papersCount, insertedPapers.size());
                                for (String id : paperIds) {
                                    context.assertTrue(insertedPapersIds.contains(id));
                                }

                                final List<String> sendersNames = insertedPapers.stream().map(p -> {
                                    context.assertEquals(1, p.getSenders().size());
                                    return p.getSenders().get(0).getFullName();
                                }).collect(Collectors.toList());
                                context.assertEquals(papersCount, sendersNames.size());
                                for (Paper p : insertedPapers) {
                                    context.assertEquals(1, p.getSenders().size());
                                    final Sender actual = p.getSenders().get(0);
                                    context.assertEquals(senderName, actual.getFullName());
                                    context.assertEquals(finalValue, actual.isStarred());
                                }

                            });

                     */
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

}
