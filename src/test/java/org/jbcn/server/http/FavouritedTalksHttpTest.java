package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.model.votes.FavouritedTalk;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.TestDataGenerator;
import util.VertxTestSetup;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.FAVOURITED_TALKS;
import static util.TestDataGenerator.getTestFavouritedTalk;
import static util.VertxSetup.*;


@RunWith(VertxUnitRunner.class)
public class FavouritedTalksHttpTest extends VertxTestSetup {

    private static final String TEST_USER = "favourites_test_user";

    public static final String ENDPOINT = "/api/talks/favourites/";


    @Before
    public void before(TestContext context) {
        dropCollection(FAVOURITED_TALKS);
    }

    @Test
    public void should_fail_create_favourite_when_no_body_is_sent(TestContext context) {
        assert_fail(context, ENDPOINT, TEST_USER, null, "error.body.required");
    }

    @Test
    public void should_fail_create_favourite_when_empty_body_is_sent(TestContext context) {
        assert_fail(context, ENDPOINT, TEST_USER, "", "error.body.required");
    }

    @Test
    public void should_fail_create_favourite_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail(context, ENDPOINT, TEST_USER, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_favourite_when_talkId_is_null(TestContext context) {
        // given
        final String favourite = "{\"talkId\":null}";
        // when
        assertThat(favourite).contains("talkId");
        assert_fail(context, ENDPOINT, TEST_USER, favourite, "error.body.required_field.talkId");
    }

    @Test
    public void should_fail_add_favourite_when_talkId_is_empty(TestContext context) {
        // given
        final String favourite = "{\"talkId\": \"\"}";
        // when
        assertThat(favourite).contains("talkId");
        assert_fail(context, ENDPOINT, TEST_USER, favourite, "error.body.required_field.talkId");
    }

    @Test
    public void should_fail_add_favourite_when_user_is_null(TestContext context) {
        // given
        final String favourite = "{\"talkId\": \"1234\", \"user\": null}";
        // when
        assertThat(favourite).contains("user");
        assert_fail(context, ENDPOINT, TEST_USER, favourite, "error.body.required_field.user");
    }

    @Test
    public void should_fail_add_favourite_when_user_is_empty(TestContext context) {
        // given
        final String favourite = "{\"talkId\": \"1234\", \"user\": \"\"}";
        // when
        assertThat(favourite).contains("user");
        assert_fail(context, ENDPOINT, TEST_USER, favourite, "error.body.required_field.user");
    }

    // all fields are mandatory
    @Test
    public void should_create_favourite(TestContext context) {
        // given
        final FavouritedTalk testFavourite = TestDataGenerator.getTestFavouritedTalk();

        final JsonObject jsonEntity = DocumentConverter.toJson(testFavourite);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document inserted = getFromDatabase(FAVOURITED_TALKS, data.getString("_id"));
                    assertCreatedFavourite(testFavourite, inserted, context);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertCreatedFavourite(FavouritedTalk testEntity, Document inserted, TestContext context) {

        context.assertNotNull(inserted.getLong(CREATED_DATE));
        context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
        context.assertNotNull(inserted.getString(LAST_ACTION_BY));
        context.assertEquals(6, inserted.keySet().size());

        context.assertEquals(testEntity.getTalkId(), inserted.getString("talkId"));
        context.assertEquals(testEntity.getUser(), inserted.getString("user"));
    }

    @Test
    public void should_fail_list_favourites_when_no_auth_token_is_present(TestContext context) {
        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT);
    }

    @Test
    public void should_list_0_favourites(TestContext context) {
        // given: clean collection

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(0, data.getInteger("total"));
                    context.assertEquals(2, data.fieldNames().size());

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(0, items.size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_favourite(TestContext context) {
        // given
        final FavouritedTalk testFavourite = getTestFavouritedTalk();
        final String id = insertToDatabase(FAVOURITED_TALKS, DocumentConverter.getDocument(testFavourite));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(1, items.size());
                    context.assertEquals(1, data.getInteger("total"));
                    context.assertEquals(2, data.fieldNames().size());

                    assertRetrievedFavouritedTalk(testFavourite, items.getJsonObject(0), context, 4);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_favourites(TestContext context) {
        // given
        int testSize = 12;
        final Map<String, FavouritedTalk> testData = getTestFavourites(testSize);

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(12, data.getInteger("total"));

                    items.forEach(it -> {
                        assertRetrievedFavouritedTalk(testData.get(((JsonObject) it).getString("_id")), (JsonObject) it, context, 4);
                    });
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_favourite_by_id(TestContext context) {
        // given
        final FavouritedTalk testFavourite = getTestFavouritedTalk();
        final String id = insertToDatabase(FAVOURITED_TALKS, DocumentConverter.getDocument(testFavourite));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject json = responseBody.getJsonObject("data");
                    // audit fields are null because insertToDatabase does not set them
                    assertRetrievedFavouritedTalk(testFavourite, json, context, 3);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertRetrievedFavouritedTalk(FavouritedTalk favouritedTalk, JsonObject json, TestContext context, int fields) {
        context.assertNotNull(json.getString("_id"));
        context.assertEquals(favouritedTalk.getTalkId(), json.getString("talkId"));
        context.assertEquals(favouritedTalk.getUser(), json.getString("user"));

        // date are not present in hardcoded test data
        context.assertNull(json.getLong(CREATED_DATE));
        context.assertNull(json.getLong(LAST_UPDATE));
        context.assertEquals(fields, json.fieldNames().size());
        if (fields > 6)
            context.assertEquals(1, json.getInteger("talkFavs"));
    }

    @Test
    public void should_fail_get_favourite_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_favourite_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + fakeId)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 404, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.not_found", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_favourites_by_talk_id(TestContext context) {
        // given
        final Map<String, FavouritedTalk> extraData = getTestFavourites(10);

        int testSize = 5;
        final String talkId = "talkId-" + UUID.randomUUID();
        final Map<String, FavouritedTalk> favourites = getTestFavourites(testSize, FavouritedTalk.of(talkId, UUID.randomUUID().toString()));

        final JsonObject searchParams = new JsonObject()
                .put("filters", new JsonArray()
                        .add(new JsonObject()
                                .put("field", "talkId")
                                .put("value", talkId)));
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    items.forEach(it -> {
                        context.assertEquals(talkId, ((JsonObject) it).getString("talkId"));
                    });
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    @Test
    public void should_filter_favourites_by_user_id(TestContext context) {
        // given
        final Map<String, FavouritedTalk> extraData = getTestFavourites(10);

        int testSize = 5;
        final String user = "user-" + UUID.randomUUID();
        final Map<String, FavouritedTalk> favourites = getTestFavourites(testSize, FavouritedTalk.of(UUID.randomUUID().toString(), user));

        final JsonObject searchParams = new JsonObject()
                .put("filters", new JsonArray()
                        .add(new JsonObject()
                                .put("field", "user")
                                .put("value", user)));
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    items.forEach(it -> {
                        context.assertEquals(user, ((JsonObject) it).getString("user"));
                    });
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO pagination tests
    // TODO select fields tests
    // TODO sorting

    private Map<String, FavouritedTalk> getTestFavourites(int size, FavouritedTalk favourite) {
        return IntStream.range(0, size)
                .mapToObj(i -> {
                    final FavouritedTalk testVote = favourite == null ? getTestFavouritedTalk() : favourite;
                    return Pair.of(insertToDatabase(FAVOURITED_TALKS, DocumentConverter.getDocument(testVote)), testVote);
                }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (FavouritedTalk) pair.getSecond()));
    }

    private Map<String, FavouritedTalk> getTestFavourites(int size) {
        return getTestFavourites(size, null);
    }

    @Test
    public void should_delete_favourite_by_id(TestContext context) {
        // given
        final FavouritedTalk testFavourite = getTestFavouritedTalk();
        final String id = insertToDatabase(FAVOURITED_TALKS, DocumentConverter.getDocument(testFavourite));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO validate cases in which none or one of the fields is not present
    @Test
    public void should_delete_favourite_by_talkdId_and_user(TestContext context) {
        // given
        final FavouritedTalk testFavourite = getTestFavouritedTalk();
        final String id = insertToDatabase(FAVOURITED_TALKS, DocumentConverter.getDocument(testFavourite));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + "?talkId=" + testFavourite.getTalkId() + "&user=" + testFavourite.getUser())
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_when_id_format_is_not_valid(TestContext context) {
        // given
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_fail_delete_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c3b64d4aefd3931c0ec7b54";
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + fakeId)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(1, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
