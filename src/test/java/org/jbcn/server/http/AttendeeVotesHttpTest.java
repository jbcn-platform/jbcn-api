package org.jbcn.server.http;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import org.bson.Document;
import org.jbcn.server.model.votes.AttendeeVote;
import org.jbcn.server.model.votes.VoteSource;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.vertx.core.http.HttpHeaders.CONTENT_LENGTH;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.ATTENDEES_VOTES;
import static util.TestDataGenerator.getTestAttendeeVote;
import static util.VertxSetup.*;

// TODO document (the url is a quick though and us not Restful)
// TODO create -> should validate that the user hasn't already voted for the same session (key: (talkId, userEmail))
// TODO create -> validate value is between a configurable range oif values in properties
// TODO update id -> ensure it is not allowed, it is in fact a create with side effects
// Votes received from the GluonMobile API
@RunWith(VertxUnitRunner.class)
public class AttendeeVotesHttpTest extends VertxTestSetup {

    private static final String TEST_USER = "att_votes_test_user";

    public static final String ENDPOINT = "/api/attendees/votes/";


    @Before
    public void before(TestContext context) {
        dropCollection(ATTENDEES_VOTES);
    }

    @Test
    public void should_fail_create_vote_when_no_body_is_sent(TestContext context) {
        assert_fail_add_vote(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_vote_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_vote(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_vote_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_vote(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_vote_when_talkId_is_null(TestContext context) {
        // given
        final String vote = "{\"talkId\":null}";
        // when
        assertThat(vote).contains("talkId");
        assert_fail_add_vote(context, vote, "error.body.required_field.talkId");
    }

    @Test
    public void should_fail_add_vote_when_talkId_is_empty(TestContext context) {
        // given
        final String vote = "{\"talkId\":\"\"}";
        // when
        assertThat(vote).contains("talkId");
        assert_fail_add_vote(context, vote, "error.body.required_field.talkId");
    }

    @Test
    public void should_fail_add_vote_when_email_is_null(TestContext context) {
        // given
        final String vote = "{\"talkId\": \"1234\", \"userEmail\": null}";

        // when
        assertThat(vote).contains("userEmail");
        assert_fail_add_vote(context, vote, "error.body.required_field.userEmail");
    }

    @Test
    public void should_fail_add_vote_when_email_is_empty(TestContext context) {
        // given
        final String vote = "{\"talkId\":\"1234\", \"userEmail\":\"\"}";

        // when
        assertThat(vote).contains("userEmail");
        assert_fail_add_vote(context, vote, "error.body.required_field.userEmail");
    }

    @Test
    public void should_fail_add_vote_when_value_is_null(TestContext context) {
        // given
        final String vote = "{\"talkId\":\"1234\", \"userEmail\":\"some_mail@something.com\", \"value\":null }";

        // when
        assertThat(vote).contains("value");
        assert_fail_add_vote(context, vote, "error.body.required_field.value");
    }

    @Test
    public void should_fail_add_vote_when_value_is_0(TestContext context) {
        // given
        final String vote = DocumentConverter.toJson(new HashMap<String, Object>() {{
            put("talkId", "1234");
            put("userEmail", "some_mail@something.com");
            put("value", 0);
        }}).encode();

        // when
        assert_fail_add_vote(context, vote, "error.attendee.vote.invalid_value");
    }

    @Test
    public void should_fail_add_vote_when_value_is_greater_than_5(TestContext context) {
        // given
        final String vote = DocumentConverter.toJson(new HashMap<String, Object>() {{
            put("talkId", "1234");
            put("userEmail", "some_mail@something.com");
            put("value", 6);
        }}).encode();

        // when
        assert_fail_add_vote(context, vote, "error.attendee.vote.invalid_value");
    }

    @Test
    public void should_fail_add_vote_when_value_is_lower_than_1(TestContext context) {
        // given
        final String vote = DocumentConverter.toJson(new HashMap<String, Object>() {{
            put("talkId", "1234");
            put("userEmail", "some_mail@something.com");
            put("value", -1);
        }}).encode();

        // when
        assert_fail_add_vote(context, vote, "error.attendee.vote.invalid_value");
    }

    @Test
    public void assert_fail_add_vote_when_no_auth_token_is_present(TestContext context) {
        // given
        final JsonObject testVote = new JsonObject()
                .put("name", "some_name");

        // when
        final Async async = context.async();
        webClient.post("/api/sessions/")
                .sendJsonObject(testVote, handler -> {
                    if (handler.succeeded()) {
                        final HttpResponse<Buffer> result = handler.result();
                        context.assertEquals(401, result.statusCode());
                        context.assertEquals("12", result.headers().get(CONTENT_LENGTH.toString()));
                        context.assertEquals("Bearer", result.headers().get("WWW-Authenticate"));
                        context.assertEquals(2, result.headers().size());
                        context.assertEquals("Unauthorized", result.bodyAsString());
                    } else {
                        context.fail(handler.cause());
                    }
                    async.complete();
                });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    public void assert_fail_add_vote(TestContext context, String body, String errorMessage) {
        assert_fail(context, ENDPOINT, TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_vote_with_minimal_fields(TestContext context) {
        // given
        final AttendeeVote testVote = getTestAttendeeVote();
        final JsonObject jsonEntity = DocumentConverter.toJson(testVote);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document inserted = getFromDatabase(ATTENDEES_VOTES, data.getString("_id"));
                    context.assertEquals(testVote.getTalkId(), inserted.getString("talkId"));
                    context.assertEquals(testVote.getUserEmail(), inserted.getString("userEmail"));
                    context.assertEquals(testVote.getValue(), inserted.getInteger("value"));
                    context.assertFalse(inserted.containsKey("delivery"));
                    context.assertFalse(inserted.containsKey("other"));
                    context.assertEquals(testVote.getSource().name(), inserted.getString("source"));
                    context.assertNotNull(inserted.getLong(CREATED_DATE));
                    context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
                    context.assertEquals(TEST_USER, inserted.getString(LAST_ACTION_BY));
                    context.assertEquals(8, inserted.keySet().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_vote_with_all_fields(TestContext context) {
        // given
        final String suffix = UUID.randomUUID().toString();
        final LocalDateTime now = LocalDateTime.now();
        final AttendeeVote testVote = new AttendeeVote(null,
                "talkId-" + suffix,
                "user-" + suffix + "@email.com",
                5, "delivery-" + suffix,
                "other-" + suffix,
                VoteSource.mobile,
                now,
                now,
                "test_user");

        final JsonObject jsonEntity = DocumentConverter.toJson(testVote);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document inserted = getFromDatabase(ATTENDEES_VOTES, data.getString("_id"));
                    assertCreatedVote(testVote, inserted, context);

                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertCreatedVote(AttendeeVote testEntity, Document inserted, TestContext context) {
        context.assertEquals(testEntity.getValue(), inserted.getInteger("value"));
        context.assertEquals(testEntity.getDelivery(), inserted.getString("delivery"));
        context.assertEquals(testEntity.getOther(), inserted.getString("other"));
        context.assertEquals(testEntity.getOther(), inserted.getString("other"));

        context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));

        assertVote(testEntity, inserted, context);
    }

    @Test
    public void should_add_vote_with_PUT_when_it_does_not_exist(TestContext context) {
        // given
        final String suffix = UUID.randomUUID().toString();
        final LocalDateTime now = LocalDateTime.now();
        final AttendeeVote testVote = new AttendeeVote(null,
                "talkId-" + suffix,
                "user-" + suffix + "@email.com",
                5, "delivery-" + suffix,
                "other-" + suffix,
                VoteSource.mobile,
                now,
                now,
                "test_user");

        final JsonObject jsonEntity = DocumentConverter.toJson(testVote);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document inserted = getFromDatabase(ATTENDEES_VOTES, data.getString("_id"));
                    context.assertEquals(5, inserted.get("value"));
                    assertCreatedVote(testVote, inserted, context);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // checks for talkId + userEmail
    @Test
    public void should_update_vote_with_PUT_when_it_exist(TestContext context) {
        // given
        final String suffix = UUID.randomUUID().toString();
        final LocalDateTime now = LocalDateTime.now();
        final AttendeeVote testVote = new AttendeeVote(null,
                "talkId-" + suffix,
                "user-" + suffix + "@email.com",
                5, "delivery-" + suffix,
                "other-" + suffix,
                VoteSource.tablet,
                now,
                now,
                "test_user");
        final String voteId = insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));

        final Integer newValue = 1;
        final String newDelivery = "new-delivery-" + UUID.randomUUID();
        final String newOther = "new-other-" + UUID.randomUUID();
        final JsonObject jsonEntity = DocumentConverter.toJson(testVote)
                .put("value", newValue)
                .put("delivery", newDelivery)
                .put("other", newOther);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document inserted = getFromDatabase(ATTENDEES_VOTES, data.getString("_id"));
                    context.assertEquals(1, inserted.get("value"));
                    context.assertEquals(newDelivery, inserted.get("delivery"));
                    context.assertEquals(newOther, inserted.get("other"));
                    assertUpdatedVote(testVote, inserted, context);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_add_source_to_vote_with_PUT_when_it_exist(TestContext context) {
        // given
        final String suffix = UUID.randomUUID().toString();
        final LocalDateTime now = LocalDateTime.now();
        final AttendeeVote testVote = new AttendeeVote(null,
                "talkId-" + suffix,
                "user-" + suffix + "@email.com",
                5, "delivery-" + suffix,
                "other-" + suffix,
                null,
                now,
                now,
                "test_user");
        final String voteId = insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));

        final JsonObject jsonEntity = DocumentConverter.toJson(testVote)
                .put("source", VoteSource.tablet);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document inserted = getFromDatabase(ATTENDEES_VOTES, data.getString("_id"));
                    context.assertEquals(testVote.getTalkId(), inserted.getString("talkId"));
                    context.assertEquals(testVote.getUserEmail(), inserted.getString("userEmail"));
                    context.assertEquals(testVote.getValue(), inserted.get("value"));
                    context.assertEquals(testVote.getDelivery(), inserted.getString("delivery"));
                    context.assertEquals(testVote.getOther(), inserted.getString("other"));
                    context.assertEquals("tablet", inserted.getString("source"));

                    context.assertNotNull(inserted.getLong(CREATED_DATE));
                    context.assertEquals(TEST_USER, inserted.getString(LAST_ACTION_BY));
                    context.assertEquals(10, inserted.keySet().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertUpdatedVote(AttendeeVote testEntity, Document inserted, TestContext context) {
        context.assertTrue(inserted.getLong(CREATED_DATE) < inserted.getLong(LAST_UPDATE));

        assertVote(testEntity, inserted, context);
    }

    private void assertVote(AttendeeVote testEntity, Document inserted, TestContext context) {
        context.assertNotNull(inserted.getObjectId("_id"));
        context.assertEquals(testEntity.getTalkId(), inserted.getString("talkId"));
        context.assertEquals(testEntity.getUserEmail(), inserted.getString("userEmail"));
        context.assertEquals(testEntity.getSource().name(), inserted.getString("source"));
        context.assertNotNull(inserted.getLong(CREATED_DATE));
        context.assertEquals(TEST_USER, inserted.getString(LAST_ACTION_BY));
        context.assertEquals(10, inserted.keySet().size());
    }

    @Test
    public void should_fail_list_votes_when_no_auth_token_is_present(TestContext context) {
        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .send(handler -> {
                    if (handler.succeeded()) {
                        final HttpResponse<Buffer> result = handler.result();
                        context.assertEquals(401, result.statusCode());
                        context.assertEquals("12", result.headers().get("Content-Length"));
                        context.assertEquals("Bearer", result.headers().get("WWW-Authenticate"));
                        context.assertEquals(2, result.headers().size());
                        context.assertEquals("Unauthorized", result.bodyAsString());
                    } else {
                        context.fail(handler.cause());
                    }
                    async.complete();
                });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_0_votes(TestContext context) {
        // given: clean collection

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonArray items = responseBody.getJsonObject("data").getJsonArray("items");
                    context.assertEquals(0, items.size());
                    context.assertEquals(0, responseBody.getJsonObject("data").getInteger("total"));
                    context.assertEquals(2, responseBody.getJsonObject("data").fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_vote(TestContext context) {
        // given
        final AttendeeVote testAttendeeVote = getTestAttendeeVote();
        final String id = insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testAttendeeVote));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonArray items = responseBody.getJsonObject("data").getJsonArray("items");
                    context.assertEquals(1, items.size());
                    context.assertEquals(1, responseBody.getJsonObject("data").getInteger("total"));
                    context.assertEquals(2, responseBody.getJsonObject("data").fieldNames().size());

                    assertRetrievedAttendeeVote(testAttendeeVote, items.getJsonObject(0), 7, context);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_votes(TestContext context) {
        // given
        int testSize = 12;
        final Map<String, AttendeeVote> testData = IntStream.range(0, testSize)
                .mapToObj(i -> {
                    final AttendeeVote testVote = getTestAttendeeVote();
                    return Pair.of(insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote)), testVote);
                }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (AttendeeVote) pair.getSecond()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonArray items = responseBody.getJsonObject("data").getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(12, responseBody.getJsonObject("data").getInteger("total"));

                    items.forEach(it -> {
                        assertRetrievedAttendeeVote(testData.get(((JsonObject) it).getString("_id")), (JsonObject) it, 7, context);
                    });

                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_vote_by_id(TestContext context) {
        // given
        final AttendeeVote testVote = getTestAttendeeVote();
        final String id = insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject json = responseBody.getJsonObject("data");
                    assertRetrievedAttendeeVote(testVote, json, 7, context);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertRetrievedAttendeeVote(AttendeeVote vote, JsonObject json, int returnedKeys, TestContext context) {
        context.assertNotNull(json.getString("_id"));
        context.assertEquals(vote.getTalkId(), json.getString("talkId"));
        context.assertEquals(vote.getUserEmail(), json.getString("userEmail"));
        context.assertEquals(vote.getValue(), json.getInteger("value"));
        context.assertEquals(vote.getDelivery(), json.getString("delivery"));
        context.assertEquals(vote.getOther(), json.getString("other"));
        context.assertEquals(vote.getSource().name(), json.getString("source"));
        context.assertNotNull(json.getLong(CREATED_DATE));
        context.assertEquals(json.getLong(CREATED_DATE), json.getLong(LAST_UPDATE));
        context.assertFalse(json.containsKey(LAST_ACTION_BY));
        context.assertEquals(returnedKeys, json.fieldNames().size());
    }

    @Test
    public void should_fail_get_vote_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_vote_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + fakeId)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 404, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.not_found", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId(TestContext context) {
        // given
        final Map<String, AttendeeVote> extraVotes = getTestAttendeeVotes(10);

        int testSize = 5;
        final String talkId = "talkId-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of(talkId, "user-" + UUID.randomUUID() + "@email.com", 3, VoteSource.tablet, LocalDateTime.now()));

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        final String msgBody = JsonObject.mapFrom(searchParams).encode();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(talkId, items.getJsonObject(0).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(1).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(2).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(3).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(4).getString("talkId"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO validate if filter field exists
    @Test
    public void should_filter_votes_by_userEmail(TestContext context) {
        // given
        final Map<String, AttendeeVote> extraVotes = getTestAttendeeVotes(10);

        int testSize = 3;
        final String userEmail = "email-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of("talk-" + UUID.randomUUID(), userEmail, 3, VoteSource.tablet, LocalDateTime.now()));

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "userEmail");
                    put("value", userEmail);
                }}));
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        final String msgBody = JsonObject.mapFrom(searchParams).encode();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(userEmail, items.getJsonObject(0).getString("userEmail"));
                    context.assertEquals(userEmail, items.getJsonObject(1).getString("userEmail"));
                    context.assertEquals(userEmail, items.getJsonObject(2).getString("userEmail"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId_and_userEmail(TestContext context) {
        // given
        final Map<String, AttendeeVote> extraVotes = getTestAttendeeVotes(10);

        int testSize = 3;
        final String userEmail = "email-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of("talk-" + UUID.randomUUID(), userEmail, 3, VoteSource.mobile, LocalDateTime.now()));
        final String talkId = votes.values().iterator().next().getTalkId();

        final JsonObject searchParams = new JsonObject()
                .put("filters", new JsonArray()
                        .add(new JsonObject()
                                .put("field", "talkId")
                                .put("value", talkId))
                        .add(new JsonObject()
                                .put("field", "userEmail")
                                .put("value", userEmail))
                );
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        final String msgBody = searchParams.encode();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(userEmail, items.getJsonObject(0).getString("userEmail"));
                    context.assertEquals(userEmail, items.getJsonObject(1).getString("userEmail"));
                    context.assertEquals(userEmail, items.getJsonObject(2).getString("userEmail"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_and_return_0_items_when_page_is_empty(TestContext context) {
        // given
        final Map<String, AttendeeVote> extraVotes = getTestAttendeeVotes(10);

        int testSize = 5;
        final String talkId = "talkId-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of(talkId, "user-" + UUID.randomUUID() + "@email.com", 3, VoteSource.tablet, LocalDateTime.now()));

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        searchParams.put("page", new HashMap<String, Object>() {{
            put("index", 10);
            put("size", 50);
        }});
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(0, items.size());
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId_with_pagination(TestContext context) {
        // given
        final Map<String, AttendeeVote> extraVotes = getTestAttendeeVotes(10);

        int testSize = 5;
        final String talkId = "talkId-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of(talkId, "user-" + UUID.randomUUID() + "@email.com", 3, VoteSource.tablet, LocalDateTime.now()));

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        searchParams.put("page", new HashMap<String, Object>() {{
            put("index", 0);
            put("size", 50);
        }});
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(talkId, items.getJsonObject(0).getString("talkId"));
                    context.assertEquals(7, items.getJsonObject(0).fieldNames().size());
                    context.assertEquals(talkId, items.getJsonObject(1).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(2).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(3).getString("talkId"));
                    context.assertEquals(talkId, items.getJsonObject(4).getString("talkId"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId_and_return_selected_fields(TestContext context) {
        // given
        getTestAttendeeVotes(10);

        int testSize = 3;
        final String talkId = "talkId-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of(talkId, "user-" + UUID.randomUUID() + "@email.com", 3, VoteSource.mobile, LocalDateTime.now()));

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        searchParams.put("fields", Arrays.asList("talkId"));
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        final String msgBody = JsonObject.mapFrom(searchParams).encode();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());

                    Set<String> fieldNames = items.getJsonObject(0).fieldNames();
                    context.assertTrue(fieldNames.contains("_id"));
                    context.assertTrue(fieldNames.contains("talkId"));
                    context.assertEquals(2, fieldNames.size());
                    context.assertEquals(talkId, items.getJsonObject(0).getString("talkId"));

                    fieldNames = items.getJsonObject(1).fieldNames();
                    context.assertTrue(fieldNames.contains("_id"));
                    context.assertTrue(fieldNames.contains("talkId"));
                    context.assertEquals(2, fieldNames.size());
                    context.assertEquals(talkId, items.getJsonObject(1).getString("talkId"));

                    fieldNames = items.getJsonObject(2).fieldNames();
                    context.assertTrue(fieldNames.contains("_id"));
                    context.assertTrue(fieldNames.contains("talkId"));
                    context.assertEquals(2, fieldNames.size());
                    context.assertEquals(talkId, items.getJsonObject(2).getString("talkId"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId_and_return_page_with_less_items_than_total(TestContext context) {
        // given
        final Map<String, AttendeeVote> extraVotes = getTestAttendeeVotes(10);

        int testSize = 5;
        final String talkId = "talkId-" + UUID.randomUUID();
        final Map<String, AttendeeVote> votes = getTestAttendeeVotes(testSize,
                AttendeeVote.of(talkId, "user-" + UUID.randomUUID() + "@email.com", 3, VoteSource.tablet, LocalDateTime.now()));

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        searchParams.put("page", new HashMap<String, Object>() {{
            put("index", 2);
            put("size", 1);
        }});
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        final String msgBody = JsonObject.mapFrom(searchParams).encode();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(1, items.size());
                    context.assertEquals(talkId, items.getJsonObject(0).getString("talkId"));
                    context.assertEquals("tablet", items.getJsonObject(0).getString("source"));
                    context.assertEquals(7, items.getJsonObject(0).fieldNames().size());
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId_and_return_sorted_by_email_asc(TestContext context) {
        // given
        getTestAttendeeVotes(10);

        int testSize = 6;
        final String talkId = "talkId-" + UUID.randomUUID();
        for (int i = 0; i < testSize; i++) {
            final AttendeeVote testVote = AttendeeVote.of(talkId, "user-" + i + "@email.com", 3, VoteSource.mobile, LocalDateTime.now());
            insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));
        }

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        searchParams.put("sort", "userEmail");
        searchParams.put("asc", false);
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        final String msgBody = JsonObject.mapFrom(searchParams).encode();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals("user-5@email.com", items.getJsonObject(0).getString("userEmail"));
                    context.assertEquals(7, items.getJsonObject(0).fieldNames().size());
                    context.assertEquals("user-4@email.com", items.getJsonObject(1).getString("userEmail"));
                    context.assertEquals("user-3@email.com", items.getJsonObject(2).getString("userEmail"));
                    context.assertEquals("user-2@email.com", items.getJsonObject(3).getString("userEmail"));
                    context.assertEquals("user-1@email.com", items.getJsonObject(4).getString("userEmail"));
                    context.assertEquals("user-0@email.com", items.getJsonObject(5).getString("userEmail"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_filter_votes_by_talkId_and_return_sorted_by_email_desc(TestContext context) {
        // given
        getTestAttendeeVotes(10);

        int testSize = 6;
        final String talkId = "talkId-" + UUID.randomUUID();
        for (int i = 0; i < testSize; i++) {
            final AttendeeVote testVote = AttendeeVote.of(talkId, "user-" + i + "@email.com", 3, VoteSource.tablet, LocalDateTime.now());
            insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));
        }

        final Map searchParams = new HashMap<>();
        searchParams.put("filters", Arrays.asList(
                new HashMap<String, Object>() {{
                    put("field", "talkId");
                    put("value", talkId);
                }}));
        searchParams.put("sort", "userEmail");
        searchParams.put("asc", true);
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT + "search")
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(testSize, data.getInteger("total"));

                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(7, items.getJsonObject(0).fieldNames().size());
                    context.assertEquals("user-0@email.com", items.getJsonObject(0).getString("userEmail"));
                    context.assertEquals("user-1@email.com", items.getJsonObject(1).getString("userEmail"));
                    context.assertEquals("user-2@email.com", items.getJsonObject(2).getString("userEmail"));
                    context.assertEquals("user-3@email.com", items.getJsonObject(3).getString("userEmail"));
                    context.assertEquals("user-4@email.com", items.getJsonObject(4).getString("userEmail"));
                    context.assertEquals("user-5@email.com", items.getJsonObject(5).getString("userEmail"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    /**
     * Returns a map of atendeeeVoteIt -> AttendeeVote
     */
    private Map<String, AttendeeVote> getTestAttendeeVotes(int size, AttendeeVote vote) {
        return IntStream.range(0, size)
                .mapToObj(i -> {
                    final AttendeeVote testVote = vote == null ? getTestAttendeeVote() : vote;
                    return Pair.of(insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote)), testVote);
                }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (AttendeeVote) pair.getSecond()));
    }


    private Map<String, AttendeeVote> getTestAttendeeVotes(int size) {
        return getTestAttendeeVotes(size, null);
    }

    @Test
    public void should_delete_vote(TestContext context) {
        // given
        final AttendeeVote testVote = getTestAttendeeVote();
        final String id = insertToDatabase(ATTENDEES_VOTES, DocumentConverter.getDocument(testVote));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_when_id_format_is_not_valid(TestContext context) {
        // given
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_fail_delete_when_id_does_not_exist(TestContext context) {
        // given
        final String fakeId = "5c3b64d4aefd3931c0ec7b54";
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + fakeId)
                .bearerTokenAuthentication(getNewSession(TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(1, responseBody.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
