package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.scheduling.Room;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;
import util.asserters.JsonApiEntity;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.ROOMS;
import static util.TestDataGenerator.getTestRoom;
import static util.VertxSetup.*;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.PersistedApiEntityAsserter.assertThat;

// TODO document
@RunWith(VertxUnitRunner.class)
public class RoomsHttpTest extends VertxTestSetup {

    private static final String ENDPOINT = "/api/rooms/";
    private static final String TEST_USER = "rooms_test_user";

    @Before
    public void before(TestContext context) {
        dropCollection(ROOMS);
    }


    @Test
    public void should_fail_create_room_when_no_body_is_sent(TestContext context) {
        assert_fail_add_room(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_room_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_room(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_room_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_room(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_room_when_name_is_null(TestContext context) {
        // given
        final String testRoom = "{\"name\":null}";

        // when
        assert_fail_add_room(context, testRoom, "error.body.required_field.name");
    }

    @Test
    public void assert_fail_add_room_when_no_auth_token_is_present(TestContext context) {
        // given
        final JsonObject testRoom = new JsonObject()
            .put("name", "some_name");
        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT, testRoom);
    }

    public void assert_fail_add_room(TestContext context, String body, String errorMessage) {
        assert_fail(context, ENDPOINT, TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_room_with_minimal_fields(TestContext context) {
        final String name = "Room-name-" + UUID.randomUUID();
        final JsonObject testRoom = new JsonObject()
            .put("name", name);

        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(testRoom, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .getId();

                assertThat(getFromDatabase(ROOMS, id))
                    .with(context)
                    .hasStringEqualsTo("name", testRoom.getString("name"))
                    .hasCreatedMetadata()
                    .hasKeys(5);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_room_with_all_fields(TestContext context) {
        final var testRoom = getTestRoom();
        final JsonObject jsonEntity = DocumentConverter.toJson(testRoom);

        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .getId();

                assertThat(getFromDatabase(ROOMS, id))
                    .with(context)
                    .hasStringEqualsTo("name", testRoom.getString("name"))
                    .hasIntegerEqualsTo("capacity", testRoom.getInteger("capacity"))
                    .hasCreatedMetadata()
                    .hasKeys(6);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_room_setting_name_as_integer(TestContext context) {
        // given
        final Integer name = 1234567890;
        final JsonObject testRoom = new JsonObject()
            .put("name", name);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(testRoom, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .getId();

                final Document insertedUser = getFromDatabase(ROOMS, id);
                context.assertEquals(name.toString(), insertedUser.getString("name"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_rooms_when_no_auth_token_is_present(TestContext context) {
        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
    }

    @Test
    public void should_list_0_rooms(TestContext context) {
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .isEmpty();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_room(TestContext context) {
        final var testRoom = getTestRoom();
        final String id = insertToDatabase(ROOMS, testRoom);

        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .hasSize(1)
                    .hasItem(0, entity -> {
                        context.assertEquals(id, entity.getString("_id"));
                        context.assertEquals(testRoom.getString("name"), entity.getString("name"));
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_rooms(TestContext context) {
        int testSize = 12;
        final Map<String, Document> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final var testRoom = getTestRoom();
                return Pair.of(insertToDatabase(ROOMS, testRoom), testRoom);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (Document) pair.getSecond()));

        final Async async = context.async();
        webClient.get("/api/rooms")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .hasSize(12)
                    .allItems(item -> {
                        final Room expected = DocumentConverter.parseToObject(testData.get(item.getId()), Room.class);
                        assertEquals(context, item, expected);
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertEquals(TestContext context, JsonApiEntity actual, Room expected) {
        context.assertEquals(expected.getName(), actual.getString("name"));
        context.assertEquals(expected.getCapacity(), actual.getDouble("capacity").intValue());
        context.assertNotNull(expected.getCreatedDate());
        context.assertEquals(expected.getCreatedDate(), expected.getLastUpdate());
        context.assertEquals(expected.getLastActionBy(), actual.getString(LAST_ACTION_BY));
        context.assertEquals(6, actual.fieldNames().size());
    }

    @Test
    public void should_get_room_by_id(TestContext context) {
        final var testRoomDoc = getTestRoom();
        final String id = insertToDatabase(ROOMS, testRoomDoc);
        final Room testRoom = DocumentConverter.parseToObject(testRoomDoc, Room.class);

        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isEntityResponse()
                    .hasId()
                    .hasStringEqualsTo("name", testRoom.getName())
                    .hasIntegerEqualsTo("capacity", testRoom.getCapacity())
                    .hasCreatedMetadata();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_getting_room_when_user_is_voter(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_getting_room_when_user_is_sponsor(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPONSOR, context);
    }

    @Test
    public void should_not_authorize_getting_room_when_user_is_speaker(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPEAKER, context);
    }

    private void assert_forbidden_when_getting_by_id(Role role, TestContext context) {
        // given
        final var testRoom = getTestRoom();
        final String id = insertToDatabase(ROOMS, testRoom);

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, role))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_room_by_id_when_id_format_is_not_valid(TestContext context) {
        final String id = "1234";

        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_room_by_id_when_id_does_not_exists(TestContext context) {
        final String id = "5c24e4b0554b4947c8163049";

        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_room_name(TestContext context) {
        final var testRoom = getTestRoom();
        final String id = insertToDatabase(ROOMS, testRoom);

        final String newName = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("name", newName);

        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(ROOMS, id))
                    .with(context)
                    .hasStringEqualsTo("name", newName)
                    .hasIntegerEqualsTo("capacity", testRoom.getInteger("capacity"))
                    .hasUpdatedMetadata(TEST_USER)
                    .hasKeys(6);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_room_capacity(TestContext context) {
        // given
        final var testRoom = getTestRoom();
        final String id = insertToDatabase(ROOMS, testRoom);

        final JsonObject updateBody = new JsonObject()
            .put("capacity", 999);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                final Document updatedUser = getFromDatabase(ROOMS, id);
                context.assertEquals(testRoom.getString("name"), updatedUser.getString("name"));
                context.assertEquals(999, updatedUser.getInteger("capacity"));
                context.assertTrue(updatedUser.getLong(LAST_UPDATE) > updatedUser.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_room_when_id_does_not_exists(TestContext context) {
        final String fakeId = "5c4a3ea8aefd394cce055574";
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        final Async async = context.async();
        webClient.put(ENDPOINT + fakeId)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_room_when_id_format_is_not_valid(TestContext context) {
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        final Async async = context.async();
        webClient.put("/api/rooms/123")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_room(TestContext context) {
        final var testRoom = getTestRoom();
        final String id = insertToDatabase(ROOMS, testRoom);

        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_track_when_id_format_is_not_valid(TestContext context) {
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }
}
