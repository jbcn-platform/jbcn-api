package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.Sender;
import org.jbcn.server.model.Talk;
import org.jbcn.server.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.TestDataGenerator;
import util.VertxTestSetup;

import java.util.List;
import java.util.Map;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.PAPER;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestPaper;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.*;

@RunWith(VertxUnitRunner.class)
public class PublicPaperHttpTest extends VertxTestSetup {

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }


    @Test
    public void should_create_a_paper(TestContext context) {
        // given
        final long timestamp = System.currentTimeMillis();
        final Paper testPaper = TestDataGenerator.getTestPaper();
        testPaper.setPaperAbstract("abstract :D");
        testPaper.setSponsor(null);
        testPaper.setPreferenceDay("2020/10/10");
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setBiography("speaker bio");
        testSender.setCompany("FancyCorp s.a");
        final JsonObject jsonObject = JsonObject.mapFrom(testPaper);
        final String paperJson = JsonUtils.removeNullValues(jsonObject).encode();

        // when
        final Async async = context.async();

        webClient.post("/public/api/paper/")
            .sendJsonObject(jsonObject, assertJsonResponse(async, context, 200, responseBody -> {
                final JsonObject entries = responseBody;
                context.assertEquals(TRUE, entries.getBoolean("status"));
                context.assertEquals(1, entries.fieldNames().size());

                final List<Document> papers = getFromDatabase(PAPER);
                context.assertEquals(1, papers.size());

                final Document createdPaper = papers.get(0);
                context.assertEquals(16, createdPaper.keySet().size());
                context.assertNotNull(createdPaper.getObjectId("_id"));
                context.assertNotNull(createdPaper.getLong(CREATED_DATE));
                context.assertNotNull(createdPaper.getLong(LAST_UPDATE));

                context.assertEquals(createdPaper.getLong(CREATED_DATE), createdPaper.getLong(LAST_UPDATE));
                context.assertTrue(timestamp < createdPaper.getLong(LAST_UPDATE));

                context.assertEquals(testPaper.getTitle(), createdPaper.getString("title"));
                context.assertEquals(testPaper.getEdition(), createdPaper.getString("edition"));
                context.assertEquals(testPaper.getLevel(), createdPaper.getString("level"));
                context.assertEquals(testPaper.getType(), createdPaper.getString("type"));
                context.assertEquals(testPaper.getPreferenceDay(), createdPaper.getString("preferenceDay"));
                context.assertEquals(0.0d, createdPaper.getDouble("averageVote"));
                context.assertEquals(0, createdPaper.getInteger("votesCount"));
                context.assertEquals(testPaper.getComments(), createdPaper.getString("comments"));
                context.assertEquals(1, ((List) createdPaper.get("tags")).size());
                context.assertEquals("tag", ((List) createdPaper.get("tags")).get(0));
                context.assertEquals("sent", createdPaper.getString("state"));
                context.assertEquals(testPaper.getPaperAbstract(), createdPaper.getString("abstract"));
                context.assertEquals(FALSE, createdPaper.getBoolean("sponsor"));

                final List<Document> senders = (List<Document>) createdPaper.get("senders");
                context.assertEquals(1, senders.size());
                final Document sender = senders.get(0);
                context.assertEquals(17, sender.keySet().size());
                context.assertEquals(testSender.getFullName(), sender.getString("fullName"));
                context.assertEquals("I rock!!", sender.getString("jobTitle"));
                context.assertNotNull(sender.getString("code"));
                context.assertEquals(testSender.getEmail(), sender.getString("email"));
                context.assertEquals("speaker bio", sender.getString("biography"));
                context.assertEquals("FancyCorp s.a", sender.getString("company"));
                context.assertEquals("https://trololo.com", sender.getString("picture"));
                context.assertEquals("https://whatever.com", sender.getString("web"));
                context.assertEquals("twitter", sender.getString("twitter"));
                context.assertEquals("https://www.linkedin.com/in/something-here", sender.getString("linkedin"));
                context.assertEquals(FALSE, sender.getBoolean("travelCost"));
                context.assertEquals(FALSE, sender.getBoolean("attendeesParty"));
                context.assertEquals(FALSE, sender.getBoolean("speakersParty"));
                context.assertEquals("XXL", sender.getString("tshirtSize"));
                context.assertEquals("cheap wine, bad beer", sender.getString("allergies"));
                context.assertEquals(FALSE, sender.getBoolean("starred"));
                final Document dataConsent = (Document) sender.get("dataConsent");
                context.assertEquals(3, dataConsent.keySet().size());
                context.assertEquals(TRUE, dataConsent.getBoolean("identification"));
                context.assertEquals(TRUE, dataConsent.getBoolean("contact"));
                context.assertEquals(TRUE, dataConsent.getBoolean("financial"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_a_paper_marked_as_sponsor(TestContext context) {
        // given
        final long timestamp = System.currentTimeMillis();
        final Paper testPaper = TestDataGenerator.getTestPaper();
        testPaper.setPaperAbstract("abstract :D");
        testPaper.setSponsor(TRUE);
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setBiography("speaker bio");
        final JsonObject paperJson = JsonUtils.removeNullValues(JsonObject.mapFrom(testPaper));

        // when
        final Async async = context.async();
        webClient.post("/public/api/paper/")
            .sendJsonObject(paperJson, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final List<Document> papers = getFromDatabase(PAPER);
                context.assertEquals(1, papers.size());

                final Document createdPaper = papers.get(0);
                context.assertEquals(15, createdPaper.keySet().size());
                context.assertNotNull(createdPaper.getObjectId("_id"));
                context.assertNotNull(createdPaper.getLong(CREATED_DATE));
                context.assertNotNull(createdPaper.getLong(LAST_UPDATE));

                context.assertEquals(createdPaper.getLong(CREATED_DATE), createdPaper.getLong(LAST_UPDATE));
                context.assertTrue(timestamp < createdPaper.getLong(LAST_UPDATE));

                context.assertEquals(testPaper.getTitle(), createdPaper.getString("title"));
                context.assertEquals(testPaper.getEdition(), createdPaper.getString("edition"));
                context.assertEquals(testPaper.getLevel(), createdPaper.getString("level"));
                context.assertEquals(testPaper.getType(), createdPaper.getString("type"));
                context.assertEquals(0.0d, createdPaper.getDouble("averageVote"));
                context.assertEquals(0, createdPaper.getInteger("votesCount"));
                context.assertEquals(testPaper.getComments(), createdPaper.getString("comments"));
                context.assertEquals(1, ((List) createdPaper.get("tags")).size());
                context.assertEquals("tag", ((List) createdPaper.get("tags")).get(0));
                context.assertEquals("sent", createdPaper.getString("state"));
                context.assertEquals(testPaper.getPaperAbstract(), createdPaper.getString("abstract"));
                context.assertEquals(TRUE, createdPaper.getBoolean("sponsor"));

                final List<Document> senders = (List<Document>) createdPaper.get("senders");
                context.assertEquals(1, senders.size());
                final Document sender = senders.get(0);
                context.assertEquals(16, sender.keySet().size());
                context.assertEquals(testSender.getFullName(), sender.getString("fullName"));
                context.assertEquals("I rock!!", sender.getString("jobTitle"));
                context.assertNotNull(sender.getString("code"));
                context.assertEquals(testSender.getEmail(), sender.getString("email"));
                context.assertEquals("speaker bio", sender.getString("biography"));
                context.assertEquals("https://trololo.com", sender.getString("picture"));
                context.assertEquals("https://whatever.com", sender.getString("web"));
                context.assertEquals("twitter", sender.getString("twitter"));
                context.assertEquals("https://www.linkedin.com/in/something-here", sender.getString("linkedin"));
                context.assertEquals(FALSE, sender.getBoolean("travelCost"));
                context.assertEquals(FALSE, sender.getBoolean("attendeesParty"));
                context.assertEquals(FALSE, sender.getBoolean("speakersParty"));
                context.assertEquals("XXL", sender.getString("tshirtSize"));
                context.assertEquals("cheap wine, bad beer", sender.getString("allergies"));
                context.assertEquals(FALSE, sender.getBoolean("starred"));
                final Document dataConsent = (Document) sender.get("dataConsent");
                context.assertEquals(3, dataConsent.keySet().size());
                context.assertEquals(TRUE, dataConsent.getBoolean("identification"));
                context.assertEquals(TRUE, dataConsent.getBoolean("contact"));
                context.assertEquals(TRUE, dataConsent.getBoolean("financial"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_a_paper_with_language(TestContext context) {
        // given
        final long timestamp = System.currentTimeMillis();
        final Paper testPaper = TestDataGenerator.getTestPaper();
        testPaper.setPaperAbstract("abstract :D");
        testPaper.setSponsor(null);
        testPaper.setPreferenceDay("2020/10/10");
        testPaper.setLanguages(List.of("ES", "ENG"));
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setBiography("speaker bio");
        testSender.setCompany("FancyCorp s.a");
        final JsonObject jsonObject = JsonObject.mapFrom(testPaper);
        final String paperJson = JsonUtils.removeNullValues(jsonObject).encode();

        // when
        final Async async = context.async();

        webClient.post("/public/api/paper/")
            .sendJsonObject(jsonObject, assertJsonResponse(async, context, 200, responseBody -> {
                final JsonObject entries = responseBody;
                context.assertEquals(TRUE, entries.getBoolean("status"));
                context.assertEquals(1, entries.fieldNames().size());

                final List<Document> papers = getFromDatabase(PAPER);
                context.assertEquals(1, papers.size());

                final Document createdPaper = papers.get(0);
                context.assertEquals(17, createdPaper.keySet().size());
                context.assertNotNull(createdPaper.getObjectId("_id"));
                context.assertNotNull(createdPaper.getLong(CREATED_DATE));
                context.assertNotNull(createdPaper.getLong(LAST_UPDATE));

                context.assertEquals(createdPaper.getLong(CREATED_DATE), createdPaper.getLong(LAST_UPDATE));
                context.assertTrue(timestamp < createdPaper.getLong(LAST_UPDATE));

                context.assertEquals(testPaper.getTitle(), createdPaper.getString("title"));
                context.assertEquals(testPaper.getEdition(), createdPaper.getString("edition"));
                context.assertEquals(testPaper.getLevel(), createdPaper.getString("level"));
                context.assertEquals(testPaper.getType(), createdPaper.getString("type"));
                context.assertEquals(testPaper.getPreferenceDay(), createdPaper.getString("preferenceDay"));
                context.assertEquals(0.0d, createdPaper.getDouble("averageVote"));
                context.assertEquals(0, createdPaper.getInteger("votesCount"));
                context.assertEquals(testPaper.getComments(), createdPaper.getString("comments"));
                context.assertEquals(1, ((List) createdPaper.get("tags")).size());
                context.assertEquals("tag", ((List) createdPaper.get("tags")).get(0));
                context.assertEquals("sent", createdPaper.getString("state"));
                context.assertEquals(testPaper.getPaperAbstract(), createdPaper.getString("abstract"));
                context.assertEquals(FALSE, createdPaper.getBoolean("sponsor"));
                context.assertEquals(2, ((List) createdPaper.get("languages")).size());
                context.assertTrue(((List<String>) createdPaper.get("languages")).containsAll(List.of("ES", "ENG")), "languages contains ES and ENG");

                final List<Document> senders = (List<Document>) createdPaper.get("senders");
                context.assertEquals(1, senders.size());
                final Document sender = senders.get(0);
                context.assertEquals(17, sender.keySet().size());
                context.assertEquals(testSender.getFullName(), sender.getString("fullName"));
                context.assertEquals("I rock!!", sender.getString("jobTitle"));
                context.assertNotNull(sender.getString("code"));
                context.assertEquals(testSender.getEmail(), sender.getString("email"));
                context.assertEquals("speaker bio", sender.getString("biography"));
                context.assertEquals("FancyCorp s.a", sender.getString("company"));
                context.assertEquals("https://trololo.com", sender.getString("picture"));
                context.assertEquals("https://whatever.com", sender.getString("web"));
                context.assertEquals("twitter", sender.getString("twitter"));
                context.assertEquals("https://www.linkedin.com/in/something-here", sender.getString("linkedin"));
                context.assertEquals(FALSE, sender.getBoolean("travelCost"));
                context.assertEquals(FALSE, sender.getBoolean("attendeesParty"));
                context.assertEquals(FALSE, sender.getBoolean("speakersParty"));
                context.assertEquals("XXL", sender.getString("tshirtSize"));
                context.assertEquals("cheap wine, bad beer", sender.getString("allergies"));
                context.assertEquals(FALSE, sender.getBoolean("starred"));
                final Document dataConsent = (Document) sender.get("dataConsent");
                context.assertEquals(3, dataConsent.keySet().size());
                context.assertEquals(TRUE, dataConsent.getBoolean("identification"));
                context.assertEquals(TRUE, dataConsent.getBoolean("contact"));
                context.assertEquals(TRUE, dataConsent.getBoolean("financial"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_paper_when_title_is_not_set(TestContext context) {
        // given
        final Paper testPaper = TestDataGenerator.getTestPaper();
        testPaper.setPaperAbstract("abstract :D");
        testPaper.setTitle(null);
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setBiography("speaker bio");
        final JsonObject paperJson = JsonUtils.removeNullValues(JsonObject.mapFrom(testPaper));

        // when
        final Async async = context.async();
        webClient.post("/public/api/paper/")
            .sendJsonObject(paperJson, assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.paper.title_mandatory", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));
        async.awaitSuccess(10000l);
    }

    @Test
    public void should_get_paper_with_state(TestContext context) {
        // given
        final Paper testPaper = TestDataGenerator.getTestPaper();
        testPaper.setPaperAbstract("Line1\nLine2");
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        // when
        final Async async = context.async();

        webClient.get("/public/api/paper/state/" + paperId)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);
                final JsonObject paper = responseBody.getJsonObject("data").getJsonObject("instance");
                context.assertFalse(paper.containsKey("preferenceDay"));
                
                System.out.println(paper.fieldNames());
                context.assertEquals(9, paper.fieldNames().size());

                context.assertEquals(testPaper.getTitle(), paper.getString("title"));
                context.assertEquals(testPaper.getEdition(), paper.getString("edition"));

                final JsonArray senders = paper.getJsonArray("senders");
                context.assertEquals(1, senders.size());
                final Map<String, Object> sender = (Map<String, Object>) senders.getList().get(0);
                context.assertFalse(sender.containsKey("company"));
                context.assertEquals(16, sender.keySet().size());
                context.assertEquals(testPaper.getSenders().get(0).getFullName(), sender.get("fullName"));
                context.assertEquals(testPaper.getSenders().get(0).getEmail(), sender.get("email"));
                context.assertEquals(testPaper.getSenders().get(0).getBiography(), sender.get("biography"));
                context.assertEquals(testPaper.getSenders().get(0).getPicture(), sender.get("picture"));
                context.assertEquals(testPaper.getSenders().get(0).getWeb(), sender.get("web"));
                context.assertEquals(testPaper.getSenders().get(0).getTwitter(), sender.get("twitter"));
                context.assertEquals(testPaper.getSenders().get(0).getLinkedin(), sender.get("linkedin"));
                context.assertEquals(testPaper.getSenders().get(0).getTshirtSize(), sender.get("tshirtSize"));
                context.assertEquals(testPaper.getSenders().get(0).isAttendeesParty(), sender.get("attendeesParty"));
                context.assertEquals(testPaper.getSenders().get(0).isSpeakersParty(), sender.get("speakersParty"));
                // this line sometimes fails: says it's null
                context.assertEquals(testPaper.getSenders().get(0).getCode(), sender.get("code"));
                context.assertEquals(3, ((Map) sender.get("dataConsent")).keySet().size());
                context.assertEquals(testPaper.getType(), paper.getString("type"));
                context.assertEquals(testPaper.getLevel(), paper.getString("level"));
                context.assertEquals("<p>Line1</p><p>Line2</p>", paper.getString("abstract"));
                context.assertEquals(testPaper.getState().getState(), paper.getString("state"));
                final JsonArray tags = paper.getJsonArray("tags");
                context.assertEquals(1, tags.size());
                context.assertEquals("tag", tags.getList().get(0));

                context.assertEquals(testPaper.getPreferenceDay(), paper.getString("preferenceDay"));
                context.assertEquals(testPaper.getComments(), paper.getString("comments"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_with_sender_company(TestContext context) {
        // given
        final Paper testPaper = TestDataGenerator.getTestPaper();
        final String company = "FancyCorp Ltd.";
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setCompany(company);
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        // when
        final Async async = context.async();

        webClient.get("/public/api/paper/state/" + paperId)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);
                final JsonObject paper = responseBody.getJsonObject("data").getJsonObject("instance");
                context.assertEquals(9, paper.fieldNames().size());

                context.assertEquals(testPaper.getTitle(), paper.getString("title"));
                context.assertEquals(testPaper.getEdition(), paper.getString("edition"));

                final JsonArray senders = paper.getJsonArray("senders");
                context.assertEquals(1, senders.size());
                final Map<String, Object> sender = (Map<String, Object>) senders.getList().get(0);
                context.assertEquals(17, sender.keySet().size());
                context.assertEquals(testSender.getFullName(), sender.get("fullName"));
                context.assertEquals(testSender.getEmail(), sender.get("email"));
                context.assertEquals(testSender.getBiography(), sender.get("biography"));
                context.assertEquals(testSender.getPicture(), sender.get("picture"));
                context.assertEquals(testSender.getWeb(), sender.get("web"));
                context.assertEquals(testSender.getTwitter(), sender.get("twitter"));
                context.assertEquals(testSender.getLinkedin(), sender.get("linkedin"));
                context.assertEquals(testSender.getTshirtSize(), sender.get("tshirtSize"));
                context.assertEquals(testSender.isAttendeesParty(), sender.get("attendeesParty"));
                context.assertEquals(testSender.isSpeakersParty(), sender.get("speakersParty"));
                context.assertEquals(testSender.getCompany(), company);
                // this line sometimes fails: says it's null
                context.assertEquals(testSender.getCode(), sender.get("code"));
                context.assertEquals(3, ((Map) sender.get("dataConsent")).keySet().size());
                context.assertEquals(testPaper.getType(), paper.getString("type"));
                context.assertEquals(testPaper.getLevel(), paper.getString("level"));
                context.assertEquals("<p>" + testPaper.getPaperAbstract() + "</p>", paper.getString("abstract"));
                context.assertEquals(testPaper.getState().getState(), paper.getString("state"));
                final JsonArray tags = paper.getJsonArray("tags");
                context.assertEquals(1, tags.size());
                context.assertEquals("tag", tags.getList().get(0));

                context.assertEquals(testPaper.getPreferenceDay(), paper.getString("preferenceDay"));
                context.assertEquals(testPaper.getComments(), paper.getString("comments"));

            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_paper_state_when_id_is_null(TestContext context) {

        // when
        final Async async = context.async();
        webClient.get("/public/api/paper/state/")
            // then: cannot map the proper url
            .send(handler -> {
                if (handler.succeeded()) {
                    context.assertEquals(handler.result().statusCode(), 404);
                } else {
                    context.fail(handler.cause());
                }
                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_with_talkId_if_exists(TestContext context) {
        // linked by talk.paperId = paper._id
        final String title = "some random text with (parentheses) in the title";
        final Paper testPaper = getTestPaper();
        testPaper.setTitle(title);
        final Talk testTalk = getTestTalk();
        testTalk.setTitle(title);

        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        testTalk.setPaperId(paperId);
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        entries.remove("id");
        final String talkId = insertToDatabase(TALK, Document.parse(entries.encode()));
        // when
        final Async async = context.async();
        webClient.get("/public/api/paper/state/" + paperId)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);
                final JsonObject paper = responseBody.getJsonObject("data").getJsonObject("instance");
                context.assertEquals(title, paper.getString("title"));
                context.assertEquals(talkId, paper.getString("talkId"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_get_paper_with_talkId_if_talk_does_not_exists(TestContext context) {
        // given: linked by title
        final Paper testPaper = getTestPaper();
        final Talk testTalk = getTestTalk();

        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        entries.remove("id");

        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        insertToDatabase(TALK, Document.parse(entries.encode()));
        // when
        final Async async = context.async();
        webClient.get("/public/api/paper/state/" + paperId)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, true);
                final JsonObject paper = responseBody.getJsonObject("data").getJsonObject("instance");
                context.assertEquals(testPaper.getTitle(), paper.getString("title"));
                context.assertFalse(paper.containsKey("talkId"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
