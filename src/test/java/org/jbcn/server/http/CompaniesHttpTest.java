package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.Talk;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.jbcn.server.persistence.Collections.PAPER;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getTestPaper;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;
import static util.asserters.ApiModelAsserter.assertThat;


@RunWith(VertxUnitRunner.class)
public class CompaniesHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/api/companies";
    private static final String TEST_USER = "company_user";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
        dropCollection(TALK);
    }


    @Test
    public void assert_fail_list_companies_when_no_auth_token_is_present(TestContext context) {
        // given
        final Paper testPaper1 = getTestPaper();
        final String company1 = UUID.randomUUID().toString();
        testPaper1.getSenders().get(0).setCompany(company1);
        insertToDatabase(PAPER, testPaper1.toBsonDocument());

        final Paper testPaper2 = getTestPaper();
        final String company2 = UUID.randomUUID().toString();
        testPaper2.getSenders().get(0).setCompany(company2);
        insertToDatabase(PAPER, testPaper2.toBsonDocument());

        // when-then
        final Async async = context.async();
        webClient.get(RESOURCE + "?proposalPhase=paper")
            .send(handler -> {
                if (handler.succeeded()) {
                    var result = handler.result();
                    context.assertEquals(result.statusCode(), 401);
                    context.assertFalse(result.headers().contains(CONTENT_TYPE.toString()));

                    context.assertEquals("Unauthorized", result.bodyAsString());
                } else {
                    context.fail(handler.cause());
                }
                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_paper_companies(TestContext context) {
        // given
        final Paper testPaper1 = getTestPaper();
        final String company1 = "b-" + UUID.randomUUID();
        testPaper1.getSenders().get(0).setCompany(company1);
        insertToDatabase(PAPER, testPaper1.toBsonDocument());

        final Paper testPaper2 = getTestPaper();
        final String company2 = "c-" + UUID.randomUUID();
        testPaper2.getSenders().get(0).setCompany(company2);
        insertToDatabase(PAPER, testPaper2.toBsonDocument());

        // company with empty company are not returned
        final Paper testPaper3 = getTestPaper();
        final String company3 = "a-" + UUID.randomUUID();
        testPaper3.getSenders().get(0).setCompany(company3);
        insertToDatabase(PAPER, testPaper3.toBsonDocument());

        final Paper testPaper4 = getTestPaper();
        testPaper4.getSenders().get(0).setCompany("");
        insertToDatabase(PAPER, testPaper4.toBsonDocument());

        // when-then
        final Async async = context.async();
        webClient.get(RESOURCE + "?proposalPhase=paper")
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                isCollectionAndContainsCompaniesInOrder(context, responseBody, company3, company2, company1);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_talk_companies(TestContext context) {
        // given
        final Talk testTalk1 = getTestTalk();
        final String company1 = "z-" + UUID.randomUUID();
        testTalk1.getSpeakers().get(0).setCompany(company1);
        final JsonObject entries1 = JsonObject.mapFrom(testTalk1);
        entries1.remove("_id");
        insertToDatabase(TALK, Document.parse(entries1.encode()));

        // company with empty string are not returned
        final Talk testTalk20 = getTestTalk();
        testTalk20.getSpeakers().get(0).setCompany("");
        JsonObject entries20 = JsonObject.mapFrom(testTalk20);
        entries20.remove("_id");
        insertToDatabase(TALK, Document.parse(entries20.encode()));

        final Talk testTalk2 = getTestTalk();
        final String company2 = "a-" + UUID.randomUUID();
        testTalk2.getSpeakers().get(0).setCompany(company2);
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        insertToDatabase(TALK, Document.parse(entries2.encode()));

        // when-then
        final Async async = context.async();
        webClient.get(RESOURCE + "?proposalPhase=talk")
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                isCollectionAndContainsCompaniesInOrder(context, responseBody, company2, company1);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void isCollectionAndContainsCompaniesInOrder(TestContext context, JsonObject responseBody, String... expectedCompanies) {

        final JsonArray items = assertThat(responseBody)
            .with(context)
            .isSuccessful()
            .isCollectionResponse()
            .hasSize(expectedCompanies.length)
            .allItems(item -> {
                context.assertNotNull(item.getString("name"));
                context.assertEquals(1, item.fieldNames().size());
            }).getItems();

        final List<String> names = items.stream()
            .map(item -> ((JsonObject) item).getString("name"))
            .collect(Collectors.toList());
        assertThat(names)
            .containsExactlyInAnyOrder(expectedCompanies)
            .isSortedAccordingTo(Comparator.naturalOrder());
    }

    @Test
    public void should_list_zero_talk_companies(TestContext context) {
        final Async async = context.async();
        webClient.get(RESOURCE + "?proposalPhase=talk")
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .isEmpty();
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_talk_companies_when_type_is_not_present(TestContext context) {
        final Async async = context.async();
        webClient.get(RESOURCE)
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.search.conditions_minimum");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_talk_companies_when_phase_is_not_valid(TestContext context) {
        final Async async = context.async();
        webClient.get(RESOURCE + "?proposalPhase=this_is_not_a_valid_value")
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.company.invalid_proposal_type");
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }
}
