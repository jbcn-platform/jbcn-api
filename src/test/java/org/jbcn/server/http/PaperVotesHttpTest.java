package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.PaperVote;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Role.ADMIN;
import static org.jbcn.server.model.Role.VOTER;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.PAPER;
import static util.TestDataGenerator.getTestPaper;
import static util.VertxSetup.*;

@RunWith(VertxUnitRunner.class)
public class PaperVotesHttpTest extends VertxTestSetup {

    // User with voting rights
    private static final String VOTE_TEST_USER = "voter2";
    public static final String RESOURCE = "/api/vote/";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }


    @Test
    public void should_add_first_vote_when_user_is_voter(TestContext context) {
        should_add_first_vote_when_user_has_valid_role(context, VOTER);
    }

    @Test
    public void should_add_first_vote_when_user_is_admin(TestContext context) {
        should_add_first_vote_when_user_has_valid_role(context, ADMIN);
    }

    public void should_add_first_vote_when_user_has_valid_role(TestContext context, Role role) {
        // given
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));
        final JsonObject vote = new JsonObject()
            .put("id", paperId)
            .put("vote", 8.5f);

        // when
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(VOTE_TEST_USER, role))
            .sendJsonObject(vote, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedPaper = getFromDatabase(PAPER, paperId);
                context.assertEquals(2.83d, updatedPaper.getDouble("averageVote"));
                context.assertEquals("voting", updatedPaper.getString("state"));
                context.assertEquals(null, updatedPaper.getString(Traceable.LAST_ACTION_BY));
                Long updatedTime = updatedPaper.getLong(LAST_UPDATE);
                context.assertTrue(updatedTime > updatedPaper.getLong(CREATED_DATE));

                final List<Document> votes = (List) updatedPaper.get("votes");
                context.assertEquals(1, votes.size());
                context.assertEquals(3, votes.get(0).keySet().size());
                context.assertEquals(VOTE_TEST_USER, votes.get(0).getString("username"));
                context.assertEquals(updatedTime, votes.get(0).getLong("date"));
                context.assertEquals(8.5d, votes.get(0).getDouble("vote"));
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_add_second_vote(TestContext context) {
        // given
        final String voter1 = "voter1";
        final String voter3 = "voter3";
        final Paper testPaper = getTestPaper();
        final Date voteTime = new Date();
        testPaper.setVotes(Arrays.asList(new PaperVote(voter1, voter1, 8d, voteTime)));

        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));
        final JsonObject vote = new JsonObject()
            .put("id", paperId)
            .put("vote", 6.75f);

        // when
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(voter3, VOTER))
            .sendJsonObject(vote, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedPaper = getFromDatabase(PAPER, paperId);
                context.assertEquals(4.92d, updatedPaper.getDouble("averageVote"));
                context.assertEquals("voting", updatedPaper.getString("state"));
                context.assertEquals(null, updatedPaper.getString(Traceable.LAST_ACTION_BY));
                Long updatedTime = updatedPaper.getLong(LAST_UPDATE);
                context.assertTrue(updatedTime > updatedPaper.getLong(CREATED_DATE));

                final List<Document> votes = (List) updatedPaper.get("votes");
                context.assertEquals(2, votes.size());

                final Document previousVote = votes.stream().filter(v -> v.get("username").equals(voter1)).findFirst().get();
                context.assertEquals(4, previousVote.keySet().size());
                context.assertEquals(voter1, previousVote.getString("username"));
                context.assertEquals(voter1, previousVote.getString("userId"));
                context.assertEquals(voteTime.getTime(), previousVote.getLong("date"));
                context.assertEquals(8.0d, previousVote.getDouble("vote"));

                final Document newVote = votes.stream().filter(v -> v.get("username").equals(voter3)).findFirst().get();
                context.assertEquals(3, newVote.keySet().size());
                context.assertEquals(voter3, newVote.getString("username"));
                context.assertEquals(updatedTime, newVote.getLong("date"));
                context.assertEquals(6.75d, newVote.getDouble("vote"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // NOTE: create & update use POST (upsert)
    @Test
    public void should_not_authorize_create_vote_when_user_is_not_voter_or_admin(TestContext context) {
        // given
        final JsonObject vote = new JsonObject()
            .put("id", "any-paper-id")
            .put("vote", 9.10f);
        // when
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(VOTE_TEST_USER, List.of(Role.HELPER)))
            .sendJsonObject(vote, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_vote_when_user_already_voted(TestContext context) {
        // given
        final String voter1 = "voter1";
        final String voter3 = "voter3";
        final Paper testPaper = getTestPaper();
        final Date voteTime = new Date();
        testPaper.setVotes(Arrays.asList(
            new PaperVote(voter1, voter1, 8d, voteTime),
            new PaperVote(voter3, voter3, 6d, voteTime)
        ));

        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));
        final JsonObject vote = new JsonObject()
            .put("id", paperId)
            .put("vote", 9.10f);

        // when
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(voter3, VOTER))
            .sendJsonObject(vote, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedPaper = getFromDatabase(PAPER, paperId);
                context.assertEquals(5.7d, updatedPaper.getDouble("averageVote"));
                context.assertEquals("voting", updatedPaper.getString("state"));
                context.assertEquals(null, updatedPaper.getString(Traceable.LAST_ACTION_BY));
                Long updatedTime = updatedPaper.getLong(LAST_UPDATE);
                context.assertTrue(updatedTime > updatedPaper.getLong(CREATED_DATE));

                final List<Document> votes = (List) updatedPaper.get("votes");
                context.assertEquals(2, votes.size());

                final Document previousVote = votes.stream().filter(v -> v.get("username").equals(voter1)).findFirst().get();
                context.assertEquals(4, previousVote.keySet().size());
                context.assertEquals(voter1, previousVote.getString("username"));
                context.assertEquals(voter1, previousVote.getString("userId"));
                context.assertEquals(voteTime.getTime(), previousVote.getLong("date"));
                context.assertEquals(8.0d, previousVote.getDouble("vote"));

                final Document newVote = votes.stream().filter(v -> v.get("username").equals(voter3)).findFirst().get();
                context.assertEquals(4, newVote.keySet().size());
                context.assertEquals(voter3, newVote.getString("username"));
                context.assertEquals(voter3, newVote.getString("userId"));
                context.assertEquals(updatedTime, newVote.getLong("date"));
                context.assertEquals(9.1d, newVote.getDouble("vote"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_vote_when_paper_id_does_not_exists(TestContext context) {
        // given
        final String paperId = "5c2ceeaeaefd397e088196cc";
        final JsonObject vote = new JsonObject()
            .put("id", paperId)
            .put("vote", 8.5f);

        // when
        assert_fail(context, RESOURCE, VOTE_TEST_USER, vote.encode(), "error.paper.unknown_paper", 404);
    }

    @Test
    public void should_fail_vote_when_paper_id_format_is_not_valid(TestContext context) {
        // given
        final String paperId = "123";
        final JsonObject vote = new JsonObject()
            .put("id", paperId)
            .put("vote", 8.5f);

        // when
        assert_fail(context, RESOURCE, VOTE_TEST_USER, vote.encode(), "error.id.invalid_id", 400);
    }

    @Test
    public void should_fail_vote_when_no_body_is_sent(TestContext context) {
        assert_fail(context, RESOURCE, VOTE_TEST_USER, null, "error.body.required", 400);
    }

    @Test
    public void should_fail_vote_when_empty_body_is_sent(TestContext context) {
        assert_fail(context, RESOURCE, VOTE_TEST_USER, "", "error.body.required", 400);
    }

    @Test
    public void should_fail_vote_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail(context, RESOURCE, VOTE_TEST_USER, "{}", "error.body.required", 400);
    }

    @Test
    public void should_get_empty_vote_stream_when_there_are_no_papers(TestContext context) {
        final String username = "voter2";
        // when
        final Async async = context.async();
        webClient.get("/api/vote/stream")
            .bearerTokenAuthentication(getNewSession(username, VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(0, responseBody.getJsonArray("data").size());
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_vote_stream_when_user_is_not_voter_or_admin(TestContext context) {
        // when
        final Async async = context.async();
        webClient.get("/api/vote/stream")
            .bearerTokenAuthentication(getNewSession(VOTE_TEST_USER, List.of(Role.HELPER)))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_empty_vote_stream_when_user_is_not_a_voter(TestContext context) {
        // given
        final String nonVoter = "random_user";
        // when
        final Async async = context.async();
        webClient.get("/api/vote/stream")
            .bearerTokenAuthentication(getNewSession(nonVoter, VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());
                context.assertEquals(0, responseBody.getJsonArray("data").size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_vote_stream_when_user_is_admin(TestContext context) {
        should_get_vote_stream_for_a_user_with_voting_rights(context, ADMIN);
    }

    @Test
    public void should_get_vote_stream_when_user_is_voter(TestContext context) {
        should_get_vote_stream_for_a_user_with_voting_rights(context, VOTER);
    }

    @Test
    public void should_get_vote_stream_in_ascending_order_by_date_by_default(TestContext context) {
        // Newer first
        should_get_vote_stream_for_a_user_with_voting_rights(context, VOTER, Sorting.empty());
    }

    @Test
    public void should_get_vote_stream_in_descending_order_when_set(TestContext context) {
        should_get_vote_stream_for_a_user_with_voting_rights(context, VOTER, Sorting.of("createdDate", false));
    }

    public void should_get_vote_stream_for_a_user_with_voting_rights(TestContext context, Role role) {
        should_get_vote_stream_for_a_user_with_voting_rights(context, role, Sorting.empty());
    }

    public void should_get_vote_stream_for_a_user_with_voting_rights(TestContext context, Role role, Sorting sorting) {
        // given
        final String username = "voter2";
        final Paper testPaper = getTestPaper();
        final List<String> paperIds = IntStream.range(0, 10)
            .mapToObj(i -> {
                final Date createdDate = testPaper.getCreatedDate();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(createdDate);
                calendar.add(Calendar.SECOND, i - 60);
                final Date newDate = calendar.getTime();
                // Simulate chronological order creation
                testPaper.setCreatedDate(newDate);
                return insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));
            })
            .collect(Collectors.toList());

        // when
        final Async async = context.async();
        final String uri = "/api/vote/stream" + (sorting.isSet() ? String.format("?sort=%s&asc=%s", sorting.field, sorting.asc) : "");
        webClient.get(uri)
            .bearerTokenAuthentication(getNewSession(username, role))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());
                final JsonArray papers = responseBody.getJsonArray("data");
                context.assertEquals(10, papers.size());

                final List<String> expectedPaperField = Arrays.asList(
                    "_id",
                    "title",
                    "edition",
                    "senders",
                    "type",
                    "level",
                    "abstract",
                    "state",
                    "averageVote",
                    "votesCount",
                    "tags",
                    "preferenceDay",
                    "comments",
                    "sponsor",
                    "votes",
                    CREATED_DATE,
                    LAST_UPDATE
                );

                final List<Long> creationDates = papers.stream()
                    .map(it -> ((JsonObject) it).getLong(CREATED_DATE))
                    .collect(Collectors.toList());
                for (int i = 0; i < 8; i++) {
                    if (sorting.isAsc()) {
                        context.assertTrue(creationDates.get(i) < creationDates.get(i + 1));
                    } else {
                        context.assertTrue(creationDates.get(i) > creationDates.get(i + 1));
                    }
                }

                papers.stream().forEach(it -> {
                    final JsonObject paper = (JsonObject) it;
                    context.assertEquals(15, paper.fieldNames().size());
                    context.assertTrue(paperIds.contains(paper.getString("_id")));
                    paper.fieldNames().forEach(field -> {
                        context.assertTrue(expectedPaperField.contains(field), field + " missing");
                    });
                    paper.getJsonArray("senders").forEach(it2 -> {
                        final JsonObject sender = (JsonObject) it2;
                        context.assertTrue(sender.getString("fullName").startsWith("Java Rockstar "));
                        context.assertNotNull(sender.getString("code"));
                        context.assertTrue(sender.getString("email").endsWith("@barcelonajug.org"));
                        context.assertEquals("Stop talking, brain thinking.\nHush. I am the Doctor, and you are the Daleks!\n\nThe way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.", sender.getString("biography"));
                        context.assertEquals("https://trololo.com", sender.getString("picture"));
                        context.assertEquals("https://whatever.com", sender.getString("web"));
                        context.assertEquals("twitter", sender.getString("twitter"));
                        context.assertEquals("https://www.linkedin.com/in/something-here", sender.getString("linkedin"));
                        context.assertEquals(FALSE, sender.getBoolean("travelCost"));
                        context.assertEquals(FALSE, sender.getBoolean("attendeesParty"));
                        context.assertEquals(FALSE, sender.getBoolean("speakersParty"));
                        context.assertEquals("XXL", sender.getString("tshirtSize"));
                        context.assertEquals("cheap wine, bad beer", sender.getString("allergies"));
                        context.assertEquals(FALSE, sender.getBoolean("starred"));
                        context.assertEquals("{\"identification\":true,\"contact\":true,\"financial\":true}", sender.getJsonObject("dataConsent").toString());
                    });
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    static class Sorting {
        private final String field;
        private final Boolean asc;

        private Sorting(String field, Boolean asc) {
            this.field = field;
            this.asc = asc;
        }

        public static Sorting of(String field, Boolean asc) {
            return new Sorting(field, asc);
        }

        public static Sorting empty() {
            return new Sorting(null, null);
        }

        public boolean isSet() {
            return asc != null;
        }

        public boolean isAsc() {
            return asc == null || asc;
        }
    }

}
