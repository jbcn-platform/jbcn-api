package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.User;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class PaperVotersHttpTest extends VertxTestSetup {

    private static final String USERS_TEST_USER = "voters_test_user";

    @Test
    public void should_return_voters(TestContext context) {

        final List<String> voters = List.of("Elros Tar-Minyatur", "Tar-Vardamir", "Tar-Amandil", "Tar-Elendil");
        for (String username : voters) {
            insertToDatabase("user", DocumentConverter.getDocument(User.builder().username(username).roles(Set.of(Role.VOTER)).build()));
        }

        final Async async = context.async();
        webClient.get("/api/voters")
                .bearerTokenAuthentication(getNewSession(USERS_TEST_USER))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    final List<String> usernames = responseBody.getJsonObject("data")
                            .getJsonArray("items").stream()
                            .map(e -> ((JsonObject) e).getString("username"))
                            .collect(Collectors.toList());
                    voters.stream()
                            .forEach(username -> context.assertTrue(usernames.contains(username)));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }
}
