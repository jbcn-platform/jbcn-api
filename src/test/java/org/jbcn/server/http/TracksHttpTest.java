package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.TRACKS;
import static util.TestDataGenerator.getTestTrack;
import static util.VertxSetup.*;

@RunWith(VertxUnitRunner.class)
public class TracksHttpTest extends VertxTestSetup {

    private static final String TRACKS_TEST_USER = "tracks_test_user";
    public static final String ENDPOINT = "/api/tracks/";

    @Before
    public void before(TestContext context) {
        dropCollection(TRACKS);
    }

    @Test
    public void should_fail_create_track_when_no_body_is_sent(TestContext context) {
        assert_fail_add_track(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_track_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_track(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_track_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_track(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_track_when_name_is_null(TestContext context) {
        // given
        final String data = "{\"name\":null}";

        // when
        assert_fail_add_track(context, data, "error.body.required_field.name");
    }

    @Test
    public void assert_fail_add_room_when_no_auth_token_is_present(TestContext context) {
        // given
        final JsonObject testTrack = new JsonObject()
                .put("name", "random name -" + UUID.randomUUID())
                .put("description", "random description -" + UUID.randomUUID());

        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT, testTrack);
    }

    public void assert_fail_add_track(TestContext context, String body, String errorMessage) {
        assert_fail(context, ENDPOINT, TRACKS_TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_track_with_minimal_fields(TestContext context) {
        // given
        final String name = "Track-name-" + UUID.randomUUID();
        final JsonObject testTrack = new JsonObject("{ \"name\":\"" + name + "\" }");

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.HELPER))
                .sendJsonObject(testTrack, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document insertedRoom = getFromDatabase(TRACKS, data.getString("_id"));
                    context.assertEquals(name, insertedRoom.getString("name"));
                    context.assertNull(insertedRoom.getString("description"));
                    context.assertNotNull(insertedRoom.getLong(CREATED_DATE));
                    context.assertNotNull(insertedRoom.getLong(LAST_UPDATE));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_creating_track_when_user_is_voter(TestContext context) {
        assert_forbidden_when_creating_track(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_creating_track_when_user_is_speaker(TestContext context) {
        assert_forbidden_when_creating_track(Role.SPEAKER, context);
    }

    @Test
    public void should_not_authorize_creating_track_when_user_is_sponsor(TestContext context) {
        assert_forbidden_when_creating_track(Role.SPONSOR, context);
    }

    private void assert_forbidden_when_creating_track(Role admin, TestContext context) {
        // given
        final String name = "Track-name-" + UUID.randomUUID();
        final JsonObject testTrack = new JsonObject("{ \"name\":\"" + name + "\" }");

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, admin))
                .sendJsonObject(testTrack, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    @Test
    public void should_create_track_with_all_fields(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final JsonObject asJson = DocumentConverter.toJson(testTrack);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .sendJsonObject(asJson, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    final Document insertedRoom = getFromDatabase(TRACKS, data.getString("_id"));
                    context.assertEquals(testTrack.getName(), insertedRoom.getString("name"));
                    context.assertEquals(testTrack.getDescription(), insertedRoom.getString("description"));
                    context.assertNotNull(insertedRoom.getObjectId("_id"));
                    context.assertNotNull(insertedRoom.getLong(CREATED_DATE));
                    context.assertEquals(insertedRoom.getLong(CREATED_DATE), insertedRoom.getLong(LAST_UPDATE));
                    context.assertEquals(6, insertedRoom.keySet().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_track_setting_name_as_integer(TestContext context) {
        // given
        final Integer name = 1234567890;
        final JsonObject testTrack = new JsonObject("{ \"name\":\"" + name + "\" }");

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .sendJsonObject(testTrack, assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertNotNull(data.getString("_id"));

                    context.assertNotNull(data.getString("_id"));

                    final Document insertedUser = getFromDatabase(TRACKS, data.getString("_id"));
                    context.assertEquals(name.toString(), insertedUser.getString("name"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_tracks_when_no_auth_token_is_present(TestContext context) {
        // when
        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
    }

    @Test
    public void should_list_0_tracks(TestContext context) {
        // given: clean collection

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(0, items.size());
                    context.assertEquals(0, data.getInteger("total"));
                    context.assertEquals(2, data.fieldNames().size());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_room(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final String id = insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(1, items.size());
                    context.assertEquals(1, data.getInteger("total"));
                    context.assertEquals(2, data.fieldNames().size());

                    JsonObject o = items.getJsonObject(0);
                    context.assertEquals(id, o.getString("_id"));
                    context.assertEquals(testTrack.getName(), o.getString("name"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_rooms(TestContext context) {
        // given
        int testSize = 12;
        final Map<String, Track> testData = IntStream.range(0, testSize)
                .mapToObj(i -> {
                    final Track testTrack = getTestTrack();
                    return Pair.of(insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack)), testTrack);
                }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (Track) pair.getSecond()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    final JsonArray items = data.getJsonArray("items");
                    context.assertEquals(testSize, items.size());
                    context.assertEquals(12, data.getInteger("total"));

                    items.forEach(it -> {
                        final JsonObject json = (JsonObject) it;
                        final String testId = json.getString("_id");

                        final Track testTrack = testData.get(testId);
                        context.assertEquals(testTrack.getName(), json.getString("name"));

                        context.assertNotNull(testTrack.getCreatedDate());
                        context.assertEquals(testTrack.getCreatedDate(), testTrack.getLastUpdate());
                    });
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_track_by_id(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final String id = insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject json = responseBody.getJsonObject("data");
                    context.assertEquals(6, json.fieldNames().size());
                    context.assertEquals(24, json.getString("_id").length());
                    context.assertEquals(testTrack.getName(), json.getString("name"));
                    context.assertEquals(testTrack.getDescription(), json.getString("description"));
                    context.assertNotNull(testTrack.getCreatedDate());
                    context.assertEquals(testTrack.getCreatedDate(), testTrack.getLastUpdate());
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_track_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_track_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + fakeId)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 404, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.not_found", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_track_name(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final String id = insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack));

        final String newName = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject("{\"name\":\"" + newName + "\"}");

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                    final Boolean status = responseBody.getBoolean("status");
                    context.assertEquals(TRUE, status);
                    context.assertEquals(1, responseBody.fieldNames().size());

                    final Document updatedUser = getFromDatabase(TRACKS, id);
                    context.assertEquals(newName, updatedUser.getString("name"));
                    context.assertTrue(updatedUser.getLong(LAST_UPDATE) > updatedUser.getLong(CREATED_DATE));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_track_description(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final String id = insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack));

        final String newDescription = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject("{\"description\":\"" + newDescription + "\"}");

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                    final Boolean status = responseBody.getBoolean("status");
                    context.assertEquals(TRUE, status);
                    context.assertEquals(1, responseBody.fieldNames().size());

                    final Document updatedUser = getFromDatabase(TRACKS, id);
                    context.assertEquals(testTrack.getName(), updatedUser.getString("name"));
                    context.assertEquals(newDescription, updatedUser.getString("description"));

                    context.assertTrue(updatedUser.getLong(LAST_UPDATE) > updatedUser.getLong(CREATED_DATE));

                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_track_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c4a3ea8aefd394cce055574";
        final JsonObject updateBody = new JsonObject()
                .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + fakeId)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .sendJsonObject(updateBody, assertJsonResponse(async, context, 404, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.not_found", responseBody.getString("error"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_track_when_id_format_is_not_valid(TestContext context) {
        // given
        final JsonObject updateBody = new JsonObject()
                .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + "123")
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .sendJsonObject(updateBody, assertJsonResponse(async, context, 400, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_track(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final String id = insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_track_when_id_format_is_not_valid(TestContext context) {
        // given
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
                .bearerTokenAuthentication(getNewSession(TRACKS_TEST_USER, Role.ADMIN))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
