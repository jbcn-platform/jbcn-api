package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.NOTES;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;
import static util.asserters.ApiModelAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class NotesHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/api/notes";
    private static final String TEST_USER = "test_user";
    private static final String NON_EXISTENT_NOTE_ID = "5a915a9561e9a41a381487e0";

    @Before
    public void setup(TestContext context) {
        dropCollection(NOTES);
    }

    // NOTE: paper id existence is not validated
    @Test
    public void should_create_a_note_when_user_is_voter(TestContext context) {
        should_create_a_note(context, Role.VOTER);
    }

    @Test
    public void should_create_a_note_when_user_is_admin(TestContext context) {
        should_create_a_note(context, Role.ADMIN);
    }

    public void should_create_a_note(TestContext context, Role role) {
        // given
        final JsonObject note = new JsonObject()
            .put("paperId", "1234567890")
            .put("text", "This is a note created on " + new Date());

        // when-then
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(role)))
            .sendJsonObject(note, assertJsonResponse(async, context, 201, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful();

                context.assertNotNull(responseBody.getJsonObject("data").getString("id"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_create_when_creating_a_note_without_paperId(TestContext context) {
        // given
        final JsonObject note = new JsonObject()
            .put("text", "This is a note created on " + new Date());

        // when-then
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .sendJsonObject(note, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.note.paperid_mandatory");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_a_note_when_user_is_voter(TestContext context) {
        should_get_a_note(context, Role.VOTER);
    }

    @Test
    public void should_get_a_note_when_user_is_admin(TestContext context) {
        should_get_a_note(context, Role.ADMIN);
    }

    public void should_get_a_note(TestContext context, Role role) {
        // given
        final String randomPaperId = randomId();
        final String noteId = createTestNote(randomPaperId);
        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(role)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isEntityResponse()
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("id", noteId)
                    .hasStringEqualsTo("owner", TEST_USER)
                    .hasStringEqualsTo("paperId", randomPaperId)
                    .hasStringStartingWith("text", "This is a note created");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_when_note_id_format_is_not_valid(TestContext context) {
        // given
        final String noteId = randomId();
        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_when_note_is_not_found_by_id(TestContext context) {
        // given
        final String noteId = "5a915a9561e9a41a381487e0";
        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.note.unknown_note");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_a_note_when_user_is_voter_and_the_owner(TestContext context) {
        should_delete_a_note(context, Role.VOTER);
    }

    @Test
    public void should_delete_a_note_when_user_is_admin_and_the_owner(TestContext context) {
        should_delete_a_note(context, Role.ADMIN);
    }

    public void should_delete_a_note(TestContext context, Role role) {
        // given
        final String noteId = createTestNote();
        // when-then
        final Async async = context.async();
        webClient.delete(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(role)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_a_note_text_when_user_is_voter_and_the_owner(TestContext context) {
        should_update_a_note_text(context, Role.VOTER);
    }

    @Test
    public void should_update_a_note_text_when_user_is_admin_and_the_owner(TestContext context) {
        should_update_a_note_text(context, Role.ADMIN);
    }

    public void should_update_a_note_text(TestContext context, Role role) {
        // given
        final String newText = "This is a new Text, updated on " + new Date();
        final String noteId = createTestNote();

        // when-then
        final Async async = context.async();
        final JsonObject msgBody = new JsonObject()
            .put("text", newText);
        webClient.put(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(role)))
            .sendJsonObject(msgBody, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertNull(responseBody.getString("data"));
            }));
        async.awaitSuccess(TEST_TIMEOUT);
        // and then
        final Async assertAsync = context.async();
        webClient.get(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(assertAsync, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(TEST_USER, data.getString("owner"));
                context.assertEquals(newText, data.getString("text"));
                context.assertNotNull(data.getLong(CREATED_DATE));
                context.assertNotNull(data.getLong(LAST_UPDATE));
                context.assertTrue(data.getLong(CREATED_DATE) < data.getLong(LAST_UPDATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_when_owner_is_not_the_same(TestContext context) {
        // given
        final String user1 = getRandomUsername();
        final String noteId = createTestNote(randomId(), user1);
        final String user2 = getRandomUsername();

        // when-then
        final Async async = context.async();
        final JsonObject msgBody = new JsonObject()
            .put("text", "This is a new Text, updated on " + new Date());
        webClient.put(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(user2, List.of(Role.VOTER)))
            .sendJsonObject(msgBody, assertJsonResponse(async, context, 403, responseBody -> {
                // TODO should not return any message
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.note.permission");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_when_note_is_not_found_by_id(TestContext context) {
        // given
        final String newText = "This is a new Text, updated on " + new Date();
        final String noteId = NON_EXISTENT_NOTE_ID;
        // when-then
        final Async async = context.async();
        final JsonObject msgBody = new JsonObject()
            .put("text", newText);
        webClient.put(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .sendJsonObject(msgBody, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.note.unknown_note");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_when_note_is_not_found_by_id(TestContext context) {
        // given
        final String noteId = NON_EXISTENT_NOTE_ID;
        // when-then
        final Async async = context.async();
        webClient.delete(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.note.unknown_note");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_note_when_owner_is_not_the_same(TestContext context) {
        // given
        final String user1 = getRandomUsername();
        final String noteId = createTestNote(randomId(), user1);
        final String user2 = getRandomUsername();

        // when-then
        final Async async = context.async();
        final JsonObject msgBody = new JsonObject()
            .put("text", "This is a new Text, updated on " + new Date());
        webClient.delete(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(user2, List.of(Role.VOTER)))
            .sendJsonObject(msgBody, assertJsonResponse(async, context, 403, responseBody -> {
                // TODO should not return any message
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.note.permission");
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_some_notes_by_paperId(TestContext context) throws InterruptedException {
        // given
        final String paperId = randomId();
        final String testNote1 = createTestNote(paperId, TEST_USER);
        // Ensure order by date is assertions
        Thread.sleep(5l);
        final String testNote2 = createTestNote(paperId, TEST_USER);
        Thread.sleep(5l);
        final String testNote3 = createTestNote(paperId, TEST_USER);

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s?paperId=%s", RESOURCE, paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isNotesCollectionResponse()
                    .hasSize(3)
                    .hasItem(0, item -> context.assertEquals(testNote3, item.getString("id")))
                    .hasItem(1, item -> context.assertEquals(testNote2, item.getString("id")))
                    .hasItem(2, item -> context.assertEquals(testNote1, item.getString("id")))
                    .allItems(item -> {
                        context.assertEquals(TEST_USER, item.getString("owner"));
                        context.assertEquals(paperId, item.getString("paperId"));
                    });
            }));
        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_some_notes_by_owner(TestContext context) throws InterruptedException {
        // given
        final String user1 = getRandomUsername();
        final String user2 = getRandomUsername();

        final String paperId = randomId();
        final String testNote1 = createTestNote(paperId + "1", user1);
        Thread.sleep(10l);
        final String testNote2 = createTestNote(paperId + "1", user1);
        Thread.sleep(10l);
        final String testNote3 = createTestNote(paperId + "2", user2);
        // when-then
        final Async async = context.async(2);

        webClient.get(String.format("%s?owner=%s", RESOURCE, user1))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isNotesCollectionResponse()
                    .hasSize(2)
                    // returned in creation date desc by default
                    .hasItem(0, item -> context.assertEquals(testNote2, item.getString("id")))
                    .hasItem(1, item -> context.assertEquals(testNote1, item.getString("id")))
                    .allItems(item -> {
                        context.assertEquals(user1, item.getString("owner"));
                        context.assertEquals(paperId + "1", item.getString("paperId"));
                    });
            }));
        // and then
        webClient.get(String.format("%s?owner=%s", RESOURCE, user2))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isNotesCollectionResponse()
                    .hasSize(1)
                    .hasItem(0, item -> context.assertEquals(testNote3, item.getString("id")))
                    .allItems(item -> {
                        context.assertEquals(user2, item.getString("owner"));
                        context.assertEquals(paperId + "2", item.getString("paperId"));
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_notes_sorted_by_date_desc_by_default(TestContext context) throws InterruptedException {
        // given
        final String paperId = randomId();
        final String note1 = createTestNote(paperId);
        Thread.sleep(10l);
        final String note2 = createTestNote(paperId);
        Thread.sleep(10l);
        final String note3 = createTestNote(paperId);
        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s?paperId=%s", RESOURCE, paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isNotesCollectionResponse()
                    .hasSize(3)
                    .hasItem(0, item -> context.assertEquals(note3, item.getString("id")))
                    .hasItem(1, item -> context.assertEquals(note2, item.getString("id")))
                    .hasItem(2, item -> context.assertEquals(note1, item.getString("id")));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_0_items_if_filters_dont_match_any(TestContext context) {
        // given
        final String nonExistentPaper = randomId();
        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s?paperId=%s", RESOURCE, nonExistentPaper))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isNotesCollectionResponse()
                    .isEmpty();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_when_no_filter_is_set(TestContext context) {
        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s", RESOURCE))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.VOTER)))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.search.conditions_minimum");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private String randomId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    private String createTestNote() {
        return createTestNote(randomId(), TEST_USER);
    }

    private String createTestNote(String paperId) {
        return createTestNote(paperId, TEST_USER);
    }

    private String createTestNote(String paperId, String username) {
        final long currentTime = System.currentTimeMillis();
        return insertToDatabase(NOTES, new JsonObject()
            .put("owner", username)
            .put("paperId", paperId)
            .put("text", "This is a note created on " + new Date())
            .put(CREATED_DATE, currentTime)
            .put(LAST_UPDATE, currentTime));
    }

    /*****************************************
     * PERMISSIONS
     *****************************************/

    @Test
    public void should_not_authorize_create_note_when_user_is_not_voter(TestContext context) {
        // given
        final JsonObject note = new JsonObject()
            .put("paperId", "1234567890")
            .put("text", "This is a note created on " + new Date());
        // when
        final Async async = context.async();
        webClient.post(RESOURCE)
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.HELPER)))
            .sendJsonObject(note, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_get_note_when_user_is_not_voter(TestContext context) {
        // given
        final String randomPaperId = randomId();
        // when
        final Async async = context.async();
        webClient.get(String.format("%s/%s", RESOURCE, randomPaperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.HELPER)))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_list_notes_when_user_is_not_voter(TestContext context) {
        // given
        final String randomPaperId = randomId();
        // when
        final Async async = context.async();
        webClient.get(String.format("%s?paperId=%s", RESOURCE, randomPaperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, List.of(Role.HELPER)))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_update_note_when_user_is_not_voter(TestContext context) {
        // given
        final String newText = "This is a new Text, updated on " + new Date();
        final String noteId = createTestNote();
        // when
        final Async async = context.async();
        final JsonObject msgBody = new JsonObject()
            .put("text", newText);
        webClient.put(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .sendJsonObject(msgBody, assertForbiddenWithEmptyBody(context, async));
    }

    @Test
    public void should_not_authorize_delete_note_when_user_is_not_voter(TestContext context) {
        // given
        final String newText = "This is a new Text, updated on " + new Date();
        final String noteId = createTestNote();
        // when
        final Async async = context.async();
        final JsonObject msgBody = new JsonObject()
            .put("text", newText);
        webClient.delete(String.format("%s/%s", RESOURCE, noteId))
            .bearerTokenAuthentication(getNewSession(TEST_USER))
            .sendJsonObject(msgBody, assertForbiddenWithEmptyBody(context, async));
    }
}
