package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Speaker;
import org.jbcn.server.model.Talk;
import org.jbcn.server.site.SiteUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.getSpeaker;
import static util.TestDataGenerator.getTestTalk;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

@RunWith(VertxUnitRunner.class)
public class PublishedSpeakersTest extends VertxTestSetup {

    private static final String PUBLIC_ENDPOINT = "/public/speakers/";

    @Before
    public void setup(TestContext context) {
        dropCollection(TALK);
    }

    @Test
    public void should_generate_speakers_json_for_site(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        final Speaker speaker1 = getSpeaker("-1");
        speaker1.setJobTitle("Amazing speaker!!");
        testTalk.setSpeakers(Arrays.asList(speaker1));
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));

        final Talk testTalk2 = getTestTalk();
        testTalk2.setPublished(true);
        final Speaker speaker2 = getSpeaker("-2");
        final Speaker speaker3 = getSpeaker("-3");
        final Speaker speaker4 = getSpeaker("-4");
        final Speaker speaker5 = getSpeaker("-5");
        testTalk2.setSpeakers(Arrays.asList(speaker2, speaker3, speaker4, speaker5));
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        final String talkId2 = insertToDatabase("talk", Document.parse(entries2.encode()));

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final JsonArray items = responseBody.getJsonArray("items");
                context.assertEquals(5, items.size());

                for (Object o : items) {
                    context.assertEquals(11, ((JsonObject) o).fieldNames().size());
                }
                // results are sorted by date
                final Map<String, JsonObject> speakers = items.stream()
                    .collect(Collectors.toMap(it -> ((JsonObject) it).getString("name"), it -> ((JsonObject) it)));
                context.assertEquals(speakers.get("Test speaker -1").getString("name"), speaker1.getFullName());
                context.assertEquals(speakers.get("Test speaker -1").getString("description"), speaker1.getJobTitle());
                context.assertEquals(speakers.get("Test speaker -2").getString("name"), speaker2.getFullName());
                context.assertEquals(speakers.get("Test speaker -2").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -3").getString("name"), speaker3.getFullName());
                context.assertEquals(speakers.get("Test speaker -3").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -4").getString("name"), speaker4.getFullName());
                context.assertEquals(speakers.get("Test speaker -4").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -5").getString("name"), speaker5.getFullName());
                context.assertEquals(speakers.get("Test speaker -5").getString("description"), "");

                final List<String> nullValues = items.stream()
                    .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                    .filter(e -> e.getValue() == null)
                    .map(e -> e.getKey())
                    .collect(Collectors.toList());
                context.assertEquals(nullValues.size(), 0);
                // `description` is generated empty right now
                final List<String> emptyValues = items.stream()
                    .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                    .filter(e -> ((String) e.getValue()).isEmpty())
                    .map(e -> e.getKey())
                    .collect(Collectors.toList());
                context.assertEquals(emptyValues.size(), 4);
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_all_speakers_with_public_data(TestContext context) {
        should_get_all_speakers_with_public_data(context, "", """
            A life of:
            * Passion
            - Adventure
                            
            ...and stuff""");
    }

    @Test
    public void should_get_all_speakers_with_public_data_and_formatted_bio(TestContext context) {
        should_get_all_speakers_with_public_data(context, "?format=html",
            """
                <p>A life of:</p>
                <ul>
                <li>Passion</li>
                <li>Adventure</li>
                </ul>
                <p>...and stuff</p>
                """.replaceAll("\n", ""));
    }

    public void should_get_all_speakers_with_public_data(TestContext context, String urlOptions, String expectedBio) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        final Speaker speaker1 = getSpeaker("-1");
        speaker1.setJobTitle("Amazing speaker!!");
        testTalk.setSpeakers(Arrays.asList(speaker1));
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));

        final Talk testTalk2 = getTestTalk();
        testTalk2.setPublished(true);
        final Speaker speaker2 = getSpeaker("-2");
        final Speaker speaker3 = getSpeaker("-3");
        final Speaker speaker4 = getSpeaker("-4");
        final Speaker speaker5 = getSpeaker("-5");
        testTalk2.setSpeakers(Arrays.asList(speaker2, speaker3, speaker4, speaker5));
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        final String talkId2 = insertToDatabase("talk", Document.parse(entries2.encode()));

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT + urlOptions)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final JsonArray items = responseBody.getJsonArray("items");
                context.assertEquals(5, items.size());

                for (Object o : items) {
                    context.assertEquals(11, ((JsonObject) o).fieldNames().size());
                }
                // results are sorted by date
                final Map<String, JsonObject> speakers = items.stream()
                    .collect(Collectors.toMap(it -> ((JsonObject) it).getString("name"), it -> ((JsonObject) it)));
                context.assertEquals(speakers.get("Test speaker -1").getString("name"), speaker1.getFullName());
                context.assertEquals(speakers.get("Test speaker -1").getString("description"), speaker1.getJobTitle());
                context.assertEquals(speakers.get("Test speaker -1").getString("biography"), expectedBio);
                
                context.assertEquals(speakers.get("Test speaker -2").getString("name"), speaker2.getFullName());
                context.assertEquals(speakers.get("Test speaker -2").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -2").getString("biography"), expectedBio);
                
                context.assertEquals(speakers.get("Test speaker -3").getString("name"), speaker3.getFullName());
                context.assertEquals(speakers.get("Test speaker -3").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -3").getString("biography"), expectedBio);
                
                context.assertEquals(speakers.get("Test speaker -4").getString("name"), speaker4.getFullName());
                context.assertEquals(speakers.get("Test speaker -4").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -4").getString("biography"), expectedBio);
                
                context.assertEquals(speakers.get("Test speaker -5").getString("name"), speaker5.getFullName());
                context.assertEquals(speakers.get("Test speaker -5").getString("description"), "");
                context.assertEquals(speakers.get("Test speaker -5").getString("biography"), expectedBio);
                

                final List<String> nullValues = items.stream()
                    .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                    .filter(e -> e.getValue() == null)
                    .map(e -> e.getKey())
                    .collect(Collectors.toList());
                context.assertEquals(nullValues.size(), 0);
                // `description` is generated empty right now
                final List<String> emptyValues = items.stream()
                    .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                    .filter(e -> ((String) e.getValue()).isEmpty())
                    .map(e -> e.getKey())
                    .collect(Collectors.toList());
                context.assertEquals(emptyValues.size(), 4);
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_speaker_by_id(TestContext context) {
        should_get_speaker_by_id_with_formatted_bio(context, "",
            """
                A life of:
                * Passion
                - Adventure
                                
                ...and stuff""");
    }

    @Test
    public void should_get_speaker_by_id_with_formatted_bio(TestContext context) {
        should_get_speaker_by_id_with_formatted_bio(context, "?format=html",
            """
                <p>A life of:</p>
                <ul>
                <li>Passion</li>
                <li>Adventure</li>
                </ul>
                <p>...and stuff</p>
                """.replaceAll("\n", ""));
    }

    public void should_get_speaker_by_id_with_formatted_bio(TestContext context, String urlOptions, String expectedBio) {
        // given
        final Talk testTalk = getTestTalk();
        testTalk.setPublished(true);
        final Speaker speaker = getSpeaker("-1");
        speaker.setJobTitle("Amazing speaker!!");
        testTalk.setSpeakers(Arrays.asList(speaker));
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase(TALK, Document.parse(entries.encode()));
        final String systemUpdatedRef = Speaker.generateRef(speaker.getFullName(), speaker.getEmail());

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT + speaker.getRef() + urlOptions)
            .send(assertJsonResponse(async, context, 200, speakerJson -> {
                context.assertEquals(speakerJson.getString("name"), speaker.getFullName());
                context.assertEquals(speakerJson.getString("company"), speaker.getCompany());
                context.assertEquals(speakerJson.getString("description"), speaker.getJobTitle());
                context.assertEquals(speakerJson.getString("image"), SiteUtils.getImageUrl(speaker.getPicture()));
                context.assertEquals(speakerJson.getString("url"), "infoSpeaker.html?ref=" + systemUpdatedRef);
                context.assertEquals(speakerJson.getString("homepage"), speaker.getWeb());
                context.assertEquals(speakerJson.getString("twitter"), speaker.getTwitter());
                context.assertEquals(speakerJson.getString("linkedin"), speaker.getLinkedin());
                context.assertEquals(speakerJson.getString("ref"), systemUpdatedRef);
                context.assertEquals(speakerJson.getString("biography"), expectedBio);
                // 11 b/c we don't care about 'enabled'
                context.assertEquals(11, speakerJson.fieldNames().size());
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }


    @Test
    public void should_generate_speakers_without_including_null_values(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final Speaker speaker1 = getSpeaker("-1");
        speaker1.setWeb(null);
        speaker1.setTwitter(null);
        speaker1.setLinkedin(null);
        testTalk.setPublished(true);
        testTalk.setSpeakers(Arrays.asList(speaker1));
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase("talk", Document.parse(entries.encode()));

        final Talk testTalk2 = getTestTalk();
        testTalk2.setPublished(true);
        final Speaker speaker2 = getSpeaker("-2");
        final Speaker speaker3 = getSpeaker("-3");
        final Speaker speaker4 = getSpeaker("-4");
        final Speaker speaker5 = getSpeaker("-5");
        testTalk2.setSpeakers(Arrays.asList(speaker2, speaker3, speaker4, speaker5));
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        insertToDatabase("talk", Document.parse(entries2.encode()));

        // when
        final Async async = context.async();
        webClient.get(PUBLIC_ENDPOINT)
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(1, responseBody.fieldNames().size());
                final JsonArray items = responseBody.getJsonArray("items");
                context.assertEquals(5, items.size());

                // then: speaker 1 has 3 values with null out of 10
                JsonObject o1 = (JsonObject) items.stream()
                    .filter(o -> ((JsonObject) o).getString("name").endsWith("-1"))
                    .findFirst()
                    .get();
                context.assertEquals(8, o1.fieldNames().size());

                // and-then: all other speakers, have all fields with values
                for (Object o : items.stream().filter(o -> !((JsonObject) o).getString("name").endsWith("-1")).collect(Collectors.toList())) {
                    context.assertEquals(11, ((JsonObject) o).fieldNames().size());
                }
                // results are sorted by date
                final List<String> names = items.stream()
                    .map(it -> ((JsonObject) it).getString("name"))
                    .sorted()
                    .collect(Collectors.toList());
                context.assertEquals(names.get(0), speaker1.getFullName());
                context.assertEquals(names.get(1), speaker2.getFullName());
                context.assertEquals(names.get(2), speaker3.getFullName());
                context.assertEquals(names.get(3), speaker4.getFullName());
                context.assertEquals(names.get(4), speaker5.getFullName());

                final List<String> nullValues = items.stream()
                    .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                    .filter(e -> e.getValue() == null)
                    .map(e -> e.getKey())
                    .collect(Collectors.toList());
                context.assertEquals(nullValues.size(), 0);
                // `description` is generated empty right now
                final List<String> emptyValues = items.stream()
                    .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                    .filter(e -> ((String) e.getValue()).isEmpty())
                    .map(e -> e.getKey())
                    .collect(Collectors.toList());
                context.assertEquals(emptyValues.size(), 5);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO what happens if a speaker is in a published an unpublished talk?
    /*
    // WIP: system gets stuck in this case
    @Test
    public void should_generate_speakers_with_only_published_talks(TestContext context) {
        // given
        final Talk testTalk = getTestTalk();
        final Speaker speaker1 = getSpeaker("-1");
        testTalk.setPublished(true);
        testTalk.setSpeakers(Arrays.asList(speaker1));
        final JsonObject entries = JsonObject.mapFrom(testTalk);
        entries.remove("_id");
        insertToDatabase("talk", Document.parse(entries.encode()));

        final Talk testTalk2 = getTestTalk();
        testTalk2.setPublished(false);
        final Speaker speaker2 = getSpeaker("-2");
        testTalk2.setSpeakers(Arrays.asList(speaker2));
        final JsonObject entries2 = JsonObject.mapFrom(testTalk2);
        entries2.remove("_id");
        insertToDatabase("talk", Document.parse(entries2.encode()));

        // when
        final Async async = context.async();
        httpClient.get("/public/data/speakers.json")
                .putHeader(AUTHORIZATION, "Bearer " + getNewSession(TALKS_TEST_USER, context))
                .handler(response -> {
                    context.assertEquals(response.statusCode(), 200);
                    context.assertTrue(response.headers().get(CONTENT_TYPE).contains("application/json"));
                    response.bodyHandler(body -> {
                        context.assertEquals(1, body.toJsonObject().fieldNames().size());
                        final JsonArray items = body.toJsonObject().getJsonArray("items");
                        context.assertEquals(1, items.size());

                        for (Object o : items) {
                            context.assertEquals(10, ((JsonObject) o).fieldNames().size());
                        }
                        // results are sorted by date
                        final List<String> names = items.stream()
                                .map(it -> ((JsonObject) it).getString("name"))
                                .sorted()
                                .collect(Collectors.toList());
                        context.assertEquals(names.get(0), speaker1.getFullName());
                        context.assertEquals(names.get(1), speaker2.getFullName());

                        final List<String> nullValues = items.stream()
                                .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                                .filter(e -> e.getValue() == null)
                                .map(e -> e.getKey())
                                .collect(Collectors.toList());
                        context.assertEquals(nullValues.size(), 0);
                        // `description` is generated empty right now
                        final List<String> emptyValues = items.stream()
                                .flatMap(it -> ((JsonObject) it).getMap().entrySet().stream())
                                .filter(e -> ((String) e.getValue()).isEmpty())
                                .map(e -> e.getKey())
                                .collect(Collectors.toList());
                        context.assertEquals(emptyValues.size(), 5);
                        async.complete();
                    });
                })
                .end();

        async.awaitSuccess(TEST_TIMEOUT);
    }
    */
}
