package org.jbcn.server.http;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Date;
import java.util.UUID;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.persistence.Collections.PUBLIC_VOTES;
import static util.VertxSetup.dropCollection;
import static util.VertxSetup.insertToDatabase;

/**
 * Old API - To be deleted (confirm with jguitart)
 */
@RunWith(VertxUnitRunner.class)
public class MobileVotesHttpTest extends VertxTestSetup {

    @Before
    public void before(TestContext context) {
        dropCollection(PUBLIC_VOTES);
    }


    @Test
    public void should_add_first_vote_to_talk_as_int(TestContext context) {
        // given
        final String scheduleId = "schedude_id";
        final String deviceId = "device_id";
        final Integer vote = 5;

        // when
        final Async async = context.async();
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, deviceId, vote))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertEquals(5.0d, data.getDouble("average"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_add_first_vote_to_talk_as_double(TestContext context) {
        // given
        final String scheduleId = "schedude_id";
        final String deviceId = "device_id";
        final double vote = 5.5;

        // when
        final Async async = context.async();
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, deviceId, vote))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());
                    context.assertEquals("error.talk.vote.invalid_format", responseBody.getString("error"));
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_add_first_vote_when_schedule_does_not_exist(TestContext context) {
        // given
        final String scheduleId = UUID.randomUUID().toString();
        final String deviceId = UUID.randomUUID().toString();
        final Integer vote = 7;

        // when
        final Async async = context.async();
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, deviceId, vote))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertEquals(7.0d, data.getDouble("average"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_add_and_update_votes_from_different_devices(TestContext context) {
        // given
        final String scheduleId = UUID.randomUUID().toString();
        final String[] devices = new String[]{"device_0", "device_1"};

        insertVoteSyc(scheduleId, devices[0], 5, 5.0, context);
        insertVoteSyc(scheduleId, devices[1], 6, 5.5, context);

        // when
        final Async async = context.async();
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, devices[1], 10))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertEquals(7.5d, data.getDouble("average"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void insertVoteSyc(String scheduleId, String device, int vote, double expectedAvg, TestContext context) {
        final Async givenAsync = context.async();
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, device, vote))
                .send(handler -> {
                    if (handler.succeeded()) {
                        final JsonObject jsonObject = handler.result().bodyAsJsonObject();
                        final JsonObject data = jsonObject.getJsonObject("data");
                        context.assertEquals(1, data.fieldNames().size());
//                        context.assertEquals(expectedAvg, data.getDouble("average"));
                    } else {
                        context.fail(handler.cause());
                    }
                    givenAsync.complete();
                });

        givenAsync.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_vote_on_single_device(TestContext context) {
        // given
        final String scheduleId = "schedule_id";
        final String deviceId = "device_id";

        final Async givenAsync = context.async();
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, deviceId, 7))
                .send(handler -> {
                    if (handler.succeeded()) givenAsync.complete();
                    else context.fail(handler.cause());
                });
        givenAsync.awaitSuccess(TEST_TIMEOUT);

        // when
        final Async async = context.async(4);
        webClient.get(String.format("/public/api/vote/%s/%s/%s", scheduleId, deviceId, 10))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertEquals(10.0d, data.getDouble("average"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_average_vote_for_a_schedule(TestContext context) {
        // given
        final String scheduleId = "schedule_id";
        final String[] devices = new String[]{"device_0", "device_1"};

        insertVoteSyc(scheduleId, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleId, devices[1], 6, 7.0, context);
        // when
        final Async async = context.async(4);
        webClient.get(String.format("/public/api/vote/%s", scheduleId))
                .send(assertJsonResponse(async, context, 200, responseBody -> {
                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
                    context.assertEquals(2, responseBody.fieldNames().size());

                    final JsonObject data = responseBody.getJsonObject("data");
                    context.assertEquals(1, data.fieldNames().size());
                    context.assertEquals(7.0d, data.getDouble("average"));
                    async.complete();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_all_average_votes(TestContext context) {
        // given
        final String scheduleId = "schedule";
        final String scheduleIdON = "ON-schedule";
        final String scheduleIdUE = "UE-schedule";
        final String scheduleIdED = "ED-schedule";
        final String[] devices = new String[]{"device_0", "device_1", "device_2"};

        insertVoteSyc(scheduleId, devices[0], 10, 10.0, context);
        insertVoteSyc(scheduleId, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleId, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleId, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleId, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleId, devices[2], 3, 5.0, context);

        insertVoteSyc(scheduleIdON, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleIdON, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleIdON, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleIdON, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleIdON, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleIdON, devices[2], 3, 5.0, context);

        insertVoteSyc(scheduleIdUE, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleIdUE, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleIdUE, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleIdUE, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleIdUE, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleIdUE, devices[2], 3, 5.0, context);

        insertVoteSyc(scheduleIdED, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleIdED, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleIdED, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleIdED, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleIdED, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleIdED, devices[2], 3, 5.0, context);

        // when
        final Async async = context.async(4);
        webClient.get(String.format("/public/api/votes", scheduleId))
                .send(handler -> {
                    if (handler.succeeded()) {
                        final HttpResponse<Buffer> result = handler.result();
                        context.assertEquals(result.statusCode(), 200);
                        context.assertTrue(result.headers().get(CONTENT_TYPE).contains("text/csv"));
                        final String[] lines = handler.result().bodyAsString().split("\n");
                        context.assertEquals(12, lines.length);
                        context.assertEquals("#schedule,3", lines[0]);
                        context.assertEquals("#schedule,4", lines[1]);
                        context.assertEquals("#schedule,10", lines[2]);
                        context.assertEquals("#TUE-schedule,3", lines[3]);
                        context.assertEquals("#TUE-schedule,4", lines[4]);
                        context.assertEquals("#TUE-schedule,8", lines[5]);
                        context.assertEquals("#MON-schedule,3", lines[6]);
                        context.assertEquals("#MON-schedule,4", lines[7]);
                        context.assertEquals("#MON-schedule,8", lines[8]);
                        context.assertEquals("#WED-schedule,3", lines[9]);
                        context.assertEquals("#WED-schedule,4", lines[10]);
                        context.assertEquals("#WED-schedule,8", lines[11]);
                    } else {
                        context.fail(handler.cause());
                    }
                    async.complete();
                });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_all_average_votes_correctly_even_when_database_has_duplicated_values(TestContext context) {
        // given: inconsistent duplicated values in the database
        final String scheduleId = "schedule";
        final String scheduleIdON = "ON-schedule";
        final String scheduleIdUE = "UE-schedule";
        final String scheduleIdED = "ED-schedule";
        final String[] devices = new String[]{"device_0", "device_1", "device_2"};

        // duplicated value inserted manually
        final Document document = new Document();
        document.put("scheduleId", scheduleId);
        document.put("deviceId", devices[0]);
        document.put("vote", 0);
        document.put("date", new Date());
        insertToDatabase(PUBLIC_VOTES, document);

        insertVoteSyc(scheduleId, devices[0], 10, 10.0, context);
        insertVoteSyc(scheduleId, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleId, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleId, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleId, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleId, devices[2], 3, 5.0, context);

        insertVoteSyc(scheduleIdON, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleIdON, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleIdON, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleIdON, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleIdON, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleIdON, devices[2], 3, 5.0, context);

        insertVoteSyc(scheduleIdUE, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleIdUE, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleIdUE, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleIdUE, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleIdUE, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleIdUE, devices[2], 3, 5.0, context);

        insertVoteSyc(scheduleIdED, devices[0], 8, 8.0, context);
        insertVoteSyc(scheduleIdED, devices[1], 6, 7.0, context);
        insertVoteSyc(scheduleIdED, devices[1], 5, 6.5, context);
        insertVoteSyc(scheduleIdED, devices[2], 9, 7.333333333333333, context);
        insertVoteSyc(scheduleIdED, devices[1], 4, 7.0, context);
        insertVoteSyc(scheduleIdED, devices[2], 3, 5.0, context);

        // when
        final Async async = context.async(4);
        webClient.get(String.format("/public/api/votes", scheduleId))
                .send(handler -> {
                    if (handler.succeeded()) {
                        final HttpResponse<Buffer> result = handler.result();
                        context.assertEquals(result.statusCode(), 200);
                        context.assertTrue(result.headers().get(CONTENT_TYPE).contains("text/csv"));
                        final String[] lines = handler.result().bodyAsString().split("\n");
                        context.assertEquals(12, lines.length);
                        context.assertEquals("#schedule,3", lines[0]);
                        context.assertEquals("#schedule,4", lines[1]);
                        context.assertEquals("#schedule,10", lines[2]);
                        context.assertEquals("#TUE-schedule,3", lines[3]);
                        context.assertEquals("#TUE-schedule,4", lines[4]);
                        context.assertEquals("#TUE-schedule,8", lines[5]);
                        context.assertEquals("#MON-schedule,3", lines[6]);
                        context.assertEquals("#MON-schedule,4", lines[7]);
                        context.assertEquals("#MON-schedule,8", lines[8]);
                        context.assertEquals("#WED-schedule,3", lines[9]);
                        context.assertEquals("#WED-schedule,4", lines[10]);
                        context.assertEquals("#WED-schedule,8", lines[11]);
                    } else {
                        context.fail(handler.cause());
                    }
                    async.complete();
                });

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
