package org.jbcn.server.http;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.scheduling.Session;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.persistence.Collections;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.SESSIONS;
import static org.jbcn.server.persistence.Collections.TRACKS;
import static org.jbcn.server.utils.TimeUtils.toTimestamp;
import static util.TestDataGenerator.getTestSession;
import static util.TestDataGenerator.getTestTrack;
import static util.VertxSetup.*;

// TODO document
// TODO should not allow session without trackId, or non existent track
@RunWith(VertxUnitRunner.class)
public class SessionsHttpTest extends VertxTestSetup {

    private static final String SESSIONS_TEST_USER = "sessions_test_user";
    public static final String ENDPOINT = "/api/sessions/";


    @Before
    public void before(TestContext context) {
        dropCollection(SESSIONS);
    }

    @Test
    public void should_fail_create_session_when_no_body_is_sent(TestContext context) {
        assert_fail_add_session(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_session_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_session(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_session_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_session(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_session_when_slotId_is_null(TestContext context) {
        // given
        final String session = "{\"slotId\":null}";

        // when
        assert_fail_add_session(context, session, "error.body.required_field.slotId");
    }

    @Test
    public void should_fail_add_session_when_slotId_is_empty(TestContext context) {
        // given
        final String session = "{\"slotId\":\"\"}";

        // when
        assert_fail_add_session(context, session, "error.body.required_field.slotId");
    }

    @Test
    public void should_fail_add_session_when_day_is_null(TestContext context) {
        // given
        final String session = "{\"slotId\":\"123\", \"day\":null}";

        // when
        assert_fail_add_session(context, session, "error.body.required_field.day");
    }

    @Test
    public void should_fail_add_session_when_day_is_empty(TestContext context) {
        // given
        final String session = "{\"slotId\":\"123\", \"day\":\"\"}";

        // when
        assert_fail_add_session(context, session, "error.body.required_field.day");
    }

    @Test
    public void assert_fail_add_session_when_no_auth_token_is_present(TestContext context) {
        // given
        final JsonObject jsonEntity = new JsonObject()
            .put("name", "some_name");

        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT, jsonEntity);
    }

    public void assert_fail_add_session(TestContext context, String body, String errorMessage) {
        assert_fail(context, "/api/sessions", SESSIONS_TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_session_with_minimal_fields(TestContext context) {
        // given
        final String slotId = "slotId-" + UUID.randomUUID();
        final String day = "day-" + UUID.randomUUID();
        final JsonObject testSession = new JsonObject()
            .put("slotId", slotId)
            .put("day", day);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .sendJsonObject(testSession, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.fieldNames().size());
                context.assertNotNull(data.getString("_id"));

                final Document inserted = getFromDatabase(SESSIONS, data.getString("_id"));
                context.assertEquals(slotId, inserted.getString("slotId"));
                context.assertEquals(day, inserted.getString("day"));
                context.assertNotNull(inserted.getLong(CREATED_DATE));
                context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
                context.assertEquals(SESSIONS_TEST_USER, inserted.getString(LAST_ACTION_BY));
                context.assertEquals(6, inserted.keySet().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_creating_session_when_user_is_helper(TestContext context) {
        assert_forbidden_when_creating_session(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_creating_session_when_user_is_voter(TestContext context) {
        assert_forbidden_when_creating_session(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_creating_session_when_user_is_sponsor(TestContext context) {
        assert_forbidden_when_creating_session(Role.SPONSOR, context);
    }

    @Test
    public void should_not_authorize_creating_session_when_user_is_speaker(TestContext context) {
        assert_forbidden_when_creating_session(Role.SPEAKER, context);
    }

    private void assert_forbidden_when_creating_session(Role role, TestContext context) {
        // given
        final String slotId = "slotId-" + UUID.randomUUID();
        final String day = "day-" + UUID.randomUUID();
        final JsonObject testSession = new JsonObject()
            .put("slotId", slotId)
            .put("day", day);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, role))
            .sendJsonObject(testSession, assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_session_with_all_fields(TestContext context) {
        // given
        final String testTrackId = insertToDatabase(TRACKS, DocumentConverter.getDocument(getTestTrack()));
        final Session testSession = getTestSession(testTrackId);
        final JsonObject testRoomAsJson = DocumentConverter.toJson(testSession);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .sendJsonObject(testRoomAsJson, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());
                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(1, data.fieldNames().size());
                context.assertNotNull(data.getString("_id"));

                final Document inserted = getFromDatabase(SESSIONS, data.getString("_id"));
                assertCreatedSession(testSession, inserted, context);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertCreatedSession(Session testSession, Document inserted, TestContext context) {
        context.assertNotNull(inserted.getObjectId("_id"));
        context.assertEquals(testSession.getSlotId(), inserted.getString("slotId"));
        context.assertEquals(testSession.getDay(), inserted.getString("day"));
        context.assertEquals(testSession.getStartTime(), inserted.getString("startTime"));
        context.assertEquals(testSession.getEndTime(), inserted.getString("endTime"));
        context.assertEquals(testSession.getRoomId(), inserted.getString("roomId"));
        context.assertEquals(testSession.getTrackId(), inserted.getString("trackId"));
        context.assertNotNull(inserted.getLong(CREATED_DATE));
        context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
        context.assertEquals(10, inserted.keySet().size());

        final Document track = getFromDatabase(Collections.TRACKS, testSession.getTrackId());
        // null only in tests, real case should have _id
        context.assertNotNull(track.getObjectId("_id"));
        context.assertNotNull(track.getString("name"));
        context.assertNotNull(track.getString("description"));
        context.assertNotNull(track.getLong(CREATED_DATE));
        context.assertNotNull(track.getLong(LAST_UPDATE));
        context.assertNotNull(track.getString(LAST_ACTION_BY));
        context.assertEquals(6, track.keySet().size());
    }


    @Test
    public void should_fail_list_sessions_when_no_auth_token_is_present(TestContext context) {
        // when
        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
    }

    @Test
    public void should_list_0_sessions(TestContext context) {
        // given: clean collection

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(0, data.getInteger("total"));
                context.assertEquals(2, data.fieldNames().size());
                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(0, items.size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_session(TestContext context) {
        // given
        final String testTrackId = insertToDatabase(TRACKS, DocumentConverter.getDocument(getTestTrack()));
        final Session testSession = getTestSession(testTrackId);
        final String id = insertToDatabase(SESSIONS, DocumentConverter.getDocument(testSession));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject data = responseBody.getJsonObject("data");
                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(1, items.size());
                context.assertEquals(1, data.getInteger("total"));
                context.assertEquals(2, data.fieldNames().size());

                assertRetrievedSession(testSession, items.getJsonObject(0), context);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_sessions(TestContext context) {
        // given
        int testSize = 12;
        final String testTrackId = insertToDatabase(TRACKS, DocumentConverter.getDocument(getTestTrack()));

        final Map<String, Session> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final Session testSession = getTestSession(testTrackId);
                return Pair.of(insertToDatabase(SESSIONS, DocumentConverter.getDocument(testSession)), testSession);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (Session) pair.getSecond()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject data = responseBody.getJsonObject("data");
                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(testSize, items.size());
                context.assertEquals(12, data.getInteger("total"));

                items.forEach(it -> {
                    assertRetrievedSession(testData.get(((JsonObject) it).getString("_id")), (JsonObject) it, context);
                });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_session_by_id(TestContext context) {
        // given
        final String testTrackId = insertToDatabase(TRACKS, DocumentConverter.getDocument(getTestTrack()));
        final Session testSession = getTestSession(testTrackId);
        final String id = insertToDatabase(SESSIONS, DocumentConverter.getDocument(testSession));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
                context.assertEquals(2, responseBody.fieldNames().size());

                final JsonObject json = responseBody.getJsonObject("data");
                assertRetrievedSession(testSession, json, context);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertRetrievedSession(Session testSession, JsonObject json, TestContext context) {
        context.assertNotNull(json.getString("_id"));
        context.assertEquals(testSession.getSlotId(), json.getString("slotId"));
        context.assertEquals(testSession.getDay(), json.getString("day"));
        context.assertEquals(testSession.getStartTime(), json.getString("startTime"));
        context.assertEquals(testSession.getEndTime(), json.getString("endTime"));
        context.assertEquals(testSession.getRoomId(), json.getString("roomId"));
        context.assertEquals(testSession.getTrackId(), json.getString("trackId"));
        context.assertNotNull(json.getLong(CREATED_DATE));
        context.assertEquals(json.getLong(CREATED_DATE), json.getLong(LAST_UPDATE));
        context.assertFalse(json.containsKey(LAST_ACTION_BY));
        context.assertEquals(9, json.fieldNames().size());

        final Document track = getFromDatabase(Collections.TRACKS, testSession.getTrackId());
        // null only in tests, real case should have _id
        context.assertNotNull(track.getObjectId("_id"));
        context.assertNotNull(track.getString("name"));
        context.assertNotNull(track.getString("description"));
        context.assertNotNull(track.getLong(CREATED_DATE));
        context.assertNotNull(track.getLong(LAST_UPDATE));
        context.assertNotNull(track.getString(LAST_ACTION_BY));
        context.assertEquals(6, track.keySet().size());
    }

    @Test
    public void should_fail_get_session_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_session_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + fakeId)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.not_found", responseBody.getString("error"));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_session_slotId(TestContext context) {
        // given
        final Session testSession = getTestSession();
        final String userId = insertToDatabase(SESSIONS, DocumentConverter.getDocument(testSession));

        final String newSlotIt = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject("{\"slotId\":\"" + newSlotIt + "\"}");

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + userId)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(TRUE, status);
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedSession = getFromDatabase(SESSIONS, userId);
                context.assertEquals(newSlotIt, updatedSession.getString("slotId"));
                context.assertEquals(toTimestamp(testSession.getCreatedDate()), updatedSession.getLong(CREATED_DATE));
                context.assertTrue(updatedSession.getLong(LAST_UPDATE) > updatedSession.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_session_track(TestContext context) {
        // given
        final Track testTrack = getTestTrack();
        final String originalTrackId = insertToDatabase(TRACKS, DocumentConverter.getDocument(getTestTrack()));
        final Session testSession = getTestSession(originalTrackId);
        final String sessionId = insertToDatabase(SESSIONS, DocumentConverter.getDocument(testSession));

        final String newTrackId = insertToDatabase(TRACKS, DocumentConverter.getDocument(testTrack));
        final JsonObject updateBody = new JsonObject()
            .put("trackId", newTrackId);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + sessionId)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(TRUE, status);
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedSession = getFromDatabase(SESSIONS, sessionId);
                final String updatedTrack = updatedSession.getString("trackId");
                context.assertEquals(newTrackId, updatedTrack);
                context.assertEquals(toTimestamp(testSession.getCreatedDate()), updatedSession.getLong(CREATED_DATE));
                context.assertTrue(updatedSession.getLong(LAST_UPDATE) > updatedSession.getLong(CREATED_DATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_session_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c4a3ea8aefd394cce055574";
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + fakeId)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 404, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.not_found", responseBody.getString("error"));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_session_when_id_format_is_not_valid(TestContext context) {
        // given
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put("/api/sessions/123")
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_session(TestContext context) {
        // given
        final Session testSession = getTestSession();
        final String id = insertToDatabase(SESSIONS, DocumentConverter.getDocument(testSession));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_when_id_format_is_not_valid(TestContext context) {
        // given
        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
            .bearerTokenAuthentication(getNewSession(SESSIONS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
                async.complete();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

}
