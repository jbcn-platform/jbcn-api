package org.jbcn.server.http;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import org.bson.Document;
import org.jbcn.server.model.*;
import org.jbcn.server.model.compliance.DataConsent;
import org.jbcn.server.model.search.SearchFilter;
import org.jbcn.server.utils.DocumentConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;
import util.asserters.PersistedApiEntity;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.jbcn.server.model.PaperState.PAPER_STATE_ACCEPTED;
import static org.jbcn.server.model.PaperState.PAPER_STATE_CANCELED;
import static org.jbcn.server.model.Speaker.calculateCode;
import static org.jbcn.server.model.TalkState.TALK_STATE_UNCONFIRMED;
import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static org.jbcn.server.persistence.Collections.PAPER;
import static org.jbcn.server.persistence.Collections.TALK;
import static util.TestDataGenerator.*;
import static util.VertxSetup.*;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.PersistedApiEntityAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class PaperHttpTest extends VertxTestSetup {

    private static final String PAPERS_TEST_USER = "papers_test_user";
    private static final String FAKE_PAPER_ID = "5c24e4b0554b4947c8163049";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }

    @Test
    public void should_not_authorize_getting_paper_for_sponsors(TestContext context) {
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.SPONSOR))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_with_all_data_consent_successfully(TestContext context) {
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasId()
                    .hasEntity(instance -> {
                        final JsonArray senders = instance.getObjectArray("senders");
                        context.assertEquals(senders.size(), 1);
                        final JsonObject dataConsent = ((JsonObject) senders.getValue(0)).getJsonObject("dataConsent");
                        context.assertTrue(dataConsent.getBoolean("identification"));
                        context.assertTrue(dataConsent.getBoolean("contact"));
                        context.assertTrue(dataConsent.getBoolean("financial"));
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_with_linkedIn_info_in_sender_data(TestContext context) {
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasId()
                    .hasEntity(instance -> {
                        final JsonArray senders = instance.getObjectArray("senders");
                        context.assertEquals(senders.size(), 1);
                        final String linkedInUrl = ((JsonObject) senders.getValue(0)).getString("linkedin");
                        context.assertEquals("https://www.linkedin.com/in/something-here", linkedInUrl);
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_with_only_identification_data_consent_successfully(TestContext context) {
        final Paper testPaper = getTestPaper();
        testPaper.getSenders().get(0).setDataConsent(DataConsent.builder().identification(true).build());

        Async async = context.async();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasId()
                    .hasEntity(entity -> {
                        final JsonArray senders = entity.getObjectArray("senders");
                        context.assertEquals(senders.size(), 1);
                        final JsonObject jsonDataConsent = ((JsonObject) senders.getValue(0)).getJsonObject("dataConsent");
                        context.assertTrue(jsonDataConsent.getBoolean("identification"));
                        context.assertFalse(jsonDataConsent.getBoolean("contact"));
                        context.assertFalse(jsonDataConsent.getBoolean("financial"));
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_by_id_without_voting_info(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        testPaper.setPreferenceDay("22/11/1984");
        testPaper.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 10.0)));
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        // when
        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasIdEqualsTo(paperId)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .doesNotContainKey("votes")
                    .doesNotContainKey("averageVote")
                    .doesNotContainKey("votesCount")
                    // -2 because test paper does not contain languages and feedback
                    .hasKeys(Paper.BASE_FIELD_NAMES.size() - 2);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_by_id_with_abstract_and_bio_formatted_as_html(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        testPaper.setPreferenceDay("22/11/1984");
        testPaper.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 10.0)));
        testPaper.setPaperAbstract("""
            Just some names:
            * Maedhros
            * Maglor
            * Celegorm
            * Caranthir
            * Curufin
            * Amrod
            * Amras""");
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        // when
        final Async async = context.async();
        webClient.get("/api/paper/" + paperId + "?format=html")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasIdEqualsTo(paperId)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .hasStringEqualsTo("abstract", "<p>Just some names:</p><ul><li>Maedhros</li><li>Maglor</li><li>Celegorm</li><li>Caranthir</li><li>Curufin</li><li>Amrod</li><li>Amras</li></ul>")
                    .doesNotContainKey("votes")
                    .doesNotContainKey("averageVote")
                    .doesNotContainKey("votesCount")
                    // -2 because test paper does not contain languages and feedback
                    .hasKeys(Paper.BASE_FIELD_NAMES.size() - 2);
                final String senderBio = responseBody.getJsonObject("data")
                    .getJsonObject("instance")
                    .getJsonArray("senders")
                    .getJsonObject(0)
                    .getString("biography");
                context.assertEquals("<p>Stop talking, brain thinking.</p><p>Hush. I am the Doctor, and you are the Daleks!</p><p>The way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.</p>",
                    senderBio);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_by_id_with_voting_info_when_user_is_admin(TestContext context) {
        should_get_paper_by_id_with_voting_info_when_user_has_role(context, Role.ADMIN);
    }

    @Test
    public void should_get_paper_by_id_with_voting_info_when_user_is_voter(TestContext context) {
        should_get_paper_by_id_with_voting_info_when_user_has_role(context, Role.VOTER);
    }

    public void should_get_paper_by_id_with_voting_info_when_user_has_role(TestContext context, Role role) {
        // given
        final Paper testPaper = getTestPaper();
        testPaper.setPreferenceDay("22/11/1984");
        testPaper.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 10.0)));
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        // when
        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, role))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasIdEqualsTo(paperId)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .containKey("votes")
                    .containKey("averageVote")
                    .containKey("votesCount")
                    .hasKeys(Paper.BASE_FIELD_NAMES.size() + 1);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_get_paper_with_votes_by_id(TestContext context) {
        // given: votes inserted as double with decimals
        final Paper testPaper = getTestPaper();
        final Date voteTime = new Date();
        testPaper.setVotes(Arrays.asList(
            new PaperVote("voter1", "voter1", 0d, voteTime),
            new PaperVote("voter3", "voter3", 0d, voteTime)
        ));
        testPaper.setAverageVote(Math.round(((8.5 + 6.75) / 3) * 100) / 100);
        final Document paperBson = testPaper.toBsonDocument();
        ((Document) ((List) paperBson.get("votes")).get(0)).put("vote", 8.5);
        ((Document) ((List) paperBson.get("votes")).get(1)).put("vote", 6.75);

        final String paperId = insertToDatabase(PAPER, paperBson);
        // when
        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .hasDoubleEqualsTo("averageVote", 5.0d);

                final JsonObject paper = responseBody.getJsonObject("data")
                    .getJsonObject("instance");

                final JsonArray votes = paper.getJsonArray("votes");
                final JsonObject voter1 = (JsonObject) votes.stream()
                    .filter(i -> ((JsonObject) i).getString("username").equals("voter1"))
                    .findFirst()
                    .get();
                context.assertEquals("voter1", voter1.getString("userId"));
                context.assertEquals(8.5d, voter1.getDouble("vote"));

                final JsonObject voter3 = (JsonObject) votes.stream()
                    .filter(i -> ((JsonObject) i).getString("username").equals("voter3"))
                    .findFirst()
                    .get();
                context.assertEquals("voter3", voter3.getString("userId"));
                context.assertEquals(6.75d, voter3.getDouble("vote"));

            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_paper_when_id_does_not_exists(TestContext context) {
        // given
        final String paperId = FAKE_PAPER_ID;
        // when
        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.unknown_paper");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_paper_when_id_format_is_not_valid(TestContext context) {
        // given
        final String paperId = "123";
        // when
        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_paper_when_id_is_not_set_and_return_default_vertx_msg(TestContext context) {
        // when
        final Async async = context.async();
        webClient.get("/api/paper/")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .send(handler -> {
                if (handler.succeeded()) {
                    final HttpResponse<Buffer> result = handler.result();
                    context.assertEquals(result.statusCode(), 404);
                    context.assertTrue(result.headers().get(CONTENT_TYPE).contains("text/html; charset=utf-8"));
                } else {
                    context.fail(handler.cause());
                }
                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_skip_fail_in_delete_paper_when_id_format_is_not_valid(TestContext context) {
        // given
        final String paperId = "123";
        // when
        final Async async = context.async();
        webClient.delete("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_approve_paper_and_create_talk(TestContext context) {
        // given
        final User responsiblePerson = getTestUser();
        final JsonObject jsonEntity = JsonObject.mapFrom(responsiblePerson);
        final Paper testPaper = getTestPaper();
        testPaper.setSponsor(TRUE);
        testPaper.getSenders().get(0).setCompany("FancyCompany ltd.");
        testPaper.setLanguages(List.of("es", "eng", "ru"));
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final long timestamp = System.currentTimeMillis();
        // when
        final Async async = context.async();
        webClient.post("/api/paper/" + paperId + "/approve")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final String talkId = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyIdResponse()
                    .getId();

                final Document updatedPaper = getFromDatabase(PAPER, paperId);
                context.assertEquals(PAPER_STATE_ACCEPTED.getState(), updatedPaper.getString(Paper.STATE));
                final Long lastUpdate = updatedPaper.getLong(LAST_UPDATE);
                context.assertTrue(lastUpdate > timestamp);
                context.assertTrue(updatedPaper.getLong(CREATED_DATE) < lastUpdate);

                final Document createdTalk = getFromDatabase(TALK, talkId);
                context.assertEquals(18, createdTalk.keySet().size());
                context.assertEquals(talkId, createdTalk.getObjectId("_id").toString());
                context.assertEquals(paperId, createdTalk.getString("paperId"));
                context.assertEquals(TALK_STATE_UNCONFIRMED.getState(), createdTalk.getString("state"));
                context.assertEquals(FALSE, createdTalk.getBoolean("published"));
                context.assertEquals(TRUE, createdTalk.getBoolean("sponsor"));
                // talk is created after paper is updated
                context.assertTrue(createdTalk.getLong(LAST_UPDATE) > lastUpdate);
                context.assertEquals(createdTalk.getLong(CREATED_DATE), createdTalk.getLong(LAST_UPDATE));
                final List<String> languages = (List) createdTalk.get("languages");
                context.assertEquals(3, languages.size());
                context.assertTrue(languages.containsAll(List.of("es", "eng", "ru")), "languages contains es, eng, ru");

                final List<Document> createdSpeakers = (List<Document>) createdTalk.get("speakers");
                context.assertEquals(1, createdSpeakers.size());
                final Document createdSpeaker = createdSpeakers.get(0);
                context.assertEquals(17, createdSpeaker.keySet().size());
                context.assertEquals("FancyCompany ltd.", createdSpeaker.getString("company"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_approve_paper_when_no_body_is_sent(TestContext context) {
        // given
        final String paperId = insertToDatabase(PAPER, getTestPaper().toBsonDocument());
        // when
        final Async async = context.async();
        webClient.post("/api/paper/" + paperId + "/approve")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasBodyRequiredMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_approve_paper_when_empty_body_is_sent(TestContext context) {
        // given
        final String paperId = insertToDatabase(PAPER, getTestPaper().toBsonDocument());
        // when
        final Async async = context.async();
        webClient.post("/api/paper/" + paperId + "/approve")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendBuffer(
                Buffer.buffer(""),
                assertJsonResponse(async, context, 400, responseBody -> {
                    assertThat(responseBody)
                        .with(context)
                        .isError()
                        .hasBodyRequiredMessage();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_approve_paper_when_empty_json_body_is_sent(TestContext context) {
        // given
        final String paperId = insertToDatabase(PAPER, getTestPaper().toBsonDocument());
        // when
        final Async async = context.async();
        webClient.post("/api/paper/" + paperId + "/approve")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendBuffer(
                Buffer.buffer("{}"),
                assertJsonResponse(async, context, 400, responseBody -> {
                    assertThat(responseBody)
                        .with(context)
                        .isError()
                        .hasBodyRequiredMessage();
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_approve_paper_when_id_format_is_not_valid(TestContext context) {
        // given
        final String paperId = "1234";
        final JsonObject jsonEntity = new JsonObject()
            .put("username", "something");

        // when
        final Async async = context.async();
        webClient.post("/api/paper/" + paperId + "/approve")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_approve_paper_when_id_does_not_exists(TestContext context) {
        // given
        final String paperId = FAKE_PAPER_ID;
        final JsonObject jsonEntity = new JsonObject()
            .put("username", "something");
        // when
        final Async async = context.async();
        webClient.post("/api/paper/" + paperId + "/approve")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.unknown_paper");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_title_when_sending_all_data(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setComments("this is a comment");
        testPaper.setPaperAbstract("abstract");
        testPaper.setTitle("original title");
        testPaper.getSenders().get(0).setBiography("bio");
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setCode(UUID.randomUUID().toString());
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String newTitle = "new title";
        final JsonObject jsonEntity = jsonFrom(testPaper, json -> {
            json.put("title", newTitle);
            json.remove(CREATED_DATE);
            json.remove(LAST_UPDATE);
        });

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasStringEqualsTo("title", newTitle)
                    .hasStringEqualsTo("type", testPaper.getType())
                    .hasStringEqualsTo("comments", testPaper.getComments())
                    .hasStringEqualsTo("abstract", testPaper.getPaperAbstract())
                    .hasStringEqualsTo("edition", testPaper.getEdition())
                    .hasStringEqualsTo("state", testPaper.getState().getState())
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .doesNotContainKey("preferenceDay")
                    .has(paper -> assertUpdatedSender(context, testSender, paper))
                    .hasKeys(15);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_title_when_sending_only_title(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setComments("this is a comment");
        testPaper.setPaperAbstract("abstract");
        testPaper.setTitle("original title");
        testPaper.getSenders().get(0).setBiography("bio");
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setCode(UUID.randomUUID().toString());
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String newTitle = "new title-" + UUID.randomUUID();
        final JsonObject newTestPaper = new JsonObject()
            .put("title", newTitle);

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestPaper, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasStringEqualsTo("title", newTitle)
                    .hasStringEqualsTo("type", testPaper.getType())
                    .hasStringEqualsTo("comments", testPaper.getComments())
                    .hasStringEqualsTo("abstract", testPaper.getPaperAbstract())
                    .hasStringEqualsTo("edition", testPaper.getEdition())
                    .hasStringEqualsTo("state", testPaper.getState().getState())
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .doesNotContainKey("preferenceDay")
                    .has(paper -> assertUpdatedSender(context, testSender, paper))
                    .hasKeys(15);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_state_when_sending_only_state(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setState(PAPER_STATE_ACCEPTED);
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String newState = PAPER_STATE_CANCELED.getState();
        final JsonObject newTestPaper = new JsonObject()
            .put("state", newState);

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTestPaper, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .hasStringEqualsTo("type", testPaper.getType())
                    .hasStringEqualsTo("comments", testPaper.getComments())
                    .hasStringEqualsTo("abstract", testPaper.getPaperAbstract())
                    .hasStringEqualsTo("edition", testPaper.getEdition())
                    .hasStringEqualsTo("state", newState)
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .doesNotContainKey("preferenceDay")
                    .has(paper -> assertUpdatedSender(context, testPaper.getSenders().get(0), paper))
                    .hasKeys(15);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_preferenceDay_when_sending_only_preferenceDay(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setState(PAPER_STATE_ACCEPTED);
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String newDay = "2020/03/04";
        final JsonObject newTestPaper = new JsonObject()
            .put("preferenceDay", newDay);

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestPaper, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .hasStringEqualsTo("type", testPaper.getType())
                    .hasStringEqualsTo("comments", testPaper.getComments())
                    .hasStringEqualsTo("abstract", testPaper.getPaperAbstract())
                    .hasStringEqualsTo("edition", testPaper.getEdition())
                    .hasStringEqualsTo("preferenceDay", newDay)
                    .hasStringEqualsTo("state", testPaper.getState().getState())
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .has(paper -> assertUpdatedSender(context, testPaper.getSenders().get(0), paper))
                    .hasKeys(16);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // TODO assert tags
    @Test
    public void should_update_paper_comments_when_sending_only_comments(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setComments("this is a comment");
        testPaper.setPaperAbstract("abstract");
        testPaper.setTitle("original title");
        testPaper.getSenders().get(0).setBiography("bio");
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setCode(UUID.randomUUID().toString());
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String newComments = "new comment-" + UUID.randomUUID();
        final JsonObject newTestPaper = new JsonObject()
            .put("comments", newComments);

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestPaper, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .hasStringEqualsTo("type", testPaper.getType())
                    .hasStringEqualsTo("comments", newComments)
                    .hasStringEqualsTo("abstract", testPaper.getPaperAbstract())
                    .hasStringEqualsTo("edition", testPaper.getEdition())
                    .hasStringEqualsTo("preferenceDay", testPaper.getPreferenceDay())
                    .hasStringEqualsTo("state", testPaper.getState().getState())
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .has(paper -> assertUpdatedSender(context, testPaper.getSenders().get(0), paper))
                    .hasKeys(15);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertUpdatedSender(TestContext context, Sender testSender, PersistedApiEntity paper) {
        final List<Document> senders = paper.getObjectCollection("senders");
        context.assertEquals(1, senders.size());
        final Document actual = senders.get(0);
        context.assertEquals(16, actual.keySet().size());
        context.assertEquals(testSender.getFullName(), actual.getString("fullName"));
        context.assertEquals(testSender.getJobTitle(), actual.getString("jobTitle"));
        context.assertNotNull(actual.getString("code"));
        context.assertEquals(testSender.getEmail(), actual.getString("email"));
        context.assertEquals(testSender.getBiography(), actual.getString("biography"));
        context.assertEquals(testSender.getPicture(), actual.getString("picture"));
        context.assertEquals(testSender.getWeb(), actual.getString("web"));
        context.assertEquals(testSender.getTwitter(), actual.getString("twitter"));
        context.assertEquals(testSender.getLinkedin(), actual.getString("linkedin"));
        context.assertEquals(FALSE, actual.getBoolean("travelCost"));
        context.assertEquals(FALSE, actual.getBoolean("attendeesParty"));
        context.assertEquals(FALSE, actual.getBoolean("speakersParty"));
        context.assertEquals(testSender.getTshirtSize(), actual.getString("tshirtSize"));
        context.assertEquals(testSender.getAllergies(), actual.getString("allergies"));
        context.assertEquals(FALSE, actual.getBoolean("starred"));
        final Document dataConsent = (Document) actual.get("dataConsent");
        context.assertEquals(3, dataConsent.keySet().size());
        context.assertEquals(TRUE, dataConsent.getBoolean("identification"));
        context.assertEquals(TRUE, dataConsent.getBoolean("contact"));
        context.assertEquals(TRUE, dataConsent.getBoolean("financial"));
    }

    @Test
    public void should_not_update_paper_when_data_does_not_match_any_paper_property_name(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("random_stuff", UUID.randomUUID().toString());

        // then
        should_not_update_paper(context, emptyJson);
    }

    @Test
    public void should_not_update_paper_tags_when_empty_list_is_sent(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("tags", new ArrayList<String>());

        // then
        should_not_update_paper(context, emptyJson);
    }

    @Test
    public void should_not_update_paper_tags_when_empty_values_are_sent(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("tags", Arrays.asList("", null, "  ", "\n", "\t"));

        // then
        should_not_update_paper(context, emptyJson);
    }

    @Test
    public void should_update_paper_tags_with_only_not_empty_values(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.getSenders().get(0).setBiography("bio");
        final Sender testSender = testPaper.getSenders().get(0);
        testSender.setCode(UUID.randomUUID().toString());
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String newTag = "new Tag-" + UUID.randomUUID();
        final JsonObject newTestPaper = new JsonObject()
            .put("tags", List.of("  ", newTag, "\t"));

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestPaper, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasStringEqualsTo("title", testPaper.getTitle())
                    .hasStringEqualsTo("type", testPaper.getType())
                    .hasStringEqualsTo("comments", testPaper.getComments())
                    .hasStringEqualsTo("abstract", testPaper.getPaperAbstract())
                    .hasStringEqualsTo("edition", testPaper.getEdition())
                    .hasStringEqualsTo("preferenceDay", testPaper.getPreferenceDay())
                    .hasStringEqualsTo("state", testPaper.getState().getState())
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .hasCollectionEqualsTo("tags", List.of(newTag))
                    .has(paper -> assertUpdatedSender(context, testPaper.getSenders().get(0), paper))
                    .hasKeys(15);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_update_paper_sender_when_empty_list_is_sent(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("senders", new ArrayList<String>());

        // then
        should_not_update_paper(context, emptyJson);
    }

    @Test
    public void should_not_update_paper_sender_when_senders_without_fullname_are_sent(TestContext context) {
        // given
        final JsonObject emptyJson = new JsonObject()
            .put("senders", Arrays.asList(
                new JsonObject().put("jobTitle", "something"),
                new JsonObject().put("jobTitle", "else")
            ));

        // then
        should_not_update_paper(context, emptyJson);
    }

    public void should_not_update_paper(TestContext context, JsonObject body) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // then
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(body, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                // traceable data is not modified
                final Document insertedPaper = getFromDatabase(PAPER, paperId);
                context.assertEquals(now.getTime(), insertedPaper.getLong(CREATED_DATE));
                context.assertEquals(now.getTime(), insertedPaper.getLong(LAST_UPDATE));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_sender_when_sender_misses_mandatory_fields(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        final JsonObject body = new JsonObject()
            .put("senders", Arrays.asList(
                new JsonObject()
                    .put("fullName", "speaker 1")
                    .put("jobTitle", "something"),
                new JsonObject()
                    .put("fullName", "speaker 2")
                    .put("jobTitle", "else")
            ));

        // then
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(body, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.sender.biography_mandatory");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // NOTE: id is the first validation
    @Test
    public void should_fail_update_paper_when_id_format_is_not_valid(TestContext context) {
        // given
        final String paperId = "123";
        final JsonObject paper = new JsonObject()
            .put("title", "amazing paper title");
        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(paper, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_when_state_is_not_valid(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // when
        final JsonObject newTestTalk = new JsonObject()
            .put("state", "1234567890");
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.invalid_state");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_when_type_is_not_valid(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // when
        final JsonObject newTestTalk = new JsonObject()
            .put("type", "1234567890");
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.invalid_type");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_level_is_not_valid(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // when
        final JsonObject newTestTalk = new JsonObject()
            .put("level", "1234567890");
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(newTestTalk, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.invalid_level");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_when_no_body_is_sent(TestContext context) {
        assert_fail_update_paper_when_empty_body(context, null);
    }

    @Test
    public void should_fail_update_paper_when_empty_body_is_sent(TestContext context) {
        assert_fail_update_paper_when_empty_body(context, "");
    }

    @Test
    public void should_fail_update_paper_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_update_paper_when_empty_body(context, "{}");
    }

    private void assert_fail_update_paper_when_empty_body(TestContext context, String newTalk) {
        final Paper testPaper = getTestPaper();
        final String paper = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));
        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paper)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendBuffer(newTalk == null ? null : Buffer.buffer(newTalk), assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasBodyRequiredMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private JsonObject jsonFrom(Paper testPaper, Consumer<JsonObject> updated) {
        JsonObject json = JsonObject.mapFrom(testPaper);
        if (updated != null)
            updated.accept(json);
        return json;
    }


    /**
     * Default values if not set:
     * - page = 0
     * - size = 50
     * - sort (column) = title
     * - asc = true
     * - conditions = null
     */
    @Test
    public void should_search_paper_with_minimum_options(TestContext context) {
        // given
        final Paper testPaper1 = getTestPaper();
        final String paperId1 = insertToDatabase(PAPER, testPaper1.toBsonDocument());
        final Paper testPaper2 = getTestPaper();
        final String paperId2 = insertToDatabase(PAPER, testPaper2.toBsonDocument());
        final Paper testPaper3 = getTestPaper();
        final String paperId3 = insertToDatabase(PAPER, testPaper3.toBsonDocument());

        final Map searchParams = new HashMap<>();
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);
        // when
        final Async async = context.async();
        webClient.post("/api/paper/search")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, TRUE);

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(items.size(), 3);

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId1));
                context.assertTrue(retriedIds.contains(paperId3));
                context.assertTrue(retriedIds.contains(paperId2));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_paper_with_minimum_options_and_do_not_return_voting_info_is_user_is_helper(TestContext context) {
        // given
        final Paper testPaper1 = getTestPaper();
        testPaper1.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 1.0)));
        final String paperId1 = insertToDatabase(PAPER, testPaper1.toBsonDocument());
        final Paper testPaper2 = getTestPaper();
        testPaper2.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 2.0)));
        final String paperId2 = insertToDatabase(PAPER, testPaper2.toBsonDocument());
        final Paper testPaper3 = getTestPaper();
        testPaper3.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 3.0)));
        final String paperId3 = insertToDatabase(PAPER, testPaper3.toBsonDocument());

        final Map searchParams = new HashMap<>();
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);
        // when
        final Async async = context.async();
        webClient.post("/api/paper/search")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, TRUE);

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(items.size(), 3);

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId1));
                context.assertTrue(retriedIds.contains(paperId3));
                context.assertTrue(retriedIds.contains(paperId2));

                for (Map<String, Object> paper : (List<Map<String, Object>>) items.getList()) {
                    context.assertFalse(paper.containsKey("votes"));
                    context.assertFalse(paper.containsKey("votesCount"));
                    context.assertFalse(paper.containsKey("averageVote"));
                }
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_paper_with_minimum_options_and_do_return_voting_info_is_user_is_voter(TestContext context) {
        // given
        final Paper testPaper1 = getTestPaper();
        testPaper1.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 1.0)));
        final String paperId1 = insertToDatabase(PAPER, testPaper1.toBsonDocument());
        final Paper testPaper2 = getTestPaper();
        testPaper2.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 2.0)));
        final String paperId2 = insertToDatabase(PAPER, testPaper2.toBsonDocument());
        final Paper testPaper3 = getTestPaper();
        testPaper3.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 3.0)));
        final String paperId3 = insertToDatabase(PAPER, testPaper3.toBsonDocument());

        final Map searchParams = new HashMap<>();
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);
        // when
        final Async async = context.async();
        webClient.post("/api/paper/search")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, TRUE);

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(items.size(), 3);

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId1));
                context.assertTrue(retriedIds.contains(paperId3));
                context.assertTrue(retriedIds.contains(paperId2));

                for (Map<String, Object> paper : (List<Map<String, Object>>) items.getList()) {
                    context.assertTrue(paper.containsKey("votes"));
                    context.assertTrue(paper.containsKey("votesCount"));
                    context.assertTrue(paper.containsKey("averageVote"));
                }
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_paper_with_minimum_options_and_do_return_voting_info_is_user_is_admin(TestContext context) {
        // given
        final Paper testPaper1 = getTestPaper();
        testPaper1.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 1.0)));
        final String paperId1 = insertToDatabase(PAPER, testPaper1.toBsonDocument());
        final Paper testPaper2 = getTestPaper();
        testPaper2.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 2.0)));
        final String paperId2 = insertToDatabase(PAPER, testPaper2.toBsonDocument());
        final Paper testPaper3 = getTestPaper();
        testPaper3.setVotes(List.of(PaperVote.of(PAPERS_TEST_USER, 3.0)));
        final String paperId3 = insertToDatabase(PAPER, testPaper3.toBsonDocument());

        final Map searchParams = new HashMap<>();
        final JsonObject jsonEntity = JsonObject.mapFrom(searchParams);
        // when
        final Async async = context.async();
        webClient.post("/api/paper/search")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final Boolean status = responseBody.getBoolean("status");
                context.assertEquals(status, TRUE);

                final JsonObject data = responseBody.getJsonObject("data");
                context.assertEquals(data.getInteger("total"), 3);

                final JsonArray items = data.getJsonArray("items");
                context.assertEquals(items.size(), 3);

                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId1));
                context.assertTrue(retriedIds.contains(paperId3));
                context.assertTrue(retriedIds.contains(paperId2));

                for (Map<String, Object> paper : (List<Map<String, Object>>) items.getList()) {
                    context.assertTrue(paper.containsKey("votes"));
                    context.assertTrue(paper.containsKey("votesCount"));
                    context.assertTrue(paper.containsKey("averageVote"));
                }
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_search_paper_by_exact_title_using_term(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Map searchParams = Map.of("conditions",
            Map.of("all", List.of(Map.of(
                "name", "term",
                "value", testPaper.getTitle()))
            ));
        // when
        should_search_paper(context, searchParams, (json) -> {
            assertThat(json)
                .with(context)
                .isSuccessful()
                .isCollectionResponse();

            hasSize(json, context, 1);
            final JsonArray items = json.getJsonObject("data")
                .getJsonArray("items");
            final List<String> retriedIds = items.stream()
                .map(v -> ((JsonObject) v).getString("_id"))
                .collect(Collectors.toList());
            context.assertTrue(retriedIds.contains(paperId));
        });
    }

    @Test
    public void should_search_paper_by_contains_title_using_term(TestContext context) {
        // given
        final Map searchParams = Map.of("conditions",
            Map.of("all", List.of(Map.of(
                "name", "term",
                "value", "Test"))
            ));
        // when
        should_search_paper(context, searchParams, (json) -> {
            final Boolean status = json.getBoolean("status");
            context.assertEquals(status, TRUE);

            JsonObject data = json.getJsonObject("data");
            context.assertEquals(data.getInteger("total"), 3);

            final JsonArray items = data.getJsonArray("items");
            context.assertEquals(items.size(), 3);
        });
    }

    @Test
    public void should_search_paper_by_exact_tag(TestContext context) {
        // given
        final String tag = "tag-" + UUID.randomUUID();
        final Paper testPaper = getTestPaper();
        testPaper.setTags(List.of("tag_1", tag, "tag_2"));
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Map searchParams = Map.of("conditions",
            Map.of("all", List.of(Map.of(
                "name", "tags",
                "value", tag))
            ));
        // when
        should_search_paper(context, searchParams, (json) -> {
            assertThat(json)
                .with(context)
                .isSuccessful()
                .isCollectionResponse();

            hasSize(json, context, 1);

            final JsonArray items = json.getJsonObject("data")
                .getJsonArray("items");
            final List<String> retriedIds = items.stream()
                .map(v -> ((JsonObject) v).getString("_id"))
                .collect(Collectors.toList());
            context.assertTrue(retriedIds.contains(paperId));
        });
    }

    @Test
    public void should_search_paper_by_contains_tag(TestContext context) {
        // given
        final String tag = "tag-" + UUID.randomUUID();
        final Paper testPaper = getTestPaper();
        testPaper.setTags(List.of("tag_1", tag, "tag_2"));
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Map searchParams = Map.of("conditions",
            Map.of("all", List.of(Map.of(
                "op", SearchFilter.CONTAINS,
                "name", "tags",
                "value", tag.substring(5, 25)))
            ));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 1);

                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId));
            });
    }

    @Test
    public void should_search_paper_by_tag_starting_by(TestContext context) {
        // given
        final Paper paperWithoutTags = getTestPaper();
        paperWithoutTags.setTags(List.of());
        final String paperId = insertToDatabase(PAPER, paperWithoutTags.toBsonDocument());
        final Map searchParams = Map.of("conditions",
            Map.of("all", List.of(Map.of(
                "op", SearchFilter.CONTAINS,
                "name", "tags",
                "value", "tag"))
            ));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 3);

                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(!retriedIds.contains(paperId));
            });
    }

    @Test
    public void should_search_paper_by_fuzzy_sender_name(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Map searchParams = Map.of("conditions",
            Map.of("all", List.of(Map.of(
                "name", "term",
                "value", testPaper.getSenders().get(0).getFullName()))
            ));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 1);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId));
            });
    }

    @Test
    public void should_search_paper_by_exact_sender_name(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Map searchParams = new HashMap<>();
        searchParams.put("conditions",
            Map.of("all",
                List.of(Map.of("name", "sender.fullName",
                    "value", testPaper.getSenders().get(0).getFullName()))
            ));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 1);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId));
            });
    }

    @Test
    public void should_search_paper_by_voter_and_vote_value(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        final String voterName = "voter-00";
        double vote = 9.666d;
        testPaper.setVotes(Arrays.asList(
            PaperVote.of(voterName, vote),
            PaperVote.of("another-user", 3.0d)
        ));
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final String paperId2 = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Map searchParams = new HashMap<>();
        final Paper testPaper3 = getTestPaper();
        testPaper3.setVotes(Arrays.asList(PaperVote.of(voterName, 3.0d)));
        insertToDatabase(PAPER, testPaper3.toBsonDocument());
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "vote",
                    "value", PaperVote.of(voterName, vote))
            )));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 2);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                context.assertTrue(retriedIds.contains(paperId));
                context.assertTrue(retriedIds.contains(paperId2));
            });
    }

    @Test
    public void should_search_paper_by_notPublishedSpeakers(TestContext context) {
        // given
        // 3 papers with single speaker who has an approved talk
        final Talk talk = getTestTalk();
        talk.setPublished(false);
        insertToDatabase(TALK, DocumentConverter.getDocument(talk));
        final Speaker approvedSpeaker = talk.getSpeakers().get(0);

        String paperId1 = insertApprovedPaper(approvedSpeaker);
        String paperId2 = insertApprovedPaper(approvedSpeaker);
        String paperId3 = insertApprovedPaper(approvedSpeaker);

        final Map searchParams = new HashMap<>();
        searchParams.put("edition", 2019);
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "not-published-speakers",
                    "value", "this is ignored")
            ))
        );
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();
                // then the created paper is not returned
                hasSize(json, context, 3);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                // does NOT contain
                context.assertFalse(retriedIds.contains(paperId1));
                context.assertFalse(retriedIds.contains(paperId2));
                context.assertFalse(retriedIds.contains(paperId3));
            });
    }

    private String insertApprovedPaper(Speaker approvedSpeaker) {
        // Copy name, email & code to avoid flake depending on whether 'recalculateAll' runs during tests.
        final Paper testPaper = getTestPaper();
        final Sender sender = testPaper.getSenders().get(0);
        sender.setFullName(approvedSpeaker.getFullName());
        sender.setEmail(approvedSpeaker.getEmail());
        sender.setCode(approvedSpeaker.getCode());
        return insertToDatabase(PAPER, testPaper.toBsonDocument());
    }

    @Test
    public void should_search_paper_by_notPublishedSpeakers_and_return_if_one_speaker_is_not_accepted(TestContext context) {
        // given
        // removes speakers with accepted talks
        final Talk talk = getTestTalk();
        talk.setPublished(false);
        insertToDatabase(TALK, DocumentConverter.getDocument(talk));
        final Speaker approvedSpeaker = talk.getSpeakers().get(0);

        String paperId1 = insertPaperWithOneApprovedSpeaker(approvedSpeaker);
        String paperId2 = insertPaperWithOneApprovedSpeaker(approvedSpeaker);

        final Map searchParams = new HashMap<>();
        searchParams.put("edition", 2019);
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "not-published-speakers",
                    "value", "this is ignored")
            ))
        );
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();
                // then the created paper is returned
                hasSize(json, context, 5);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");
                final List<String> retriedIds = items.stream()
                    .map(v -> ((JsonObject) v).getString("_id"))
                    .collect(Collectors.toList());
                // contains
                context.assertTrue(retriedIds.contains(paperId1));
                context.assertTrue(retriedIds.contains(paperId2));
            });
    }

    private String insertPaperWithOneApprovedSpeaker(Speaker approvedSpeaker) {
        // Copy name, email & code to avoid flake depending on whether 'recalculateAll' runs during tests.
        final Paper testPaper = getTestPaper();
        final Sender approvedSender = testPaper.getSenders().get(0);
        approvedSender.setFullName(approvedSpeaker.getFullName());
        approvedSender.setEmail(approvedSpeaker.getEmail());
        approvedSender.setCode(approvedSpeaker.getCode());

        final Sender notApprovedSpeaker = getTestSender();
        testPaper.setSenders(List.of(approvedSender, notApprovedSpeaker));
        return insertToDatabase(PAPER, testPaper.toBsonDocument());
    }

    @Test
    public void should_search_paper_by_sponsor(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        testPaper.setSponsor(TRUE);
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());
        final Paper testPaper2 = getTestPaper();
        testPaper.setSponsor(FALSE);
        final String paperId2 = insertToDatabase(PAPER, testPaper2.toBsonDocument());

        final Map searchParams = new HashMap<>();
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "sponsor",
                    "value", TRUE))
            ));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 1);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");

                final String id = items.getJsonObject(0).getString("_id");
                context.assertEquals(paperId, id);
            });
    }

    @Test
    public void should_search_paper_by_type(TestContext context) {
        // given
        final Paper testPaper = getTestPaper();
        testPaper.setType(ProposalType.WORKSHOP.getType());
        final List<String> ids = new ArrayList<>();
        ids.add(insertToDatabase(PAPER, testPaper.toBsonDocument()));
        final Paper testPaper2 = getTestPaper();
        testPaper2.setType(ProposalType.WORKSHOP.getType());
        ids.add(insertToDatabase(PAPER, testPaper2.toBsonDocument()));

        final Map searchParams = new HashMap<>();
        searchParams.put("conditions",
            Map.of("all", List.of(
                    Map.of("name", "type",
                        "value", ProposalType.WORKSHOP.getType())
                )
            ));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 2);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");

                final String id0 = items.getJsonObject(0).getString("_id");
                context.assertTrue(ids.contains(id0));
                final String id1 = items.getJsonObject(1).getString("_id");
                context.assertTrue(ids.contains(id1));
            });
    }

    @Test
    public void should_search_paper_by_exact_sender_company(TestContext context) {
        // given
        final String company = "jBCN corp " + UUID.randomUUID();
        final List<String> ids = new ArrayList<>();

        final Paper testPaper = getTestPaper();
        testPaper.getSenders().get(0).setCompany(company);
        ids.add(insertToDatabase(PAPER, testPaper.toBsonDocument()));
        final Paper testPaper2 = getTestPaper();
        testPaper2.getSenders().get(0).setCompany(company);

        ids.add(insertToDatabase(PAPER, testPaper2.toBsonDocument()));

        final Map searchParams = new HashMap<>();
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "sender.company",
                    "value", company)
            )));
        // when
        should_search_paper(context, searchParams,
            (json) -> {
                assertThat(json)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse();

                hasSize(json, context, 2);
                final JsonArray items = json.getJsonObject("data")
                    .getJsonArray("items");

                final String id0 = items.getJsonObject(0).getString("_id");
                context.assertTrue(ids.contains(id0));
                final String id1 = items.getJsonObject(1).getString("_id");
                context.assertTrue(ids.contains(id1));
            });
    }

    @Test
    public void should_search_paper_by_sender_company_containing(TestContext context) {
        // given
        final String company = "jBCN corp " + UUID.randomUUID();
        final List<String> ids = new ArrayList<>();

        final Paper testPaper = getTestPaper();
        testPaper.getSenders().get(0).setCompany(company);
        ids.add(insertToDatabase(PAPER, testPaper.toBsonDocument()));
        final Paper testPaper2 = getTestPaper();
        testPaper2.getSenders().get(0).setCompany(company);

        ids.add(insertToDatabase(PAPER, testPaper2.toBsonDocument()));

        final Map searchParams = new HashMap<>();
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "sender.company",
                    "value", company.substring(2, company.length() - 2),
                    "op", SearchFilter.CONTAINS)
            )));
        // when
        should_search_paper(context, searchParams, (json) -> {
            assertThat(json)
                .with(context)
                .isSuccessful()
                .isCollectionResponse();

            hasSize(json, context, 2);
            final JsonArray items = json.getJsonObject("data")
                .getJsonArray("items");

            final String id0 = items.getJsonObject(0).getString("_id");
            context.assertTrue(ids.contains(id0));
            final String id1 = items.getJsonObject(1).getString("_id");
            context.assertTrue(ids.contains(id1));
        });
    }

    private void hasSize(JsonObject json, TestContext context, int size) {
        final JsonObject data = json.getJsonObject("data");
        context.assertEquals(size, data.getInteger("total"));
        final JsonArray items = data.getJsonArray("items");
        context.assertEquals(size, items.size());
        final Integer voters = data.getInteger("votersCount");
        context.assertEquals(3, voters);
    }

    @Test
    public void should_search_zero_papers_by_sender_company_that_does_not_exists(TestContext context) {
        // given
        final String company = "jBCN corp " + UUID.randomUUID();
        final Map searchParams = new HashMap<>();
        searchParams.put("conditions",
            Map.of("all", List.of(
                Map.of("name", "sender.company",
                    "value", company)
            )));
        // when
        should_search_paper(context, searchParams, (json) -> {
            assertThat(json)
                .with(context)
                .isSuccessful()
                .isCollectionResponse();
            // TODO avoid returning voters count, impacts admin-ui
            hasSize(json, context, 0);
        });
    }

    /**
     * NOTE: inserts 3 random Papers
     */
    public void should_search_paper(TestContext context,
                                    Map<String, Object> searchParams,
                                    Consumer<JsonObject> responseAsserter) {
        // given
        final Paper testPaper1 = getTestPaper();
        insertToDatabase(PAPER, testPaper1.toBsonDocument());
        final Paper testPaper2 = getTestPaper();
        insertToDatabase(PAPER, testPaper2.toBsonDocument());
        final Paper testPaper3 = getTestPaper();
        insertToDatabase(PAPER, testPaper3.toBsonDocument());

        // when
        final Async async = context.async();
        webClient.post("/api/paper/search")
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(JsonObject.mapFrom(searchParams), assertJsonResponse(async, context, 200, responseBody -> {
                responseAsserter.accept(responseBody);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_sender_company(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final String newCompany = "new Company " + UUID.randomUUID();
        final Sender sender = testPaper.getSenders().get(0);
        sender.setCompany(newCompany);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        final JsonObject newPaper = DocumentConverter.toJson(testPaper);

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .sendJsonObject(newPaper, assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.getBoolean("status"));
                context.assertEquals(1, responseBody.fieldNames().size());

                final Document updatedPaper = getFromDatabase(PAPER, paperId);
                final Document updatedSender = ((List<Document>) updatedPaper.get("senders")).get(0);
                context.assertEquals(newCompany, updatedSender.getString("company"));
                // updates code
                context.assertEquals(calculateCode(sender.getFullName(), sender.getEmail()), updatedSender.getString("code"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_senders_company(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        final Sender testSender1 = testPaper.getSenders().get(0);
        testSender1.setCompany("new Company " + UUID.randomUUID());
        final Sender testSender2 = getTestSender();
        testSender2.setCompany("new Company " + UUID.randomUUID());
        testPaper.setSenders(Arrays.asList(testSender1, testSender2));
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        final JsonObject newPaper = DocumentConverter.toJson(testPaper);

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .sendJsonObject(newPaper, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful();

                final Document updatedPaper = getFromDatabase(PAPER, paperId);

                final List<Document> updatedSenders = ((List<Document>) updatedPaper.get("senders"));
                context.assertEquals(2, updatedSenders.size());
                final Document updatedSender1 = updatedSenders.stream()
                    .filter(it -> it.getString("fullName").equals(testSender1.getFullName()))
                    .findFirst()
                    .get();
                context.assertEquals(testSender1.getCompany(), updatedSender1.getString("company"));
                // updates code
                context.assertEquals(calculateCode(testSender1.getFullName(), testSender1.getEmail()), updatedSender1.getString("code"));
                final Document updatedSender2 = updatedSenders.stream()
                    .filter(it -> it.getString("fullName").equals(testSender2.getFullName()))
                    .findFirst()
                    .get();
                context.assertEquals(testSender2.getCompany(), updatedSender2.getString("company"));
                // updates code
                context.assertEquals(calculateCode(testSender2.getFullName(), testSender2.getEmail()), updatedSender2.getString("code"));
            }));


        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_sponsor_to_true(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setSponsor(false);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId + "/sponsor/" + true)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                final Document updatedPaper = getFromDatabase(PAPER, paperId);
                context.assertEquals(TRUE, updatedPaper.getBoolean("sponsor"));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_languages(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setLanguages(List.of("es", "eng"));
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        final JsonObject updateRequest = new JsonObject()
            .put("languages", new JsonArray(List.of("jp")));

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .sendJsonObject(updateRequest, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasCollectionEqualsTo("languages", List.of("jp"))
                    .hasKeys(16);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_sponsor_to_false(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setSponsor(true);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId + "/sponsor/" + false)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(PAPER, paperId))
                    .with(context)
                    .hasUpdatedMetadata(PAPERS_TEST_USER)
                    .hasBooleanEqualsTo("sponsor", FALSE)
                    .hasKeys(15);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    // FIXME should return 400
    @Test
    public void should_fail_update_paper_sponsor_when_id_format_is_not_valid(TestContext context) {
        // given
        final String fakeId = UUID.randomUUID().toString();

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + fakeId + "/sponsor/" + true)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_sponsor_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c3b64d4aefd3931c0ec7b54";

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + fakeId + "/sponsor/" + false)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.unknown_paper");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_sponsor_when_value_is_not_boolean(TestContext context) {
        // given
        final Date now = new Date();
        final Paper testPaper = getTestPaper(now);
        testPaper.setSponsor(true);
        final String paperId = insertToDatabase(PAPER, DocumentConverter.getDocument(testPaper));

        // when
        final Async async = context.async();
        webClient.put("/api/paper/" + paperId + "/sponsor/" + 123)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.paper.sponsor.invalid_format");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_paper_sponsor_when_paper_is_not_set_and_return_default_vertx_msg(TestContext context) {
        final Async async = context.async();
        webClient.put("/api/paper/sponsor/" + true)
            .bearerTokenAuthentication(getNewSession(PAPERS_TEST_USER, Role.HELPER))
            .send(handler -> {
                if (handler.succeeded()) {
                    final HttpResponse<Buffer> result = handler.result();
                    context.assertEquals(result.statusCode(), 404);
                    context.assertTrue(result.headers().get(CONTENT_TYPE).contains("text/html; charset=utf-8"));

                } else {
                    context.fail(handler.cause());
                }
                async.complete();
            });
        async.awaitSuccess(TEST_TIMEOUT);
    }
}

