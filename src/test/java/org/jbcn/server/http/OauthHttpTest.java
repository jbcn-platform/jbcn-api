package org.jbcn.server.http;

import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.bson.Document;
import org.jbcn.server.persistence.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;

import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static util.VertxSetup.insertToDatabase;


@RunWith(VertxUnitRunner.class)
public class OauthHttpTest extends VertxTestSetup {

    private static final String RESOURCE = "/oauth/token";


    @Test
    public void should_fail_creating_token_when_grantType_is_missing(TestContext context) {
        // given
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?username=%s&password=%s&client_id=%s", RESOURCE, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.grant_type", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_grantType_is_empty(TestContext context) {
        // given
        final String grantType = "";
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.grant_type", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_grantType_is_duplicated(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&grant_type=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, grantType, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.not_single_value.grant_type", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_username_is_missing(TestContext context) {
        // given
        final String grantType = "password";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&&password=%s&client_id=%s", RESOURCE, grantType, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.username", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_username_is_empty(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.username", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_username_is_duplicated(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, username, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.not_single_value.username", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_password_is_missing(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&client_id=%s", RESOURCE, grantType, username, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.password", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_password_is_empty(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.password", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_password_is_duplicated(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&password=%s&client_id=%s", RESOURCE, grantType, username, password, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.not_single_value.password", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_clientId_is_missing(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "fake_password";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s", RESOURCE, grantType, username, password))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.client_id", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_clientId_is_empty(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, username, password, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.required_field.client_id", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_creating_token_when_clientId_is_duplicated(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "fake_username";
        final String password = "fake_password";
        final String clientId = "fake_client-id";

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&client_id=%s&client_id=%s", RESOURCE, grantType, username, password, clientId, clientId))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                context.assertEquals(FALSE, responseBody.getBoolean("status"));
                context.assertEquals("error.url_param.not_single_value.client_id", responseBody.getString("error"));
                context.assertEquals(2, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_token(TestContext context) {
        // given
        final String grantType = "password";
        final String username = "test-user";
        final String password = "test-password";
        final String clientId = "fake_client-id";
        initTestUser(username);

        // when-then
        final Async async = context.async();
        webClient.get(String.format("%s/?grant_type=%s&username=%s&password=%s&client_id=%s", RESOURCE, grantType, username, password, clientId))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                context.assertEquals(TRUE, responseBody.fieldNames().contains("access_token"));
                context.assertEquals("bearer", responseBody.getString("token_type"));
                context.assertEquals(10800, responseBody.getInteger("expires_in"));

                context.assertEquals(3, responseBody.fieldNames().size());
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void initTestUser(String username) {
        final Document user = new Document()
            .append("username", username)
            .append("password", "C36232D738F6B374D73028A7A8AFD14E80946D4DA3B82039F024A5E2E0F02793701123A4E19B448A10F4E8DA4D935FF574A09A1C0F1F496729F5DA1175D5F817")
            .append("salt", "E31A879094B0BE7A67E3210EDDCE638C59CA8EFA971EB04D393D9C315714E2E3")
            .append("roles", List.of("admin", "voter"));
        insertToDatabase(Collections.USER, user);
    }

}
