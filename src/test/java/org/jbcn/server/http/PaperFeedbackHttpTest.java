package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.jbcn.server.model.Paper;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.Traceable;
import org.jbcn.server.utils.TimeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;
import util.asserters.VertxAsyncResultAsserter;

import java.util.UUID;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.jbcn.server.model.Traceable.*;
import static org.jbcn.server.persistence.Collections.PAPER;
import static util.TestDataGenerator.getTestPaper;
import static util.VertxSetup.*;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.PersistedApiEntityAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class PaperFeedbackHttpTest extends VertxTestSetup {

    private static final String TEST_USER = "papers_feedback_test_user";
    private static final String FAKE_PAPER_ID = "5c24e4b0554b4947c8163049";

    @Before
    public void setup(TestContext context) {
        dropCollection(PAPER);
    }


    @Test
    public void should_fail_add_feedback_when_body_is_missing(TestContext context) {
        final String paperId = FAKE_PAPER_ID;

        final Async async = context.async();
        webClient.post(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasBodyRequiredMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_add_feedback_when_body_is_empty(TestContext context) {
        final String paperId = FAKE_PAPER_ID;

        final JsonObject feedback = new JsonObject();

        final Async async = context.async();
        webClient.post(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
            .sendJsonObject(feedback, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasBodyRequiredMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_add_feedback_when_text_is_empty(TestContext context) {
        final String paperId = FAKE_PAPER_ID;

        final JsonObject feedback = new JsonObject()
            .put("text", "");

        final Async async = context.async();
        webClient.post(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.VOTER))
            .sendJsonObject(feedback, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasMessage("error.body.required_field.feedback.text");
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_add_paper_feedback(TestContext context) {
        final Paper testPaper = getTestPaper();
        final String paperId = insertToDatabase(PAPER, testPaper.toBsonDocument());

        final String feedbackText = "This paper is awes0m3!! " + UUID.randomUUID();
        final JsonObject feedback = new JsonObject()
            .put("text", feedbackText);

        final Async async = context.async();
        webClient.post(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(feedback, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                final Document fromDatabase = getFromDatabase(PAPER, paperId);
                assertThat((Document) fromDatabase.get("feedback"))
                    .with(context)
                    .hasStringEqualsTo("text", feedbackText)
                    .hasStringEqualsTo(Traceable.LAST_ACTION_BY, TEST_USER)
                    .hasCreatedMetadata()
                    .hasKeys(4);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_paper_feedback(TestContext context) {
        final String paperId = insertToDatabase(PAPER, createTestPaperWithFeedback());

        final String newFeedbackText = "New feedback";
        final JsonObject feedbackUpdate = new JsonObject()
            .put("text", newFeedbackText);

        final String username = TEST_USER + "2";
        final Async async = context.async();
        webClient.put(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(username, Role.ADMIN))
            .sendJsonObject(feedbackUpdate, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                final Document fromDatabase = getFromDatabase(PAPER, paperId);
                assertThat((Document) fromDatabase.get("feedback"))
                    .with(context)
                    .hasStringEqualsTo("text", newFeedbackText)
                    .hasStringEqualsTo(Traceable.LAST_ACTION_BY, username)
                    .hasUpdatedMetadata(username)
                    .hasKeys(4);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_paper_feedback(TestContext context) {
        final String paperId = insertToDatabase(PAPER, createTestPaperWithFeedback());

        final Async async = context.async();
        webClient.delete(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(handler -> {
                VertxAsyncResultAsserter.assertThat(handler)
                    .with(context)
                    .isSuccessful()
                    .returnsStatus(204)
                    .doesNotContainHeader(CONTENT_TYPE.toString());

                final Document fromDatabase = getFromDatabase(PAPER, paperId);
                Assertions.assertThat(fromDatabase).isNull();

                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private Document createTestPaperWithFeedback() {
        final Document testPaper = getTestPaper().toBsonDocument();
        final long now = TimeUtils.currentTime();
        final Document bson = new Document()
            .append("text", "Original feedback")
            .append(CREATED_DATE, now)
            .append(LAST_UPDATE, now)
            .append(LAST_ACTION_BY, TEST_USER);
        return testPaper.append("feedback", bson);
    }

    @Test
    public void should_get_only_feedback(TestContext context) {
        final String paperId = insertToDatabase(PAPER, createTestPaperWithFeedback());

        final Async async = context.async();
        webClient.get(paperFeedbackUri(paperId))
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isEntityResponse()
                    .hasEntity(instance -> {
                        context.assertEquals(instance.getString("text"), "Original feedback");
                        context.assertNotNull(instance.getLong(CREATED_DATE));
                        context.assertNotNull(instance.getLong(LAST_UPDATE));
                        context.assertEquals(instance.getString(LAST_ACTION_BY), TEST_USER);
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private String paperFeedbackUri(String paperId) {
        return "/api/paper/" + paperId + "/feedback";
    }

    @Test
    public void should_get_paper_with_feedback(TestContext context) {
        final String paperId = insertToDatabase(PAPER, createTestPaperWithFeedback());

        final Async async = context.async();
        webClient.get("/api/paper/" + paperId)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isLegacyInstanceResponse()
                    .hasId()
                    .hasEntity(instance -> {
                        final JsonObject feedback = instance.getJsonObject("feedback");
                        context.assertEquals(feedback.getString("text"), "Original feedback");
                        context.assertNotNull(feedback.getLong(CREATED_DATE));
                        context.assertNotNull(feedback.getLong(LAST_UPDATE));
                        context.assertEquals(feedback.getString(LAST_ACTION_BY), TEST_USER);
                    });
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }
}
