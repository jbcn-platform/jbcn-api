package org.jbcn.server.http;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jbcn.server.model.Role;
import org.jbcn.server.model.sponsor.Sponsor;
import org.jbcn.server.utils.DocumentConverter;
import org.jbcn.server.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import util.VertxTestSetup;
import util.asserters.JsonApiEntity;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.jbcn.server.persistence.Collections.SPONSORS;
import static util.CustomAssertions.equalMap;
import static util.TestDataGenerator.getTestSponsor;
import static util.VertxSetup.*;
import static util.asserters.ApiModelAsserter.assertThat;
import static util.asserters.PersistedApiEntityAsserter.assertThat;

@RunWith(VertxUnitRunner.class)
public class SponsorsHttpTest extends VertxTestSetup {

    private static final String ENDPOINT = "/api/sponsors/";
    private static final String TEST_USER = "sponsor_test_user";

    @Before
    public void before(TestContext context) {
        dropCollection(SPONSORS);
    }


    @Test
    public void should_fail_create_sponsor_when_no_body_is_sent(TestContext context) {
        assert_fail_add_sponsor(context, null, "error.body.required");
    }

    @Test
    public void should_fail_create_sponsor_when_empty_body_is_sent(TestContext context) {
        assert_fail_add_sponsor(context, "", "error.body.required");
    }

    @Test
    public void should_fail_create_sponsor_when_empty_json_talk_is_sent(TestContext context) {
        assert_fail_add_sponsor(context, "{}", "error.body.required");
    }

    @Test
    public void should_fail_add_sponsor_when_name_is_null(TestContext context) {
        // given
        final String testInstance = "{\"name\":null}";

        // when
        assert_fail_add_sponsor(context, testInstance, "error.body.required_field.name");
    }

    @Test
    public void should_fail_add_sponsor_when_description_is_null(TestContext context) {
        // given
        final String testInstance = "{\"name\":\"a_name\",\"description\":null}";

        // when
        assert_fail_add_sponsor(context, testInstance, "error.body.required_field.description");
    }

    @Test
    public void should_fail_add_sponsor_when_level_is_null(TestContext context) {
        // given
        final String testInstance = "{\"name\":\"a_name\",\"description\":\"some_description\",\"level\":null}";

        // when
        assert_fail_add_sponsor(context, testInstance, "error.body.required_field.level");
    }

    @Test
    public void assert_fail_add_sponsor_when_no_auth_token_is_present(TestContext context) {
        // given
        final JsonObject testInstance = new JsonObject()
            .put("name", "some_name")
            .put("description", "some_description")
            .put("level", "the_level");
        // when
        assertNoAuthTokenIsPresentWithHttpPOST(context, ENDPOINT, testInstance);
    }

    public void assert_fail_add_sponsor(TestContext context, String body, String errorMessage) {
        assert_fail(context, ENDPOINT, TEST_USER, body, errorMessage);
    }

    @Test
    public void should_create_sponsor_with_minimal_fields(TestContext context) {
        // given
        final UUID random = UUID.randomUUID();
        final String name = "name-" + random;
        final String description = "description-" + random;
        final String level = "level-" + random;
        final JsonObject testInstance = new JsonObject()
            .put("name", name)
            .put("description", description)
            .put("level", level);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .sendJsonObject(testInstance, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .hasValidFormat()
                    .getId();

                assertThat(getFromDatabase(SPONSORS, id))
                    .with(context)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("name", testInstance.getString("name"))
                    .hasStringEqualsTo("description", testInstance.getString("description"))
                    .hasStringEqualsTo("level", testInstance.getString("level"))
                    .hasKeys(7);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_create_sponsor_with_all_fields(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final JsonObject jsonEntity = DocumentConverter.toJson(testInstance);

        // when
        final Async async = context.async();
        webClient.post(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
                final String id = assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isIdResponse()
                    .hasValidFormat()
                    .getId();

                assertThat(getFromDatabase(SPONSORS, id))
                    .with(context)
                    .hasCreatedMetadata()
                    .hasStringEqualsTo("name", testInstance.getName())
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("level", testInstance.getLevel())
                    .hasObjectEqualsTo("links", testInstance.getLinks())
                    .hasKeys(8);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_list_sponsors_when_no_auth_token_is_present(TestContext context) {
        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
    }

    @Test
    public void should_list_0_sponsors(TestContext context) {
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .isEmpty();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_a_single_sponsor(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .hasSize(1)
                    .hasItem(0, item -> assertEquals(context, item, testInstance));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_list_all_sponsors(TestContext context) {
        // given
        int testSize = 12;
        final Map<String, Sponsor> testData = IntStream.range(0, testSize)
            .mapToObj(i -> {
                final Sponsor testSponsor = getTestSponsor();
                return Pair.of(insertToDatabase(SPONSORS, DocumentConverter.getDocument(testSponsor)), testSponsor);
            }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (Sponsor) pair.getSecond()));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isCollectionResponse()
                    .hasSize(12)
                    .allItems(item -> assertEquals(context, item, testData.get(item.getId())));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    private void assertEquals(TestContext context, JsonApiEntity actual, Sponsor expected) {
        context.assertEquals(expected.getName(), actual.getString("name"));
        context.assertEquals(expected.getDescription(), actual.getString("description"));
        context.assertEquals(expected.getLevel(), actual.getString("level"));
        context.assertTrue(equalMap(actual.getJsonObject("links"), expected.getLinks()));
        context.assertNotNull(expected.getCreatedDate());
        context.assertEquals(expected.getCreatedDate(), expected.getLastUpdate());
        context.assertEquals(8, actual.fieldNames().size());
    }

    @Test
    public void should_get_sponsor_by_id(TestContext context) {
        // given
        final Sponsor testSponsor = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testSponsor));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.HELPER))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isEntityResponse()
                    .hasId()
                    .hasEntity(entity -> assertEquals(context, entity, testSponsor));
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_not_authorize_getting_sponsor_when_user_is_voter(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.VOTER, context);
    }

    @Test
    public void should_not_authorize_getting_sponsor_when_user_is_sponsor(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPONSOR, context);
    }

    @Test
    public void should_not_authorize_getting_sponsor_when_user_is_speaker(TestContext context) {
        assert_forbidden_when_getting_by_id(Role.SPEAKER, context);
    }

    private void assert_forbidden_when_getting_by_id(Role role, TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, role))
            .send(assertForbiddenWithEmptyBody(context, async));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_sponsor_by_id_when_id_format_is_not_valid(TestContext context) {
        // given
        final String id = "1234";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_get_sponsor_by_id_when_id_does_not_exists(TestContext context) {
        // given
        final String id = "5c24e4b0554b4947c8163049";

        // when
        final Async async = context.async();
        webClient.get(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_sponsor_name(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        final String newName = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("name", newName);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(SPONSORS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("name", newName)
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("level", testInstance.getLevel())
                    .hasObjectEqualsTo("links", testInstance.getLinks())
                    .hasKeys(8);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_sponsor_name_and_description(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        final var random = UUID.randomUUID();
        final String newName = "a-new-name-" + random;
        final String newDescription = "a-new-description-" + random;
        final JsonObject updateBody = new JsonObject()
            .put("name", newName)
            .put("description", newDescription);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(SPONSORS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("name", newName)
                    .hasStringEqualsTo("description", newDescription)
                    .hasStringEqualsTo("level", testInstance.getLevel())
                    .hasObjectEqualsTo("links", testInstance.getLinks())
                    .hasKeys(8);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_sponsor_level(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        final String newLevel = UUID.randomUUID().toString();
        final JsonObject updateBody = new JsonObject()
            .put("level", newLevel);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(SPONSORS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("name", testInstance.getName())
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("level", newLevel)
                    .hasObjectEqualsTo("links", testInstance.getLinks())
                    .hasKeys(8);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_update_sponsor_links(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        final var newLinks = Map.of("new-fancy-network", "https://fancy.com/stuff");
        final JsonObject updateBody = new JsonObject()
            .put("links", newLinks);

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();

                assertThat(getFromDatabase(SPONSORS, id))
                    .with(context)
                    .hasUpdatedMetadata(TEST_USER)
                    .hasStringEqualsTo("name", testInstance.getName())
                    .hasStringEqualsTo("description", testInstance.getDescription())
                    .hasStringEqualsTo("level", testInstance.getLevel())
                    .hasObjectEqualsTo("links", newLinks)
                    .hasKeys(8);
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_sponsor_when_id_does_not_exists(TestContext context) {
        // given
        final String fakeId = "5c4a3ea8aefd394cce055574";
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + fakeId)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 404, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasIdNotFoundMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_update_sponsor_when_id_format_is_not_valid(TestContext context) {
        // given
        final JsonObject updateBody = new JsonObject()
            .put("name", UUID.randomUUID().toString());

        // when
        final Async async = context.async();
        webClient.put(ENDPOINT + "123")
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .sendJsonObject(updateBody, assertJsonResponse(async, context, 400, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_delete_sponsor(TestContext context) {
        // given
        final Sponsor testInstance = getTestSponsor();
        final String id = insertToDatabase(SPONSORS, DocumentConverter.getDocument(testInstance));

        // when
        final Async async = context.async();
        webClient.delete(ENDPOINT + id)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isSuccessful()
                    .isVoidResponse();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    @Test
    public void should_fail_delete_sponsor_when_id_format_is_not_valid(TestContext context) {
        final Async async = context.async();
        webClient.delete(ENDPOINT + 123)
            .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
            .send(assertJsonResponse(async, context, 200, responseBody -> {
                assertThat(responseBody)
                    .with(context)
                    .isError()
                    .hasInvalidIdMessage();
            }));

        async.awaitSuccess(TEST_TIMEOUT);
    }
}
