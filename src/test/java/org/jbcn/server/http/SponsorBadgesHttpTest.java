//package org.jbcn.server.http;
//
//import io.vertx.core.json.JsonArray;
//import io.vertx.core.json.JsonObject;
//import io.vertx.ext.unit.Async;
//import io.vertx.ext.unit.TestContext;
//import io.vertx.ext.unit.junit.VertxUnitRunner;
//import org.bson.Document;
//import org.jbcn.server.model.Attendee;
//import org.jbcn.server.model.Role;
//import org.jbcn.server.model.ScannedBadge;
//import org.jbcn.server.model.sponsor.Sponsor;
//import org.jbcn.server.model.sponsor.SponsorImage;
//import org.jbcn.server.model.sponsor.SponsorLevel;
//import org.jbcn.server.utils.DocumentConverter;
//import org.jbcn.server.utils.Pair;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import util.VertxTestSetup;
//
//import java.util.*;
//import java.util.stream.Collectors;
//import java.util.stream.IntStream;
//
//import static java.lang.Boolean.FALSE;
//import static java.lang.Boolean.TRUE;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.jbcn.server.model.Traceable.*;
//import static org.jbcn.server.persistence.Collections.BADGES;
//import static util.TestDataGenerator.getTestBadge;
//import static util.VertxSetup.*;
//
//
//@RunWith(VertxUnitRunner.class)
//public class SponsorBadgesHttpTest extends VertxTestSetup {
//
//    private static final String TEST_USER = "sponsor_badges_test_user";
//
//    public static final String ENDPOINT = "/api/badges/";
//
//
//    @Before
//    public void before(TestContext context) {
//        dropCollection(BADGES);
//    }
//
//    @Test
//    public void should_fail_create_badge_when_no_body_is_sent(TestContext context) {
//        assert_fail_add_badge(context, null, "error.body.required");
//    }
//
//    @Test
//    public void should_fail_create_badge_when_empty_body_is_sent(TestContext context) {
//        assert_fail_add_badge(context, "", "error.body.required");
//    }
//
//    @Test
//    public void should_fail_create_badge_when_empty_json_talk_is_sent(TestContext context) {
//        assert_fail_add_badge(context, "{}", "error.body.required");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_sponsor_is_null(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\":null}";
//        // when
//        assertThat(badge).contains("sponsor");
//        assert_fail_add_badge(context, badge, "error.body.required_field.sponsor");
//    }
//
//    // NOTE: error returned corresponds to first field being validated
//    @Test
//    public void should_fail_add_badge_when_all_sponsor_fields_are_null(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\":{}}";
//        // when
//        assertThat(badge).contains("sponsor");
//        assert_fail_add_badge(context, badge, "error.body.required_field.sponsor.id");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_sponsor_id_is_null(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": null}}";
//        // when
//        assertThat(badge).contains("sponsor");
//        assertThat(badge).contains("id");
//        assert_fail_add_badge(context, badge, "error.body.required_field.sponsor.id");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_sponsor_id_is_empty(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": \"\"}}";
//        // when
//        assertThat(badge).contains("sponsor");
//        assert_fail_add_badge(context, badge, "error.body.required_field.sponsor.id");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_attendee_is_null(TestContext context) {
//        // given
//        final String vote = "{\"sponsor\": { \"id\": \"12\", \"name\": \"hello\", \"code\": \"hello\"}," +
//                "\"attendee\":null}";
//        // when
//        assertThat(vote).contains("attendee");
//        assert_fail_add_badge(context, vote, "error.body.required_field.attendee");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_attendee_email_is_null(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": \"12\", \"name\": \"hello\", \"code\": \"hello\"}," +
//                "\"attendee\": {\"email\": null}}";
//        // when
//        assertThat(badge).contains("attendee");
//        assert_fail_add_badge(context, badge, "error.body.required_field.attendee.email");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_attendee_email_is_empty(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": \"12\", \"name\": \"hello\", \"code\": \"hello\"}," +
//                "\"attendee\": {\"email\": \"\"}}";
//        // when
//        assertThat(badge).contains("attendee");
//        assertThat(badge).contains("email");
//        assert_fail_add_badge(context, badge, "error.body.required_field.attendee.email");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_attendee_name_is_null(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": \"12\", \"name\": \"hello\", \"code\": \"hello\"}," +
//                "\"attendee\": {\"email\": \"email@dot.com\", \"name\":null}}";
//        // when
//        assertThat(badge).contains("attendee");
//        assertThat(badge).contains("name");
//        assert_fail_add_badge(context, badge, "error.body.required_field.attendee.name");
//    }
//
//    @Test
//    public void should_fail_add_badge_when_attendee_name_is_empty(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": \"12\", \"name\": \"hello\", \"code\": \"hello\"}," +
//                "\"attendee\": {\"email\": \"email@dot.com\", \"name\":null}}";
//        // when
//        assertThat(badge).contains("attendee");
//        assertThat(badge).contains("name");
//        assert_fail_add_badge(context, badge, "error.body.required_field.attendee.name");
//    }
//
//    // NOTE: error returned corresponds to first field being validated
//    @Test
//    public void should_fail_add_badge_when_all_attendee_fields_are_null(TestContext context) {
//        // given
//        final String badge = "{\"sponsor\": { \"id\": \"12\", \"name\": \"hello\", \"code\": \"hello\"}," +
//                "\"attendee\":{}}";
//        // when
//        assertThat(badge).contains("attendee");
//        assert_fail_add_badge(context, badge, "error.body.required_field.attendee.email");
//    }
//
//    public void assert_fail_add_badge(TestContext context, String body, String errorMessage) {
//        assert_fail(context, ENDPOINT, TEST_USER, body, errorMessage);
//    }
//
//    @Test
//    public void should_create_badge_with_minimal_fields_with_POST_when_user_us_admin(TestContext context) {
//        // given
//        final ScannedBadge testBadge = getTestBadge();
//        assertThat(testBadge.getDetails()).isNullOrEmpty();
//        assertThat(testBadge.getSponsor().getName()).isNull();
//        assertThat(testBadge.getSponsor().getCode()).isNull();
//        assertThat(testBadge.getCreatedDate()).isNull();
//        assertThat(testBadge.getCreatedDate()).isNull();
//        assertThat(testBadge.getLastActionBy()).isNull();
//
//        final JsonObject jsonEntity = DocumentConverter.toJson(testBadge);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(1, data.fieldNames().size());
//                    context.assertNotNull(data.getString("_id"));
//
//                    final Document inserted = getFromDatabase(BADGES, data.getString("_id"));
//                    assertCreatedBadge(testBadge, inserted, 6, context);
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_create_badge_with_minimal_fields_with_POST_when_user_us_sponsor(TestContext context) {
//        // given
//        final ScannedBadge testBadge = getTestBadge();
//        assertThat(testBadge.getDetails()).isNullOrEmpty();
//        assertThat(testBadge.getSponsor().getName()).isNull();
//        assertThat(testBadge.getSponsor().getCode()).isNull();
//        assertThat(testBadge.getCreatedDate()).isNull();
//        assertThat(testBadge.getCreatedDate()).isNull();
//        assertThat(testBadge.getLastActionBy()).isNull();
//
//        final JsonObject jsonEntity = DocumentConverter.toJson(testBadge);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(1, data.fieldNames().size());
//                    context.assertNotNull(data.getString("_id"));
//
//                    final Document inserted = getFromDatabase(BADGES, data.getString("_id"));
//                    assertCreatedBadge(testBadge, inserted, 6, context);
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_not_authorize_create_badge_when_user_is_helper(TestContext context) {
//        assert_forbidden_when_creating_badge(Role.HELPER, context);
//    }
//
//    @Test
//    public void should_not_authorize_create_badge_when_user_is_voter(TestContext context) {
//        assert_forbidden_when_creating_badge(Role.VOTER, context);
//    }
//
//    @Test
//    public void should_not_authorize_create_badge_when_user_is_speaker(TestContext context) {
//        assert_forbidden_when_creating_badge(Role.SPEAKER, context);
//    }
//
//    private void assert_forbidden_when_creating_badge(Role role, TestContext context) {
//        // given
//        final ScannedBadge testBadge = getTestBadge();
//        final JsonObject jsonEntity = DocumentConverter.toJson(testBadge);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, role))
//                .sendJsonObject(jsonEntity, assertForbidden(context, async));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_create_badge_with_all_fields_with_POST(TestContext context) {
//        // given
//        final String sponsorUuid = UUID.randomUUID().toString();
//        final Sponsor sponsor = new Sponsor(
//                "sponsor-id-" + sponsorUuid,
//                "sponsor-name-" + sponsorUuid,
//                "sponsor-href-" + sponsorUuid,
//                new SponsorImage("image-src-" + sponsorUuid, "image-alt-" + sponsorUuid),
//                new SponsorLevel(2, "sponsor-level-name-" + sponsorUuid),
//                "sponsor-code-" + sponsorUuid);
//        final ScannedBadge testBadge = getTestBadge("details-" + UUID.randomUUID(), null, sponsor);
//        assertThat(testBadge.getDetails()).isNotEmpty();
//
//        final JsonObject jsonEntity = DocumentConverter.toJson(testBadge);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(1, data.fieldNames().size());
//                    context.assertNotNull(data.getString("_id"));
//
//                    final Document inserted = getFromDatabase(BADGES, data.getString("_id"));
//                    assertCreatedBadgeWithAllFields(testBadge, inserted, context);
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    // when id is null or blank
//    @Test
//    public void should_create_badge_with_all_fields_with_PUT_when_does_exist(TestContext context) {
//        // given
//        final String sponsorUuid = UUID.randomUUID().toString();
//        final Sponsor sponsor = new Sponsor(
//                "sponsor-id-" + sponsorUuid,
//                "sponsor-name-" + sponsorUuid,
//                "sponsor-href-" + sponsorUuid,
//                new SponsorImage("image-src-" + sponsorUuid, "image-alt-" + sponsorUuid),
//                new SponsorLevel(2, "sponsor-level-name-" + sponsorUuid),
//                "sponsor-code-" + sponsorUuid);
//        final ScannedBadge testBadge = getTestBadge("details-" + UUID.randomUUID(), null, sponsor);
//        assertThat(testBadge.getDetails()).isNotEmpty();
//
//        final JsonObject jsonEntity = DocumentConverter.toJson(testBadge);
//
//        // when
//        final Async async = context.async();
//        webClient.put(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(1, data.fieldNames().size());
//                    context.assertNotNull(data.getString("_id"));
//
//                    final Document inserted = getFromDatabase(BADGES, data.getString("_id"));
//                    assertCreatedBadgeWithAllFields(testBadge, inserted, context);
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    // TODO document that upsert returns the id always, to make it idempotent no matter if creating or updating
//    @Test
//    public void should_update_badge_details_with_all_fields_with_PUT_when_it_exist(TestContext context) {
//        // given
//        final String sponsorUuid = UUID.randomUUID().toString();
//        final Sponsor sponsor = new Sponsor(
//                "sponsor-id-" + sponsorUuid,
//                "sponsor-name-" + sponsorUuid,
//                "sponsor-href-" + sponsorUuid,
//                new SponsorImage("image-src-" + sponsorUuid, "image-alt-" + sponsorUuid),
//                new SponsorLevel(2, "sponsor-level-name-" + sponsorUuid),
//                "sponsor-code-" + sponsorUuid);
//        final ScannedBadge testBadge = getTestBadge("old-details-" + UUID.randomUUID(), null, sponsor);
//        assertThat(testBadge.getDetails()).isNotEmpty();
//
//        final Document document = DocumentConverter.getDocument(testBadge);
//        document.put(CREATED_DATE, System.currentTimeMillis());
//        final String id = insertToDatabase(BADGES, document);
//        final String newDetails = "new-details-" + UUID.randomUUID();
//        final JsonObject jsonEntity = DocumentConverter.toJson(testBadge)
//                .put("_id", id)
//                .put("details", newDetails);
//
//        // when
//        final Async async = context.async();
//        webClient.put(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .sendJsonObject(jsonEntity, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(1, data.fieldNames().size());
//                    context.assertNotNull(data.getString("_id"));
//
//                    final Document inserted = getFromDatabase(BADGES, data.getString("_id"));
//                    context.assertEquals(newDetails, inserted.getString("details"));
//                    assertUpdatedBadgeWithAllFields(testBadge, inserted, context);
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    private void assertCreatedBadge(ScannedBadge testEntity, Document inserted, int sponsorReturnedKeys, TestContext context) {
//
//        context.assertNotNull(inserted.getLong(CREATED_DATE));
//        context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
//        context.assertNotNull(inserted.getString(LAST_ACTION_BY));
//        context.assertEquals(6, inserted.keySet().size());
//
//        final Document sponsor = (Document) inserted.get("sponsor");
//        context.assertEquals(testEntity.getSponsor().getId(), sponsor.getString("id"));
//        context.assertEquals(testEntity.getSponsor().getName(), sponsor.getString("name"));
//        context.assertEquals(testEntity.getSponsor().getCode(), sponsor.getString("code"));
//        context.assertNull(sponsor.getString("href"));
//        context.assertNull(sponsor.getString("image"));
//        context.assertNull(sponsor.getInteger("level"));
//        context.assertEquals(sponsorReturnedKeys, sponsor.keySet().size());
//
//        final Document attendee = (Document) inserted.get("attendee");
//        assertCreatedAttendee(testEntity, context, attendee);
//    }
//
//    private void assertUpdatedBadgeWithAllFields(ScannedBadge testEntity, Document inserted, TestContext context) {
//        context.assertNotEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
//        assertBadgeWithAllFields(testEntity, inserted, context);
//    }
//
//    private void assertCreatedBadgeWithAllFields(ScannedBadge testEntity, Document inserted, TestContext context) {
//
//        context.assertEquals(inserted.getLong(CREATED_DATE), inserted.getLong(LAST_UPDATE));
//        context.assertEquals(testEntity.getDetails(), inserted.getString("details"));
//        assertBadgeWithAllFields(testEntity, inserted, context);
//    }
//
//    private void assertBadgeWithAllFields(ScannedBadge testEntity, Document inserted, TestContext context) {
//
//        context.assertNotNull(inserted.getLong(CREATED_DATE));
//        context.assertNotNull(inserted.getLong(LAST_UPDATE));
//        context.assertNotNull(inserted.getString(LAST_ACTION_BY));
//        context.assertEquals(7, inserted.keySet().size());
//
//        final Document sponsor = (Document) inserted.get("sponsor");
//        context.assertEquals(6, sponsor.keySet().size());
//        context.assertEquals(testEntity.getSponsor().getId(), sponsor.getString("id"));
//        context.assertEquals(testEntity.getSponsor().getName(), sponsor.getString("name"));
//        context.assertEquals(testEntity.getSponsor().getHref(), sponsor.getString("href"));
//        context.assertEquals(testEntity.getSponsor().getCode(), sponsor.getString("code"));
//
//        final SponsorImage sponsorImage = testEntity.getSponsor().getImage();
//        context.assertEquals(sponsorImage.getAlt(), ((Document) sponsor.get("image")).getString("alt"));
//        context.assertEquals(sponsorImage.getSrc(), ((Document) sponsor.get("image")).getString("src"));
//
//        final SponsorLevel level = testEntity.getSponsor().getLevel();
//        context.assertEquals(level.getPriority(), ((Document) sponsor.get("level")).getInteger("priority"));
//        context.assertEquals(level.getName(), ((Document) sponsor.get("level")).getString("name"));
//
//        final Document attendee = (Document) inserted.get("attendee");
//        assertCreatedAttendee(testEntity, context, attendee);
//    }
//
//
//    private void assertCreatedAttendee(ScannedBadge testEntity, TestContext context, Document attendee) {
//        context.assertEquals(10, attendee.keySet().size());
//        context.assertEquals(testEntity.getAttendee().getName(), attendee.getString("name"));
//        context.assertEquals(testEntity.getAttendee().getEmail(), attendee.getString("email"));
//        context.assertEquals(testEntity.getAttendee().getLanguage(), attendee.getString("language"));
//        context.assertEquals(testEntity.getAttendee().getAge(), attendee.getInteger("age"));
//        context.assertEquals(testEntity.getAttendee().getGender().name(), attendee.getString("gender"));
//
//        context.assertEquals(testEntity.getAttendee().getCompany(), attendee.getString("company"));
//        context.assertEquals(testEntity.getAttendee().getJobTitle(), attendee.getString("jobTitle"));
//        context.assertEquals(testEntity.getAttendee().getCity(), attendee.getString("city"));
//        context.assertEquals(testEntity.getAttendee().getCountry(), attendee.getString("country"));
//        context.assertEquals(testEntity.getAttendee().getProgrammingLanguages(), attendee.getString("programmingLanguages"));
//    }
//
//    @Test
//    public void should_fail_list_badges_when_no_auth_token_is_present(TestContext context) {
//        // when
//        assertNoAuthTokenIsPresentWithHttpGET(context, ENDPOINT);
//    }
//
//    @Test
//    public void should_list_0_votes(TestContext context) {
//        // given: clean collection
//
//        // when
//        final Async async = context.async();
//        webClient.get(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(0, data.getInteger("total"));
//                    context.assertEquals(2, data.fieldNames().size());
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(0, items.size());
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_list_a_single_vote(TestContext context) {
//        // given
//        final ScannedBadge testBadge = getTestBadge();
//        final String id = insertToDatabase(BADGES, DocumentConverter.getDocument(testBadge));
//
//        // when
//        final Async async = context.async();
//        webClient.get(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(1, items.size());
//                    context.assertEquals(1, data.getInteger("total"));
//                    context.assertEquals(2, data.fieldNames().size());
//
//                    assertRetrievedBadge(testBadge, items.getJsonObject(0), context);
//                }));
//
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_list_all_badges(TestContext context) {
//        // given
//        int testSize = 12;
//        final Map<String, ScannedBadge> testData = IntStream.range(0, testSize)
//                .mapToObj(i -> {
//                    final ScannedBadge badge = getTestBadge();
//                    final String first = insertToDatabase(BADGES, DocumentConverter.getDocument(badge));
//                    return Pair.of(first, badge);
//                }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (ScannedBadge) pair.getSecond()));
//
//        // when
//        final Async async = context.async();
//        webClient.get(ENDPOINT)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(testSize, items.size());
//                    context.assertEquals(12, data.getInteger("total"));
//
//                    items.forEach(it -> {
//                        assertRetrievedBadge(testData.get(((JsonObject) it).getString("_id")), (JsonObject) it, context);
//                    });
//                }));
//
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_get_badge_by_id(TestContext context) {
//        // given
//        final ScannedBadge testVote = getTestBadge();
//        final String id = insertToDatabase(BADGES, DocumentConverter.getDocument(testVote));
//
//        // when
//        final Async async = context.async();
//        webClient.get(ENDPOINT + id)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(Boolean.TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(2, responseBody.fieldNames().size());
//
//                    final JsonObject json = responseBody.getJsonObject("data");
//                    assertRetrievedBadge(testVote, json, context);
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    private void assertRetrievedBadge(ScannedBadge badge, JsonObject json, TestContext context) {
//        context.assertNotNull(json.getString("_id"));
//        context.assertEquals(badge.getDetails(), json.getString("details"));
//
//        final Sponsor sponsor = badge.getSponsor();
//        final JsonObject sponsorJson = json.getJsonObject("sponsor");
//        context.assertEquals(sponsor.getId(), sponsorJson.getString("id"));
//        context.assertEquals(sponsor.getName(), sponsorJson.getString("name"));
//        context.assertEquals(sponsor.getCode(), sponsorJson.getString("code"));
//        // null values are not returned
//        context.assertEquals(1, sponsorJson.fieldNames().size());
//
//        final Attendee attendee = badge.getAttendee();
//        final JsonObject attendeeJson = json.getJsonObject("attendee");
//        context.assertEquals(attendee.getName(), attendeeJson.getString("name"));
//        context.assertEquals(attendee.getEmail(), attendeeJson.getString("email"));
//        context.assertEquals(attendee.getAge(), attendeeJson.getInteger("age"));
//        context.assertEquals(attendee.getGender().name(), attendeeJson.getString("gender"));
//        context.assertEquals(attendee.getLanguage(), attendeeJson.getString("language"));
//        context.assertEquals(attendee.getCompany(), attendeeJson.getString("company"));
//        context.assertEquals(attendee.getJobTitle(), attendeeJson.getString("jobTitle"));
//        context.assertEquals(attendee.getCity(), attendeeJson.getString("city"));
//        context.assertEquals(attendee.getCountry(), attendeeJson.getString("country"));
//        context.assertEquals(attendee.getProgrammingLanguages(), attendeeJson.getString("programmingLanguages"));
//        context.assertEquals(10, attendeeJson.fieldNames().size());
//
//        // date are not present in hardcoded test data
//        context.assertNull(json.getLong(CREATED_DATE));
//        context.assertNull(json.getLong(LAST_UPDATE));
//        context.assertEquals(3, json.fieldNames().size());
//    }
//
//    @Test
//    public void should_fail_get_badge_by_id_when_id_format_is_not_valid(TestContext context) {
//        // given
//        final String id = "1234";
//
//        // when
//        final Async async = context.async();
//        webClient.get(ENDPOINT + id)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .send(assertJsonResponse(async, context, 400, responseBody -> {
//                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
//                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
//                    async.complete();
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_by_sponsor_id(TestContext context) {
//        // given
//        final Map<String, ScannedBadge> extraBadges = getTestBadges(10);
//        int testSize = 5;
//        final String details = "details-" + UUID.randomUUID();
//
//        final String sponsorId = UUID.randomUUID().toString();
//        final Sponsor sponsor = Sponsor.builder()
//                .id(sponsorId)
//                .name("ss")
//                .code("sss")
//                .href("https://www.href.com")
//                .build();
//        final Map<String, ScannedBadge> badges = getTestBadges(testSize, getTestBadge(details, null, sponsor));
//
//        final JsonObject searchParams = new JsonObject()
//                .put("filters", new JsonArray()
//                        .add(new JsonObject()
//                                .put("field", "sponsor.id")
//                                .put("value", sponsorId)));
//        final JsonObject msgBody = JsonObject.mapFrom(searchParams);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .sendJsonObject(msgBody, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(testSize, items.size());
//                    items.forEach(it -> {
//                        context.assertEquals(details, ((JsonObject) it).getString("details"));
//                        context.assertEquals(sponsorId, ((JsonObject) it).getJsonObject("sponsor").getString("id"));
//                    });
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_fail_get_badge_by_id_when_id_does_not_exists(TestContext context) {
//        // given
//        final String fakeId = "5c24e4b0554b4947c8163049";
//
//        // when
//        final Async async = context.async();
//        webClient.get(ENDPOINT + fakeId)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .send(assertJsonResponse(async, context, 404, responseBody -> {
//                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
//                    context.assertEquals("error.id.not_found", responseBody.getString("error"));
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_by_sponsor_id_and_details(TestContext context) {
//        // given
//        final Map<String, ScannedBadge> extraBadges = getTestBadges(10);
//        int testSize = 5;
//        final String details = "details-" + UUID.randomUUID();
//
//        final String sponsorId = UUID.randomUUID().toString();
//        final Sponsor sponsor = Sponsor.builder()
//                .id(sponsorId)
//                .name("ss")
//                .code("sss")
//                .href("https://www.href.com")
//                .build();
//        final Map<String, ScannedBadge> badges = getTestBadges(testSize, getTestBadge(details, null, sponsor));
//
//        final JsonObject searchParams = new JsonObject()
//                .put("filters", new JsonArray()
//                        .add(new JsonObject()
//                                .put("field", "details")
//                                .put("value", details))
//                        .add(new JsonObject()
//                                .put("field", "sponsor.id")
//                                .put("value", sponsorId)));
//        final JsonObject msgBody = JsonObject.mapFrom(searchParams);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .sendJsonObject(msgBody, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(testSize, items.size());
//                    items.forEach(it -> {
//                        context.assertEquals(details, ((JsonObject) it).getString("details"));
//                        context.assertEquals(sponsorId, ((JsonObject) it).getJsonObject("sponsor").getString("id"));
//                    });
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_and_return_0_items_when_page_is_empty(TestContext context) {
//        // given
//        final Map<String, ScannedBadge> extraBadges = getTestBadges(10);
//        int testSize = 5;
//        final String details = "details-" + UUID.randomUUID();
//        final Map<String, ScannedBadge> badges = getTestBadges(testSize, getTestBadge(details));
//
//        final JsonObject searchParams = new JsonObject()
//                .put("filters", new JsonArray()
//                        .add(new JsonObject()
//                                .put("field", "details")
//                                .put("value", details)))
//                .put("page", new JsonObject()
//                        .put("index", 10)
//                        .put("size", 50));
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(0, items.size());
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_by_attendee_name_and_return_only_attendee_object(TestContext context) {
//        // given
//        getTestBadges(10);
//
//        int testSize = 3;
//        final String attendeeName = "sponsorId-" + UUID.randomUUID();
//        final Attendee attendee = new Attendee(attendeeName, null, null, null, null, null, null, null, null, null);
//        final Map<String, ScannedBadge> badges = getTestBadges(testSize, getTestBadge(null, attendee, null));
//
//        final JsonObject searchParams = new JsonObject()
//                .put("filters", new JsonArray()
//                        .add(new JsonObject()
//                                .put("field", "attendee.name")
//                                .put("value", attendeeName)))
//                .put("fields", Arrays.asList("attendee"));
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(testSize, items.size());
//
//                    items.forEach(it -> {
//                        Set<String> fieldNames = ((JsonObject) it).fieldNames();
//                        context.assertTrue(fieldNames.contains("_id"));
//                        context.assertTrue(fieldNames.contains("attendee"));
//                        context.assertEquals(2, fieldNames.size());
//
//                        final JsonObject attendeeJson = ((JsonObject) it).getJsonObject("attendee");
//                        context.assertEquals(attendeeName, attendeeJson.getString("name"));
//                        context.assertEquals(1, attendeeJson.fieldNames().size());
//                    });
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_by_attendee_name_and_return_only_attendee_name(TestContext context) {
//        // given
//        getTestBadges(10);
//
//        int testSize = 3;
//        final String attendeeName = "sponsorId-" + UUID.randomUUID();
//        final Attendee attendee = new Attendee(attendeeName, null, null, null, null, null, null, null, null, null);
//        final Map<String, ScannedBadge> badges = getTestBadges(testSize, getTestBadge(null, attendee, null));
//
//        final JsonObject searchParams = new JsonObject()
//                .put("filters", new JsonArray()
//                        .add(new JsonObject()
//                                .put("field", "attendee.name")
//                                .put("value", attendeeName)))
//                .put("fields", Arrays.asList("attendee.name"));
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(testSize, items.size());
//
//                    items.forEach(it -> {
//                        Set<String> fieldNames = ((JsonObject) it).fieldNames();
//                        context.assertTrue(fieldNames.contains("_id"));
//                        context.assertTrue(fieldNames.contains("attendee"));
//                        context.assertEquals(2, fieldNames.size());
//
//                        final JsonObject attendeeJson = ((JsonObject) it).getJsonObject("attendee");
//                        context.assertEquals(attendeeName, attendeeJson.getString("name"));
//                        context.assertEquals(1, attendeeJson.fieldNames().size());
//                    });
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_by_sponsor_id_and_return_page_with_less_items_than_total(TestContext context) {
//        // given
//        final Map<String, ScannedBadge> extraVotes = getTestBadges(10);
//
//        int testSize = 5;
//        final String sponsorId = UUID.randomUUID().toString();
//        final Sponsor sponsor = Sponsor.builder()
//                .id(sponsorId)
//                .name("ss")
//                .code("sss")
//                .href("https://www.href.com")
//                .build();
//        final Map<String, ScannedBadge> badges = getTestBadges(testSize, getTestBadge(null, null, sponsor));
//
//
//        final JsonObject searchParams = new JsonObject()
//                .put("filters", new JsonArray()
//                        .add(new JsonObject()
//                                .put("field", "sponsor.id")
//                                .put("value", sponsorId)))
//                .put("page", new JsonObject()
//                        .put("index", 2)
//                        .put("size", 1));
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .sendJsonObject(searchParams, assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(1, items.size());
//                    context.assertEquals(sponsorId, items.getJsonObject(0).getJsonObject("sponsor").getString("id"));
//                    context.assertEquals(3, items.getJsonObject(0).fieldNames().size());
//                    async.complete();
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_filter_badges_by_sponsor_id_and_return_sorted_by_details(TestContext context) {
//        // given
//        getTestBadges(10);
//
//        int testSize = 6;
//        final String sponsorId = "sponsor-id-" + UUID.randomUUID();
//        final Sponsor sponsor = Sponsor.builder().id(sponsorId).name("ss").build();
//        for (int i = 0; i < testSize; i++) {
//            insertToDatabase(BADGES, DocumentConverter.getDocument(getTestBadge("details-" + i, null, sponsor)));
//        }
//
//        final Map searchParams = new HashMap<>();
//        searchParams.put("filters", Arrays.asList(
//                new HashMap<String, Object>() {{
//                    put("field", "sponsor.id");
//                    put("value", sponsorId);
//                }}));
//        searchParams.put("sort", "details");
//        searchParams.put("asc", false);
//
//        // when
//        final Async async = context.async();
//        webClient.post(ENDPOINT + "search")
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .sendJsonObject(JsonObject.mapFrom(searchParams), assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    final JsonObject data = responseBody.getJsonObject("data");
//                    context.assertEquals(testSize, data.getInteger("total"));
//
//                    final JsonArray items = data.getJsonArray("items");
//                    context.assertEquals(testSize, items.size());
//                    context.assertEquals(4, items.getJsonObject(0).fieldNames().size());
//                    context.assertEquals("details-5", items.getJsonObject(0).getString("details"));
//                    context.assertEquals("details-4", items.getJsonObject(1).getString("details"));
//                    context.assertEquals("details-3", items.getJsonObject(2).getString("details"));
//                    context.assertEquals("details-2", items.getJsonObject(3).getString("details"));
//                    context.assertEquals("details-1", items.getJsonObject(4).getString("details"));
//                    context.assertEquals("details-0", items.getJsonObject(5).getString("details"));
//                    async.complete();
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    /**
//     * Returns a map of badgeId -> ScannedBadge
//     */
//    private Map<String, ScannedBadge> getTestBadges(int size, ScannedBadge badge) {
//        return IntStream.range(0, size)
//                .mapToObj(i -> {
//                    final ScannedBadge testVote = badge == null ? getTestBadge() : badge;
//                    return Pair.of(insertToDatabase(BADGES, DocumentConverter.getDocument(testVote)), testVote);
//                }).collect(Collectors.toMap(pair -> (String) pair.getFirst(), pair -> (ScannedBadge) pair.getSecond()));
//    }
//
//    private Map<String, ScannedBadge> getTestBadges(int size) {
//        return getTestBadges(size, null);
//    }
//
//    @Test
//    public void should_delete_badge(TestContext context) {
//        // given
//        final ScannedBadge testBadge = getTestBadge();
//        final String id = insertToDatabase(BADGES, DocumentConverter.getDocument(testBadge));
//
//        // when
//        final Async async = context.async();
//        webClient.delete(ENDPOINT + id)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(1, responseBody.fieldNames().size());
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_fail_delete_when_id_format_is_not_valid(TestContext context) {
//        // given
//        // when
//        final Async async = context.async();
//        webClient.delete(ENDPOINT + 123)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.ADMIN))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(FALSE, responseBody.getBoolean("status"));
//                    context.assertEquals("error.id.invalid_id", responseBody.getString("error"));
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//    @Test
//    public void should_not_fail_delete_when_id_does_not_exists(TestContext context) {
//        // given
//        final String fakeId = "5c3b64d4aefd3931c0ec7b54";
//        // when
//        final Async async = context.async();
//        webClient.delete(ENDPOINT + fakeId)
//                .bearerTokenAuthentication(getNewSession(TEST_USER, Role.SPONSOR))
//                .send(assertJsonResponse(async, context, 200, responseBody -> {
//                    context.assertEquals(TRUE, responseBody.getBoolean("status"));
//                    context.assertEquals(1, responseBody.fieldNames().size());
//                }));
//
//        async.awaitSuccess(TEST_TIMEOUT);
//    }
//
//}
