package org.jbcn.server.formatters;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HtmlFormatterTest {

    @Test
    public void should_init_formatter() {
        final var formatter = new HtmlFormatter();
        assertThat(formatter).isNotNull();
    }

    @Test
    public void should_format_linebreaks_as_paragraphs() {
        final var formatter = new HtmlFormatter();
        String output = formatter.format("hello\nbye!!");

        assertThat(output).isEqualTo("<p>hello</p><p>bye!!</p>");
    }

    @Test
    public void should_ignore_continuous_linebreaks() {
        final var formatter = new HtmlFormatter();
        String output = formatter.format("hello\n\nbye!!");

        assertThat(output).isEqualTo("<p>hello</p><p>bye!!</p>");
    }

    @Test
    public void should_trim_starting_blanks() {
        final var formatter = new HtmlFormatter();
        String output = formatter.format("   hello\n\n    \tbye!!");

        assertThat(output).isEqualTo("<p>hello</p><p>bye!!</p>");
    }

    @Test
    public void should_trim_trailing_blanks() {
        final var formatter = new HtmlFormatter();
        String output = formatter.format("   hello\t   \n\n    \tbye!!  ");

        assertThat(output).isEqualTo("<p>hello</p><p>bye!!</p>");
    }

    @Test
    public void should_trim_trailing_linebreaks() {
        final var formatter = new HtmlFormatter();
        String output = formatter.format("hello\n\nbye!!\n");

        assertThat(output).isEqualTo("<p>hello</p><p>bye!!</p>");
    }

    @Test
    public void should_format_unordered_list_with_bullets() {
        final var formatter = new HtmlFormatter();
        final var input = """
            This are my points:
            * un
            *dos
            *    tres
               * quatre o cinc
            Nothing else""";
        String output = formatter.format(input);

        final var expected = """
            <p>This are my points:</p>
            <ul>
            <li>un</li>
            <li>dos</li>
            <li>tres</li>
            <li>quatre o cinc</li>
            </ul>
            <p>Nothing else</p>"""
            .replaceAll("\n", "");
        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void should_format_unordered_list_with_hyphens() {
        final var formatter = new HtmlFormatter();
        final var input = """
            This are my points:
            - un
            -dos
            -    tres
              - quatre o cinc
            Nothing else""";
        String output = formatter.format(input);

        final var expected = """
            <p>This are my points:</p>
            <ul>
            <li>un</li>
            <li>dos</li>
            <li>tres</li>
            <li>quatre o cinc</li>
            </ul>
            <p>Nothing else</p>"""
            .replaceAll("\n", "");
        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void should_format_unordered_list_at_the_end() {
        final var formatter = new HtmlFormatter();
        final var input = """
            This are my points:
            * un
            * dos""";
        String output = formatter.format(input);

        final var expected = """
            <p>This are my points:</p>
            <ul>
            <li>un</li>
            <li>dos</li>
            </ul>"""
            .replaceAll("\n", "");
        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void should_not_match_in_between_bullet_unordered_list_at_the_end() {
        final var formatter = new HtmlFormatter();
        final var input = """
            Not a real list:
            not * un
            """;
        String output = formatter.format(input);

        final var expected = """
            <p>Not a real list:</p>
            <p>not * un</p>"""
            .replaceAll("\n", "");
        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void should_format_multiple_unordered_lists() {
        final var formatter = new HtmlFormatter();
        final var input = """
            This are my points:
            * un
            *dos
            More stuff below
            *    tres
              * quatre o cinc
            Nothing else""";

        String output = formatter.format(input);

        final var expected = """
            <p>This are my points:</p>
            <ul>
            <li>un</li>
            <li>dos</li>
            </ul>
            <p>More stuff below</p>
            <ul>
            <li>tres</li>
            <li>quatre o cinc</li>
            </ul>
            <p>Nothing else</p>"""
            .replaceAll("\n", "");
        assertThat(output).isEqualTo(expected);
    }
}
