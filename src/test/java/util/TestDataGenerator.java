package util;

import org.bson.Document;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.*;
import org.jbcn.server.model.compliance.DataConsent;
import org.jbcn.server.model.personal.Gender;
import org.jbcn.server.model.scheduling.Session;
import org.jbcn.server.model.scheduling.Track;
import org.jbcn.server.model.sponsor.JobOffer;
import org.jbcn.server.model.sponsor.Sponsor;
import org.jbcn.server.model.votes.AttendeeVote;
import org.jbcn.server.model.votes.FavouritedTalk;
import org.jbcn.server.model.votes.VoteSource;
import org.jbcn.server.utils.TimeUtils;

import java.time.LocalDateTime;
import java.util.*;

import static java.lang.Boolean.FALSE;
import static org.jbcn.server.model.PaperState.PAPER_STATE_SENT;

public class TestDataGenerator {

    public static Paper getTestPaper() {
        final Date now = new Date();
        final String suffix = getRandomSuffix();
        final Paper paper = new Paper();
        paper.setCreatedDate(now);
        paper.setLastUpdate(now);
        paper.setComments("comment");
        paper.setEdition(JbcnProperties.getCurrentEdition());
        paper.setState(PAPER_STATE_SENT);
        paper.setLevel("middle");
        paper.setTitle("Test paper " + suffix);
        paper.setType("talk");
        paper.setPaperAbstract("THE BIG BRAIN AM WINNING AGAIN! I AM THE GREETEST! NOW I AM LEAVING EARTH, FOR NO RAISEN! Oh, you're a dollar naughtier than most. I've got to find a way to escape the horrible ravages of youth. Suddenly, I'm going to the bathroom like clockwork, every three hours. And those jerks at Social Security stopped sending me checks. Now 'I'' have to pay ''them'!");
        paper.setTags(Collections.singletonList("tag"));
        paper.setSenders(Collections.singletonList(getTestSender()));
        paper.setSponsor(FALSE);
        return paper;
    }

    public static String getRandomSuffix() {
        return fakeId();
    }

    public static Paper getTestPaper(Date createdDate) {
        final Paper testPaper = getTestPaper();
        testPaper.setCreatedDate(createdDate);
        testPaper.setLastUpdate(createdDate);
        return testPaper;
    }

    public static Sender getTestSender(String suffix) {
        final Sender sender = new Sender();
        sender.setFullName("Java Rockstar " + suffix);
        sender.setJobTitle("I rock!!");
        sender.setEmail("email" + suffix + "@barcelonajug.org");
        sender.setBiography("Stop talking, brain thinking.\nHush. I am the Doctor, and you are the Daleks!\n\nThe way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.");
        sender.setTwitter("twitter");
        sender.setPicture("https://trololo.com");
        sender.setWeb("https://whatever.com");
        sender.setLinkedin("https://www.linkedin.com/in/something-here");
        sender.setTshirtSize("XXL");
        sender.setStarred(false);
        sender.setAllergies("cheap wine, bad beer");
        sender.setDataConsent(DataConsent.consentAll());
        sender.setCode(Speaker.calculateCode(sender.getFullName(), sender.getEmail()));
        return sender;
    }

    public static Sender getTestSender() {
        return getTestSender(getRandomSuffix());
    }

    public static Talk getTestTalk() {
        final String suffix = getRandomSuffix();
        return getTestTalk(suffix);
    }

    public static Talk getTestTalk(LocalDateTime createdDate) {
        final Talk testTalk = getTestTalk();
        testTalk.setCreatedDate(createdDate);
        testTalk.setLastUpdate(createdDate);
        return testTalk;
    }

    public static Talk getTestTalk(String suffix) {
        final LocalDateTime now = LocalDateTime.now();
        return Talk.builder()
            .paperId(fakeId())
            .type("talk")
            .createdDate(now)
            .lastUpdate(now)
            .comments("Stop talking, brain thinking. Hush. I am the Doctor, and you are the Daleks! The way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.")
            .dateAndTime(LocalDateTime.now())
            .edition("2019")
            .level("middle")
            .paperAbstract("Stop talking, brain thinking. Hush. I am the Doctor, and you are the Daleks! The way I see it, every life is a pile of good things and bad things.…hey.…the good things don't always soften the bad things; but vice-versa the bad things don't necessarily spoil the good things and make them unimportant.")
            .published(false)
            .speakers(Arrays.asList(getSpeaker(suffix)))
            .state(TalkState.toTalkState("unconfirmed"))
            .preferenceDay("preference_day")
            .title("Test talk " + suffix)
            .tags(Arrays.asList("tag1", "tag2", "tag3"))
            .sponsor(FALSE)
            .build();
    }

    public static Speaker getSpeaker() {
        return getSpeaker(getRandomSuffix());
    }

    public static Speaker getSpeaker(String suffix) {
        return Speaker.builder()
            .ref(fakeId())
            .allergies("allergies")
            .attendeesParty(true)
            .biography("A life of:\n* Passion\n- Adventure\n\n...and stuff")
            .email("some@email.com")
            .fullName("Test speaker " + suffix)
            .company("Mega Corp")
            .code(getRandomSuffix())
            .picture("https://some.server.com/file.jpg")
            .speakersParty(true)
            .travelCost(false)
            .tshirtSize("L")
            .web("https://www.some.server.com/myweb")
            .twitter("https://www.twitter.com/somedude")
            .linkedin("https://linkedin.com/work-account")
            .build();
    }

    public static User getTestUser() {
        return getTestUser(null);
    }

    public static User getTestUser(String suffix) {
        final LocalDateTime now = LocalDateTime.now();
        return User.builder()
            .username("anna" + (suffix == null ? "" : "_" + suffix))
            .email("superanna" + (suffix == null ? "" : "_" + suffix) + "@bcnjug.com")
            .roles(Set.of(Role.ADMIN, Role.VOTER))
            .createdDate(now)
            .lastUpdate(now)
            .build();
    }

    public static Document getTestRoom() {
        final String suffix = getRandomSuffix();
        final long now = TimeUtils.currentTime();
        return new Document(Map.of(
            "name", "room-" + suffix,
            "capacity", 442,
            Traceable.CREATED_DATE, now,
            Traceable.LAST_UPDATE, now,
            Traceable.LAST_ACTION_BY, "test_user"
        ));
    }

    public static Track getTestTrack() {
        final String suffix = getRandomSuffix();
        final LocalDateTime now = LocalDateTime.now();
        return new Track(null, "track name-" + suffix, "track description-" + suffix, now, now, "test_user");
    }

    public static Sponsor getTestSponsor() {
        final String suffix = getRandomSuffix();
        final LocalDateTime now = LocalDateTime.now();
        return new Sponsor(null,
            "sponsor-name-" + suffix,
            "sponsor-description-" + suffix,
            "MEGA-TOP",
            Map.of(
                "web", "https://company.org/web",
                "twitter", "https://company.org/twitter",
                "instagram", "https://company.org/instagram",
                "linkedin", "https://company.org/linkedin"
            ),
            now, now, "test_user");
    }

    public static JobOffer getTestJobOffer() {
        final String suffix = getRandomSuffix();
        final LocalDateTime now = LocalDateTime.now();
        return new JobOffer(null,
            "title-" + suffix,
            "description-" + suffix,
            "https://sponsor.org/web/offers/123",
            fakeId(),
            null,
            now, now, "test_user");
    }

    public static Session getTestSession() {
        return getTestSession(null);
    }

    public static Session getTestSession(String trackId) {
        final String suffix = getRandomSuffix();
        final LocalDateTime now = LocalDateTime.now();
        final Session session = new Session();
        session.setSlotId(suffix);
        session.setDay("27/05/2019");
        session.setStartTime("10:00");
        session.setEndTime("11:00");
        session.setTrackId(trackId);
        session.setRoomId(suffix);
        session.setCreatedDate(now);
        session.setLastUpdate(now);
        return session;
    }

    public static AttendeeVote getTestAttendeeVote() {
        final String suffix = getRandomSuffix();
        final LocalDateTime now = LocalDateTime.now();
        return AttendeeVote.of("talkId-" + suffix, "user-" + suffix + "@email.com", 3, VoteSource.mobile, now);
    }

    public static ScannedBadge getTestBadge() {
        return getTestBadge(null, null, null);
    }

    public static ScannedBadge getTestBadge(final String details) {
        return getTestBadge(details, null, null);
    }

    public static ScannedBadge getTestBadge(final String details, final Attendee attendee, final Sponsor sponsor) {
        final String suffix = getRandomSuffix();
        final Attendee _attendee = attendee != null ? attendee : new Attendee(
            "attendee-name-" + suffix,
            "attendee-email-" + suffix,
            "ca",
            33,
            Gender.M,
            "horroris",
            "Absolute master",
            "Moria",
            "Dwarrowdelf",
            "Java,Rust"
        );

        final org.jbcn.server.model.sponsor.Sponsor _sponsor = sponsor != null
            ? sponsor
            : new Sponsor(fakeId(), null, null, null, null, null, null, null);
        return ScannedBadge.of(_attendee, _sponsor, details);
    }

    public static FavouritedTalk getTestFavouritedTalk() {
        final String suffix = getRandomSuffix();
        return FavouritedTalk.of("talkId-" + suffix, "user-" + suffix);
    }

    private static String fakeId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
