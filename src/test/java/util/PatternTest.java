package util;

import org.jbcn.server.handlers.PaperHandler;
import org.junit.Assert;
import org.junit.Test;

public class PatternTest {

    @Test
    public void testEmailPattern() {
        String correctEmail = "Test@server.com";
        String[] invalidEmails = new String[] {
                "Test@server",
                "@server.com",
                "Test@server.",
                "Test.server@server.1",
                "Test.server@",
                "Test@",
                "Test.server.server.2"
        } ;
        Assert.assertTrue(PaperHandler.EMAIL_REGEX.matcher(correctEmail).find());

        for(String invalidEmail: invalidEmails) {
            Assert.assertFalse(invalidEmail+" must not match", PaperHandler.EMAIL_REGEX.matcher(invalidEmail).find());
        }
    }

    @Test
    public void testUrlPattern() {
        String[] validUrls = new String[] {
                "https://someserver.com/image.jpg",
                "https://someserver.com:9090/header.png",
                "https://some.some.some/image/url",
                "https://www.kai-waehner.de/Kai_Waehner.jpg"
        };
        String[] invalidUrls = new String[] {
                "ftp://someserver.com/image.jpg",
                "file://onyourcomputer/somefolders/image.jpg",
                "c:\\really\\you\\are\\using\\windows\\uri\\format?"
        };

        for(String validUrl: validUrls) {
            Assert.assertTrue(validUrl+" must match", PaperHandler.URL_REGEX.matcher(validUrl).find());
        }

        for(String invalidEmail: invalidUrls) {
            Assert.assertFalse(invalidEmail+" must not match", PaperHandler.URL_REGEX.matcher(invalidEmail).find());
        }
    }
}
