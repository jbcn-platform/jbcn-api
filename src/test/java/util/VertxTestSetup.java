package util;

import com.mongodb.async.client.MongoDatabase;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bson.codecs.configuration.CodecRegistry;
import org.jbcn.server.auth.JwtAuthProviderFactory;
import org.jbcn.server.config.JbcnProperties;
import org.jbcn.server.model.Role;
import org.jbcn.server.persistence.MongodbConnector;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import static io.vertx.core.http.HttpHeaders.CONTENT_LENGTH;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static util.asserters.ApiModelAsserter.assertThat;

public class VertxTestSetup {

    public static final String TEST_START_TIME = "START_TIME";

    public static final String APPLICATION_JSON = "application/json";
    public static final long TEST_TIMEOUT = 3000l;

    protected static int port = Integer.parseInt(JbcnProperties.get("vertx.port"));

    private static JWTAuth jwt;
    @Getter
    private MongoDatabase mongoDatabase;

    protected static Vertx vertx;
    protected WebClient webClient;


    @BeforeClass
    public static void initialize(TestContext context) throws IOException, ExecutionException, InterruptedException {
        CompletableFuture<Boolean> sync = new CompletableFuture<>();

        vertx = VertxSetup.startMainVerticle(port, sync);
        VertxSetup.startEmbeddedMongodb();
        jwt = new JwtAuthProviderFactory(
            JbcnProperties.get("api.auth.algorithm"),
            JbcnProperties.get("api.auth.private_key"),
            JbcnProperties.get("api.auth.public_key")
        ).jwtAuthProvider(vertx);

        // Wait for all
        sync.get();
    }

    @AfterClass
    public static void shutdown(TestContext context) {
        VertxSetup.stopMongoInMemory();
        vertx.close(context.asyncAssertSuccess());
    }

    @Before
    public void setUp(TestContext context) {
        context.put(TEST_START_TIME, System.currentTimeMillis());
        openMongoDatabase();
        webClient = WebClient.create(vertx, new WebClientOptions().setDefaultPort(port).setFollowRedirects(false));
    }

    /**
     * TODO rename to getToken
     */
    @SneakyThrows
    protected String getNewSession(String username) {
        return getNewSession(username, List.of());
    }

    @SneakyThrows
    protected String getNewSession(String username, Role... roles) {
        return getNewSession(username, List.of(roles));
    }

    @SneakyThrows
    protected String getNewSession(String username, List<Role> roles) {
        final JsonObject privateClaims = new JsonObject();
        final JWTOptions jwtOptions = new JWTOptions()
            .setAlgorithm("RS256")
            .setSubject(username)
            .setExpiresInSeconds(60 * 5)
            .setIssuer(JbcnProperties.get("jwt.issuer"));
        for (Role role : roles) {
            jwtOptions.addPermission(role.authority());
        }

        return jwt.generateToken(privateClaims, jwtOptions);
    }

    private void openMongoDatabase() {
        if (mongoDatabase == null)
            this.mongoDatabase = MongodbConnector.initializeMongoDb();
    }

    protected CodecRegistry getMongoDbCodecRegistry() {
        return mongoDatabase.getCodecRegistry();
    }

    protected String getRandomUsername() {
        return "user_" + UUID.randomUUID();
    }

    protected Handler<AsyncResult<HttpResponse<Buffer>>> assertJsonResponse(final Async async,
                                                                            final TestContext context,
                                                                            int httpStatus,
                                                                            final Consumer<JsonObject> responseConsumer) {
        return assertJsonResponse(async, true, context, httpStatus, responseConsumer);
    }

    protected Handler<AsyncResult<HttpResponse<Buffer>>> assertJsonResponse(final Async async,
                                                                            boolean asyncComplete,
                                                                            final TestContext context,
                                                                            int httpStatus,
                                                                            final Consumer<JsonObject> responseConsumer) {
        return handler -> {
            if (handler.succeeded()) {
                final var result = handler.result();
                context.assertEquals(httpStatus, result.statusCode());
                context.assertEquals(APPLICATION_JSON, result.getHeader(CONTENT_TYPE.toString()));

                responseConsumer.accept(result.bodyAsJsonObject());
            } else {
                context.fail(handler.cause());
            }
            if (asyncComplete) {
                async.complete();
            }
        };
    }

    protected Handler<AsyncResult<HttpResponse<Buffer>>> assertArrayResponse(final Async async, final TestContext context,
                                                                             int httpStatus, Consumer<JsonArray> responseConsumer) {
        return handler -> {
            if (handler.succeeded()) {
                final var result = handler.result();
                context.assertEquals(result.statusCode(), httpStatus);
                context.assertEquals(APPLICATION_JSON, result.getHeader(CONTENT_TYPE.toString()));

                responseConsumer.accept(result.bodyAsJsonArray());
            } else {
                context.fail(handler.cause());
            }
            async.complete();
        };
    }

    public void assert_fail(TestContext context, String endpoint, String user, String body, String errorMessage) {
        assert_fail(context, endpoint, user, body, errorMessage, 400);
    }

    public void assert_fail(TestContext context, String endpoint, String user, String body, String errorMessage, Integer status) {
        assert_fail(context, endpoint, user, Role.ADMIN, body, errorMessage, status);
    }

    public void assert_fail(TestContext context, String endpoint, String user, Role role, String body, String errorMessage, Integer status) {
        final Async async = context.async();
        webClient.post(endpoint)
            .bearerTokenAuthentication(getNewSession(user, role))
            .sendBuffer(
                body == null ? null : Buffer.buffer().appendString(body),
                assertJsonResponse(async, context, status, responseBody -> {
                    assertThat(responseBody)
                        .with(context)
                        .isError()
                        .hasMessage(errorMessage);
                }));

        async.awaitSuccess(TEST_TIMEOUT);
    }

    protected void assertNoAuthTokenIsPresentWithHttpGET(TestContext context, String endpoint) {
        assertNoAuthTokenIsPresentWithHttpPOST(context, HttpMethod.GET, endpoint, null);
    }

    protected void assertNoAuthTokenIsPresentWithHttpPOST(TestContext context, String endpoint) {
        assertNoAuthTokenIsPresentWithHttpPOST(context, HttpMethod.POST, endpoint, null);
    }

    protected void assertNoAuthTokenIsPresentWithHttpPOST(TestContext context, String endpoint, JsonObject body) {
        assertNoAuthTokenIsPresentWithHttpPOST(context, HttpMethod.POST, endpoint, body);
    }

    private void assertNoAuthTokenIsPresentWithHttpPOST(TestContext context, HttpMethod httpMethod, String endpoint, JsonObject body) {
        final Async async = context.async();
        webClient.request(httpMethod, endpoint)
            .sendJsonObject(body, handler -> {
                if (handler.succeeded()) {
                    final HttpResponse<Buffer> result = handler.result();
                    context.assertEquals(401, result.statusCode());
                    context.assertEquals("12", result.headers().get("Content-Length"));
                    context.assertEquals("Bearer", result.headers().get("WWW-Authenticate"));
                    context.assertEquals(2, result.headers().size());
                    context.assertEquals("12", result.getHeader(CONTENT_LENGTH.toString()));

                    context.assertEquals("Unauthorized", result.bodyAsString());
                } else {
                    context.fail(handler.cause());
                }
                async.complete();
            });

        async.awaitSuccess(TEST_TIMEOUT);
    }

    public Handler<AsyncResult<HttpResponse<Buffer>>> assertForbiddenWithEmptyBody(TestContext context, Async async) {
        return handler -> {
            if (handler.succeeded()) {
                final var result = handler.result();
                context.assertEquals(403, result.statusCode());
                context.assertNull(result.bodyAsString());
                context.assertNull(result.getHeader(CONTENT_TYPE.toString()));
                context.assertEquals("0", result.getHeader(CONTENT_LENGTH.toString()));
            } else {
                context.fail(handler.cause());
            }
            async.complete();
        };
    }

}
