package util;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lombok.SneakyThrows;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.types.ObjectId;
import org.jbcn.server.MainVerticle;
import org.jbcn.server.model.codec.PaperStateCodec;
import org.jbcn.server.model.codec.TalkStateCodec;
import org.jbcn.server.utils.DocumentConverter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static org.jbcn.server.config.JbcnProperties.getMongoConnectionUri;
import static org.jbcn.server.config.JbcnProperties.getMongoDatabaseName;

public class VertxSetup {

    private static MongodProcess mongodProcess;
    private static MongoDatabase mongoDatabase;

    public static void startEmbeddedMongodb() throws IOException {
        final MongodStarter starter = MongodStarter.getDefaultInstance();

        final IMongodConfig mongodConfig = new MongodConfigBuilder()
            .version(Version.Main.V4_0)
            .net(new Net("localhost", 27017, Network.localhostIsIPv6()))
            .build();

        final CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
            MongoClients.getDefaultCodecRegistry(),
            CodecRegistries.fromCodecs(new PaperStateCodec(), new TalkStateCodec())
        );

        MongodExecutable mongodExecutable = starter.prepare(mongodConfig);
        try {
            mongodProcess = mongodExecutable.start();
        } catch (Exception e) {
            // retry once 
            mongodProcess = mongodExecutable.start();
        }
        final MongoClient mongoClient = MongoClients.create(getMongoConnectionUri());
        mongoDatabase = mongoClient.getDatabase(getMongoDatabaseName()).withCodecRegistry(codecRegistry);
    }

    public static void stopMongoInMemory() {
        if (mongodProcess != null)
            mongodProcess.stop();
    }

    public static Vertx startMainVerticle(int port, CompletableFuture<Boolean> sync) {

        final Vertx vertx = Vertx.vertx();
        final DeploymentOptions options = new DeploymentOptions()
            .setConfig(new JsonObject().put("http.port", port));
        vertx.deployVerticle(MainVerticle.class.getName(), options, event -> {
            if (event.succeeded()) {
                sync.complete(Boolean.TRUE);
            } else {
                event.cause().printStackTrace();
                sync.complete(Boolean.FALSE);
            }
        });

        return vertx;
    }

    @SneakyThrows
    public static String dropCollection(String collectionName) {
        final CompletableFuture<String> future = new CompletableFuture<>();
        getCollection(collectionName)
            .drop((result, error) -> {
                if (error == null) {
                    future.complete(String.format("Collection %s successfully dropped", collectionName));
                } else {
                    future.completeExceptionally(error);
                }
            });
        return future.get();
    }

    @SneakyThrows
    public static String insertToDatabase(String collectionName, JsonObject jsonObject) {
        return insertToDatabase(collectionName, DocumentConverter.getDocument(jsonObject));
    }

    @SneakyThrows
    public static String insertToDatabase(String collectionName, Document document) {
        final CompletableFuture<String> future = new CompletableFuture<>();
        getCollection(collectionName)
            .insertOne(document, (result, error) -> {
                if (error == null) {
                    System.out.println("Object inserted with id: " + document.get("_id"));
                    future.complete(document.get("_id").toString());
                } else {
                    throw new RuntimeException("Error inserting test document into database", error);
                }
            });
        return future.get();
    }

    @SneakyThrows
    public static Document getFromDatabase(String collection, String id) {
        final CompletableFuture<Document> future = new CompletableFuture<>();
        getCollection(collection)
            .find(new Document("_id", new ObjectId(id)))
            .first((result, error) -> {
                if (error == null) {
                    future.complete(result == null ? null : removeNullValues(result));
                } else {
                    future.completeExceptionally(new RuntimeException("Could not retrieve json with id " + id, error));
                }
            });
        return future.get();
    }

    @SneakyThrows
    public static List<Document> getFromDatabase(String collection) {
        final CompletableFuture<List<Document>> future = new CompletableFuture<>();

        getCollection(collection)
            .find()
            .map(v -> removeNullValues(v))
            .into(new ArrayList<>(), (result, error) -> {
                if (error == null) {
                    future.complete(result);

                } else {
                    future.completeExceptionally(new RuntimeException("Could not retrieve elements from collection " + collection, error));
                }
            });
        return future.get();
    }

    private static Document removeNullValues(Document doc) {
        return doc.entrySet().stream()
            .filter(e -> e.getValue() != null)
            .map(e -> {
                final List<Object> result = new ArrayList<>();
                if (List.class.isAssignableFrom(e.getValue().getClass())) {
                    for (Object it : (List) e.getValue()) {
                        if (it instanceof Document)
                            result.add(removeNullValues((Document) it));
                    }
                }
                if (result.size() > 0)
                    e.setValue(result);
                return e;
            })
            .collect(Document::new,
                (document, entry) -> document.put(entry.getKey(), entry.getValue()),
                (document, document2) -> {
                    for (Map.Entry<String, Object> entry : document2.entrySet()) {
                        document.put(entry.getKey(), entry.getValue());
                    }
                }
            );
    }

    @SneakyThrows
    public static List<Document> getFromDatabase(String collection, String... ids) {
        final CompletableFuture<List<Document>> future = new CompletableFuture<>();

        getCollection(collection)
            .find(Filters.or(Arrays.stream(ids).map(it -> Filters.eq("_id", new ObjectId(it))).collect(Collectors.toList())))
            .into(new ArrayList<>(), (result, error) -> {
                if (error == null) {
                    future.complete(result);

                } else {
                    future.completeExceptionally(new RuntimeException("Could not retrieve json with id " + Arrays.toString(ids), error));
                }
            });
        return future.get();
    }

    public static MongoCollection<Document> getCollection(String collectionName) {
        return mongoDatabase.getCollection(collectionName);

    }
}
