package util;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomAssertions {

    public static boolean equalArrays(JsonArray actual, List<String> expected) {
        assertThat(actual.getList()).containsAll(expected);
        return true;
    }

    public static boolean equalMap(Map<String, Object> actual, Map<String, String> expected) {
        assertThat(actual).containsOnlyKeys(expected.keySet().toArray(new String[]{}));
        for (String key : actual.keySet()) {
            assertThat(actual.get(key)).isEqualTo(expected.get(key));
        }
        return true;
    }

    public static boolean equalMap(JsonObject actual, Map<String, String> expected) {
        return equalMap(actual.getMap(), expected);
    }
}
