package util;

import com.mongodb.async.client.MongoClients;
import org.bson.BsonDocument;
import org.bson.BsonRegularExpression;
import org.bson.BsonType;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import static org.assertj.core.api.Assertions.assertThat;

public class MongoQueryAssertions {

    private static final CodecRegistry DEFAULT_CODEC_REGISTRY = MongoClients.getDefaultCodecRegistry();

    public static final String MONGODB_OR = "$or";
    public static final String MONGODB_AND = "$and";

    public static BsonDocument toBsonDocument(Bson query) {
        return query.toBsonDocument(Document.class, DEFAULT_CODEC_REGISTRY);
    }

    public static void assertStringKeyWithValue(BsonDocument bson, String key, String value) {
        assertThat(bson.get(key).getBsonType()).isEqualTo(BsonType.STRING);
        assertThat(bson.getString(key).getValue()).isEqualTo(value);
    }

    public static void assertBooleanKeyWithValue(BsonDocument bson, String key, Boolean value) {
        assertThat(bson.get(key).getBsonType()).isEqualTo(BsonType.BOOLEAN);
        assertThat(bson.getBoolean(key).getValue()).isEqualTo(value);
    }

    public static void assertRegex(BsonDocument bson, String key, String pattern) {
        final BsonRegularExpression regex = bson.get(key).asRegularExpression();
        assertThat(regex.getOptions()).isEqualTo("i");
        assertThat(regex.getPattern()).isEqualTo(pattern);
    }
}
