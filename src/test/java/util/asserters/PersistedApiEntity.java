package util.asserters;

import org.bson.Document;

import java.util.List;

// TODO not used?
public class PersistedApiEntity {

    private final Document instance;

    public PersistedApiEntity(Document instance) {
        this.instance = instance;
    }

    public String getString(String key) {
        return instance.getString(key);
    }

    public List<Document> getObjectCollection(String key) {
        return (List<Document>) instance.get(key);
    }
}
