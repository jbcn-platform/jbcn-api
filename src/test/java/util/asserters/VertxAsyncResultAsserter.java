package util.asserters;

import io.vertx.core.AsyncResult;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.web.client.HttpResponse;

public class VertxAsyncResultAsserter {

    public static AsyncResultAsserterBuilder assertThat(AsyncResult<HttpResponse<Buffer>> handler) {
        return new AsyncResultAsserterBuilder(handler);
    }

    public static class AsyncResultAsserterBuilder {

        private final AsyncResult<HttpResponse<Buffer>> asyncResult;

        public AsyncResultAsserterBuilder(AsyncResult<HttpResponse<Buffer>> handler) {
            this.asyncResult = handler;
        }

        public AsyncResultAsserter with(TestContext context) {
            return new AsyncResultAsserter(asyncResult, context);
        }
    }

    public static class AsyncResultAsserter {

        private final AsyncResult<HttpResponse<Buffer>> asyncResult;
        private final TestContext testContext;

        public AsyncResultAsserter(AsyncResult<HttpResponse<Buffer>> asyncResult, TestContext testContext) {
            this.asyncResult = asyncResult;
            this.testContext = testContext;
        }

        public HttpResponseAsserter isSuccessful() {
            testContext.assertTrue(asyncResult.succeeded());
            return new HttpResponseAsserter(asyncResult.result(), testContext);
        }

    }

    public static class HttpResponseAsserter {

        private final HttpResponse<Buffer> result;
        private final TestContext testContext;

        public HttpResponseAsserter(HttpResponse<Buffer> result, TestContext testContext) {
            this.result = result;
            this.testContext = testContext;
        }

        public HttpResponseAsserter returnsStatus(int status) {
            testContext.assertEquals(status, result.statusCode());
            return this;
        }

        public HttpResponseAsserter doesNotContainHeader(String name) {
            testContext.assertNull(result.getHeader(name));
            return this;
        }

        public HttpResponseAsserter containsHeader(String headerName, String value) {
            testContext.assertEquals(value, result.getHeader(headerName));
            return this;
        }

        public HttpResponseAsserter hasBody(String value) {
            testContext.assertEquals(value, result.bodyAsString());
            return this;
        }
    }
}
