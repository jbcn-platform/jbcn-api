package util.asserters;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

import java.util.function.Consumer;

/**
 * TODO: review if this or Collections asserter should be the definitive.
 * Differences:
 *  - Returns 'id' instead of '_id'.
 *  - Returns custom error messages: e.g. 'error.note.paperid_mandatory' vs 'error.body.required_field.paperId' 
 *  - List/Search api does not return total bc is custom search based on url params that are applied as equals
 */
public class NotesCollectionAsserter {

    private static final String SIZE = "size";
    public static final String ITEMS = "items";

    private final JsonObject data;
    private final TestContext testContext;

    public NotesCollectionAsserter(JsonObject data, TestContext testContext) {
        this.data = data;
        this.testContext = testContext;
    }

    public void isEmpty() {
        hasProperties(0);
        testContext.assertEquals(0, data.getInteger(SIZE));
    }

    public NotesCollectionAsserter hasSize(int size) {
        hasProperties(size);
        testContext.assertEquals(size, data.getInteger(SIZE));
        testContext.assertEquals(size, getItems().size());
        return this;
    }

    private void hasProperties(int expectedSize) {
        testContext.assertTrue(data.fieldNames().contains(SIZE));
        if (expectedSize == 0) {
            testContext.assertEquals(1, data.fieldNames().size());
        } else {
            testContext.assertTrue(data.fieldNames().contains(ITEMS));
            testContext.assertEquals(2, data.fieldNames().size());
        }
    }

    private JsonArray getItems() {
        return data.getJsonArray(ITEMS);
    }

    public JsonObject getItem(int pos) {
        return getItems().getJsonObject(pos);
    }

    public NotesCollectionAsserter hasItem(int pos, Consumer<JsonApiEntity> itemAssertion) {
        itemAssertion.accept(new JsonApiEntity(getItem(pos)));
        return this;
    }

    public void allItems(Consumer<JsonApiEntity> itemsAssertion) {
        for (var item : getItems()) {
            itemsAssertion.accept(new JsonApiEntity((JsonObject) item));
        }
    }
}
