package util.asserters;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

import java.util.function.Consumer;

public class CollectionAsserter {

    private static final String TOTAL = "total";
    public static final String ITEMS = "items";

    private final JsonObject data;
    private final TestContext testContext;

    public CollectionAsserter(JsonObject data, TestContext testContext) {
        this.data = data;
        this.testContext = testContext;
    }

    public void isEmpty() {
        hasProperties();
        testContext.assertEquals(0, data.getInteger(TOTAL));
        testContext.assertEquals(0, getItems().size());
    }

    public CollectionAsserter hasSize(int size) {
        hasProperties();
        testContext.assertEquals(size, data.getInteger(TOTAL));
        testContext.assertEquals(size, getItems().size());
        return this;
    }

    private void hasProperties() {
        testContext.assertTrue(data.fieldNames().contains(TOTAL));
        testContext.assertTrue(data.fieldNames().contains(ITEMS));
        testContext.assertEquals(2, data.fieldNames().size());
    }

    // TODO getters should not be necessary
    public JsonArray getItems() {
        return data.getJsonArray(ITEMS);
    }

    private JsonObject getItem(int pos) {
        return getItems().getJsonObject(pos);
    }

    public CollectionAsserter hasItem(int pos, Consumer<JsonApiEntity> itemAssertion) {
        itemAssertion.accept(new JsonApiEntity(getItem(pos)));
        return this;
    }

    public CollectionAsserter allItems(Consumer<JsonApiEntity> itemsAssertion) {
        for (var item : getItems()) {
            itemsAssertion.accept(new JsonApiEntity((JsonObject) item));
        }
        return this;
    }
}
