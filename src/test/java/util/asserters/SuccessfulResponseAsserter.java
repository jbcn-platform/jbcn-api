package util.asserters;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

public class SuccessfulResponseAsserter {

    private static final String STATUS = "status";
    private static final String DATA = "data";

    private final JsonObject body;
    private final TestContext testContext;

    public SuccessfulResponseAsserter(JsonObject body, TestContext testContext) {
        this.body = body;
        this.testContext = testContext;
    }

    public IdAsserter isIdResponse() {
        hasDataResponseProperties();

        final JsonObject data = body.getJsonObject(DATA);
        testContext.assertTrue(data.fieldNames().contains("_id"));
        return new IdAsserter(data.getString("_id"), testContext);
    }

    public CollectionAsserter isCollectionResponse() {
        hasDataResponseProperties();
        return new CollectionAsserter(body.getJsonObject(DATA), testContext);
    }

    public NotesCollectionAsserter isNotesCollectionResponse() {
        hasDataResponseProperties();
        return new NotesCollectionAsserter(body.getJsonObject(DATA), testContext);
    }
    
    /**
     * Assertions for new CRUD APIs.
     */
    public EntityAsserter isEntityResponse() {
        hasDataResponseProperties();
        return new EntityAsserter(body.getJsonObject(DATA), testContext);
    }

    /**
     * Assertions for old APIs (e.g. papers).
     */
    public InstanceAsserter isLegacyInstanceResponse() {
        hasDataResponseProperties();
        final JsonObject instance = body.getJsonObject("data")
            .getJsonObject("instance");
        return new InstanceAsserter(instance, testContext);
    }

    public IdAsserter isLegacyIdResponse() {
        hasDataResponseProperties();
        final String id = body.getJsonObject("data")
            .getJsonObject("instance")
            .getString("_id");
        return new IdAsserter(id, testContext);
    }

    private void hasDataResponseProperties() {
        testContext.assertTrue(body.fieldNames().contains(STATUS));
        testContext.assertTrue(body.fieldNames().contains(DATA));
        testContext.assertEquals(2, body.fieldNames().size());
    }

    public void isVoidResponse() {
        testContext.assertTrue(body.fieldNames().contains(STATUS));
        testContext.assertEquals(1, body.fieldNames().size());
    }
}
