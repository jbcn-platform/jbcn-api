package util.asserters;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

// Just a delegator
public class InstanceAsserter extends EntityAsserter {

    public InstanceAsserter(JsonObject instance, TestContext testContext) {
        super(instance, testContext);
    }
}
