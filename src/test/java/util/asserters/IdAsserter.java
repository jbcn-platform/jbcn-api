package util.asserters;

import io.vertx.ext.unit.TestContext;

public class IdAsserter {

    private final String id;
    private final TestContext testContext;

    public IdAsserter(String id, TestContext testContext) {
        this.id = id;
        this.testContext = testContext;
    }

    public IdAsserter hasValidFormat() {
        testContext.assertEquals(24, id.length());
        return this;
    }

    public String getId() {
        return id;
    }
}
