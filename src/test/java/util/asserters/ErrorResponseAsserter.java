package util.asserters;

import io.vertx.ext.unit.TestContext;

public class ErrorResponseAsserter {

    private final String errorMessage;
    private final TestContext testContext;

    public ErrorResponseAsserter(String error, TestContext context) {
        this.errorMessage = error;
        this.testContext = context;
    }

    public void hasMessage(String message) {
        testContext.assertEquals(message, errorMessage);
    }

    public void hasInvalidIdMessage() {
        hasMessage("error.id.invalid_id");
    }

    public void hasIdNotFoundMessage() {
        hasMessage("error.id.not_found");
    }

    public void hasBodyRequiredMessage() {
        hasMessage("error.body.required");
    }
}
