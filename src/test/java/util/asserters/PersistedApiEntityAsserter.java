package util.asserters;

import io.vertx.ext.unit.TestContext;
import org.bson.Document;
import util.CustomAssertions;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;
import static util.VertxTestSetup.TEST_START_TIME;

public class PersistedApiEntityAsserter {

    private final Document instance;
    private final TestContext testContext;

    private PersistedApiEntityAsserter(Document instance, TestContext testContext) {
        this.instance = instance;
        this.testContext = testContext;
    }

    public static PersistedApiEntityAsserterBuilder assertThat(Document instance) {
        return new PersistedApiEntityAsserterBuilder(instance);
    }

    public PersistedApiEntityAsserter hasCreatedMetadata() {
        testContext.assertNotNull(instance.getLong(CREATED_DATE));
        testContext.assertNotNull(instance.getLong(LAST_UPDATE));
        testContext.assertEquals(instance.getLong(CREATED_DATE), instance.getLong(LAST_UPDATE));
        final Long time = testContext.get(TEST_START_TIME);
        testContext.assertTrue(time < instance.getLong(CREATED_DATE));
        return this;
    }

    // TODO validate lastActionBy
    public PersistedApiEntityAsserter hasUpdatedMetadata(String updater) {
        testContext.assertNotNull(instance.getLong(CREATED_DATE));
        testContext.assertNotNull(instance.getLong(LAST_UPDATE));
        testContext.assertTrue(instance.getLong(LAST_UPDATE) > instance.getLong(CREATED_DATE));
        // FIXME papers api does not set it
        // testContext.assertEquals(updater, instance.getString(LAST_ACTION_BY));
        final Long time = testContext.get(TEST_START_TIME);
        testContext.assertTrue(time < instance.getLong(CREATED_DATE));
        return this;
    }

    public PersistedApiEntityAsserter hasStringEqualsTo(String key, String expected) {
        testContext.assertEquals(expected, instance.getString(key));
        return this;
    }

    public PersistedApiEntityAsserter hasIntegerEqualsTo(String key, Integer expected) {
        testContext.assertEquals(expected, instance.getInteger(key));
        return this;
    }

    public PersistedApiEntityAsserter hasObjectEqualsTo(String key, Map<String, String> expected) {
        CustomAssertions.equalMap((Document) instance.get(key), expected);
        return this;
    }

    public PersistedApiEntityAsserter hasBooleanEqualsTo(String key, Boolean expected) {
        testContext.assertEquals(expected, instance.getBoolean(key));
        return this;
    }

    public PersistedApiEntityAsserter hasCollectionEqualsTo(String key, List<String> expected) {
        final List<String> values = (List<String>) instance.get(key);
        for (String expectedValue : expected) {
            testContext.assertTrue(values.contains(expectedValue));
        }
        testContext.assertEquals(expected.size(), values.size());
        return this;
    }

    public PersistedApiEntityAsserter hasKeys(int size) {
        testContext.assertEquals(size, instance.keySet().size());
        return this;
    }

    public PersistedApiEntityAsserter has(Consumer<PersistedApiEntity> assertion) {
        assertion.accept(new PersistedApiEntity(instance));
        return this;
    }

    public PersistedApiEntityAsserter doesNotContainKey(String key) {
        testContext.assertFalse(instance.containsKey(key));
        return this;
    }

    public static class PersistedApiEntityAsserterBuilder {

        private final Document instance;

        public PersistedApiEntityAsserterBuilder(Document instance) {
            this.instance = instance;
        }

        public PersistedApiEntityAsserter with(TestContext testContext) {
            return new PersistedApiEntityAsserter(instance, testContext);
        }
    }
}
