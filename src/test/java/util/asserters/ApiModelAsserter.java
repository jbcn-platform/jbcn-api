package util.asserters;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * Helper to apply assertions based on the API model. 
 */
public class ApiModelAsserter {

    private static final String STATUS = "status";
    private static final String ERROR = "error";

    private final JsonObject body;
    private final TestContext context;

    private ApiModelAsserter(JsonObject responseBody, TestContext context) {
        this.body = responseBody;
        this.context = context;
    }

    public static ApiModelAsserterBuilder assertThat(JsonObject responseBody) {
        return new ApiModelAsserterBuilder(responseBody);
    }

    public static class ApiModelAsserterBuilder {

        private final JsonObject responseBody;

        public ApiModelAsserterBuilder(JsonObject responseBody) {
            this.responseBody = responseBody;
        }

        public StatusAsserter with(TestContext testContext) {
            return new StatusAsserter(responseBody, testContext);
        }
    }

    public static class StatusAsserter {

        private final JsonObject body;
        private final TestContext testContext;

        public StatusAsserter(JsonObject responseBody, TestContext testContext) {
            this.body = responseBody;
            this.testContext = testContext;
        }

        public SuccessfulResponseAsserter isSuccessful() {
            testContext.assertEquals(TRUE, body.getBoolean(STATUS));
            return new SuccessfulResponseAsserter(body, testContext);
        }

        public ErrorResponseAsserter isError() {
            testContext.assertEquals(FALSE, body.getBoolean(STATUS));
            testContext.assertTrue(body.fieldNames().contains(ERROR));
            testContext.assertEquals(2, body.fieldNames().size());
            return new ErrorResponseAsserter(body.getString(ERROR), testContext);
        }
    }
}
