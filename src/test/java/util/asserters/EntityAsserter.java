package util.asserters;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

import java.util.function.Consumer;

public class EntityAsserter {

    private final JsonApiEntity entity;
    private final TestContext testContext;

    public EntityAsserter(JsonObject jsonObject, TestContext testContext) {
        this.entity = new JsonApiEntity(jsonObject);
        this.testContext = testContext;
    }

    public EntityAsserter hasId() {
        new IdAsserter(entity.getId(), testContext).hasValidFormat();
        return this;
    }

    public EntityAsserter hasIdEqualsTo(String id) {
        testContext.assertEquals(id, entity.getId());
        return this;
    }

    public EntityAsserter hasEntity(Consumer<JsonApiEntity> entityAssertion) {
        entityAssertion.accept(entity);
        return this;
    }

    public EntityAsserter hasCreatedMetadata() {
        final Long createDate = entity.getCreateDate();
        testContext.assertNotNull(createDate);
        final Long lastUpdate = entity.getLastUpdate();
        testContext.assertNotNull(lastUpdate);
        testContext.assertEquals(createDate, lastUpdate);
        return this;
    }

    public EntityAsserter hasStringEqualsTo(String key, String expected) {
        testContext.assertEquals(expected, entity.getString(key));
        return this;
    }

    public EntityAsserter hasIntegerEqualsTo(String key, Integer expected) {
        testContext.assertEquals(expected, entity.getDouble(key).intValue());
        return this;
    }

    public EntityAsserter hasStringStartingWith(String key, String expected) {
        testContext.assertTrue(entity.getString(key).startsWith(expected));
        return this;
    }

    public EntityAsserter hasDoubleEqualsTo(String key, double expected) {
        testContext.assertEquals(expected, entity.getDouble(key));
        return this;
    }

    public EntityAsserter containKey(String key) {
        testContext.assertTrue(entity.containsKey(key));
        return this;
    }

    public EntityAsserter doesNotContainKey(String key) {
        testContext.assertFalse(entity.containsKey(key));
        return this;
    }

    public EntityAsserter hasKeys(int size) {
        testContext.assertEquals(size, entity.fieldNames().size());
        return this;
    }

}
