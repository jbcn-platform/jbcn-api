package util.asserters;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Set;

import static org.jbcn.server.model.Traceable.CREATED_DATE;
import static org.jbcn.server.model.Traceable.LAST_UPDATE;

/**
 * JsonObject wrapper to facilitate handling Api entities
 * and its common behaviors.
 */
public class JsonApiEntity {

    private final JsonObject jsonObject;

    public JsonApiEntity(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getId() {
        return jsonObject.getString("_id");
    }

    public String getString(String key) {
        return jsonObject.getString(key);
    }

    public JsonObject getJsonObject(String key) {
        return jsonObject.getJsonObject(key);
    }

    public Set<String> fieldNames() {
        return jsonObject.fieldNames();
    }

    public boolean getBoolean(String key) {
        return jsonObject.getBoolean(key);
    }

    public Double getDouble(String key) {
        return jsonObject.getDouble(key);
    }

    public JsonArray getObjectArray(String key) {
        return jsonObject.getJsonArray(key);
    }

    public Long getCreateDate() {
        return jsonObject.getLong(CREATED_DATE);
    }

    public Long getLastUpdate() {
        return jsonObject.getLong(LAST_UPDATE);
    }

    public boolean containsKey(String key) {
        return jsonObject.containsKey(key);
    }

    public Object getLong(String key) {
        return jsonObject.getLong(key);
    }
}
