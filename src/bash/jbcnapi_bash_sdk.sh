#!/bin/bash

######################################################################
# Usage:
#   - Define 2 bash variables called `_japi_user` & `_japi_password`
#     with your username and password.
#   - Interpret the script with `source` or `.` commands.
######################################################################

__japi_endpoint="https://apijbcn.cleverapps.io"

function setLocalEnv() {
  __japi_endpoint="http://localhost:8080"
}

function getToken() {
  __japi_token=$(curl -s "$__japi_endpoint/oauth/token?grant_type=password&username=$_japi_user&password=$_japi_password&client_id=1" | jq -r '.access_token')
}

function getToken_deprecated() {
  __japi_token=$(curl -s $__japi_endpoint/login -X POST -d "{ \"username\":\"$_japi_user\", \"password\":\"$_japi_password\"}" | jq -r '.data.token')
}

function createUser() {
  echo "Type the username to use to login:"
  read __jusername
  echo "Type your email:"
  read __jemail
  echo "Type your password:"
  read __jpassword

  curl -X POST -v "$__japi_endpoint/api/users"  \
    -H "Authorization: Bearer $__japi_token" \
    -H "Content-Type: application/json" \
    -d "{ \"username\": \"$__jusername\", \"email\": \"$__jemail\", \"password\": \"$__jpassword\", \"roles\":[\"helper\"] }"
}

function getPapers() {
  __searchResource "paper" $1
}

function getPaperById() {
  __getResourceById "paper" $1
}

function setPaperStateToSent() {
  curl -X PUT "$__japi_endpoint/api/paper/$1"  \
    -H "Authorization: Bearer $__japi_token" \
    -H "Content-Type: application/json" \
    -d '{ "state": "sent" }'
}

function getPaperPublicState() {
  curl -X GET "$__japi_endpoint/public/api/paper/state/$1" | jq .
}

function deletePaperById() {
  __deleteResourceById "paper" $1
}

function getTalks() {
  __searchResource "talk" $1
}

function getTags() {
  __getResourceById "talks/tags"
}

function getTalksBySessionId() {
    local response=$(curl -s "$__japi_endpoint/api/talk/search" \
-X POST \
-H "Authorization: Bearer $__japi_token" \
-H "Content-Type: application/json" \
-d "{
  \"asc\": false,
  \"filters\": [{
    \"name\": \"sessionId\",
    \"value\": "\"$1\"",
  }],
  \"page\": 0,
  \"size\": 200
}")
  echo ${response} | jq -C '.'
  echo "Found: $(echo ${response} | jq -C '.data.total')"
}

function deleteTalkById() {
  __deleteResourceById "talk" $1
}

function getSpeakers() {
    # byId is not possible due to current model
  __getResourceById "speakers"
}

function getMobileVotes() {
  __getResourceById "attendees/votes"
}

function getTabletVotes() {
  curl -X GET "$__japi_endpoint/api/talks/votes"  \
    -H "Authorization: Bearer $__japi_token" \
    -d '{ "state": "sent" }'
}

function deleteAttendeesVotes() {
  __deleteResourceById "attendees/votes" $1
}

function getTalkById() {
  __getResourceById "talk" $1
}

function getUsers() {
   __getResourceById "users"
}

function getUserById() {
   __getResourceById "user" $1
}

function searchUsers() {
   __searchResource "user"
}

function getSessions() {
  __getResourceById "sessions" $1
}

function getRooms() {
  __getResourceById "rooms" $1
}

function getRoomsById() {
  __getResourceById "rooms" $1
}

function getSponsors() {
   __getResourceById "sponsors"
}

function getSponsorById() {
   __getResourceById "sponsors" $1
}

function getBadges() {
  __getResourceById "badges" $1
}

function __deleteAllBadges() {
  __deleteAllByResource "badges"
}

function getFavored() {
  __getResourceById "talks/favourites" $1
}

function deleteFavoredById() {
  __deleteResourceById "talks/favourites" $1
}

function deleteFavored() {
  local response=$(curl "$__japi_endpoint/api/talks/favourites?talkId=$1&user=$2" \
    -X DELETE \
    -H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

function searchFavoredByUser() {
  curl -X POST $__japi_endpoint/api/talks/favourites/search  \
    -H "Authorization: Bearer $__japi_token" \
    -H "Content-Type: application/json" \
    -d "{
      \"filters\": [
        {
          \"field\": \"user\",
          \"value\": \"$1\"
        }
      ],
      \"page\": {
        \"index\": 0,
        \"size\": 1000
      }
    }" | jq .
}

function searchBadgesBySponsor() {
  curl -X POST $__japi_endpoint/api/badges/search  \
    -H "Authorization: Bearer $__japi_token" \
    -H "Content-Type: application/json" \
    -d "{
      \"filters\": [
        {
          \"field\": \"sponsor.id\",
          \"value\": \"$1\"
        }
      ],
      \"page\": {
        \"index\": 0,
        \"size\": 1000
      }
    }" | jq .
}


function getVoters() {
  local response=$(curl -s $__japi_endpoint/api/voters \
  -X GET \
  -H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

function getVoteStream() {
  local response=$(curl -s $__japi_endpoint/api/vote/stream \
  -X GET \
  -H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

function deleteMobileVoteById() {
  __deleteResourceById "attendees/votes" $1
}

function __deleteAllMobileVotes() {
  __deleteAllByResource "attendees/votes"
}

# Cannot be used once the app is life
# function __deleteAllFavored() {
#  __deleteAllByResource "talks/favourites"
# }

function getTalkFeedback() {
# TODO does not work if url ends with /
  local response=$(curl -s $__japi_endpoint/api/talk/$1/feedback \
-H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

function __deleteAllByResource() {
  local response=$(curl -s $__japi_endpoint/api/$1/ \
-X GET \
-H "Authorization: Bearer $__japi_token")

  IFS='
  '
  local values=$(echo "$response" | jq '.data.items[]._id')
  local count=0
  for id in $values
  do
    id=$(echo $id | cut -c2-25)
    count=$((count+1))
    echo "($count) Deleting $id" && \
    __deleteResourceById $1 $id
  done
}

function getPublishedSpeakersJson() {
  local response=$(curl -s $__japi_endpoint/public/speakers \
-X GET )
  echo ${response} | jq -C .
}

function getPublishedTalks() {
  local response=$(curl -s $__japi_endpoint/public/talks \
-X GET)
  echo ${response} | jq -C .
}

function approvePaper() {
  if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters."
    echo "Usage approvePaper {paperId} {responsiblePersonName}"
  fi
  local response=$(curl -s $__japi_endpoint/api/paper/$1/approve \
-X POST \
-d "{ \"username\":\"$2\" }" \
-H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

## Internals

function __getResourceById() {
  local response=$(curl -s $__japi_endpoint/api/$1/$2 \
-X GET \
-H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

function __deleteResourceById() {
  local response=$(curl -s $__japi_endpoint/api/$1/$2 \
-X DELETE \
-H "Authorization: Bearer $__japi_token")
  echo ${response} | jq -C .
}

function __searchResource() {
# Use temp file to avoid error when response is too big or contains special chars
  local responseFile=$(mktemp /tmp/abc-script.XXXXXX)
  curl -s $__japi_endpoint/api/$1/search \
-X POST \
-H "Authorization: Bearer $__japi_token" \
-H "Content-Type: application/json" \
-d "{
  \"term\": \"$2\",
  \"sort\": \"createdDate\",
  \"asc\": false,
  \"filters\": [],
  \"page\": 0,
  \"size\": 200
}" > "$responseFile"

  cat ${responseFile} | jq -C '.' && \
  echo "Found: $(cat ${responseFile} | jq -C '.data.total')"
}

function __createTestWorkshop {
  __createTestProposal "workshop" $1
}

function __createTestPaper {
  __createTestProposal "talk" $1
}

# $1: type: 'talk' or 'workshop'
# $2: first speaker name suffix
# $3: second speaker name suffix
# $4: (optional) title suffix
function __createTestProposal {
  curl -X POST "$__japi_endpoint/public/api/paper" \
-H "Authorization: Bearer $__japi_token" \
-H 'Content-Type: application/json' \
-d "{
    \"edition\": \"2020\",
    \"title\": \"Testing! ($1) $4 $(date +"(%Y-%m-%d %H:%M:%S)")\",
    \"type\": \"$1\",
    \"level\": \"beginner\",
    \"abstract\": \"This is the most amazing abstract ever\",
	  \"preferenceDay\": \"2018/06/11\",
    \"tags\": [
        \"All\",
        \"The\",
        \"Fancy\",
        \"Words_and_more\"
    ],
    \"comments\": \"\",
    \"senders\": [
        {
            \"fullName\": \"Robert Shaftoe $2\",
            \"email\": \"first-mail@demo.mail\",
            \"twitter\": \"@bobby\",
            \"picture\": \"https://www.jbcnconf.com/2018/assets/img/logos/logo-jbcnconf.png\",
            \"web\": \"\",
            \"linkedin\": \"https://cosas\",
            \"travelCost\": false,
            \"attendeesParty\": false,
            \"speakersParty\": true,
            \"tshirtSize\": \"S\",
            \"company\": \"RedHat\",
            \"biography\": \"I was born awesome and from that, I just kept going.\"
        },
        {
            \"fullName\": \"Robert Shaftoe $3\",
            \"email\": \"second-mail-2@demo.mail\",
            \"twitter\": \"@bobby2\",
            \"picture\": \"https://www.jbcnconf.com/2018/assets/img/logos/logo-jbcnconf.png\",
            \"web\": \"\",
            \"linkedin\": \"https://cosas-2\",
            \"travelCost\": false,
            \"attendeesParty\": false,
            \"speakersParty\": true,
            \"tshirtSize\": \"XL\",
            \"company\": \"IBM\",
            \"biography\": \"I was born awesome and from that, I just kept going.\"
        }
    ]
  }"
}

function __createTestPapers() {
  local speakerSuffix1=$RANDOM
  local speakerSuffix2=$RANDOM
  for i in {1..$1}
  do
    __createTestProposal "talk" $speakerSuffix1 $speakerSuffix2 $i
    __createTestProposal "workshop" $speakerSuffix1 $speakerSuffix2 $i
  done
}

function __createTestVote() {
  local response=$(curl -s $__japi_endpoint/api/attendees/votes \
-X POST \
-H "Authorization: Bearer $__japi_token" \
-H "Content-Type: application/json" \
-d "{
  \"talkId\": \"5b0f095838da163c1bd328c6\",
  \"userEmail\": \"fake_user@fake.com\",
  \"value\": 5
}")
  echo ${response} | jq -C '.'
}

function __createTestBadgeMinimal() {
  local response=$(curl -s $__japi_endpoint/api/badges \
-X POST \
-H "Authorization: Bearer $__japi_token" \__createTestPapers 1
-H "Content-Type: application/json" \
-d '{
  "attendee": {
    "name": "attendee name",
    "email": "attendee.email@dot.com"
  },
  "sponsor": {
     "id": "99999"
  },
  "details": "test badge"
}')
  echo ${response} | jq -C '.'
}

function __createTestBadgeComplete() {
  local response=$(curl -s $__japi_endpoint/api/badges \
-X POST \
-H "Authorization: Bearer $__japi_token" \
-H "Content-Type: application/json" \
-d '{
  "attendee": {
    "name": "full attendee",
    "email": "attendee.email@dot.com",
    "language": "ca",
    "age": 33,
    "gender": "M",
    "company": "my home Ltd.",
    "jobTitle": "F*ck*ng boss",
    "city": "BCN",
    "country": "Supein",
    "programmingLanguages": "Java,Rust,Oh_Good!_Why_me?"
  },
  "sponsor": {
     "id": "99999",
     "name": "sponsor-name",
     "code": "sponsor-code"
  },
  "details": "test badge"
}')
  echo ${response} | jq -C '.'
}
